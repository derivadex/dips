## Preamble

```
dip: 7
title: Lift Governance Cliff
author: CMS Holdings
discussions-to: https://forum.derivadex.com/t/lift-the-governance-cliff/35
status: Proposed
type: State Upgrade
created: 2020-12-17
```

## Purpose

This DIP aims to lift the governance cliff. All DDX tokens are currently non-transferable (insurance-mined rewards, investor proceeds, and otherwise). Lifting the cliff will allow users to withdraw (and subsequently transfer) claimed DDX tokens from their Trader wallers, thereby realizing the token’s governance and staking utility in the DerivaDEX ecosystem.

## Technical Specification (if applicable)

This is a straightforward State Update proposal. If successfully executed, the proposal executes the `setRewardCliff(bool _rewardCliff)` function defined in the `Trader` facet with the value of `true`. This sets the `rewardCliff` storage value defined in `LibDiamondStorageTrader.DiamondStorageTrader` to be `true`.

From this point onwards, the `postRewardCliff` modifier on the `withdrawDDXToTrader(uint96 _amount)` function will pass, allowing users to withdraw DDX.

## Side-effects

Transferability is an intended feature of DDX. We do not see any side effects for implementing transferability. As mentioned earlier, this proposal is a `State Upgrade` proposal, which modifies a parameter or storage in the DerivaDEX system and has no corresponding smart contract business logic update.

However, voters should be aware that transferability may impact participation rates in insurance mining. In our opinion, transferability will provide a more accurate participation rate in insurance mining, enabling the DerivaDAO to better plan for insurance fund policy development.

## Test Cases

`packages/protocol/test/deployment/TestLiftGovernanceCliff.ts`

The above mainnet fork test validates failed withdrawals prior to lifting the governance cliff and successful withdrawals afterwards.

## Security Considerations

This proposal is a `State Upgrade`, which modifies a parameter or storage in the DerivaDEX system (e.g. adding new collateral types) with no corresponding smart contract business logic update. As such we do not see direct critical security considerations.

One consideration is that when DDX is transferrable, users who withdraw DDX from their Trader wallet will now directly custody their DDX however they choose.

Therefore, users must exercise caution when transferring DDX, approving contracts to spend DDX, or other transactions involving DDX. The DerivaDEX protocol has no ability to provide roll-backs or restitution to users who lose their DDX.

## Copyright Waiver

Copyright and related rights waived via [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
