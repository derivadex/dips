## DerivaDEX Improvement Proposals (DIPs)

DerivaDEX Improvement Proposals (DIPs) outline the core standards, specifications,
parameters, and contract upgrade implementations.

| DIP |     Title     |  Author   | Type |  Created   |
| :-: | :-----------: | :-------: | :--: | :--------: |
|  0  | DIP Framework | DerivaDEX | Meta | 2020-12-16 |

\*\* Note, refer to the raw markdown code to see how this table was constructed.

## What is a DIP?

DIP is short for DerivaDEX Improvement Proposal, and offers a design and/or architecture document that outlines a proposal to the DerivaDEX ecosystem to be ultimately executed on-chain.

## DIP Rationale

DIPs are how new features (i.e. smart contract upgrades) or governance actions (parameter modifications, role assignment, etc.) will be proposed to the DerivaDEX community, to be voted upon by DDX stakeholders and executed on-chain.

They take inspiration from Ethereum's DIP format, so should offer a familiar process to the broad community.

## DIP Types

There are two types of DIPs as of now:

-   A **Logic Upgrade** describes new or enhanced business logic to the suite of DerivaDEX smart contracts.
-   A **State Upgrade** modifies a parameter or storage in the DerivaDEX system (e.g. adding new collateral types) with no corresponding smart contract business logic update.

A single DIP maps to a single governance proposal. Keep in mind a proposal can have more than one action. If a proposal is so large that it needs to be broken up into several governance proposals (e.g. due to gas constraints), this must be broken up into several DIPs.

## DIP Work Flow

### DIP Process

Each DIP will consist of 2 separate, but related pull requests.

-   **Proposal Spec** - A `DIP-n.md` file detailing the proposal. The proposal spec PR will be merged to `master` if and only if an on-chain governance proposal is actually made.
-   **Implementation** - The corresponding implementation. The implementation PR will be merged to `master` if and only if the proposal successfully executes on-chain.

The following is the standardization process for all DIPs in all tracks:

**Idea** - An idea that is pre-draft. This is not tracked within the DIP Repository. There is no proposal spec PR nor implementation PR open.

**Draft** - The first formally tracked stage of a DIP in development. At minimum, a proposal spec PR has been opened to garner feedback on the [forum](https://forum.derivadex.com/). An implementation PR at some point will be opened as well to seek additional input from the community.

**Proposed** - An on-chain governance proposal has been made. The proposal spec PR will be merged to `master`. The implementation `PR` will remain open.

**Succeeded** - The proposal has succeeded on-chain, but has not yet been queued for execution.

**Failed** - The proposal has failed on-chain.

**Queued** - The successful proposal has been queued for execution.

**Canceled** - The proposal has been canceled.

**Expired** - The queued proposal has expired prior to execution.

**Executed** - The proposal has successfully executed on-chain.

## What belongs in a successful DIP?

Each DIP should have the following parts:

-   Preamble - DIP ID, title, author, type, and date.
-   Purpose - A short (~200 word) description of the proposal's intention.
-   Technical Specification (if applicable) - The technical specification should describe the syntax and semantics of any new feature (include any relevant code snippets and/or architecture diagrams).
-   Side-effects - Any positive and/or negative results if the proposal were to ultimately succeed.
-   Test Cases - Test cases for an implementation are mandatory for DIPs.
-   Security Considerations - All DIPs must contain a section that discusses the security implications/considerations relevant to the proposed change. Include information that might be important for security discussions, surfaces risks and can be used throughout the life cycle of the proposal. E.g. include security-relevant design decisions, concerns, important discussions, implementation-specific guidance and pitfalls, an outline of threats and risks and how they are being addressed.
-   Copyright Waiver - All DIPs must be in the public domain. See the bottom of this DIP for an example copyright waiver.

## DIP Formats and Templates

DIPs should be written in [markdown] format. There is a [template](https://gitlab.com/derivadex/dips/-/blob/master/dips/dip-n.md) to follow.

## DIP Header Preamble

Each DIP must begin with preamble arranged in tabular format. The headers must appear in the following order. Headers marked with "\*" are optional and are described below. All other headers are required.

`DIP:` _DIP number_ (this is determined by the DIP editor)

`title:` _DIP title_

`author:` _a list of the author's or authors' name(s) and/or username(s), or name(s) and email(s). Details are below._

`* discussions-to:` _a url pointing to the official [forum](https://forum.derivadex.com/) discussion thread_

`status:` _Draft, Proposed, Succeeded, Failed, Queued, Canceled, Expired, Executed_

`type:` _Logic Upgrade, State Upgrade_

`created:` _date created on_

`* updated:` _comma separated list of dates_

`* requires:` _DIP number(s)_

`* replaces:` _DIP number(s)_

`* superseded-by:` _DIP number(s)_

`* resolution:` _a url pointing to the resolution (etherscan transaction of successful execution) of this DIP_

Headers that permit lists must separate elements with commas.

Headers requiring dates will always do so in the format of ISO 8601 (yyyy-mm-dd).

#### `author` header

The `author` header lists the names, email addresses or usernames of the authors/owners of the DIP. Those who prefer anonymity may use a username only, or a first name and a username. The format of the author header value must be:

> Random J. User &lt;address@dom.ain&gt;

or

> Random J. User (@username)

if the email address or GitLab username is included, and

> Random J. User

if the email address is not given.

It is not possible to use both an email and a GitLab username at the same time. If important to include both, one could include their name twice, once with the GitLab username, and once with the email.

At least one author must use a GitLab username, in order to get notified on change requests and have the capability to approve or reject them.

#### `resolution` header

The `resolution` header contains a URL that should point to an Etherscan link of the proposal's successful on-chain execution.

#### `discussions-to` header

While an DIP is a draft or not yet finalized, a `discussions-to` header will indicate the Discourse forum where the DIP is being discussed.

#### `status` header

The `status` header specifies the state of the proposal: Draft, Proposed, Succeeded, Failed, Queued, Canceled, Expired, or Executed.

#### `type` header

The `type` header specifies the type of DIP: Logic Upgrade or State Upgrade.

#### `created` header

The `created` header records the date that the DIP was assigned a number. Both headers should be in yyyy-mm-dd format, e.g. 2001-08-14.

#### `updated` header

The `updated` header records the date(s) when the DIP was updated with "substantial" changes. This header is only valid for DIPs of Draft and Active status.

#### `requires` header

DIPs may have a `requires` header, indicating the DIP numbers that this DIP depends on.

#### `superseded-by` and `replaces` headers

DIPs may also have a `superseded-by` header indicating that an DIP has been rendered obsolete by a later document; the value is the number of the DIP that replaces the current document. The newer DIP must have a `replaces` header containing the number of the DIP that it rendered obsolete.

## Linking to other DIPs

References to other DIPs should follow the format `DIP-N` where `N` is the DIP number you are referring to. Each DIP that is referenced in an DIP **MUST** be accompanied by a relative markdown link the first time it is referenced, and **MAY** be accompaniede by a link on subsequent references. The link **MUST** always be done via relative paths so that the links work in this GitLab repository, forks of this repository, the main DIPs site, mirrors of the main DIP site, etc. For example, you would link to this DIP with `[DIP-1](./DIP-1.md)`.

## Auxiliary Files

Images, diagrams and auxiliary files should be included in a subdirectory of the `assets` folder for that DIP as follows: `assets/DIP-N` (where **N** is to be replaced with the DIP number). When linking to an image in the DIP, use relative links such as `../assets/DIP-1/image.png`.

## Transferring DIP Ownership

It occasionally becomes necessary to transfer ownership of DIPs to a new champion. In general, we'd like to retain the original author as a co-author of the transferred DIP, but that's really up to the original author. A good reason to transfer ownership is because the original author no longer has the time or interest in updating it or following through with the DIP process, or has fallen off the face of the 'net (i.e. is unreachable or isn't responding to email). A bad reason to transfer ownership is because you don't agree with the direction of the DIP. We try to build consensus around an DIP, but if that's not possible, you can always submit a competing DIP.

If you are interested in assuming ownership of an DIP, send a message asking to take over, addressed to both the original author and the DIP editor. If the original author doesn't respond to email in a timely manner, the DIP editor will make a unilateral decision (it's not like such decisions can't be reversed :)).

## DIP Editor Responsibilities

For each new DIP that comes in, an editor does the following:

-   Read the DIP to check if it is ready: sound and complete. The ideas must make technical sense, even if they don't seem likely to get to final status.
-   The title should accurately describe the content.
-   Check the DIP for language (spelling, grammar, sentence structure, etc.), markup (GitLab flavored Markdown), code style

If the DIP isn't ready, the editor will send it back to the author for revision, with specific instructions.

Once the DIP is ready for the repository, the DIP editor will:

-   Assign an DIP number (generally the PR number or, if preferred by the author, the Issue # if there was discussion in the Issues section of this repository about this DIP)

-   Merge the corresponding pull request

-   Send a message back to the DIP author with the next step.

Many DIPs are written and maintained by developers with write access to the Ethereum codebase. The DIP editors monitor DIP changes, and correct any structure, grammar, spelling, or markup mistakes we see.

The editors don't pass judgment on DIPs. We merely do the administrative & editorial part.

## Style Guide

When referring to an DIP by number, it should be written in the hyphenated form `DIP-X` where `X` is the DIP's assigned number.

## History and References

This document was derived heavily from Ethereum's EIP-1 document, and thus adopts the same references.

## Copyright

Copyright and related rights waived via [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
