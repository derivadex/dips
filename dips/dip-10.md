## Preamble

```
dip: 10
title: Deploy DerivaDEX Pilot to Mainnet
author: DerivaDEX, Ltd.
discussions-to: https://forum.derivadex.com/t/deploy-derivadex-pilot-to-mainnet/184
status: Proposed
type: Logic Upgrade
created: 2023-08-29
```

## Background

DerivaDEX is a decentralized derivatives platform, unique in its approach to performance and security. The protocol has been deployed to mainnet, and the governance protocol has been implemented. This proposal is to deploy an initial version of the platform to mainnet via a governance-approved upgrade of the DerivaDEX contracts.

This proposal contains both on-chain and off-chain components, and governance participants should only vote for this proposal if they are aligned with **all** elements of this proposal.

_The DerivaDEX Foundation expects an on-chain proposal in line with this plan for "Stage 1: Pilot" will be made by early next week. All DAO members should ensure they have DDX in a wallet that they are able to sign transactions from prior to the date of proposal submission if they plan to vote on this proposal. All DAO members who plan to delegate should ensure delegations have been made prior to the date of proposal submission._

This proposal should be seen as the first in a two (or more) step process in fully bringing platform operations online:

### Step 1: Pilot (this proposal only covers Pilot)

Pilot is a limited release, which brings the operator network online and adds the platform to the smart contracts. However, access is restricted to a few early testing partners (who are obligated via agreement to return any DDX earned via trade mining). The purpose of Pilot is to de-risk the launch of the platform by sharply limiting users for an initial period.

### Step 2: Guarded Launch

Guarded launch allows a larger number of participants, but still has many limits in place on various parameters (such as a 10,000 USD cap on collateral deposits). Guarded Launch can be implemented via a separate, second proposal sometime after Pilot is deployed if the Pilot proposal is successful. This proposal does NOT otherwise include or cover Guarded Launch.

### Step N: Beyond Guarded Launch

Progressive expansion of the platform's offerings and removal of certain safety limits (i.e., an increase in the number of users or collateral size) should be proposed in a fashion that satisfies safety and business requirements.

At no point will persons or entities in the United States and its territories be permitted to participate on the platform in transacting any derivative contracts.

## Description

This proposal (DerivaDEX Pilot) seeks to make on-chain changes including:

1. Deploy Ethereum smart contracts that enable deposits, withdrawals, and checkpointing and
2. to modify the DerivaDEX diamond to include these contracts and
3. to set the operator addresses and execution code and other on-chain parameters necessary for the off-chain operator network to begin performing platform execution logic and submitting checkpoints.

This proposal also includes off-chain elements and specifies additional parameters as detailed below.

**This proposal has an important, temporary limitation: only a small selection of addresses, intended to include partner integrations, partner testing accounts, and engineering contributors, can participate as traders.** This limitation is implemented via NGINX, and will be removed at the time of a subsequent successful governance proposal (i.e., in an upgrade to Guarded Launch). These initial trading partners will return all DDX earned as part of trade mining during this period to a DAO-controlled account.

**The purpose of this temporary limitation is to take reasonable measures to reduce the risk of a mainnet deployment.**

In other respects, this proposal is consistent with all the MVP requirements established by DerivaDEX Ltd and other contributing partners. A selection of these requirements is described below in the section “Selected Platform Design Elements”.

This proposal exactly reflects the version deployed to [staging.derivadex.io ](http://staging.derivadex.io). This proposal also exactly reflects the version deployed currently to [testnet.derivadex.io ](http://testnet.derivadex.io), with the single exception that on [testnet.derivadex.io ](http://testnet.derivadex.io) there is no limitation on user access. This is to facilitate open testing of all other features.

All smart contract code has been audited by Quantstamp. The goal of these audits is to provide reasonable assurances of code quality, however **no audit is able to guarantee the security of any application, including this version of DerivaDEX**. Users should always perform their own assessment of the safety of any contracts they interact with, and fully assume the risk of the protocol when using it.

Off-chain platform code is planned to be open-sourced in the future, however all users can verify the execution logic and correctness of the off-chain code via the DerivaDEX Auditor.

## Selected Platform Design Elements

The following sections describe the current implementation of several platform components and processes. Every aspect of the platform can be modified via governance, these current parameters are selected for this initial proposal because they are reasonable starting points from a safety and usability standpoint. **Governance participants should ONLY vote to pass this proposal if they agree with all elements of the proposal, including off-chain components.**

#### Trading Access

Only a select number of addresses can participate in this _Pilot_ release. This is anticipated to be a temporary measure. This selection is implemented via a webserver controlled by DerivaDEX LTD. Any trade mining rewards collected by participants during this closed phase will be returned to the DAO.

Access to the platform will also be blocked for all US residents and for residents of restricted territories.

#### Trading Products, Collateral Deposits, and Withdrawals

The platform code implemented in this proposal is for a single-collateral, cross-margin derivatives platform with two perpetual trading products, BTCP and ETHP. USDC is accepted as collateral, and accounting is done in USD (where 1 USDC = 1 USD).

Collateral deposits are limited to a maximum of 10,000 USDC per trader. Any additional amount deposited will be automatically added to frozen collateral, where it can be withdrawn, but will not count towards trading free collateral. All deposits must be larger than 1,000 USDC.

_Note: Depositing 1,000,000 DDX or more enables traders to deposit up to 10,000,000 USDC._

Withdrawals depend on checkpoints, which occur roughly every 10 minutes and can be called by any user. There is a rate-limit on withdrawals at the contract layer of 1 million tokens per 50 blocks, plus 10% of the total USDC in the system. For example, if there were 1 million USDC in the system, there would be a maximum withdrawal limit per 50 block interval of 1,100,000 USDC.

#### Prices and Funding Rates

The mark prices for BTCP and ETHP are set via price feed oracle and the exponential moving average, as described here: [Price feed - DerivaDEX](https://docs.derivadex.io/trading/price-feed).

Mark prices are kept in alignment with the underlying via a _funding rate_. The funding rate is assessed every 8 hours, and is described in more detail here: [Funding rate - DerivaDEX](https://docs.derivadex.io/trading/funding-rate)

#### Leverage

Leverage is limited in this release to 3x for all products.

#### Liquidations

Liquidations occur when a trader’s margin fraction goes below their maintenance margin ratio (MMR). The MMR in this proposal is set to 5%. The liquidation calculations are described in more detail here: [Liquidations - DerivaDEX ](https://docs.derivadex.io/trading/liquidations)

When a trader is liquidated, there is either a surplus or a deficit, depending on the price at which the liquidation engine can close the trader’s position. Surpluses are added to the platform insurance fund. Deficits are drawn down from the insurance fund.

#### Trading

Only limit and market orders are enabled in Pilot. Limit orders that cross the book are treated as market orders. The “close position” button in the user interface closes the amount of the open position with a market order. Advanced order types are not currently supported.

Some additional controls are in place in this release including:

-   Users are prevented from “self-matching”, aka filling their own orders.
-   Users are prevented from placing a trade that would result in greater than 2% slippage for market orders.
-   Users may have a maximum of 10 open orders per market
-   Orders are rate-limited at the application layer, based on user’s DDX deposit, and at the webserver layer.

#### Fees

The fee schedule in this proposal is as described here: [Fees - DerivaDEX ](https://docs.derivadex.io/trading/fees)

For funding rate fee information, see above section on Prices and Funding Rate. There are no withdrawal or deposit fees aside from gas assessed by the Ethereum blockchain.

Trading fees paid in USDC accrue to the insurance fund. Trading fees paid in DDX are shared by the operator nodes.

#### Trade mining

As mentioned in the description, only a select number of addresses can participate in this Pilot release. These participants are obligated to send all trade-mined DDX to the DAO at the conclusion of this phase.

The trade mining program is currently implemented as described here: [Trade mining - DerivaDEX ](https://docs.derivadex.io/trading/trade-mining)

#### Operator Nodes and Bond Providers

DerivaDEX Ltd proposes the following addresses as Bond Providers. Engineering contributor DEX Labs will _not_ be running an operator node.

-   0x944933f24e8eeDFE568BB4E198878239e95F2899
-   0xAcc288bb7127251C3B372a66840016D4adA9B878
-   0x699c982b9b00b8008EDeAAEA359CD8B1B998831d

Bond Providers are responsible for ensuring the continuous operation of their operator nodes. In return, Bond Providers receive a portion of the trading fees that have been paid in DDX.

These selected Bond Providers are strictly proposed for the Pilot release; future updates or versions should either re-confirm these Bond Providers and set a required DDX bond, or select new Bond Providers.

Due to the platform's unique design where execution and matching occur within the secure enclave of machines equipped with Intel SGX, Bond Providers cannot modify platform logic, account balances, or order flow, nor do they have privileged access to order flow.
