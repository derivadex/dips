## DerivaDEX Improvement Proposals (DIPs)

This is the suggested template for new DIPs.

Note that an DIP number will be assigned by an editor. When opening a pull request to submit your DIP, please use an abbreviated title in the filename, `DIP-draft_title_abbrev.md`.

The title should be 44 characters or less.

## Preamble (tabular format)

```
dip: <to be assigned>
title: <DIP title>
author: <a list of the author's or authors' name(s) and/or username(s), or name(s) and email(s), e.g. (use with the parentheses or triangular brackets): FirstName LastName (@GitHubUsername), FirstName LastName <foo@bar.com>, FirstName (@GitHubUsername) and GitHubUsername (@GitHubUsername)>
discussions-to: <URL>
status: Draft
type: <Logic Upgrade or State Upgrade>
created: <date created on, in ISO 8601 (yyyy-mm-dd) format>
requires (*optional): <DIP number(s)>
replaces (*optional): <DIP number(s)>
```

## Purpose

A short (~200 word) description of the proposal's intention.

## Technical Specification (if applicable)

The technical specification should describe the syntax and semantics of any new feature (include any relevant code snippets and/or architecture diagrams).

## Side-effects

Any positive and/or negative results if the proposal were to ultimately succeed.

## Test Cases

Test cases for an implementation are mandatory for DIPs.

## Security Considerations

Test cases for an implementation are mandatory for DIPs that are affecting consensus changes. Other DIPs can choose to include links to test cases if applicable.

## Copyright Waiver

Copyright and related rights waived via [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
