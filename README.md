## DerivaDEX Improvement Proposals (DIPs)

## Prelimary Steps (macOS)

1. Install [brew](https://brew.sh/)
2. You'll need node v18.15.0 to build the code.
   Use [nvm](https://github.com/nvm-sh/nvm#install--update-script) to run additional node versions:
    1. Install nvm using instructions above
    2. Fetch node v18.15.0 with `nvm install 18.15.0`
    3. Set node to version 18: `nvm use 18.15.0`
    4. (optional) Set node version 18 as default `nvm alias default 18.15.0`
3. Install yarn: `brew install yarn`

## Setup

1. Clone the repository: `git clone git@gitlab.com:derivadex/dips.git`
2. Confirm node version:  
    cd into the `dips` directory and confirm you're running node v18.15.0:  
   `node -v`
3. Install packages: `yarn install`
4. Build project: `yarn build`
