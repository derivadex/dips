import { ContractAbi } from 'ethereum-types';
import { readFile } from 'fs';
import { join } from 'path';
import { promisify } from 'util';

export * from './artifacts';

// This reference is made from the transpiled lib/ folder, thus we go back an extra directory
const artifactsPath = join(__dirname, '../../contracts/');

export async function getDerivaDAOABIAsync(): Promise<{ abi: ContractAbi }> {
    const abiPath = join(artifactsPath, 'CompleteDerivaDEXArtifact.json');
    const bufferedABI = await promisify(readFile)(abiPath);
    return JSON.parse(bufferedABI.toString());
}

export async function getDummyTokenABIAsync(): Promise<{ abi: ContractAbi }> {
    const abiPath = join(artifactsPath, 'DummyTokenABI.json');
    const bufferedABI = await promisify(readFile)(abiPath);
    return JSON.parse(bufferedABI.toString());
}
