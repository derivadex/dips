import { appendFileAsync, lstatAsync, readdirAsync, readFileAsync, writeFileAsync } from '@derivadex/dev-utils';
import { spawnSync } from 'child_process';
import globby from 'globby';
import * as path from 'path';

// TODO(jalextowle): Replace these with CLI args prior to sharing externally.
const INPUT_DIR = 'contracts';
const CONFIG_DIR = 'docgen';
const OUTPUT_DIR = 'docgen/docs';
const README_FILE = 'README.md';
const SUMMARY_FILE = 'docgen/SUMMARY.md';
const EXCLUDE_FILE = 'docgen/exclude.txt';
const relativePath = path.relative(path.dirname(SUMMARY_FILE), OUTPUT_DIR);

async function linesAsync(pathName: string): Promise<string[]> {
    const contents = await readFileAsync(pathName, { encoding: 'utf8' });
    return contents
        .split('\r')
        .join('')
        .split('\n')
        .filter((line) => line !== '');
}

/**
 * Gets the summary section associated with a given path. This can be used to
 * generate a Gitbook SUMMARY.md file that excludes all files in the excluded
 * file list from the summary.
 */
async function getSummarySectionForPathAsync(
    excludeList: string[],
    pathName: string,
    indentation: string,
): Promise<string> {
    if (!excludeList.includes(pathName)) {
        if ((await lstatAsync(pathName)).isDirectory()) {
            let summarySubsectionForDirectory = '';
            for (const fileName of await readdirAsync(pathName)) {
                const summarySubsectionForFile = await getSummarySectionForPathAsync(
                    excludeList,
                    pathName + '/' + fileName,
                    indentation + '    ',
                );
                if (summarySubsectionForFile !== '') {
                    summarySubsectionForDirectory += summarySubsectionForFile;
                }
            }
            // NOTE(jalextowle): We don't want to add a summary section for
            // directories that do not have children that can be documented.
            if (summarySubsectionForDirectory === '') {
                return summarySubsectionForDirectory;
            }
            return indentation + '-   ' + path.basename(pathName) + '\n' + summarySubsectionForDirectory;
        } else if (pathName.endsWith('.sol')) {
            const text = path.basename(pathName).slice(0, -4);
            const link = pathName.slice(INPUT_DIR.length, -4);
            return indentation + '-   [' + text + '](' + relativePath + link + '.md)\n';
        }
    }
    return '';
}

/**
 * Recursively descends directories to sanitize any markdown files that are found
 * within them. Sanitization occurs in two phases:
 *   - Pass 1 focuses on cleaning up regions of text that are associated with
 *     bulleted lists. Extraneous newlines in bulleted regions causes
 *     pathological rendering in Gitbook, and it must be fixed for the docs to
 *     be legible.
 *   - Pass 2 trims extraneous whitespace on each line of text. In the event that
 *     the line contains a markdown heading, a newline will be added in front of
 *     the line to ensure that formatting is consistent regardless of the existence
 *     of bulleted lists (these appear to impact formatting unless there are at
 *     least two newlines ahead of the markdown heading).
 * @param pathName The path to be recursively sanitized.
 */
async function sanitizeRecursiveAsync(pathName: string): Promise<void> {
    if ((await lstatAsync(pathName)).isDirectory()) {
        for (const fileName of await readdirAsync(pathName)) {
            await sanitizeRecursiveAsync(pathName + '/' + fileName);
        }
    } else if (pathName.endsWith('.md')) {
        // Remove all newlines from bullet blocks. An example of a snippet that
        // this process would fix is:
        //
        // Before:
        // ```
        // - `_calldata`: A function call, including function selector and arguments
        //         _calldata is executed with delegatecall on _init
        // ```
        //
        // After:
        // ```
        // - `_calldata`: A function call, including function selector and arguments  _calldata is executed with delegatecall on _init
        // ```
        //
        // This sanitization step ensures that bullets are resolved properly when
        // rendered in Gitbook.
        const bulletRegionRegex = /(?<=\n)\-(.|\n(?!(\-|#)))*\n(?=(\-|#))/g;
        const contents = await readFileAsync(pathName);
        const replacementContent = contents.toString().replace(bulletRegionRegex, (match: string) => {
            // NOTE(jalextowle): Remove any extraneous newlines and whitespace
            // that is found in the matched text.
            return match.replace(/(\r\n|\n|\r)/gm, '').replace(/\s+/gm, ' ') + '\n';
        });
        await writeFileAsync(pathName, replacementContent);

        // Trim all lines, filter out empty lines, and add newlines before
        // Markdown headings.
        await writeFileAsync(
            pathName,
            (
                await linesAsync(pathName)
            )
                .filter((line) => line.trim().length > 0)
                .map((line) => (/#.*/.test(line) ? '\n' + line : line))
                .join('\n') + '\n',
        );
    }
}

(async () => {
    const excludeListGlobs = (await linesAsync(EXCLUDE_FILE)).map((line) => INPUT_DIR + '/' + line);
    const excludeList = await globby(excludeListGlobs, { onlyFiles: true });

    await writeFileAsync(SUMMARY_FILE, '# Summary\n');
    await writeFileAsync('.gitbook.yaml', 'root: ./\n');
    await appendFileAsync('.gitbook.yaml', 'structure:\n');
    await appendFileAsync('.gitbook.yaml', '  readme: ' + README_FILE + '\n');
    await appendFileAsync('.gitbook.yaml', '  summary: ' + SUMMARY_FILE + '\n');

    const summaryFileContents = await getSummarySectionForPathAsync(excludeList, INPUT_DIR, '');
    await appendFileAsync(SUMMARY_FILE, summaryFileContents);

    const args = ['solidity-docgen', '--input=' + INPUT_DIR, '--output=' + OUTPUT_DIR, '--templates=' + CONFIG_DIR];

    const result = spawnSync('yarn', args, { stdio: ['inherit', 'inherit', 'pipe'] });
    if (result.stderr.length > 0) {
        throw new Error(result.stderr.toString());
    }

    await sanitizeRecursiveAsync(OUTPUT_DIR);
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
