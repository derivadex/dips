# docs-gen

## Installation

The `docs-gen` command can be installed by adding the npm package `@derivadex/docs-gen`
as a dependency. With `yarn`, this can be accomplished with the following command:

```
yarn add @derivadex/docs-gen
```

## Use

The `@derivadex/docs-gen` provides a `bin` script called `docs-gen`. This means
that `docs-gen` can be invoked within any yarn package that depends on
`@derivadex/docs-gen` using the command

```
yarn docs-gen
```

For more information on npm `bin` scripts, see [NPM's documentation on bin](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#bin).
