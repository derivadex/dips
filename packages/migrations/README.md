#

@derivadex/migrations

The migrations package contains several scripts that can be used to perform contract
migrations for the DerivaDEX smart contracts.

At a high level, there are 3 types of transaction senders:

-   Deployer - ETH address that deploys much of the DerivaDEX system. This address deploys most
    of the contracts and facets individually, prior to them being pieced together as a cohesive
    DerivaDEX diamond system. This address very transciently is the owner for these contracts
    prior to ownership of DerivaDEX being transferred to itself (i.e. governance).
-   Multisig - A GnosisSafe multisig wallet (2-of-3) that deploys the DDX token contract and receives
    the pre-mine treasury allocation. Ownership of the DDX token is almost immediately transferred
    from the multisig wallet to the DerivaDEX system which will be in charge of issuing the
    remaining 50% of DDX tokens via governance-defined liquidity mining schedules.
-   Any address - Some functions can be called by any address, such as queuing and executing
    governance transactions.

## Architecture

The migration scripts have broadly been divided into two categories: `setup` and `gnosis-safe`.

### Setup

As described above, a deploying address will deploy much of the DerivaDEX system (prior to it
being fully set up and initialized). This `deploy` script resides in this directory.

### GnosisSafe

GnosisSafe is a powerful multisig wallet that supports arbitrary transaction execution with
m-of-n signature approval. This file includes a `gnosis-safe-setup` script that takes in
command-line args to set up the GnosisSafe with the correct authorizing `owners` and threshold
required to approve transactions. From there, there are two categories of scripts:

#### Intents

Signatories can run `intent-builder` scripts that are pieced together from `intent-blocks`
to generate the calldata needed for transaction execution and most importantly, their signature.
The `intent-builders` allow for participants to create arbitrarily complex gnosis safe
workflows that can be consumed by an executing script to run lengthy migrations. The result
of running an `intent-builder` is a JSON file with all the GnosisSafe intent metadata.

#### Execs

An executor can run `exec-builder` scripts that reflect their corresponding `intent-builder`
with potentially some other non-GnosisSafe logic as well. These scripts will consume any
number of intent metadata files and verify their equality (apart from the signature) and
call the generic `gnosis-safe-execute` function to perform the transaction.

### DerivaDEX setup flow

1. Setup GnosisSafe
2. Deploy DDX token (from GnosisSafe)
3. Deploy DerivaDEX contracts and facets (from a deploying address)
4. Transfer DDX token ownership to DerivaDEX (from GnosisSafe)
5. Add Pause and Governance facets and transfer ownership of DerivaDEX to itself
   (from deploying address)
6. Add InsuranceFund, USDT family, USDC family, HUSD, and GUSD collaterals (proposals and
   casting vote from GnosisSafe, execution from any address)
7. Set timelock delay to 3 days (proposals and
   casting vote from GnosisSafe, execution from any address)

### Examples

#### GnosisSafe setup

These migration scripts pertain to the GnosisSafe workflow (intentions & executions).

```
yarn gnosis-safe-setup --config <path>
```

### Deploy Contracts

First, check out and build the branch containing the contracts you want to deploy:

```bash
git fetch
git checkout origin/release/0.6.7
yarn install && yarn build
# Or, faster:
cd packages/migrations
yarn rebuild
```

To check if and when the contracts have changed, use `git log` or `tig`.

Next, create a json configuration file in this directory if you don't have one already.
Using a dot-name (.beta.json) is convenient for git-ignore but not required. The following
is an example of a config file for deploying contracts for beta goerli. Copy and paste
it and update the settings as needed, the 'from' and 'pk' in particular. We'll assume
the file is named '.beta.json' in this example.

Notes:

-   The infura url is the Dexlabs team infura account.
-   The 'from' and 'pk' (private key) values are for the user deploying the contracts (you).
-   'from' is 0x prefixed but 'pk' is not.
-   Be sure the account is funded with some goerli eth.

```json
{
    "rpcUrl": "https://goerli.infura.io/v3/a34d603fd5fb4af8ab6c24724394b2b1",
    "chainId": 5,
    "confirmations": 1,
    "deployment": "beta",
    "from": "0xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    "pk": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
    "accountsToFund": [
        "0x3658E568173DFE6400fCaa1D6F085C644E22bE13",
        "0x4DbaEb213F91B022D0f238a2510380BE149b091a",
        "0xE2F3eC385F57e9Fe8fd2e6938D539BE86c2eDf0F",
        "0xb724D8C629A163d0E1809fAB27420fdf7f72a02c",
        "0x8D6F6EA2598e8b1dAC9C434a1FcD579f507Db18F",
        "0x2EadF973FeB265129c04b9fcE6f730e48212F88A",
        "0xE9F6FfDF8e0E649089d90CFDb1E7a312746C0927",
        "0x0067A8a383157b761C004EB7fd15Efd724B0B915",
        "0x43a5b78655584B9e71f4E00f3f638529B96283c0",
        "0xaCcf7803C88B05846c46946D7B46418A9ADAd578",
        "0xE1D50e4df88ABfea75557334D3eb8fCe52Daa58C",
        "0xc1e5f9e5532ed65b27d3f8f0771b066158839c16",
        "0x2F2661614526e03D7a394EB1F825CE94E4E112fb",
        "0x7e8Ea10440a732884D5D15305218801DcF1a63A2"
    ]
}
```

Now run the contract migration (deployment):

```bash
# Switch to this directory:
cd packages/migrations
# Kick off the migration (contract deployment). This will take approximately 15-30 minutes
yarn migrate-testnet --config .beta.json
```

A successful migration will update the file `packages/contract-addresses/addresses.json` with the
new addresses.

⚠️ `packages/contract-addresses/addresses.json` **must be committed back into the git repo!**

Now that the contracts have been re-deployed and `addresses.json` committed back into git, the next
beta deployment will reference the new contracts. It's not essential to do a site redeploy immediately; once
`addresses.json` is committed, the next (beta) deployment will pick up the latest `addresses.json` and reference the newly deployed contracts.

#### Deploy DDX token (intent)

```
yarn deploy-ddx-token-intent --config <path>
```

##### Deploy DDX token (exec)

```
yarn deploy-ddx-token-exec --config <path>
```

##### Deploying DerivaDEX

```
yarn deploy --config <path>
```

##### Initialize DerivaDEX system (intent)

```
yarn initialize-derivadex-system-intent --config <path>
yarn initialize-derivadex-system-intent --config <path>
yarn initialize-derivadex-system-intent --config <path>
```

##### Initialize DerivaDEX system (exec)

```
yarn initialize-derivadex-system-exec --config <path>
```

##### Set isPaused for system (intent)

```
yarn pause-system-intent --config <path>
yarn pause-system-intent --config <path>
yarn pause-system-intent --config <path>
```

##### Set isPaused for system (exec)

```
yarn pause-system-exec --config <path>
```

##### Transfer DDX tokens (intent)

```
yarn transfer-ddx-tokens-intent --config <path>
yarn transfer-ddx-tokens-intent --config <path>
yarn transfer-ddx-tokens-intent --config <path>
```

##### Transfer DDX tokens (exec)

```
yarn transfer-ddx-tokens-exec --config <path>
```

### Ganache config files

#### Account 0

```
{
    "rpcUrl": "http://localhost:8545",
    "from": "0x5409ed021d9299bf6814279a6a1411a7e866a631",
    "pk": "f2f48ee19680706196e2e339e5da3491186e0c4c5030670656b0e0164837257d",
    "owners": [
        "0x5409ed021d9299bf6814279a6a1411a7e866a631",
        "0x6ecbe1db9ef729cbe972c83fb886247691fb6beb",
        "0xe36ea790bc9d7ab70c55260c66d52b1eca985f84"
    ],
    "accountsToFund": [
        "0x5409ed021d9299bf6814279a6a1411a7e866a631",
        "0x6ecbe1db9ef729cbe972c83fb886247691fb6beb",
        "0xe36ea790bc9d7ab70c55260c66d52b1eca985f84"
    ],
    "compTokenDistribution": {
        "amount": 100000,
        "ethAmount": 0.5,
        "recipients": [
            "0x5409ed021d9299bf6814279a6a1411a7e866a631",
            "0x6ecbe1db9ef729cbe972c83fb886247691fb6beb",
        ]
    },
    "threshold": 2,
    "to": "0x0000000000000000000000000000000000000000",
    "data": "0x",
    "fallbackHandler": "0x0000000000000000000000000000000000000000",
    "paymentToken": "0x0000000000000000000000000000000000000000",
    "payment": 0,
    "paymentReceiver": "0x0000000000000000000000000000000000000000",
    "confirmations": 0,
    "useDeployedDDXToken": true,
    "shouldPause": false,
    "castVote": {
        "proposalId": 1,
        "support": true
    },
    "transferRecipients": [
        {
            "recipient": "0x5409ed021d9299bf6814279a6a1411a7e866a631",
            "amount": 5000000
        },
        {
            "recipient": "0x6ecbe1db9ef729cbe972c83fb886247691fb6beb",
            "amount": 5000000
        }
    ],
    "chainId": 1,
    "trezor": true,
    "ledger": false
}
```

#### Account 1

```
{
    "rpcUrl": "http://localhost:8545",
    "from": "0x6Ecbe1DB9EF729CBe972C83Fb886247691Fb6beb",
    "pk": "5d862464fe9303452126c8bc94274b8c5f9874cbd219789b3eb2128075a76f72",
    "owners": [
        "0x5409ed021d9299bf6814279a6a1411a7e866a631",
        "0x6ecbe1db9ef729cbe972c83fb886247691fb6beb",
        "0xe36ea790bc9d7ab70c55260c66d52b1eca985f84"
    ],
    "threshold": 2,
    "to": "0x0000000000000000000000000000000000000000",
    "data": "0x",
    "fallbackHandler": "0x0000000000000000000000000000000000000000",
    "paymentToken": "0x0000000000000000000000000000000000000000",
    "payment": 0,
    "paymentReceiver": "0x0000000000000000000000000000000000000000",
    "confirmations": 2,
    "useDeployedDDXToken": true,
    "shouldPause": false,
    "castVote": {
        "proposalId": 1,
        "support": true
    },
    "transferRecipients": [
        {
            "recipient": "0x5409ed021d9299bf6814279a6a1411a7e866a631",
            "amount": 5000000
        },
        {
            "recipient": "0x6ecbe1db9ef729cbe972c83fb886247691fb6beb",
            "amount": 5000000
        }
    ],
    "chainId": 1,
    "trezor": true,
    "ledger": false
}
```

#### Account 2

```
{
    "rpcUrl": "http://localhost:8545",
    "from": "0xE36Ea790bc9d7AB70C55260C66D52b1eca985f84",
    "pk": "df02719c4df8b9b8ac7f551fcb5d9ef48fa27eef7a66453879f4d8fdc6e78fb1",
    "owners": [
        "0x5409ed021d9299bf6814279a6a1411a7e866a631",
        "0x6ecbe1db9ef729cbe972c83fb886247691fb6beb",
        "0xe36ea790bc9d7ab70c55260c66d52b1eca985f84"
    ],
    "threshold": 2,
    "to": "0x0000000000000000000000000000000000000000",
    "data": "0x",
    "fallbackHandler": "0x0000000000000000000000000000000000000000",
    "paymentToken": "0x0000000000000000000000000000000000000000",
    "payment": 0,
    "paymentReceiver": "0x0000000000000000000000000000000000000000",
    "confirmations": 2,
    "useDeployedDDXToken": true,
    "shouldPause": false,
    "castVote": {
        "proposalId": 1,
        "support": true
    },
    "transferRecipients": [
        {
            "recipient": "0x5409ed021d9299bf6814279a6a1411a7e866a631",
            "amount": 5000000
        },
        {
            "recipient": "0x6ecbe1db9ef729cbe972c83fb886247691fb6beb",
            "amount": 5000000
        }
    ],
    "chainId": 1,
    "trezor": true,
    "ledger": false
}
```

### Verify Token Distribution

This script verifies token distributions.

Copy the following json into a file named `my-config.json`:

```
{
  "rpcUrl": "http://ethereum:8545",
  "chainId": 100,
  "confirmations": 1,
  "deployment": "geth",
  "from": "0x5409ed021d9299bf6814279a6a1411a7e866a631",
  "pk": "6d8a98ad0be9c9535b411921277882bae2f2eef3bb53a231b789343d3ce67e50",
  "accountsToFund": [
    "0x5409ED021D9299bf6814279A6A1411A7e866A631",
    "0x6Ecbe1DB9EF729CBe972C83Fb886247691Fb6beb",
    "0xE36Ea790bc9d7AB70C55260C66D52b1eca985f84",
    "0xE834EC434DABA538cd1b9Fe1582052B880BD7e63",
    "0x78dc5D2D739606d31509C31d654056A45185ECb6",
    "0xA8dDa8d7F5310E4A9E24F8eBA77E091Ac264f872",
    "0x06cEf8E666768cC40Cc78CF93d9611019dDcB628",
    "0x4404ac8bd8F9618D27Ad2f1485AA1B2cFD82482D",
    "0x7457d5E02197480Db681D3fdF256c7acA21bDc12",
    "0x91c987bf62D25945dB517BDAa840A6c661374402",
    "0x6A7d785FfA5d340b5c468e0725E1dF8F41B768a8",
    "0x603699848c84529987E14Ba32C8a66DEF67E9eCE",
    "0x7E98Fdf18dAe0b7b1925d6735D4B77f4889879e1",
    "0x5BcB10204f39f2d3755038c1B5A305203900099A",
    "0x88644D18A948eD8Bcb60F82FCfe1cf4631D09260",
    "0x93dcD0DD8B9e11575c1f647872aE082559691feb",
    "0xaa6676733e2e259d879127519D973ac71135F797",
    "0x4E11c2Cf71FF70f1284e871f978fCcc65a99B8dD",
    "0xdB51E4F0C7AeDB87c012748EC5Da3eAd4f4ed110",
    "0xaCcf7803C88B05846c46946D7B46418A9ADAd578"
  ]
}
```

The "accountsToFund" are taken from the list of accounts in ./README.md.

We'll run this script using the `frontend-api` docker container already running in `fuzz`.

First, we need to copy `my-config.json` into the running docker container:

```bash
> docker cp my-config.json fuzz_frontend-api_1 /usr/src/packages/migrations
```

Now that the config file has been added to the running container, we can execute it in one command:

```bash
> docker exec --workdir /usr/src/packages/migrations -it fuzz_frontend-api_1 \
        /bin/bash -c "yarn ts-node src/testnet/verify_token_distribution.ts --config my-config.json"

yarn run v1.22.5
$ /usr/src/node_modules/.bin/ts-node src/testnet/verify_token_distribution.ts --config config.json
Failed to initialize libusb.
Verifying token distribution - BEGIN
ChainId 100
Addresses {
  ddxAddress: '0x1dc4c1cefef38a777b15aa20260a54e584b16c48',
  ddxWalletCloneableAddress: '0x48bacb9266a570d521063ef5dd96e61686dbe788',
  derivaDEXAddress: '0x1d7022f5b17d2f8b695918fb48fa1089c9f85401',
[...]
  bannerAddress: '0x1e2f9e10d02a6b8f8f69fcbf515e75039d2ea30d',
  fundedInsuranceFundAddress: '0xcdb594a32b1cc3479d8746279712c39d18a07fc0',
  globalSpecsAddress: '0xbe0037eaf2d64fe5529bca93c18c9702d3930376'
}
Token Balance of Account 0x5409ED021D9299bf6814279A6A1411A7e866A631
    usdc: 10000000000000
    usdt: 10000000000000
    ddx: 0
Token Balance of Account 0x6Ecbe1DB9EF729CBe972C83Fb886247691Fb6beb
    usdc: 10000000000000
    usdt: 10000000000000
    ddx: 0
Token Balance of Account 0xE36Ea790bc9d7AB70C55260C66D52b1eca985f84
    usdc: 10000000000000
    usdt: 10000000000000
    ddx: 0
[...]
Verifying token distribution - COMPLETE
Done in 2.29s.
```
