import { SupportedProvider } from '@0x/subproviders';
import { providerUtils } from '@0x/utils';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { artifacts } from '@derivadex/contract-artifacts';
import {
    CompetitionDummyTokenContract,
    DDXContract,
    Derivadex,
    DerivaDEXContract,
    GovernanceContract,
    PauseContract,
} from '@derivadex/contract-wrappers';
import { getSelectors } from '@derivadex/dev-utils';
import { ContractAddresses, FacetCutAction } from '@derivadex/types';

import { migrateMainnetAsync } from '../mainnet/utils';
import { getMigrationConfigAsync, retryContractFnAwaitTransactionSuccessAsync, writeAddressesAsync } from '../utils';

async function prepareStaging(
    provider: SupportedProvider,
    owner: string,
    chainId: number,
    deployment: string,
): Promise<void> {
    const updatedAddresses: Partial<ContractAddresses> = {};

    // Deploy DDX
    const ddx = await DDXContract.deployFrom0xArtifactAsync(
        artifacts.DDX,
        provider,
        { from: owner, gas: 10_000_000 },
        artifacts,
    );
    updatedAddresses.ddxAddress = ddx.address;

    // Deploy DerivaDEX
    const derivaDEXContract = await DerivaDEXContract.deployFrom0xArtifactAsync(
        artifacts.DerivaDEX,
        provider,
        { from: owner, gas: 10_000_000 },
        artifacts,
        ddx.address,
    );
    updatedAddresses.derivaDEXAddress = derivaDEXContract.address;

    // Deploy Pause
    const pause = await PauseContract.deployFrom0xArtifactAsync(
        artifacts.Pause,
        provider,
        { from: owner, gas: 10_000_000 },
        artifacts,
    );
    updatedAddresses.pauseAddress = pause.address;

    // Deploy Governance
    const governance = await GovernanceContract.deployFrom0xArtifactAsync(
        artifacts.Governance,
        provider,
        { from: owner, gas: 10_000_000 },
        artifacts,
    );
    updatedAddresses.governanceAddress = governance.address;

    // Deploy USDC (Dummy Token)
    const usdcContract = await CompetitionDummyTokenContract.deployFrom0xArtifactAsync(
        artifacts.CompetitionDummyToken,
        provider,
        { from: owner, gas: 10_000_000 },
        artifacts,
        'USD Stablecoin',
        'USDC',
        6,
        derivaDEXContract.address,
    );
    updatedAddresses.usdcAddress = usdcContract.address;

    // Write the new addresses to file
    await writeAddressesAsync(updatedAddresses, deployment, chainId);

    const { addresses: ddxAddresses } = await getContractDeploymentAsync(deployment);
    const derivadex = new Derivadex(ddxAddresses, provider, chainId);

    // Transfer DDX ownership to DerivaDEX
    await retryContractFnAwaitTransactionSuccessAsync(
        provider,
        ddx.transferOwnershipToDerivaDEXProxy(derivaDEXContract.address),
        { from: owner },
    );

    // Propose pause
    const pauseSelectors = getSelectors(pause);
    await retryContractFnAwaitTransactionSuccessAsync(
        provider,
        derivadex.diamondCut(
            [
                {
                    facetAddress: derivadex.pauseContract.address,
                    action: FacetCutAction.Add,
                    functionSelectors: pauseSelectors,
                },
            ],
            derivadex.pauseContract.address,
            derivadex.initializePause().getABIEncodedTransactionData(),
        ),
        { from: owner },
    );

    // Propose governance
    const governanceSelectors = getSelectors(derivadex.governanceContract);
    await retryContractFnAwaitTransactionSuccessAsync(
        provider,
        derivadex.diamondCut(
            [
                {
                    facetAddress: derivadex.governanceContract.address,
                    action: FacetCutAction.Add,
                    functionSelectors: governanceSelectors,
                },
            ],
            derivadex.governanceContract.address,
            derivadex
                .initializeGovernance(
                    10,
                    1,
                    17280, // 3 days worth of blocks
                    1209600, // 14 days worth of seconds
                    0, // will be made 259200 ~ 3 days worth of seconds
                    4,
                    1,
                    50,
                )
                .getABIEncodedTransactionData(),
        ),
        { from: owner },
    );

    // Transfer ownership of DerivaDEX contract to itself
    await retryContractFnAwaitTransactionSuccessAsync(provider, derivadex.transferOwnershipToSelf(), { from: owner });
}

(async () => {
    const { provider, migrationEnv } = await getMigrationConfigAsync();
    const chainId = await providerUtils.getChainIdAsync(provider);
    if (chainId === 1) {
        throw new Error("Network is mainnet. This isn't safe when running a testnet migration.");
    } else if (chainId === 100 || chainId === 1337) {
        throw new Error('Network is a local blockchain.');
    }
    if (!migrationEnv.custodians) {
        throw new Error('Must provide initialization custodians in migration env');
    }
    if (!Array.isArray(migrationEnv.custodians) || migrationEnv.custodians.length < 3) {
        throw new Error('Must specify at least 3 initial custodian addresses');
    }

    // Do everything up until the main protocol proposal is created
    await prepareStaging(provider, migrationEnv.from, chainId, migrationEnv.deployment);

    const { addresses: ddxAddresses } = await getContractDeploymentAsync(migrationEnv.deployment);
    const derivadex = new Derivadex(ddxAddresses, provider, chainId);
    derivadex.setGovernanceAddressToProxy();

    // Prepare the same proposal that we will use in the mainnet migration script
    const newFacetContracts = await migrateMainnetAsync(
        derivadex,
        migrationEnv.from,
        {
            custodians: migrationEnv.custodians,
        },
        true,
    );

    // Write the new facet addresses to the file
    const newAddresses: Partial<ContractAddresses> = newFacetContracts.reduce((prev, curr) => {
        prev[`${curr.key}Address` as keyof ContractAddresses] = curr.contract.address;
        return prev;
    }, {} as Partial<ContractAddresses>);
    await writeAddressesAsync(newAddresses, migrationEnv.deployment, chainId);
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
