import { NonceTrackerSubprovider, RPCSubprovider, Web3ProviderEngine } from '@0x/subproviders';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { Derivadex } from '@derivadex/contract-wrappers';

import { migrateMainnetForkAsync } from './migrate_mainnet_fork_utils';

(async () => {
    const nonceSubprovider = new NonceTrackerSubprovider();
    const rpcSubprovider = new RPCSubprovider('http://localhost:8545');
    const provider = new Web3ProviderEngine();
    provider.addProvider(nonceSubprovider);
    provider.addProvider(rpcSubprovider);
    provider.start();

    const { addresses: ddxAddresses } = await getContractDeploymentAsync('derivadex');

    const derivadex = new Derivadex(ddxAddresses, provider);
    derivadex.setGovernanceAddressToProxy();

    const web3Wrapper = derivadex.getWeb3Wrapper();
    const accounts = await web3Wrapper.getAvailableAddressesAsync();

    const largeDDXHolderAccounts = [
        ddxAddresses.gnosisSafeProxyAddress,
        '0x9765adc04fe7dcb8913037dbb3e05fabdd4a11ff',
        '0x76472d8c89d5895646b96fb3f2033572303b641a',
    ];

    await migrateMainnetForkAsync(derivadex, accounts.concat(largeDDXHolderAccounts));
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
