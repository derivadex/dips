import { Web3Wrapper } from '@0x/web3-wrapper';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { Derivadex } from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, advanceTimeAsync } from '@derivadex/dev-utils';
import { ContractAddresses } from '@derivadex/types';
import { TransactionReceipt } from 'ethereum-types';
import { ethers } from 'ethers';

import { migrateMainnetAsync } from '../mainnet/utils';
import { writeAddressesAsync } from '../utils';

export async function migrateMainnetForkAsync(derivadex: Derivadex, accounts: string[]): Promise<void> {
    const web3Wrapper = derivadex.getWeb3Wrapper();

    for (const account of accounts.slice(30)) {
        // Impersonate the large DDX holder accounts
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [account],
        });
        console.log(`Impersonated account: ${account}`);

        // Fund the large DDX holders with ether
        const tx = await web3Wrapper.sendTransactionAsync({
            from: accounts[7],
            to: account,
            value: Web3Wrapper.toBaseUnitAmount(10, 18),
        });
        await web3Wrapper.awaitTransactionSuccessAsync(tx);
        console.log(`Funded account: ${account}`);
    }

    const newFacetContracts = await migrateMainnetAsync(
        derivadex,
        accounts[30], // deploy/propose from a large DDX holder account
        {
            custodians: accounts.slice(0, 3),
        },
        false,
    );

    const proposalNumber = (await derivadex.getProposalCountAsync()).toNumber();
    console.log(`Proposal number: ${proposalNumber}`);

    await passProposalAsync(derivadex, accounts, proposalNumber);
    console.log('Passed proposal.');

    // set all facet addresses to proxy after initializing
    derivadex.setFacetAddressesToProxy();

    // transfer USDC to accounts
    await transferUSDC(derivadex, accounts);

    // transfer DDX to accounts
    await transferDDX(derivadex, accounts);

    const mainnetForkDeployedAddresses: Partial<ContractAddresses> = newFacetContracts.reduce((prev, curr) => {
        prev[`${curr.key}Address` as keyof ContractAddresses] = curr.contract.address;
        return prev;
    }, {} as Partial<ContractAddresses>);

    const { addresses: mainnetAddresses } = await getContractDeploymentAsync('derivadex');
    const overrideSnapshotAddresses = {
        ...mainnetAddresses,
        ...mainnetForkDeployedAddresses,
    };

    // Write the updated contract addresses to a file.
    console.log(`Writing new addresses: ${JSON.stringify(overrideSnapshotAddresses)}`);
    await writeAddressesAsync(overrideSnapshotAddresses, 'snapshot', 1337);
    console.log('Wrote new addresses');
}

async function passProposalAsync(
    derivadex: Derivadex,
    accounts: string[],
    proposalNumber: number,
): Promise<TransactionReceipt> {
    await advanceBlocksAsync(derivadex.providerEngine, 2);

    // vote with the large DDX holder accounts to fast track proposal
    for (const account of accounts.slice(30)) {
        await derivadex.castVote(proposalNumber, true).awaitTransactionSuccessAsync({ from: account });
    }

    await derivadex.queue(proposalNumber).awaitTransactionSuccessAsync({ from: accounts[0] });
    await advanceTimeAsync(derivadex.providerEngine, 259200);
    await advanceBlocksAsync(derivadex.providerEngine, 1);

    // using ethers for the execution to address 'array too large' issue with Specs contract
    const derivadexContract = new ethers.Contract(
        derivadex.derivaDEXContract.address,
        derivadex.governanceContract.abi,
        derivadex.provider.getSigner(),
    );
    const tx = await derivadexContract.execute(proposalNumber);
    return tx.wait();
}

async function transferUSDC(derivadex: Derivadex, accounts: string[]): Promise<void> {
    const web3Wrapper = derivadex.getWeb3Wrapper();

    const largeUSDCHolderAddress = '0x55fe002aeff02f77364de339a1292923a15844b8';
    const usdcSendAmount = 10_000_000;

    // Impersonate the Circle account for transfering USDC
    await web3Wrapper.sendRawPayloadAsync<void>({
        method: 'hardhat_impersonateAccount',
        params: [largeUSDCHolderAddress],
    });

    await derivadex.usdcContract
        ?.approve(largeUSDCHolderAddress, Web3Wrapper.toBaseUnitAmount(usdcSendAmount * 30, 6))
        .awaitTransactionSuccessAsync({
            from: largeUSDCHolderAddress,
        });
    for (const account of accounts.slice(0, 30)) {
        console.log(`Transfering ${usdcSendAmount} USDC to ${account}`);
        await derivadex.usdcContract
            ?.transferFrom(largeUSDCHolderAddress, account, Web3Wrapper.toBaseUnitAmount(usdcSendAmount, 6))
            .awaitTransactionSuccessAsync({
                from: largeUSDCHolderAddress,
            });
    }
}

async function transferDDX(derivadex: Derivadex, accounts: string[]): Promise<void> {
    const web3Wrapper = derivadex.getWeb3Wrapper();

    const largeDDXHolderAddress = '0x9765adc04fe7dcb8913037dbb3e05fabdd4a11ff';
    const ddxSendAmount = 1_000;

    const faucetAddress = '0xaCcf7803C88B05846c46946D7B46418A9ADAd578';
    const faucetFundAmount = 1_000_000;

    // Impersonate the Circle account for transfering USDC
    await web3Wrapper.sendRawPayloadAsync<void>({
        method: 'hardhat_impersonateAccount',
        params: [largeDDXHolderAddress],
    });

    await derivadex.ddxContract
        ?.approve(
            largeDDXHolderAddress,
            Web3Wrapper.toBaseUnitAmount(ddxSendAmount * 30, 18).plus(
                Web3Wrapper.toBaseUnitAmount(faucetFundAmount, 18),
            ),
        )
        .awaitTransactionSuccessAsync({
            from: largeDDXHolderAddress,
        });

    for (const account of accounts.slice(0, 30)) {
        console.log(`Transfering ${ddxSendAmount} DDX to ${account}`);
        await derivadex.ddxContract
            ?.transferFrom(largeDDXHolderAddress, account, Web3Wrapper.toBaseUnitAmount(ddxSendAmount, 18))
            .awaitTransactionSuccessAsync({
                from: largeDDXHolderAddress,
            });
    }

    console.log(`Transfering ${faucetFundAmount} DDX to ${faucetAddress} (used for faucet)`);
    await derivadex.ddxContract
        ?.transferFrom(largeDDXHolderAddress, faucetAddress, Web3Wrapper.toBaseUnitAmount(faucetFundAmount, 18))
        .awaitTransactionSuccessAsync({
            from: largeDDXHolderAddress,
        });
}
