import { Web3Wrapper } from '@0x/web3-wrapper';
import { Derivadex } from '@derivadex/contract-wrappers';

import {
    extractDataFromReport,
    getMigrationConfigAsync,
    registerCustodian,
    retryContractFnAwaitTransactionSuccessAsync,
} from '../utils';

(async () => {
    const { provider, migrationEnv, ddxAddresses } = await getMigrationConfigAsync();
    if (!migrationEnv.registrationData) {
        throw new Error('Must provide registrationData in migration env');
    }

    let registrationData = migrationEnv.registrationData;
    if (typeof registrationData === 'string') {
        registrationData = JSON.parse(registrationData) as { report: string; signature: string };
    }
    if (!registrationData.report || !registrationData.signature) {
        throw new Error('Either report or signature missing from registrationData in migration env');
    }

    const derivadex = new Derivadex(ddxAddresses, provider);
    derivadex.setCustodianAddressToProxy();

    const [minimumBond] = await derivadex.custodianContract.getCustodianInfo().callAsync();

    if (migrationEnv.registrationParams && migrationEnv.registrationParams.shouldApproveDDX) {
        console.log(`Approving ${Web3Wrapper.toUnitAmount(minimumBond, 18)} DDX for DerivaDEX diamond`);
        await retryContractFnAwaitTransactionSuccessAsync(
            provider,
            derivadex.ddxContract.approve(ddxAddresses.derivaDEXAddress, minimumBond),
            {
                from: migrationEnv.from,
            },
        );
    }

    if (migrationEnv.registrationParams && migrationEnv.registrationParams.shouldBond) {
        console.log('Bonding custodian');
        await retryContractFnAwaitTransactionSuccessAsync(provider, derivadex.custodianContract.bond(minimumBond), {
            from: migrationEnv.from,
        });
    }

    const { releaseHash } = extractDataFromReport(registrationData.report);
    console.log(`Registering custodian for release hash: ${releaseHash}`);

    await registerCustodian(
        provider,
        migrationEnv.from,
        registrationData.report,
        registrationData.signature,
        ddxAddresses.derivaDEXAddress,
    );
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
