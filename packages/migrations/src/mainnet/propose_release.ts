import { BigNumber } from '@0x/utils';
import { Derivadex } from '@derivadex/contract-wrappers';
import { generateCallData } from '@derivadex/dev-utils';

import {
    getMigrationConfigAsync,
    hashRegistrationMetadata,
    retryContractFnAwaitTransactionSuccessAsync,
} from '../utils';

(async () => {
    const { provider, migrationEnv, ddxAddresses } = await getMigrationConfigAsync();
    if (!migrationEnv.proposedRelease) {
        throw new Error('Must provide proposedRelease in migration env');
    }
    if (
        !migrationEnv.proposedRelease.mrEnclave ||
        !migrationEnv.proposedRelease.isvSvn ||
        !migrationEnv.proposedRelease.startingEpochId
    ) {
        throw new Error('Either mrEnclave, isvSvn, or startingEpochId missing from proposedRelease in migration env');
    }
    const { mrEnclave, isvSvn, startingEpochId } = migrationEnv.proposedRelease;

    const derivadex = new Derivadex(ddxAddresses, provider);
    derivadex.setGovernanceAddressToProxy();

    const targets = [derivadex.derivaDEXContract.address];
    const values = [new BigNumber(0)];
    const signatures = [derivadex.registrationContract.getFunctionSignature('updateReleaseSchedule')];
    const calldatas = [
        generateCallData(
            derivadex.registrationContract
                .updateReleaseSchedule({ mrEnclave, isvSvn }, new BigNumber(startingEpochId))
                .getABIEncodedTransactionData(),
        ),
    ];
    const releaseHash = hashRegistrationMetadata(mrEnclave, isvSvn);
    console.log(`Proposing release with release hash ${releaseHash} for epoch ${startingEpochId}`);

    const description = `New release hash ${releaseHash} proposed for epoch ${startingEpochId}`;
    await retryContractFnAwaitTransactionSuccessAsync(
        provider,
        derivadex.governanceContract.propose(targets, values, signatures, calldatas, description),
        { from: migrationEnv.from },
    );

    console.log('Release successfully proposed.');
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
