import { BigNumber } from '@0x/utils';
import { Derivadex } from '@derivadex/contract-wrappers';
import { generateCallData } from '@derivadex/dev-utils';

import { getMigrationConfigAsync, retryContractFnAwaitTransactionSuccessAsync } from '../utils';

(async () => {
    const { provider, migrationEnv, ddxAddresses } = await getMigrationConfigAsync();
    if (!migrationEnv.proposedSpecsUpserts) {
        throw new Error('Must provide proposedSpecsUpsert in migration env');
    }
    if (!migrationEnv.proposedSpecsUpserts.every(({ key, expr }) => key !== undefined && expr !== undefined)) {
        throw new Error('Either key or expr is missing from proposedSpecsUpserts array in migration env');
    }

    const derivadex = new Derivadex(ddxAddresses, provider);
    derivadex.setGovernanceAddressToProxy();

    const targets = Array(migrationEnv.proposedSpecsUpserts.length).fill(derivadex.derivaDEXContract.address);
    const values = Array(migrationEnv.proposedSpecsUpserts.length).fill(new BigNumber(0));
    const signatures = Array(migrationEnv.proposedSpecsUpserts.length).fill(
        derivadex.specsContract.getFunctionSignature('upsertSpecs'),
    );
    const calldatas = migrationEnv.proposedSpecsUpserts.map(({ key, expr }) =>
        generateCallData(derivadex.specsContract.upsertSpecs(key, expr).getABIEncodedTransactionData()),
    );

    const description = `Specs Upsert proposed. ${migrationEnv.proposedSpecsUpserts
        .map(({ key, expr }) => `Key: ${key} Expr: ${expr}`)
        .join(',')}.`;
    await retryContractFnAwaitTransactionSuccessAsync(
        provider,
        derivadex.governanceContract.propose(targets, values, signatures, calldatas, description),
        { from: migrationEnv.from },
    );

    console.log('Specs upsert successfully proposed.');
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
