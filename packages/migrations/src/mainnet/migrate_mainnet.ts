import { providerUtils } from '@0x/utils';
import { Derivadex } from '@derivadex/contract-wrappers';
import { ContractAddresses } from '@derivadex/types';

import { getMigrationConfigAsync, writeAddressesAsync } from '../utils';
import { migrateMainnetAsync } from './utils';

(async () => {
    const { provider, migrationEnv, ddxAddresses } = await getMigrationConfigAsync();
    if (!migrationEnv.custodians) {
        throw new Error('Must provide initialization custodians in migration env');
    }
    const chainId = await providerUtils.getChainIdAsync(provider);
    if (chainId !== 1) {
        throw new Error('Migration script is designed for mainnet (chainId = 1)');
    }
    if (!Array.isArray(migrationEnv.custodians) || migrationEnv.custodians.length < 3) {
        throw new Error('Must specify at least 3 initial custodian addresses');
    }
    const derivadex = new Derivadex(ddxAddresses, provider);
    derivadex.setGovernanceAddressToProxy();
    const newFacetContracts = await migrateMainnetAsync(
        derivadex,
        migrationEnv.from,
        {
            custodians: migrationEnv.custodians,
        },
        false,
    );
    const deployedAddresses: Partial<ContractAddresses> = newFacetContracts.reduce((prev, curr) => {
        prev[`${curr.key}Address` as keyof ContractAddresses] = curr.contract.address;
        return prev;
    }, {} as Partial<ContractAddresses>);

    await writeAddressesAsync(deployedAddresses, 'derivadex', 1);
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
