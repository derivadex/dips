import { BigNumber } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import {
    BannerContract,
    CheckpointContract,
    CollateralContract,
    CustodianContract,
    Derivadex,
    FundedInsuranceFundContract,
    RegistrationContract,
    RejectContract,
    SpecsContract,
    StakeContract,
    TestCheckpointMultiNodeDeploymentContract,
    TestCollateralContract,
    TestRegistrationWithResetContract,
    TestStakeContract,
} from '@derivadex/contract-wrappers';
import {
    DEFAULT_SPEC_KEYS,
    DEFAULT_SPECS,
    generateCallData,
    generateSpecsInitializationData,
    getSelectors,
} from '@derivadex/dev-utils';
import { FacetCutAction } from '@derivadex/types';

import release_mr from '../../release_mr.json';
import { retryContractFnAwaitTransactionSuccessAsync } from '../utils';

interface NewFacetContract {
    key: string;
    contract:
        | BannerContract
        | CheckpointContract
        | TestCheckpointMultiNodeDeploymentContract
        | CollateralContract
        | TestCollateralContract
        | CustodianContract
        | FundedInsuranceFundContract
        | RegistrationContract
        | TestRegistrationWithResetContract
        | RejectContract
        | SpecsContract
        | StakeContract
        | TestStakeContract;
    initializerTransactionData: string;
    selectors?: string[];
}

interface InitializeParameters {
    custodians: string[];
}

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

async function getFacetsToAdd(
    derivadex: Derivadex,
    fromAddress: string,
    initializeParameters: InitializeParameters,
    useTestContracts: boolean,
): Promise<NewFacetContract[]> {
    const facetContractsToAdd: NewFacetContract[] = [];

    // Banner Contract
    const bannerContract = await BannerContract.deployFrom0xArtifactAsync(
        artifacts.Banner,
        derivadex.providerEngine,
        { from: fromAddress, gas: 10000000 },
        artifacts,
    );
    facetContractsToAdd.push({
        key: 'banner',
        contract: bannerContract,
        initializerTransactionData: bannerContract.initialize([]).getABIEncodedTransactionData(),
    });

    // Checkpoint Contract
    let checkpointContract;
    if (useTestContracts) {
        checkpointContract = await TestCheckpointMultiNodeDeploymentContract.deployFrom0xArtifactAsync(
            artifacts.TestCheckpointMultiNodeDeployment,
            derivadex.providerEngine,
            { from: fromAddress, gas: 10000000 },
            artifacts,
        );
    } else {
        checkpointContract = await CheckpointContract.deployFrom0xArtifactAsync(
            artifacts.Checkpoint,
            derivadex.providerEngine,
            { from: fromAddress, gas: 10000000 },
            artifacts,
        );
    }
    facetContractsToAdd.push({
        key: 'checkpoint',
        contract: checkpointContract,
        initializerTransactionData: checkpointContract
            .initialize(new BigNumber(51), new BigNumber(2))
            .getABIEncodedTransactionData(),
    });

    // Collateral Contract
    let collateralContract;
    if (useTestContracts) {
        collateralContract = await TestCollateralContract.deployFrom0xArtifactAsync(
            artifacts.TestCollateral,
            derivadex.providerEngine,
            { from: fromAddress, gas: 10000000 },
            artifacts,
        );
    } else {
        collateralContract = await CollateralContract.deployFrom0xArtifactAsync(
            artifacts.Collateral,
            derivadex.providerEngine,
            { from: fromAddress, gas: 10000000 },
            artifacts,
        );
    }
    // NOTE: Set alpha1 guard value as default for mainnet-dips repo
    // where .env configuration differs from monorepo
    const MAX_USER_COUNT = 1000;
    facetContractsToAdd.push({
        key: 'collateral',
        contract: collateralContract,
        initializerTransactionData: collateralContract
            .initialize(
                new BigNumber(50),
                new BigNumber(10),
                new BigNumber(MAX_USER_COUNT),
                Web3Wrapper.toBaseUnitAmount(new BigNumber(1_000), 6),
            )
            .getABIEncodedTransactionData(),
        // use ONLY selectors from collateral contract
        selectors: getSelectors(new CollateralContract(collateralContract.address, derivadex.providerEngine), [
            'initialize',
        ]),
    });

    // Custodian Contract
    const custodianContract = await CustodianContract.deployFrom0xArtifactAsync(
        artifacts.Custodian,
        derivadex.providerEngine,
        { from: fromAddress, gas: 10000000 },
        artifacts,
    );
    facetContractsToAdd.push({
        key: 'custodian',
        contract: custodianContract,
        initializerTransactionData: custodianContract
            .initialize(new BigNumber(0), new BigNumber(50_400))
            .getABIEncodedTransactionData(),
    });

    // Funded Insurance Fund Contract
    const fundedInsuranceContract = await FundedInsuranceFundContract.deployFrom0xArtifactAsync(
        artifacts.FundedInsuranceFund,
        derivadex.providerEngine,
        { from: fromAddress, gas: 10000000 },
        artifacts,
    );
    facetContractsToAdd.push({
        key: 'fundedInsuranceFund',
        contract: fundedInsuranceContract,
        initializerTransactionData: '0x',
    });

    // Registration Contract
    let registrationContract;
    if (useTestContracts) {
        registrationContract = await TestRegistrationWithResetContract.deployFrom0xArtifactAsync(
            artifacts.TestRegistrationWithReset,
            derivadex.providerEngine,
            { from: fromAddress, gas: 10000000 },
            artifacts,
        );
    } else {
        registrationContract = await RegistrationContract.deployFrom0xArtifactAsync(
            artifacts.Registration,
            derivadex.providerEngine,
            { from: fromAddress, gas: 10000000 },
            artifacts,
        );
    }
    facetContractsToAdd.push({
        key: 'registration',
        contract: registrationContract,
        initializerTransactionData: registrationContract
            .initialize(
                {
                    mrEnclave: release_mr.mrEnclave,
                    isvSvn: `0x${Buffer.from(release_mr.isvsvn).toString('hex')}`,
                },
                initializeParameters.custodians,
            )
            .getABIEncodedTransactionData(),
    });

    // Reject Contract
    const rejectContract = await RejectContract.deployFrom0xArtifactAsync(
        artifacts.Reject,
        derivadex.providerEngine,
        { from: fromAddress, gas: 10000000 },
        artifacts,
    );
    facetContractsToAdd.push({
        key: 'reject',
        contract: rejectContract,
        initializerTransactionData: '0x',
    });

    // Specs Contract
    const specsContract = await SpecsContract.deployFrom0xArtifactAsync(
        artifacts.Specs,
        derivadex.providerEngine,
        { from: fromAddress, gas: 10000000 },
        artifacts,
    );
    facetContractsToAdd.push({
        key: 'specs',
        contract: specsContract,
        initializerTransactionData: generateSpecsInitializationData(DEFAULT_SPEC_KEYS, DEFAULT_SPECS),
    });

    // Stake Contract
    let stakeContract;
    if (useTestContracts) {
        stakeContract = await TestStakeContract.deployFrom0xArtifactAsync(
            artifacts.TestStake,
            derivadex.providerEngine,
            { from: fromAddress, gas: 10000000 },
            artifacts,
        );
    } else {
        stakeContract = await StakeContract.deployFrom0xArtifactAsync(
            artifacts.Stake,
            derivadex.providerEngine,
            { from: fromAddress, gas: 10000000 },
            artifacts,
        );
    }
    facetContractsToAdd.push({
        key: 'stake',
        contract: stakeContract,
        initializerTransactionData: stakeContract
            .initialize(
                Web3Wrapper.toBaseUnitAmount(new BigNumber(10_000), 18),
                Web3Wrapper.toBaseUnitAmount(new BigNumber(40_000_000), 18),
            )
            .getABIEncodedTransactionData(),
    });

    return facetContractsToAdd;
}

async function prepareNewFacetCalldataAsync(
    derivadex: Derivadex,
    facetContractsToAdd: NewFacetContract[],
): Promise<string[]> {
    return facetContractsToAdd.map((f) =>
        generateCallData(
            derivadex
                .diamondCut(
                    [
                        {
                            facetAddress: f.contract.address,
                            action: FacetCutAction.Add,
                            functionSelectors: f.selectors ? f.selectors : getSelectors(f.contract, ['initialize']),
                        },
                    ],
                    f.initializerTransactionData === '0x' ? ZERO_ADDRESS : f.contract.address,
                    f.initializerTransactionData,
                )
                .getABIEncodedTransactionData(),
        ),
    );
}

export async function migrateMainnetAsync(
    derivadex: Derivadex,
    fromAddress: string,
    initializeParameters: InitializeParameters,
    useTestContracts: boolean,
): Promise<NewFacetContract[]> {
    const newFacetContracts = await getFacetsToAdd(derivadex, fromAddress, initializeParameters, useTestContracts);
    const newFacetCalldata = await prepareNewFacetCalldataAsync(derivadex, newFacetContracts);

    const targets = Array(newFacetCalldata.length).fill(derivadex.derivaDEXContract.address);
    const values = Array(newFacetCalldata.length).fill(0);
    const signatures = Array(newFacetCalldata.length).fill(
        derivadex.diamondFacetContract.getFunctionSignature('diamondCut'),
    );
    const description = 'Add remaining DerivaDEX facets.';

    // Add USDC collateral to Collateral contract
    targets.push(derivadex.derivaDEXContract.address);
    values.push(0);
    signatures.push(derivadex.collateralContract.getFunctionSignature('addExchangeCollateral'));
    const addExchangeCollateralCalldata = generateCallData(
        derivadex.collateralContract
            .addExchangeCollateral(
                derivadex.usdcContract!.address,
                Web3Wrapper.toBaseUnitAmount(new BigNumber(1_000_000), 6),
            )
            .getABIEncodedTransactionData(),
    );

    console.log('Proposing new facets to Governance.');
    const { gasUsed } = await retryContractFnAwaitTransactionSuccessAsync(
        derivadex.providerEngine,
        derivadex.propose(
            targets,
            values,
            signatures,
            newFacetCalldata.concat(addExchangeCollateralCalldata),
            description,
        ),
        { from: fromAddress, gas: 10000000 },
    );
    console.log(`Proposal successfully created. Gas Used: ${gasUsed}`);

    return newFacetContracts;
}
