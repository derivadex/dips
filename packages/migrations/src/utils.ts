import { ContractTxFunctionObj } from '@0x/base-contract';
import {
    LedgerEthereumClient,
    LedgerSubprovider,
    NonceTrackerSubprovider,
    PrivateKeyWalletSubprovider,
    RPCSubprovider,
    SupportedProvider,
    TrezorSubprovider,
    Web3ProviderEngine,
} from '@0x/subproviders';
import { BigNumber, hexUtils, providerUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { GasConsumerContract, RegistrationContract } from '@derivadex/contract-wrappers';
import { readFileAsync, writeFileAsync } from '@derivadex/dev-utils';
import { ContractAddresses, ContractDeployments, EthereumAccount, MigrationEnv } from '@derivadex/types';
import Eth from '@ledgerhq/hw-app-eth';
import TransportNodeHid from '@ledgerhq/hw-transport-node-hid';
import TrezorConnect from '@trezor/connect';
import { path as rootPath } from 'app-root-path';
import { Transaction, TransactionReceiptWithDecodedLogs, TxData } from 'ethereum-types';
import { readFileSync } from 'fs';
import * as _ from 'lodash';
import { join } from 'path';
import * as yargs from 'yargs';

export interface MigrationConfig {
    migrationEnv: MigrationEnv;
    provider: SupportedProvider;
    ddxAddresses: ContractAddresses;
    custodians?: EthereumAccount[];
    faucet?: EthereumAccount;
}

export const cliYargs = yargs
    .parserConfiguration({
        'parse-numbers': false,
    })
    .config('config', function (configPath) {
        return JSON.parse(readFileSync(configPath, { encoding: 'utf-8' }));
    });

export async function ledgerEthereumNodeJsClientFactoryAsync(): Promise<LedgerEthereumClient> {
    const ledgerConnection = await TransportNodeHid.create();
    const ledgerEthClient = new Eth(ledgerConnection);
    return ledgerEthClient;
}

export async function getMigrationConfigAsync(): Promise<MigrationConfig> {
    const args = cliYargs.argv;
    const chainId = args['chainId'] === undefined ? undefined : (args['chainId'] as number);
    if (chainId === undefined) {
        throw new Error('migration: cannot continue unless "chainId" is defined');
    }
    const rpcUrl = args['rpcUrl'] === undefined ? undefined : (args['rpcUrl'] as string);
    if (rpcUrl === undefined) {
        throw new Error('migration: cannot continue unless "rpcUrl" is defined');
    }
    const from = args['from'] === undefined ? undefined : (args['from'] as string).toLowerCase();
    if (from === undefined) {
        throw new Error('migration: cannot continue unless "from" is defined');
    }
    const confirmations = args['confirmations'] === undefined ? undefined : (args['confirmations'] as number);
    if (confirmations === undefined) {
        throw new Error('migration: cannot continue unless "confirmations" is defined');
    }
    const trezor = args['trezor'] === undefined ? false : (args['trezor'] as boolean);
    const ledger = args['ledger'] === undefined ? false : (args['ledger'] as boolean);
    if (ledger && trezor) {
        throw new Error('migration: must choose one of "ledger" and "trezor." Your config has both activated');
    }
    const baseDerivationPath =
        args['baseDerivationPath'] === undefined ? undefined : (args['baseDerivationPath'] as string);
    const pk = args['pk'] === undefined ? undefined : (args['pk'] as string);
    const deployment = args['deployment'] === undefined ? undefined : (args['deployment'] as string);
    if (deployment === undefined) {
        throw new Error('migration: cannot continue unless "deployment" is defined');
    }

    const nonceSubprovider = new NonceTrackerSubprovider();
    const rpcSubprovider = new RPCSubprovider(rpcUrl);
    const provider = new Web3ProviderEngine();
    provider.addProvider(nonceSubprovider);
    if (trezor) {
        // Trezor Connect module initialized with these parameters to
        // properly run as a node process (needs a bit more clarity)
        await TrezorConnect.init({
            manifest: {
                appUrl: 'developer@xyz.com',
                email: 'http://your.application.com',
            },
            interactionTimeout: 5000,
            transportReconnect: true,
            webusb: true,
            debug: false,
            popup: true,
        });
        const trezorSubprovider = new TrezorSubprovider({
            accountFetchingConfigs: {
                addressSearchLimit: 1000,
            },
            trezorConnectClientApi: TrezorConnect,
            networkId: chainId,
        });
        provider.addProvider(trezorSubprovider);
    } else if (ledger) {
        const ledgerSubprovider = new LedgerSubprovider({
            networkId: chainId,
            ledgerEthereumClientFactoryAsync: ledgerEthereumNodeJsClientFactoryAsync,
            baseDerivationPath: baseDerivationPath || "m/44'/60'/0'/0",
        });
        provider.addProvider(ledgerSubprovider);
    } else if (pk !== undefined && pk !== '') {
        const pkSubprovider = new PrivateKeyWalletSubprovider(pk as string, chainId);
        provider.addProvider(pkSubprovider);
    }
    provider.addProvider(rpcSubprovider);
    provider.start();
    const expectedChainId = await providerUtils.getChainIdAsync(provider);
    if (expectedChainId.toString() !== chainId.toString()) {
        throw new Error(
            `migration: expected chain id ${expectedChainId} but chain id ${chainId} was provided in the config`,
        );
    }
    const {
        addresses: ddxAddresses,
        chainId: chainId_,
        custodians,
        faucet,
    } = await getContractDeploymentAsync(deployment);
    if (chainId.toString() !== chainId_.toString()) {
        throw new Error(`Chain ID mismatch. Expected ${chainId} but got ${chainId_}`);
    }
    return {
        migrationEnv: {
            ...args,
            rpcUrl,
            from,
            confirmations,
            deployment,
        },
        provider,
        ddxAddresses,
        custodians,
        faucet,
    };
}

export function hashRegistrationMetadata(mrEnclave: string, isvSvn: string): string {
    return hexUtils.hash(hexUtils.concat(mrEnclave, hexUtils.rightPad(isvSvn, 32)));
}

function convertFromHexToString(hex: string) {
    let str = '';
    for (let i = 0; i < hex.length; i += 2) {
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    }
    return str;
}

export function extractDataFromReport(report: string): {
    signer: string;
    releaseHash: string;
} {
    const reportAsString = convertFromHexToString(report.slice(2));
    const reportJson = JSON.parse(reportAsString);
    const isvEnclaveQuoteBody = reportJson.isvEnclaveQuoteBody;
    const decodedQuoteBody = Buffer.from(isvEnclaveQuoteBody, 'base64');
    const bodyHex = decodedQuoteBody.toString('hex');
    const mrEnclave = `0x${bodyHex.substring(112 * 2, (112 + 32) * 2)}`;
    const isvSvn = `0x${bodyHex.substring(306 * 2, (306 + 2) * 2)}`;
    const signer = `0x${bodyHex.substring(368 * 2, (368 + 20) * 2)}`;
    const releaseHash = hashRegistrationMetadata(mrEnclave, isvSvn);
    return {
        signer,
        releaseHash,
    };
}

export async function registerCustodian(
    custodianProvider: SupportedProvider,
    custodianAddress: string,
    report: string,
    signature: string,
    derivaDEXAddress: string,
) {
    const custodianRegistrationContract = new RegistrationContract(derivaDEXAddress, custodianProvider);
    console.log(`Registering signer for custodian: ${custodianAddress}.`);
    await retryContractFnAwaitTransactionSuccessAsync(
        custodianProvider,
        custodianRegistrationContract.register(report, signature),
        {
            from: custodianAddress,
        },
    );
    console.log('Registration successful');
}

function adjustTxDataGas(
    txData: Partial<TxData>,
    transaction: Transaction,
    increaseGasFactor: BigNumber,
): Partial<TxData> {
    return {
        ...txData,
        nonce: transaction.nonce,
        gasPrice: BigNumber.max(
            new BigNumber(transaction.gasPrice ?? 0).multipliedBy(increaseGasFactor),
            new BigNumber(txData.gasPrice ?? 0).multipliedBy(increaseGasFactor),
        ).decimalPlaces(0),
        gas: BigNumber.max(
            new BigNumber(transaction.gas).multipliedBy(increaseGasFactor),
            new BigNumber(txData.gas ?? 0).multipliedBy(increaseGasFactor),
        ).decimalPlaces(0),
    };
}

export function isUnrecoverableError(error: any): boolean {
    const stringifiedError = JSON.stringify(error);
    return (
        stringifiedError.includes('execution reverted:') ||
        stringifiedError.includes('VM Exception while processing transaction:')
    );
}

export async function retryContractFnAwaitTransactionSuccessAsync(
    provider: SupportedProvider,
    f: ContractTxFunctionObj<any>,
    txData?: Partial<TxData>,
    gasConsumer?: GasConsumerContract,
): Promise<TransactionReceiptWithDecodedLogs> {
    let retries = 10;

    let txHash: string | null = null;
    let previousTxHash: string | null = null;
    let txDataAugmented = txData ? txData : {};

    const web3Wrapper = new Web3Wrapper(provider);

    while (retries > 0) {
        if (gasConsumer !== undefined) {
            try {
                await gasConsumer.consume().awaitTransactionSuccessAsync({ gas: 10_000_000 });
                throw new Error('Expected `consume` call to fail.');
            } catch (error) {}
        }
        try {
            console.log('TX Data: ', JSON.stringify(txDataAugmented));
            // Submit the transaction as an `eth_call`. At worst, this is
            // redundant, and with many providers, the error handling is better
            // for `eth_call`s.
            await f.callAsync(txDataAugmented);

            // Submit the transaction and await the result.
            const txHashWithPromise = f.awaitTransactionSuccessAsync(txDataAugmented, { timeoutMs: 60000 });
            previousTxHash = txHash;
            txHash = await txHashWithPromise.txHashPromise;
            console.log('TX Hash: ', txHash);

            return await txHashWithPromise;
        } catch (error: any) {
            console.log(`Encountered an error and retrying ${retries} more times: ${JSON.stringify(error, null, 4)}`);
            if (error.message !== undefined && error.message.includes('nonce too low') && txHash) {
                console.log(
                    'Handling nonce too low, which likely means the latest transaction succeeded between retries.',
                );
                return await web3Wrapper.awaitTransactionSuccessAsync(txHash, undefined, 30000);
            } else if (isUnrecoverableError(error)) {
                throw error;
            } else if (error.message !== undefined && error.message.includes('Invalid array length')) {
                // TODO: This was fixed elsewhere by using ethers when
                // interacting with the Specs contract.
                //
                // HACK: This error indicates that the array was too large
                // to allocate to memory. For now, we return an empty
                // transaction receipt, but this needs to be fixed.
                return {
                    logs: [],
                    blockHash: '',
                    blockNumber: 0,
                    transactionHash: '',
                    transactionIndex: 0,
                    from: '',
                    to: '',
                    status: 0,
                    cumulativeGasUsed: 0,
                    gasUsed: 0,
                    contractAddress: '',
                };
            } else if (
                error === 'TRANSACTION_MINING_TIMEOUT' ||
                (error.message !== undefined && error.message.includes('max fee per gas less than block base fee'))
            ) {
                console.log('Replacing transaction by increasing gas price by >10% and re-using same nonce');
                let transaction: Transaction;
                try {
                    transaction = await web3Wrapper.getTransactionByHashAsync(txHash!);
                } catch (e) {
                    console.log(
                        'Latest transaction could not be found. Attempting to return receipt from previous txHash.',
                    );
                    return await web3Wrapper.awaitTransactionSuccessAsync(previousTxHash!, undefined, 30000);
                }
                txDataAugmented = adjustTxDataGas(txDataAugmented, transaction, new BigNumber(1.101));
            }

            console.log(`Error while awaiting transaction: ${JSON.stringify(error, null, 4)}`);
            retries--;
        }
    }
    throw new Error("Couldn't await transaction with retries.");
}

export async function writeAddressesAsync(
    updatedAddresses: Partial<ContractAddresses>,
    deployment: string,
    chainId: number,
): Promise<void> {
    let derivaDEXFilePath;
    if (process.env.ADDRESSES_VOLUME) {
        derivaDEXFilePath = process.env.ADDRESSES_VOLUME + '/addresses.json';
    } else {
        const ADDRESSES_FILENAME = '/packages/contract-addresses/addresses.json';
        derivaDEXFilePath = join(rootPath, ADDRESSES_FILENAME);
    }

    let existingAddresses: ContractDeployments;
    try {
        const bufferedExistingAddresses = await readFileAsync(derivaDEXFilePath);
        existingAddresses = JSON.parse(bufferedExistingAddresses.toString());
    } catch (e) {
        console.log('No existing addresses');
        existingAddresses = {};
    }
    if (chainId !== existingAddresses[deployment].chainId) {
        throw new Error(`Can't overwrite chain ID for deployment ${deployment}`);
    }
    existingAddresses[deployment] = {
        ...existingAddresses[deployment],
        addresses: {
            ...existingAddresses[deployment].addresses,
            ..._.omitBy(updatedAddresses, _.isNil),
        },
    };
    // NOTE(jalextowle): We format the JSON before writing it to the addresses.json
    // file to make it more human-readable. This includes inserting indentation levels
    // of 4 spaces and adding an extra new line at the end of the file.
    const addressesString = JSON.stringify(existingAddresses, null, 4).concat('\n');
    await writeFileAsync(derivaDEXFilePath, addressesString);
}
