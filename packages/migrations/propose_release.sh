#!/bin/bash

echo "====================================="
echo "This script proposes a release"
echo "to the DerivaDEX governance facet."
echo "====================================="

# Immediately fail on any error
set -e

usage() {
  echo "Usage: $0 [migration config json file]";
  exit 0;
}

# Ensure that the correct arguments were passed.
MIGRATION_CONFIG=$1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
if [[ "$MIGRATION_CONFIG" == "" || "$MIGRATION_CONFIG" == "--help" ]]; then
  usage
fi

node ./lib/src/mainnet/propose_release.js --config $MIGRATION_CONFIG