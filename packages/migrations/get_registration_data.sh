#!/bin/bash

echo "====================================="
echo "This script retrieves the operator"
echo "registration data from the"
echo "operator-it service."
echo "====================================="

# Immediately fail on any error
set -e

cd "../.."

docker compose up -d operator-it
REGISTRATION_DATA=$(docker compose exec -T operator-it bash -c 'RUST_LOG="off,ddxenclave=error" ddx register')
echo "$REGISTRATION_DATA"