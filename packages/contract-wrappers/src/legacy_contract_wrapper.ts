import { ContractTxFunctionObj, ERC20TokenContract } from '@0x/contract-wrappers';
import { BigNumber } from '@0x/utils';
import { SupportedProvider, Web3Wrapper } from '@0x/web3-wrapper';
import { Account, Chain, LegacyContractAddresses, TraderInfo } from '@derivadex/types';
import { Contract, ethers, utils } from 'ethers';
import { AsyncSendable } from 'ethers/providers';

import { ERC20_DECIMALS, ONE_SECOND_MS, TEN_MINUTES_MS } from './constants';
import { LegacyDerivaDEXContract } from './generated-wrappers/legacy_deriva_d_e_x';

export class LegacyContractWrapper {
    public provider: ethers.providers.Web3Provider;
    //public exchangeContract: ExchangeContract;
    public derivaDEXContract: LegacyDerivaDEXContract;
    public quoteCurrencyContract: ERC20TokenContract;
    //  public derivaDEXTokenContract: ERC20TokenContract;
    public providerEngine: SupportedProvider;
    public chainId: number;
    public web3Wrapper: Web3Wrapper;

    /**
     * Returns an amount of seconds that is greater than the amount of seconds since epoch.
     */
    public static getRandomFutureDateInSeconds(): BigNumber {
        return new BigNumber(Date.now() + TEN_MINUTES_MS).div(ONE_SECOND_MS).integerValue(BigNumber.ROUND_CEIL);
    }

    constructor(
        contractAddresses: LegacyContractAddresses,
        provider: SupportedProvider,
        chainId: number = Chain.Ganache,
    ) {
        this.providerEngine = provider;
        this.provider = new ethers.providers.Web3Provider(provider as AsyncSendable);
        //this.exchangeContract = new ExchangeContract(getContractAddressesForChainOrThrow(chainId).exchange, provider);
        this.quoteCurrencyContract = new ERC20TokenContract(contractAddresses.quoteTokenAddress, provider);
        //this.derivaDEXTokenContract = new ERC20TokenContract(contractAddresses.derivaDEXTokenAddress, provider);
        this.chainId = chainId;
        this.derivaDEXContract = new LegacyDerivaDEXContract(contractAddresses.derivaDEXAddress, provider);
        this.web3Wrapper = new Web3Wrapper(provider);
    }

    /**
     * Create `Ethers` contract instance
     * @param address: The deployed address
     * @param abi: The contract interface object
     */
    public createContract(address: string, abi: Array<string | utils.ParamType> | string | utils.Interface): Contract {
        const checksumAddress = utils.getAddress(address);
        return new ethers.Contract(checksumAddress, abi, this.provider);
    }

    /**
     * Initialize the Margin, USDT, and 0x Exchange Contracts/Interfaces
     *
     * @param trader - Unique for each derivative
     */
    // FIXME(jalextowle): Remove this whenever this function is implemented
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public async getTraderInfoAsync(trader: string): Promise<TraderInfo> {
        //throw new Error('Not implemented in latest contracts');
        /*
        const traderInfo = await this.derivaDEXContract.getTrader(trader).callAsync();
        return {
            active: traderInfo[0],
            specialAffiliate: traderInfo[1],
            referralAddress: traderInfo[2],
            ddxBalance: Web3Wrapper.toUnitAmount(traderInfo[3], ERC20_DECIMALS),
        };
         */
        return Promise.resolve({
            active: true,
            specialAffiliate: true,
            referralAddress: '',
            ddxBalance: new BigNumber(0),
        });
    }

    /**
     * Deposit stake for trader
     *
     * @param amount - Deposit stake amount
     */
    // FIXME(jalextowle): Remove this whenever this function is implemented
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public depositDDXBalance(amount: number): ContractTxFunctionObj<void> {
        throw new Error('Not implemented in latest contracts');
        //return this.derivaDEXContract.depositDDXBalance(Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * Match two signed orders for a given derivative
     *
     * @param amount - Signed 0x taker order to match
     * @param account - Signed 0x taker order to match
     */
    public deposit(amount: number, account: string): ContractTxFunctionObj<void> {
        return this.derivaDEXContract.deposit(
            Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS),
            ethers.utils.formatBytes32String(account),
        );
    }

    /**
     * Match two signed orders for a given derivative
     *
     * @param amount - Signed 0x taker order to match
     * @param account - Signed 0x taker order to match
     */
    // FIXME(jalextowle): Remove this whenever this function is implemented
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public withdraw(amount: number, account: string): ContractTxFunctionObj<void> {
        throw new Error('Not implemented in latest contracts');
        /*
        return this.derivaDEXContract.withdraw(
            Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS),
            ethers.utils.formatBytes32String(account),
        );
         */
    }

    /**
     * Match two signed orders for a given derivative
     *
     * @param amount - Signed 0x taker order to match
     * @param accountFrom - Signed 0x taker order to match
     * @param accountTo - Signed 0x taker order to match
     */
    public transferBetweenAccounts(
        // FIXME(jalextowle): Remove the linter directives whenever this function is implemented
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        amount: number,
        // FIXME(jalextowle): Remove the linter directives whenever this function is implemented
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        accountFrom: string,
        // FIXME(jalextowle): Remove the linter directives whenever this function is implemented
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        accountTo: string,
    ): ContractTxFunctionObj<void> {
        throw new Error('Not implemented in latest contracts');
        /*
        return this.derivaDEXContract.transferBetweenAccounts(
            Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS),
            ethers.utils.formatBytes32String(accountFrom),
            ethers.utils.formatBytes32String(accountTo),
        );
         */
    }

    /**
     * Get position information for a trader
     *
     * @param account - Unique for each derivative
     * @param trader - Position owner
     */
    // FIXME(jalextowle): Remove the linter directives whenever this function is implemented
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public async getAccountByTraderAndAccountAsync(account: string, trader: string): Promise<Account> {
        const value: BigNumber = await this.getUSDTBalanceAsync(trader);
        return {
            margin: value,
            maxLeverage: new BigNumber(25),
            status: 1,
        };
    }

    /**
     * Get USDT balance of trader
     *
     * @param trader - Trader's address for which USDT balance is obtained
     */
    public async getUSDTBalanceAsync(trader: string): Promise<BigNumber> {
        const value: BigNumber = Web3Wrapper.toUnitAmount(
            await this.quoteCurrencyContract.balanceOf(trader).callAsync(),
            ERC20_DECIMALS,
        );
        return value;
    }

    public getWeb3Wrapper(): Web3Wrapper {
        return this.web3Wrapper;
    }
}
