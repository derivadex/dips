import { ContractTxFunctionObj, ERC20TokenContract } from '@0x/contract-wrappers';
import { eip712Utils } from '@0x/order-utils';
import { EIP712TypedData } from '@0x/types';
import { BigNumber } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import {
    Chain,
    ContractAddresses,
    CurrentStakeByCollateralNameAndStaker,
    CurrentTotalStakes,
    DDXClaimantState,
    DDXSupplyInfo,
    Facet,
    GovernanceParameters,
    InsuranceMineInfo,
    Proposal,
    ProposalState,
    StakeCollateral,
    TraderAttributes,
    VoteReceipt,
} from '@derivadex/types';
import { ContractArtifact, SupportedProvider } from 'ethereum-types';
// tslint:disable-next-line:no-implicit-dependencies
import * as ethUtil from 'ethereumjs-util';
import { ethers } from 'ethers';
import { AsyncSendable } from 'ethers/providers';
import _ from 'lodash';

import { ERC20_DECIMALS, ZERO_ADDRESS } from './constants';
import {
    AUSDCContract,
    AUSDTContract,
    BannerContract,
    CheckpointContract,
    CollateralContract,
    CUSDCContract,
    CUSDTContract,
    CustodianContract,
    DDXContract,
    DDXWalletCloneableContract,
    DerivaDEXContract,
    DiamondFacetContract,
    DIFundTokenContract,
    DIFundTokenFactoryContract,
    DummyTokenContract,
    FundedInsuranceFundContract,
    GovernanceContract,
    IComptrollerContract,
    InsuranceFundContract,
    OwnershipFacetContract,
    PauseContract,
    RegistrationContract,
    RejectContract,
    SafeERC20WrapperContract,
    SpecsContract,
    StakeContract,
    TraderContract,
} from './generated-wrappers';

const artifactAbis = _.mapValues(artifacts, (artifact: ContractArtifact) => artifact.compilerOutput.abi);
const UNLIMITED_ALLOWANCE = new BigNumber(2).pow(256).minus(1); // tslint:disable-line:custom-no-magic-numbers
const UNLIMITED_ALLOWANCE_96 = new BigNumber(2).pow(96).minus(1); // tslint:disable-line:custom-no-magic-numbers

// TODO(jalextowle, apalepu23): Audit these comments and ensure that they are
// accurate.
export class ContractWrapper {
    public provider: ethers.providers.Web3Provider;

    public ddxContract: DDXContract;
    public derivaDEXContract: DerivaDEXContract;
    public diamondFacetContract: DiamondFacetContract;
    public ownershipFacetContract: OwnershipFacetContract;
    public diFundTokenFactoryContract: DIFundTokenFactoryContract;
    public ddxWalletCloneableContract: DDXWalletCloneableContract;

    public bannerContract: BannerContract;
    public checkpointContract: CheckpointContract;
    public custodianContract: CustodianContract;
    public collateralContract: CollateralContract;
    public fundedInsuranceFundContract: FundedInsuranceFundContract;
    public governanceContract: GovernanceContract;
    public insuranceFundContract: InsuranceFundContract;
    public pauseContract: PauseContract;
    public registrationContract: RegistrationContract;
    public rejectContract: RejectContract;
    public specsContract: SpecsContract;
    public stakeContract: StakeContract;
    public traderContract: TraderContract;

    public ausdtContract: AUSDTContract | DummyTokenContract | SafeERC20WrapperContract | undefined;
    public cusdtContract: CUSDTContract | DummyTokenContract | SafeERC20WrapperContract | undefined;
    public usdtContract: SafeERC20WrapperContract | DummyTokenContract | undefined;
    public ausdcContract: AUSDCContract | DummyTokenContract | SafeERC20WrapperContract | undefined;
    public cusdcContract: CUSDCContract | DummyTokenContract | SafeERC20WrapperContract | undefined;
    public usdcContract: SafeERC20WrapperContract | DummyTokenContract | undefined;
    public husdContract: SafeERC20WrapperContract | DummyTokenContract | undefined;
    public gusdContract: SafeERC20WrapperContract | DummyTokenContract | undefined;

    public providerEngine: SupportedProvider;
    public chainId: number;

    public txDefaults: any;

    private static _parseSignatureHexAsRSV(signatureHex: string): ECSignature {
        const { v, r, s } = ethUtil.fromRpcSig(signatureHex);
        return {
            v,
            r: ethUtil.bufferToHex(r),
            s: ethUtil.bufferToHex(s),
        };
    }

    constructor(contractAddresses: ContractAddresses, provider: SupportedProvider, chainId: number = Chain.Ganache) {
        this.txDefaults = chainId === Chain.Ganache ? { gas: 10000000 } : {};
        this.providerEngine = provider;
        this.provider = new ethers.providers.Web3Provider(provider as AsyncSendable);
        this.ddxContract = new DDXContract(contractAddresses.ddxAddress, provider, this.txDefaults);
        this.chainId = chainId;
        this.derivaDEXContract = new DerivaDEXContract(contractAddresses.derivaDEXAddress, provider, this.txDefaults);
        this.bannerContract = new BannerContract(contractAddresses.bannerAddress, provider, this.txDefaults);
        this.governanceContract = new GovernanceContract(
            contractAddresses.governanceAddress,
            provider,
            this.txDefaults,
            artifactAbis,
        );
        this.insuranceFundContract = new InsuranceFundContract(
            contractAddresses.insuranceFundAddress,
            provider,
            this.txDefaults,
            artifactAbis,
        );
        this.custodianContract = new CustodianContract(contractAddresses.custodianAddress, provider, this.txDefaults);
        this.checkpointContract = new CheckpointContract(
            contractAddresses.checkpointAddress,
            provider,
            this.txDefaults,
        );
        this.collateralContract = new CollateralContract(
            contractAddresses.collateralAddress,
            provider,
            this.txDefaults,
            artifactAbis,
        );
        this.registrationContract = new RegistrationContract(
            contractAddresses.registrationAddress,
            provider,
            this.txDefaults,
        );
        this.rejectContract = new RejectContract(contractAddresses.rejectAddress, provider, this.txDefaults);
        this.fundedInsuranceFundContract = new FundedInsuranceFundContract(
            contractAddresses.fundedInsuranceFundAddress,
            provider,
            this.txDefaults,
            artifactAbis,
        );
        this.specsContract = new SpecsContract(contractAddresses.specsAddress, provider, this.txDefaults, artifactAbis);
        this.stakeContract = new StakeContract(contractAddresses.stakeAddress, provider, this.txDefaults, artifactAbis);
        this.traderContract = new TraderContract(contractAddresses.traderAddress, provider, this.txDefaults);
        this.pauseContract = new PauseContract(contractAddresses.pauseAddress, provider, this.txDefaults);
        this.ddxWalletCloneableContract = new DDXWalletCloneableContract(
            contractAddresses.ddxWalletCloneableAddress,
            provider,
            this.txDefaults,
            artifactAbis,
        );
        this.diFundTokenFactoryContract = new DIFundTokenFactoryContract(
            contractAddresses.diFundTokenFactoryAddress,
            provider,
            this.txDefaults,
            artifactAbis,
        );
        this.ownershipFacetContract = new OwnershipFacetContract(
            contractAddresses.derivaDEXAddress,
            provider,
            this.txDefaults,
            artifactAbis,
        );
        this.diamondFacetContract = new DiamondFacetContract(
            contractAddresses.derivaDEXAddress,
            provider,
            this.txDefaults,
            artifactAbis,
        );

        // Initialize token contract wrappers if valid token addresses are provided.
        if (contractAddresses.ausdtAddress !== '') {
            this.ausdtContract = new AUSDTContract(contractAddresses.ausdtAddress, provider, this.txDefaults);
        }
        if (contractAddresses.cusdtAddress !== '') {
            this.cusdtContract = new CUSDTContract(contractAddresses.cusdtAddress, provider, this.txDefaults);
        }
        if (contractAddresses.usdtAddress !== '') {
            this.usdtContract = new SafeERC20WrapperContract(contractAddresses.usdtAddress, provider, this.txDefaults);
        }
        if (contractAddresses.ausdcAddress !== '') {
            this.ausdcContract = new AUSDCContract(contractAddresses.ausdcAddress, provider, this.txDefaults);
        }
        if (contractAddresses.cusdcAddress !== '') {
            this.cusdcContract = new CUSDCContract(contractAddresses.cusdcAddress, provider, this.txDefaults);
        }
        if (contractAddresses.usdcAddress !== '') {
            this.usdcContract = new SafeERC20WrapperContract(contractAddresses.usdcAddress, provider, this.txDefaults);
        }
        if (contractAddresses.husdAddress !== '') {
            this.husdContract = new SafeERC20WrapperContract(contractAddresses.husdAddress, provider, this.txDefaults);
        }
        if (contractAddresses.gusdAddress !== '') {
            this.gusdContract = new SafeERC20WrapperContract(contractAddresses.gusdAddress, provider, this.txDefaults);
        }
    }

    /**
     * @notice This returns a Web3Wrapper
     * @return Web3Wrapper.
     */
    public getWeb3Wrapper(): Web3Wrapper {
        return new Web3Wrapper(this.providerEngine);
    }

    /**
     * Add/remove/update functions to the Proxy
     * @param facets - Array of bytes arrays of the format
     * @param init - Array of bytes arrays of the format
     * @param calldata - Array of bytes arrays of the format
     * [
     *  abi.encodePacked(facet, sel1, sel2, sel3, ...),
     *  abi.encodePacked(facet, sel1, sel2, sel4, ...),
     *  ...
     * ]
     * facet is the address of a facet
     * sel1, sel2, sel3 etc. are four-byte function selectors
     */
    public diamondCut(facets: Facet[], init: string, calldata: string): ContractTxFunctionObj<void> {
        return this.diamondFacetContract.diamondCut(facets, init, calldata);
    }

    /**
     *  This function transfers ownership to self. This is done
     *  so that we can ensure upgrades (using diamondCut) and
     *  various other critical parameter changing scenarios
     *  can only be done via governance (a facet).
     */
    public transferOwnershipToSelf(): ContractTxFunctionObj<void> {
        return this.ownershipFacetContract.transferOwnershipToSelf();
    }

    /**
     * @notice This gets the admin for the Proxy contract.
     * @return Admin address.
     */
    public async getAdminAsync(): Promise<string> {
        return this.ownershipFacetContract.getAdmin().callAsync();
    }

    // *** CONTRACT WRAPPERS POINTING TO PROXY *** //

    /**
     * Set the address for all DerivaDEX facets to Proxy
     */
    public setFacetAddressesToProxy(): void {
        this.bannerContract = new BannerContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.checkpointContract = new CheckpointContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.custodianContract = new CustodianContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.collateralContract = new CollateralContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.fundedInsuranceFundContract = new FundedInsuranceFundContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.governanceContract = new GovernanceContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.insuranceFundContract = new InsuranceFundContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.pauseContract = new PauseContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.registrationContract = new RegistrationContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.rejectContract = new RejectContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.specsContract = new SpecsContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.stakeContract = new StakeContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
        this.traderContract = new TraderContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Set the address for Banner contract wrapper to Proxy
     */
    public setBannerAddressToProxy(): void {
        this.bannerContract = new BannerContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Sets the address for the Checkpoint contract wrapper to Proxy
     */
    public setCheckpointAddressToProxy(): void {
        this.checkpointContract = new CheckpointContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Sets the address for the Custodian contract wrapper to Proxy
     */
    public setCustodianAddressToProxy(): void {
        this.custodianContract = new CustodianContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Set the address for Collateral contract wrapper to Proxy
     */
    public setCollateralAddressToProxy(): void {
        this.collateralContract = new CollateralContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Set the address for FundedInsuranceFund contract wrapper to Proxy
     */
    public setFundedInsuranceFundAddressToProxy(): void {
        this.fundedInsuranceFundContract = new FundedInsuranceFundContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Set the address for Governance contract wrapper to Proxy
     */
    public setGovernanceAddressToProxy(): void {
        this.governanceContract = new GovernanceContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Set the address for InsuranceFund contract wrapper to Proxy
     */
    public setInsuranceFundAddressToProxy(): void {
        this.insuranceFundContract = new InsuranceFundContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Sets the address for the Registration contract wrapper to Proxy
     */
    public setRegistrationAddressToProxy(): void {
        this.registrationContract = new RegistrationContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Sets the address for the Registration contract wrapper to Proxy
     */
    public setRejectAddressToProxy(): void {
        this.rejectContract = new RejectContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Sets the address for the Specs contract wrapper to Proxy.
     */
    public setSpecsAddressToProxy(): void {
        this.specsContract = new SpecsContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Set the address for Pause contract wrapper to Proxy
     */
    public setPauseAddressToProxy(): void {
        this.pauseContract = new PauseContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Set the address for Stake contract wrapper to Proxy
     */
    public setStakeAddressToProxy(): void {
        this.stakeContract = new StakeContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    /**
     * Set the address for Trader contract wrapper to Proxy
     */
    public setTraderAddressToProxy(): void {
        this.traderContract = new TraderContract(
            this.derivaDEXContract.address,
            this.providerEngine,
            this.txDefaults,
            artifactAbis,
        );
    }

    // *** PAUSE *** //
    /**
     * @notice This function initializes the state with some critical
     *         information. This can only be called once and must be
     *         done via governance and proxy.
     */
    public initializePause(): ContractTxFunctionObj<void> {
        return this.pauseContract.initialize();
    }

    /**
     *  This function transfers ownership to self. This is done
     *  so that we can ensure upgrades (using diamondCut) and
     *  various other critical parameter changing scenarios
     *  can only be done via governance (a facet).
     *  @param isPaused Whether contracts are paused or not.
     */
    public setIsPaused(isPaused: boolean): ContractTxFunctionObj<void> {
        return this.pauseContract.setIsPaused(isPaused);
    }

    // *** GOVERNANCE *** //
    /**
     * @notice This function initializes the state with some critical
     *         information. This can only be called once and must be
     *         done via governance and proxy.
     * @param proposalMaxOperations Max number of operations/actions a
     *        proposal can have
     * @param votingDelay Number of blocks after a proposal is made
     *        that voting begins.
     * @param votingPeriod Number of blocks voting will be held.
     * @param gracePeriod Period in which a successful proposal must be
     *        executed, otherwise will be expired.
     * @param timelockDelay Minimum time in which a successful proposal
     *        must be in the queue before it can be executed.
     * @param quorumVotes Minimum number of for votes required, even
     *        if there's a majority in favor.
     * @param proposalThreshold Minimum DDX token holdings required
     *        to create a proposal
     * @param skipRemainingVotingThreshold Number of for or against
     *        votes that are necessary to skip the remainder of the
     *        voting period.
     */
    public initializeGovernance(
        proposalMaxOperations: number,
        votingDelay: number,
        votingPeriod: number,
        gracePeriod: number,
        timelockDelay: number,
        quorumVotes: number,
        proposalThreshold: number,
        skipRemainingVotingThreshold: number,
    ): ContractTxFunctionObj<void> {
        return this.governanceContract.initialize(
            new BigNumber(proposalMaxOperations),
            new BigNumber(votingDelay),
            new BigNumber(votingPeriod),
            new BigNumber(gracePeriod),
            new BigNumber(timelockDelay),
            new BigNumber(quorumVotes),
            new BigNumber(proposalThreshold),
            new BigNumber(skipRemainingVotingThreshold),
        );
    }

    /**
     * @notice This function allows participants who have sufficient
     *         DDX holdings to create new proposals up for vote. The
     *         proposals contain the ordered lists of on-chain
     *         executable calldata. It can only be called via the proxy.
     * @param targets Addresses of contracts involved.
     * @param values Values to be passed along with the calls.
     * @param signatures Function signatures.
     * @param calldatas Calldata passed to the function.
     * @param description Text description of proposal.
     */
    public propose(
        targets: string[],
        values: number[],
        signatures: string[],
        calldatas: string[],
        description: string,
    ): ContractTxFunctionObj<BigNumber> {
        return this.governanceContract.propose(
            targets,
            values.map((value) => new BigNumber(value)),
            signatures,
            calldatas,
            description,
        );
    }

    /**
     * @notice This function allows participants to cast either in
     *         favor or against a particular proposal. It can only be
     *         called via the proxy.
     * @param proposalId Proposal id.
     * @param support In favor (true) or against (false).
     */
    public castVote(proposalId: number, support: boolean): ContractTxFunctionObj<void> {
        return this.governanceContract.castVote(new BigNumber(proposalId), support);
    }

    /**
     * @notice This function returns state of the proposal.
     * @param proposalId Proposal id.
     * @return Proposal state.
     */
    public async getStateOfProposalAsync(proposalId: number): Promise<ProposalState> {
        return this.governanceContract.state(new BigNumber(proposalId)).callAsync();
    }

    /**
     * Get Long/Short DerivativeToken contract given a token's address
     *
     * @param proposalId - Proposal ID
     * @param support - Support
     */
    public getVoteCastTypedData(proposalId: number, support: boolean): EIP712TypedData {
        const voteCastToHash = {
            proposalId,
            support,
        };
        const normalizedVoteCastToHash = _.mapValues(voteCastToHash, (value) => {
            if (_.isBoolean(value)) {
                return value ? 1 : 0;
            }
            return !_.isString(value) ? value.toString() : value;
        });

        const primaryType = 'VoteCast';
        return eip712Utils.createTypedData(
            primaryType,
            {
                VoteCast: [
                    {
                        name: 'proposalId',
                        type: 'uint128',
                    },
                    {
                        name: 'support',
                        type: 'bool',
                    },
                ],
            },
            normalizedVoteCastToHash,
            {
                name: 'DDX Governance',
                version: '1',
                chainId: this.chainId,
                verifyingContract: this.governanceContract.address.toLowerCase(),
            },
        );
    }

    /**
     * @notice Get the signature for a vote cast intent
     * @param proposalId Proposal ID
     * @param support Support for vote
     * @param signatory Signatory address casting vote
     */
    public async getSignatureForVoteCastAsync(
        proposalId: number,
        support: boolean,
        signatory: string,
    ): Promise<string> {
        const web3Wrapper = new Web3Wrapper(this.providerEngine);
        const eip712TypedData = this.getVoteCastTypedData(proposalId, support);
        const initSignature = await web3Wrapper.signTypedDataAsync(signatory, eip712TypedData);
        const ecSignatureRSV = ContractWrapper._parseSignatureHexAsRSV(initSignature);
        const signatureBuffer = Buffer.concat([
            ethUtil.toBuffer(ecSignatureRSV.v),
            ethUtil.toBuffer(ecSignatureRSV.r),
            ethUtil.toBuffer(ecSignatureRSV.s),
        ]);
        return `0x${signatureBuffer.toString('hex')}`;
    }

    /**
     * @notice This function allows participants to cast votes with
     *         offline signatures in favor or against a particular
     *         proposal.
     * @param proposalId Proposal id.
     * @param support In favor (true) or against (false).
     * @param signature Signature
     */
    public castVoteBySigAsync(proposalId: number, support: boolean, signature: string): ContractTxFunctionObj<void> {
        return this.governanceContract.castVoteBySig(new BigNumber(proposalId), support, signature);
    }

    /**
     * @notice This function allows any participant to retrieve
     *         the receipt for a given proposal and voter.
     * @param proposalId Proposal id.
     * @param voter Voter address.
     * @return Voter receipt.
     */
    public async getReceiptAsync(proposalId: number, voter: string): Promise<VoteReceipt> {
        const voteReceipt = await this.governanceContract.getReceipt(new BigNumber(proposalId), voter).callAsync();
        return {
            hasVoted: voteReceipt.hasVoted,
            support: voteReceipt.support,
            votes: Web3Wrapper.toUnitAmount(voteReceipt.votes, ERC20_DECIMALS),
        };
    }

    /**
     * @notice This function allows any participant to queue a
     *         successful proposal for execution. Proposals are deemed
     *         successful if at any point the number of for votes has
     *         exceeded the skip remaining voting threshold or if there
     *         is a simple majority (and more for votes than the
     *         minimum quorum) at the end of voting. It can only be
     *         called via the proxy.
     * @param proposalId Proposal id.
     */
    public queue(proposalId: number): ContractTxFunctionObj<void> {
        return this.governanceContract.queue(new BigNumber(proposalId));
    }

    /**
     * @notice This function allows any participant to execute a
     *         queued proposal. A proposal in the queue must be in the
     *         queue for the delay period it was proposed with prior to
     *         executing, allowing the community to position itself
     *         accordingly. It can only be called via the proxy.
     * @param proposalId Proposal id.
     */
    public execute(proposalId: number): ContractTxFunctionObj<void> {
        return this.governanceContract.execute(new BigNumber(proposalId));
    }

    /**
     * @notice This function sets the quorum votes required for a
     *         proposal to pass. It must be called via proxy and
     *         governance.
     * @param quorumVotes Quorum votes threshold.
     */
    public setQuorumVotes(quorumVotes: number): ContractTxFunctionObj<void> {
        return this.governanceContract.setQuorumVotes(new BigNumber(quorumVotes));
    }

    /**
     * @notice This function sets the token holdings threshold required
     *         to propose something. It must be called via proxy and
     *         governance.
     * @param proposalThreshold Proposal threshold.
     */
    public setProposalThreshold(proposalThreshold: number): ContractTxFunctionObj<void> {
        return this.governanceContract.setProposalThreshold(new BigNumber(proposalThreshold));
    }

    /**
     * @notice This function sets the max operations a proposal can
     *         carry out. It must be called via proxy and governance.
     * @param proposalMaxOperations Proposal's max operations.
     */
    public setProposalMaxOperations(proposalMaxOperations: number): ContractTxFunctionObj<void> {
        return this.governanceContract.setProposalMaxOperations(new BigNumber(proposalMaxOperations));
    }

    /**
     * @notice This function sets the voting delay in blocks from when
     *         a proposal is made and voting begins. It must be called
     *         via proxy and governance.
     * @param votingDelay Voting delay (blocks).
     */
    public setVotingDelay(votingDelay: number): ContractTxFunctionObj<void> {
        return this.governanceContract.setVotingDelay(new BigNumber(votingDelay));
    }

    /**
     * @notice This function sets the voting period in blocks that a
     *         vote will last. It must be called via proxy and
     *         governance.
     * @param votingPeriod Voting period (blocks).
     */
    public setVotingPeriod(votingPeriod: number): ContractTxFunctionObj<void> {
        return this.governanceContract.setVotingPeriod(new BigNumber(votingPeriod));
    }

    /**
     * @notice This function sets the threshold at which a proposal can
     *         immediately be deemed successful or rejected if the for
     *         or against votes exceeds this threshold, even if the
     *         voting period is still ongoing. It must be called via
     *         proxy and governance.
     * @param skipRemainingVotingThreshold Threshold for or against
     *        votes must reach to skip remainder of voting period.
     */
    public setSkipRemainingVotingThreshold(skipRemainingVotingThreshold: number): ContractTxFunctionObj<void> {
        return this.governanceContract.setSkipRemainingVotingThreshold(new BigNumber(skipRemainingVotingThreshold));
    }

    /**
     * @notice This function sets the grace period in seconds that a
     *         queued proposal can last before expiring. It must be
     *         called via proxy and governance.
     * @param gracePeriod Grace period (seconds).
     */
    public setGracePeriod(gracePeriod: number): ContractTxFunctionObj<void> {
        return this.governanceContract.setGracePeriod(new BigNumber(gracePeriod));
    }

    /**
     * @notice This function sets the timelock delay (s) a proposal
     *         must be queued before execution.
     * @param timelockDelay Timelock delay (seconds).
     */
    public setTimelockDelay(timelockDelay: number): ContractTxFunctionObj<void> {
        return this.governanceContract.setTimelockDelay(new BigNumber(timelockDelay));
    }

    /**
     * @notice This function gets the quorum votes required for a
     *         proposal to pass. It must be called via proxy.
     * @return quorumVotes Quorum votes threshold.
     */
    public async getGovernanceParametersAsync(): Promise<GovernanceParameters> {
        const governanceParameters = await this.governanceContract.getGovernanceParameters().callAsync();
        const [
            proposalMaxOperations,
            votingDelay,
            votingPeriod,
            gracePeriod,
            timelockDelay,
            quorumVotes,
            proposalThreshold,
            skipRemainingVotingThreshold,
        ] = governanceParameters;
        return {
            proposalMaxOperations: new BigNumber(proposalMaxOperations),
            votingDelay: new BigNumber(votingDelay),
            votingPeriod: new BigNumber(votingPeriod),
            gracePeriod: new BigNumber(gracePeriod),
            timelockDelay: new BigNumber(timelockDelay),
            quorumVotes: new BigNumber(quorumVotes),
            proposalThreshold: new BigNumber(proposalThreshold),
            skipRemainingVotingThreshold: new BigNumber(skipRemainingVotingThreshold),
        };
    }

    /**
     * @notice This function gets the quorum vote count.
     * @return Quorum vote count.
     */
    public async getQuorumVoteCountAsync(): Promise<BigNumber> {
        const quorumVoteCount = await this.governanceContract.getQuorumVoteCount().callAsync();
        return Web3Wrapper.toUnitAmount(quorumVoteCount, ERC20_DECIMALS);
    }

    /**
     * @notice This function gets the proposer threshold count.
     * @return Proposer threshold count.
     */
    public async getProposerThresholdCountAsync(): Promise<BigNumber> {
        const proposerThresholdCount = await this.governanceContract.getProposerThresholdCount().callAsync();
        return Web3Wrapper.toUnitAmount(proposerThresholdCount, ERC20_DECIMALS);
    }

    /**
     * @notice This function gets the skip remaining voting threshold count.
     * @return Skip remaining voting threshold count.
     */
    public async getSkipRemainingVotingThresholdCountAsync(): Promise<BigNumber> {
        const skipRemainingVotingThresholdCount = await this.governanceContract
            .getSkipRemainingVotingThresholdCount()
            .callAsync();
        return Web3Wrapper.toUnitAmount(skipRemainingVotingThresholdCount, ERC20_DECIMALS);
    }

    /**
     * @notice This function gets the proposal count.
     * @return Proposal count.
     */
    public async getProposalCountAsync(): Promise<BigNumber> {
        return this.governanceContract.getProposalCount().callAsync();
    }

    /**
     * @notice This function gets the proposal count.
     * @return Proposal count.
     */
    public async getProposalAsync(proposalId: number): Promise<Proposal> {
        const [canceled, executed, proposer, delay, forVotes, againstVotes, id, eta, startBlock, endBlock] =
            await this.governanceContract.getProposal(new BigNumber(proposalId)).callAsync();
        return {
            canceled,
            executed,
            proposer,
            delay: new BigNumber(delay),
            forVotes: Web3Wrapper.toUnitAmount(forVotes, ERC20_DECIMALS),
            againstVotes: Web3Wrapper.toUnitAmount(againstVotes, ERC20_DECIMALS),
            id,
            eta,
            startBlock,
            endBlock,
        };
    }

    /**
     * @notice This function gets the latest proposal ID for a user.
     * @param proposer Proposer's address.
     * @return Proposal ID.
     */
    public async getLatestProposalIdAsync(proposer: string): Promise<BigNumber> {
        return this.governanceContract.getLatestProposalId(proposer).callAsync();
    }

    // *** INSURANCE MINING *** //
    /**
     * @notice This function initializes the state with some critical
     *         information, including the number of DDX tokens
     *         mined each interval, the mining interval, and the block
     *         this contract was deployed at. This can only be called
     *         once and must be done via governance.
     * @param interval The insurance mining interval.
     * @param withdrawalFactor The insurance mining withdrawal factor.
     * @param mineRatePerBlock The DDX tokens to be mined per block.
     * @param advanceIntervalReward Reward for advancing interval.
     * @param insuranceMiningLength Length of mining (blocks).
     * @param diFundTokenFactory Reward for advancing interval.
     */
    public initializeInsuranceFund(
        interval: number,
        withdrawalFactor: number,
        mineRatePerBlock: number,
        advanceIntervalReward: number,
        insuranceMiningLength: number,
        diFundTokenFactory: string,
    ): ContractTxFunctionObj<void> {
        return this.insuranceFundContract.initialize(
            new BigNumber(interval),
            new BigNumber(withdrawalFactor),
            Web3Wrapper.toBaseUnitAmount(mineRatePerBlock, ERC20_DECIMALS),
            Web3Wrapper.toBaseUnitAmount(advanceIntervalReward, ERC20_DECIMALS),
            new BigNumber(insuranceMiningLength),
            diFundTokenFactory,
        );
    }

    /**
     * @notice This function adds a new supported collateral type that
     *         can be staked to the insurance fund. It can only
     *         be called via governance.
     * @param collateralName Name of collateral.
     * @param collateralSymbol Name of collateral.
     * @param underlyingToken Deployed address of collateral token.
     * @param collateralToken Deployed address of collateral token.
     * @param flavor Collateral flavor (Vanilla, Compound, Aave, etc.).
     */
    public addInsuranceFundCollateral(
        collateralName: string,
        collateralSymbol: string,
        underlyingToken: string,
        collateralToken: string,
        flavor: number,
    ): ContractTxFunctionObj<void> {
        return this.insuranceFundContract.addInsuranceFundCollateral(
            collateralName,
            collateralSymbol,
            underlyingToken,
            collateralToken,
            new BigNumber(flavor),
        );
    }

    /**
     * Approve the insurance fund contract to transfer collateral.
     *
     * @param collateralAddress - Collateral address type.
     */
    public approveForStakeToInsuranceFund(collateralAddress: string): ContractTxFunctionObj<void> {
        const safeERC20WrapperContract = new SafeERC20WrapperContract(
            collateralAddress,
            this.providerEngine,
            this.txDefaults,
        );
        return safeERC20WrapperContract.approve(this.insuranceFundContract.address, UNLIMITED_ALLOWANCE);
    }

    /**
     * Lock the insurance fund contract to transfer collateral.
     * @param collateralAddress - Collateral address type.
     */
    public lockStakeToInsuranceFund(collateralAddress: string): ContractTxFunctionObj<void> {
        const safeERC20WrapperContract = new SafeERC20WrapperContract(
            collateralAddress,
            this.providerEngine,
            this.txDefaults,
        );
        return safeERC20WrapperContract.approve(this.insuranceFundContract.address, new BigNumber(0));
    }

    /**
     * Get whether the insurance fund contract is locked to transfer
     * collateral.
     * @param collateralAddress - Collateral address type.
     * @param staker - Staking address.
     */
    public async getLockStatusForStakeToInsuranceFundAsync(
        collateralAddress: string,
        staker: string,
    ): Promise<boolean> {
        const safeERC20WrapperContract = new SafeERC20WrapperContract(
            collateralAddress,
            this.providerEngine,
            this.txDefaults,
        );
        const locked = await safeERC20WrapperContract.allowance(staker, this.insuranceFundContract.address).callAsync();
        return locked.gt(0);
    }

    /**
     * Get whether the insurance fund contract is locked to transfer
     * collateral.
     * @param collateralName - Collateral address type.
     */
    public getDecimalsForCollateralType(collateralName = ''): number {
        if (
            collateralName === 'DerivaDEX Insurance cUSDT' ||
            collateralName === 'DerivaDEX Insurance cUSDC' ||
            collateralName === 'DerivaDEX Insurance HUSD'
        ) {
            return 8;
        } else if (collateralName === 'DerivaDEX Insurance GUSD') {
            return 2;
        }
        return 6;
    }

    /**
     * @notice This function allows participants to stake a supported
     *         collateral type to the insurance fund. It can
     *         only be called via the proxy.
     * @param collateralName Name of collateral.
     * @param amount Amount to stake.
     */
    public stakeToInsuranceFund(collateralName: string, amount: number): ContractTxFunctionObj<void> {
        return this.insuranceFundContract.stakeToInsuranceFund(
            ethers.utils.formatBytes32String(collateralName),
            Web3Wrapper.toBaseUnitAmount(amount, this.getDecimalsForCollateralType(collateralName)),
        );
    }

    /**
     * @notice This function allows participants to withdraw a supported
     * collateral type from the insurance fund. It can
     * only be called via the proxy.
     * @param collateralName Name of collateral.
     * @param amount Amount to stake.
     */
    public withdrawFromInsuranceFund(collateralName: string, amount: number): ContractTxFunctionObj<void> {
        return this.insuranceFundContract.withdrawFromInsuranceFund(
            ethers.utils.formatBytes32String(collateralName),
            Web3Wrapper.toBaseUnitAmount(amount, this.getDecimalsForCollateralType(collateralName)),
        );
    }

    /**
     * @notice This function gets some high level insurance mining
     *         details.
     * @return Total global insurance mined amount in DDX.
     * @return Last insurance mined block.
     * @return Current insurance mine interval.
     * @return Current insurance mine rate per interval.
     * @return Insurance mine interval length (blocks).
     */
    public async getInsuranceMineInfoAsync(): Promise<InsuranceMineInfo> {
        const data = await this.insuranceFundContract.getInsuranceMineInfo().callAsync();
        const [
            interval,
            withdrawalFactor,
            advanceIntervalReward,
            minedAmount,
            mineRatePerBlock,
            miningFinalBlockNumber,
            ddxMarketState,
            collateralNames,
        ] = data;
        return {
            interval: new BigNumber(interval),
            withdrawalFactor: new BigNumber(withdrawalFactor),
            advanceIntervalReward: Web3Wrapper.toUnitAmount(advanceIntervalReward, ERC20_DECIMALS),
            minedAmount: Web3Wrapper.toUnitAmount(minedAmount, ERC20_DECIMALS),
            mineRatePerBlock: Web3Wrapper.toUnitAmount(mineRatePerBlock, ERC20_DECIMALS),
            ddxMarketStateIndex: ddxMarketState.index,
            miningFinalBlockNumber: miningFinalBlockNumber,
            ddxMarketStateBlock: new BigNumber(ddxMarketState.block),
            collateralNames: collateralNames.map((collateralName) => {
                return ethers.utils.parseBytes32String(collateralName);
            }),
        };
    }

    /**
     * @notice This function gets the DDX claimant state.
     * @param claimant Claimant's address.
     * @return Claimant state.
     */
    public async getDDXClaimantStateAsync(claimant: string): Promise<DDXClaimantState> {
        const { index, claimedDDX } = await this.insuranceFundContract.getDDXClaimantState(claimant).callAsync();
        return {
            index,
            claimedDDX: Web3Wrapper.toUnitAmount(claimedDDX, ERC20_DECIMALS),
        };
    }

    /**
     * @notice This function gets a supported collateral.
     * @param collateralName Name of collateral.
     * @return Stake collateral.
     */
    public async getStakeCollateralByCollateralNameAsync(collateralName: string): Promise<StakeCollateral> {
        const { underlyingToken, collateralToken, diFundToken, cap, withdrawalFeeCap, flavor } =
            await this.insuranceFundContract
                .getStakeCollateralByCollateralName(ethers.utils.formatBytes32String(collateralName))
                .callAsync();
        return {
            underlyingToken,
            collateralToken,
            diFundToken,
            cap: Web3Wrapper.toUnitAmount(cap, this.getDecimalsForCollateralType(collateralName)),
            withdrawalFeeCap: Web3Wrapper.toUnitAmount(
                withdrawalFeeCap,
                this.getDecimalsForCollateralType(collateralName),
            ),
            flavor,
        };
    }

    /**
     * @notice This function gets unclaimed DDX rewards for a claimant.
     * @param claimant Claimant address.
     * @return Unclaimed DDX rewards.
     */
    public async getUnclaimedDDXRewardsAsync(claimant: string): Promise<BigNumber> {
        const unclaimedDDXRewards = await this.insuranceFundContract.getUnclaimedDDXRewards(claimant).callAsync();
        return Web3Wrapper.toUnitAmount(unclaimedDDXRewards, ERC20_DECIMALS);
    }

    /**
     * @notice This function gets unclaimed DDX rewards for a claimant.
     * @param claimant Claimant address.
     * @param stakesTotal Claimant address.
     * @param stakesTrader Claimant address.
     * @param current Claimant address.
     * @return Unclaimed DDX rewards.
     */
    public async getUnclaimedDDXRewardsLocalAsync(
        claimant: string,
        stakesTotal: BigNumber,
        stakesTrader: BigNumber,
        current = true,
    ): Promise<BigNumber> {
        const insuranceMineInfo = await this.getInsuranceMineInfoAsync();
        const deltaBlocks = current
            ? new BigNumber(await this.getWeb3Wrapper().getBlockNumberAsync()).minus(
                  insuranceMineInfo.ddxMarketStateBlock,
              )
            : new BigNumber((await this.getWeb3Wrapper().getBlockNumberAsync()) + 1).minus(
                  insuranceMineInfo.ddxMarketStateBlock,
              );
        let index = insuranceMineInfo.ddxMarketStateIndex;
        if (deltaBlocks.gt(0) && insuranceMineInfo.mineRatePerBlock.gt(0)) {
            const ddxAccrued = deltaBlocks.multipliedBy(
                Web3Wrapper.toBaseUnitAmount(insuranceMineInfo.mineRatePerBlock, ERC20_DECIMALS),
            );
            const ratio = stakesTotal.gt(0) ? ddxAccrued.multipliedBy(1e36).div(stakesTotal) : new BigNumber(0);
            index = index.plus(ratio);
        }
        const ddxClaimantState = await this.getDDXClaimantStateAsync(claimant);
        if (ddxClaimantState.index.eq(0) && index.gt(0)) {
            ddxClaimantState.index = new BigNumber(1e36);
        }
        const deltaIndex = index.minus(ddxClaimantState.index);
        return Web3Wrapper.toUnitAmount(
            new BigNumber(stakesTrader).multipliedBy(deltaIndex).div(1e36).dp(0, 1),
            ERC20_DECIMALS,
        );
    }

    /**
     * @notice This function gets a participant's current stake and
     *         weighted stake amounts across all collateral types, as
     *         well as the global cap and weighted cap across all
     *         collateral types.
     * @param staker Participant's address.
     * @return Current amount staked.
     * @return Current global cap.
     */
    public async getCurrentTotalStakesAsync(staker: string): Promise<CurrentTotalStakes> {
        const data = await this.insuranceFundContract.getCurrentTotalStakes(staker).callAsync();
        const [normalizedStakerStakeSum, normalizedGlobalCapSum] = data;
        return {
            normalizedStakerStakeSum: Web3Wrapper.toUnitAmount(
                normalizedStakerStakeSum,
                this.getDecimalsForCollateralType(),
            ),
            normalizedGlobalCapSum: Web3Wrapper.toUnitAmount(
                normalizedGlobalCapSum,
                this.getDecimalsForCollateralType(),
            ),
        };
    }

    /**
     * @notice This function gets a participant's current stake and
     *         weighted stake amounts across all collateral types, as
     *         well as the global cap and weighted cap across all
     *         collateral types.
     * @param collateralName Participant's address.
     * @param staker Participant's address.
     * @return Current amount staked.
     * @return Current global cap.
     */
    public async getCurrentStakeByCollateralNameAndStakerAsync(
        collateralName: string,
        staker: string,
    ): Promise<CurrentStakeByCollateralNameAndStaker> {
        const [stakerStake, globalCap, normalizedStakerStake, normalizedGlobalCap] = await this.insuranceFundContract
            .getCurrentStakeByCollateralNameAndStaker(ethers.utils.formatBytes32String(collateralName), staker)
            .callAsync();
        return {
            stakerStake: Web3Wrapper.toUnitAmount(stakerStake, this.getDecimalsForCollateralType(collateralName)),
            globalCap: Web3Wrapper.toUnitAmount(globalCap, this.getDecimalsForCollateralType(collateralName)),
            normalizedStakerStake: Web3Wrapper.toUnitAmount(normalizedStakerStake, this.getDecimalsForCollateralType()),
            normalizedGlobalCap: Web3Wrapper.toUnitAmount(normalizedGlobalCap, this.getDecimalsForCollateralType()),
        };
    }

    /**
     * @notice This function allows participants to claim the DDX
     *         insurance mining reward they are entitled to. It can only
     *         be called via the proxy.
     */
    public advanceOtherRewardsInterval(): ContractTxFunctionObj<void> {
        return this.insuranceFundContract.advanceOtherRewardsInterval();
    }

    /**
     * @notice This function allows participants to claim the DDX
     *         insurance mining reward they are entitled to. It can only
     *         be called via the proxy.
     * @param claimant Claimant's address
     */
    public claimDDXFromInsuranceMining(claimant: string): ContractTxFunctionObj<void> {
        return this.insuranceFundContract.claimDDXFromInsuranceMining(claimant);
    }

    // *** TRADER *** //
    /**
     * @notice This function issues DDX rewards to a trader. It can
     *         only be called via another facet/governance.
     * @param ddxWalletCloneable The DDX tokens to be rewarded.
     */
    public initializeTrader(ddxWalletCloneable: string): ContractTxFunctionObj<void> {
        return this.traderContract.initialize(ddxWalletCloneable);
    }

    /**
     * @notice This function lets traders take DDX from their wallet
     *         into their on-chain DDX wallet. It's important to note
     *         that any DDX staked from the trader to this wallet
     *         delegates the voting rights of that stake back to the
     *         user. To be more explicit, if Alice's personal wallet is
     *         delegating to Bob, and she now stakes a portion of her
     *         DDX into this on-chain DDX wallet of hers, those tokens
     *         will now count towards her voting power, not Bob's, since
     *         her on-chain wallet is automatically delegating back to
     *         her.
     * @param amount The DDX tokens to be staked.
     */
    public stakeDDXFromTrader(amount: number): ContractTxFunctionObj<void> {
        return this.traderContract.stakeDDXFromTrader(Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * @notice This function lets traders send DDX from their wallet
     *         into another trader's on-chain DDX wallet. It's
     *         important to note that any DDX staked to this wallet
     *         delegates the voting rights of that stake back to the
     *         user.
     * @param trader Trader address to receive DDX (inside their
     *        wallet, which will be created if it does not already
     *        exist).
     * @param amount The DDX tokens to be staked.
     */
    public sendDDXFromTraderToTraderWallet(trader: string, amount: number): ContractTxFunctionObj<void> {
        return this.traderContract.sendDDXFromTraderToTraderWallet(
            trader,
            Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS),
        );
    }

    /**
     * @notice This function lets traders withdraw DDX from their
     *         on-chain DDX wallet to their personal wallet. It's
     *         important to note that the voting rights for any DDX
     *         withdrawn are returned back to the delegatee of the
     *         user's personal wallet. To be more explicit, if Alice is
     *         personal wallet is delegating to Bob, and she now
     *         withdraws a portion of her DDX from this on-chain DDX
     *         wallet of hers, those tokens will now count towards Bob's
     *         voting power, not her's, since her on-chain wallet is
     *         automatically delegating back to her, but her personal
     *         wallet is delegating to Bob.
     * @param amount The DDX tokens to be withdrawn.
     */
    public withdrawDDXToTrader(amount: number | BigNumber): ContractTxFunctionObj<void> {
        return this.traderContract.withdrawDDXToTrader(Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * @notice This function sets the reward cliff.
     * @param rewardCliff Reward cliff.
     */
    public setRewardCliff(rewardCliff: boolean): ContractTxFunctionObj<void> {
        return this.traderContract.setRewardCliff(rewardCliff);
    }

    /**
     * @notice This function gets the attributes for a given trader.
     * @param trader Trader address.
     */
    public async getTraderAsync(trader: string): Promise<TraderAttributes> {
        const { ddxBalance, ddxWalletContract } = await this.traderContract.getTrader(trader).callAsync();
        return {
            ddxBalance: Web3Wrapper.toUnitAmount(ddxBalance, ERC20_DECIMALS),
            ddxWalletContract,
        };
    }

    // *** DDX TOKEN *** //
    /**
     * @notice Get the number of tokens held by the `account`
     * @param account The address of the account to get the balance of
     * @return The number of tokens held
     */
    public async getBalanceOfDDXAsync(account: string): Promise<BigNumber> {
        const balance = await this.ddxContract.balanceOf(account).callAsync();
        return Web3Wrapper.toUnitAmount(balance, ERC20_DECIMALS);
    }

    /**
     * @notice Gets the current votes balance.
     * @param account The address to get votes balance.
     * @return The number of current votes.
     */
    public async getCurrentVotesAsync(account: string): Promise<BigNumber> {
        const currentVotes = await this.ddxContract.getCurrentVotes(account).callAsync();
        return Web3Wrapper.toUnitAmount(currentVotes, ERC20_DECIMALS);
    }

    /**
     * @notice Gets the delegatee for an address.
     * @param account The delegator address.
     * @return The delegatee address.
     */
    public async getDelegatee(account: string): Promise<string> {
        const delegatee = await this.ddxContract.delegates(account).callAsync();
        return delegatee === ZERO_ADDRESS ? account : delegatee;
    }

    /**
     * @notice Determine the prior number of votes for an account as of a block number
     * @dev Block number must be a finalized block or else this function will revert to prevent misinformation.
     * @param account The address of the account to check
     * @param blockNumber The block number to get the vote balance at
     * @return The number of votes the account had as of the given block
     */
    public async getPriorVotesDDXAsync(account: string, blockNumber: number): Promise<BigNumber> {
        const priorVotes = await this.ddxContract.getPriorVotes(account, new BigNumber(blockNumber)).callAsync();
        return Web3Wrapper.toUnitAmount(priorVotes, ERC20_DECIMALS);
    }

    /**
     * @notice Delegate votes from `msg.sender` to `delegatee`
     * @param delegatee The address to delegate votes to
     */
    public delegateDDX(delegatee: string): ContractTxFunctionObj<void> {
        return this.ddxContract.delegate(delegatee);
    }

    /**
     * @notice Transfer `amount` tokens from `msg.sender` to `dst`
     * @param recipient The address of the destination account
     * @param amount The number of tokens to transfer
     * @return Whether or not the transfer succeeded
     */
    public transferDDX(recipient: string, amount: number): ContractTxFunctionObj<boolean> {
        return this.ddxContract.transfer(recipient, Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * @notice Transfer `amount` tokens from `src` to `dst`
     * @param sender The address of the source account
     * @param recipient The address of the destination account
     * @param amount The number of tokens to transfer
     * @return Whether or not the transfer succeeded
     */
    public transferFromDDX(sender: string, recipient: string, amount: number): ContractTxFunctionObj<boolean> {
        return this.ddxContract.transferFrom(sender, recipient, Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * @notice Approve `spender` to transfer up to `amount` from `src`
     * @dev This will overwrite the approval amount for `spender`
     *  and is subject to issues noted [here](https://eips.ethereum.org/EIPS/eip-20#approve)
     * @param spender The address of the account which may transfer tokens
     * @param amount The number of tokens that are approved (2^256-1 means infinite)
     * @return Whether or not the approval succeeded
     */
    public approveDDX(spender: string, amount: number): ContractTxFunctionObj<boolean> {
        return this.ddxContract.approve(spender, Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * @notice Approve `spender` to transfer up to `amount` from `src`
     * @dev This will overwrite the approval amount for `spender`
     *  and is subject to issues noted [here](https://eips.ethereum.org/EIPS/eip-20#approve)
     * @param spender The address of the account which may transfer tokens
     * @return Whether or not the approval succeeded
     */
    public approveUnlimitedDDX(spender: string): ContractTxFunctionObj<boolean> {
        return this.ddxContract.approve(spender, UNLIMITED_ALLOWANCE_96);
    }

    /**
     * @notice Get the number of tokens `spender` is approved to spend on behalf of `account`
     * @param account The address of the account holding the funds
     * @param spender The address of the account spending the funds
     * @return The number of tokens approved
     */
    public async getAllowanceDDXAsync(account: string, spender: string): Promise<BigNumber> {
        const allowance = await this.ddxContract.allowance(account, spender).callAsync();
        return Web3Wrapper.toUnitAmount(allowance, ERC20_DECIMALS);
    }

    /**
     * @notice Get the current issuer of DDX tokens
     * @return The issuer's address
     */
    public async getIssuerDDXAsync(): Promise<string> {
        return this.ddxContract.issuer().callAsync();
    }

    /**
     * @notice This function transfers ownership to self. This is done
     *         so that we can ensure upgrades (using diamondCut) and
     *         various other critical parameter changing scenarios
     *         can only be done via governance (a facet).
     * @param derivaDEXProxy Address of DerivaDEX proxy contract.
     */
    public transferOwnershipToDerivaDEXProxyDDX(derivaDEXProxy: string): ContractTxFunctionObj<void> {
        return this.ddxContract.transferOwnershipToDerivaDEXProxy(derivaDEXProxy);
    }

    /**
     * @notice Creates `amount` tokens and assigns them to `account`, increasing
     *         the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements
     *
     * - `to` cannot be the zero address.
     */
    public mintDDX(recipient: string, amount: number): ContractTxFunctionObj<void> {
        return this.ddxContract.mint(recipient, Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * @dev Burns `amount` tokens and assigns them to `account`, decreasing
     *      the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements
     *
     * - `to` cannot be the zero address.
     */
    public burnDDX(amount: number): ContractTxFunctionObj<void> {
        return this.ddxContract.burn(Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * @notice Creates `amount` tokens and assigns them to `account`, increasing
     *         the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements
     *
     * - `to` cannot be the zero address.
     */
    public burnFromDDX(account: string, amount: number): ContractTxFunctionObj<void> {
        return this.ddxContract.burnFrom(account, Web3Wrapper.toBaseUnitAmount(amount, ERC20_DECIMALS));
    }

    /**
     * @notice Get the supply information.
     * @return The issued and circulating supplies.
     */
    public async getSupplyInfoDDXAsync(): Promise<DDXSupplyInfo> {
        const issuedSupply = Web3Wrapper.toUnitAmount(
            await this.ddxContract.issuedSupply().callAsync(),
            ERC20_DECIMALS,
        );
        const totalSupply = Web3Wrapper.toUnitAmount(await this.ddxContract.totalSupply().callAsync(), ERC20_DECIMALS);
        return {
            issuedSupply,
            totalSupply,
        };
    }

    /**
     * Get Delegate typed data
     *
     * @param delegatee - Delegatee address
     * @param nonce - Nonce value
     * @param expiry - Expiry time (s)
     */
    public getDelegateTypedData(delegatee: string, nonce: number, expiry: number): EIP712TypedData {
        const delegationToHash = {
            delegatee,
            nonce,
            expiry,
        };
        const normalizedDelegationToHash = _.mapValues(delegationToHash, (value) => {
            return !_.isString(value) ? value.toString() : value;
        });

        const primaryType = 'Delegation';
        return eip712Utils.createTypedData(
            primaryType,
            {
                Delegation: [
                    {
                        name: 'delegatee',
                        type: 'address',
                    },
                    {
                        name: 'nonce',
                        type: 'uint256',
                    },
                    {
                        name: 'expiry',
                        type: 'uint256',
                    },
                ],
            },
            normalizedDelegationToHash,
            {
                name: 'DerivaDAO',
                version: '1',
                chainId: this.chainId,
                verifyingContract: this.ddxContract.address.toLowerCase(),
            },
        );
    }

    /**
     * @notice Get the signature for a delegation intent
     * @param delegatee The address to delegate votes to
     * @param nonce The contract state required to match the signature
     * @param expiry The time at which to expire the signature
     * @param signatory Signatory address delegating votes
     */
    public async getSignatureForDelegationAsync(
        delegatee: string,
        nonce: number,
        expiry: number,
        signatory: string,
    ): Promise<string> {
        const web3Wrapper = new Web3Wrapper(this.providerEngine);
        const eip712TypedData = this.getDelegateTypedData(delegatee, nonce, expiry);
        const initSignature = await web3Wrapper.signTypedDataAsync(signatory, eip712TypedData);
        const ecSignatureRSV = ContractWrapper._parseSignatureHexAsRSV(initSignature);
        const signatureBuffer = Buffer.concat([
            ethUtil.toBuffer(ecSignatureRSV.v),
            ethUtil.toBuffer(ecSignatureRSV.r),
            ethUtil.toBuffer(ecSignatureRSV.s),
        ]);
        return `0x${signatureBuffer.toString('hex')}`;
    }

    /**
     * @notice Delegates votes from signatory to `delegatee`
     * @param delegatee The address to delegate votes to
     * @param nonce The contract state required to match the signature
     * @param expiry The time at which to expire the signature
     * @param signature Signature
     */
    public delegateBySigDDX(
        delegatee: string,
        nonce: number,
        expiry: number,
        signature: string,
    ): ContractTxFunctionObj<void> {
        return this.ddxContract.delegateBySig(delegatee, new BigNumber(nonce), new BigNumber(expiry), signature);
    }

    /**
     * Get Permit typed data
     *
     * @param spender - Spender address
     * @param value - Value value
     * @param nonce - Nonce value
     * @param expiry - Expiry time (s)
     */
    public getPermitTypedData(spender: string, value: BigNumber, nonce: number, expiry: number): EIP712TypedData {
        const permitToHash = {
            spender,
            value,
            nonce,
            expiry,
        };
        const normalizedPermitToHash = _.mapValues(permitToHash, (value) => {
            return !_.isString(value) ? value.toString() : value;
        });

        const primaryType = 'Permit';
        return eip712Utils.createTypedData(
            primaryType,
            {
                Permit: [
                    {
                        name: 'spender',
                        type: 'address',
                    },
                    {
                        name: 'value',
                        type: 'uint256',
                    },
                    {
                        name: 'nonce',
                        type: 'uint256',
                    },
                    {
                        name: 'expiry',
                        type: 'uint256',
                    },
                ],
            },
            normalizedPermitToHash,
            {
                name: 'DerivaDAO',
                version: '1',
                chainId: this.chainId,
                verifyingContract: this.ddxContract.address.toLowerCase(),
            },
        );
    }

    /**
     * @notice Get the signature for a permit intent
     * @param spender The address spender
     * @param value The value being approved
     * @param nonce The contract state required to match the signature
     * @param expiry The time at which to expire the signature
     * @param signatory Signatory address permitting allowance
     */
    public async getSignatureForPermitAsync(
        spender: string,
        value: number,
        nonce: number,
        expiry: number,
        signatory: string,
    ): Promise<string> {
        const web3Wrapper = new Web3Wrapper(this.providerEngine);
        const eip712TypedData = this.getPermitTypedData(
            spender,
            Web3Wrapper.toBaseUnitAmount(value, ERC20_DECIMALS),
            nonce,
            expiry,
        );
        const initSignature = await web3Wrapper.signTypedDataAsync(signatory, eip712TypedData);
        const ecSignatureRSV = ContractWrapper._parseSignatureHexAsRSV(initSignature);
        const signatureBuffer = Buffer.concat([
            ethUtil.toBuffer(ecSignatureRSV.v),
            ethUtil.toBuffer(ecSignatureRSV.r),
            ethUtil.toBuffer(ecSignatureRSV.s),
        ]);
        return `0x${signatureBuffer.toString('hex')}`;
    }

    /**
     * @notice Permits allowance votes from signatory to `spender`
     * @param spender The spender being approved
     * @param value The value being approved
     * @param nonce The contract state required to match the signature
     * @param expiry The time at which to expire the signature
     * @param signature Signature
     */
    public permitDDX(
        spender: string,
        value: number,
        nonce: number,
        expiry: number,
        signature: string,
    ): ContractTxFunctionObj<void> {
        return this.ddxContract.permit(
            spender,
            Web3Wrapper.toBaseUnitAmount(value, ERC20_DECIMALS),
            new BigNumber(nonce),
            new BigNumber(expiry),
            signature,
        );
    }

    // *** MISCELLANEOUS *** //
    /**
     * Approve the insurance fund contract to transfer collateral.
     *
     * @param collateralAddress - Collateral address type.
     * @param account - Account balance to retrieve.
     */
    public async getBalanceOfSupportedCollateralTypeAsync(
        collateralAddress: string,
        account: string,
    ): Promise<BigNumber> {
        const safeERC20WrapperContract = new SafeERC20WrapperContract(
            collateralAddress,
            this.providerEngine,
            this.txDefaults,
        );
        return safeERC20WrapperContract.balanceOf(account).callAsync();
    }

    // *** COMPOUND *** //
    /**
     * @notice Get the number of tokens held by the `account`
     * @param account The address of the account to get the balance of
     * @return The number of tokens held
     */
    public async getBalanceOfCOMPAsync(account: string): Promise<BigNumber> {
        const compContract = new SafeERC20WrapperContract(
            '0xc00e94cb662c3520282e6f5717214004a7f26888',
            this.providerEngine,
            this.txDefaults,
        );
        const balance = await compContract.balanceOf(account).callAsync();
        return Web3Wrapper.toUnitAmount(balance, ERC20_DECIMALS);
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param account The address of the account to get the balance of
     * @param cToken CToken address
     * @return The number of COMP accrued
     */
    public async getCOMPOwedAsync(account: string, cToken: string): Promise<BigNumber> {
        const web3Wrapper = new Web3Wrapper(this.providerEngine);
        const iComptrollerContract = new IComptrollerContract(
            '0x3d9819210a31b4961b30ef54be2aed79b9c9cd3b',
            this.providerEngine,
            this.txDefaults,
        );
        const cusdtContract = new CUSDTContract(cToken, this.providerEngine, this.txDefaults);
        const supplyState = await iComptrollerContract.compSupplyState(cToken).callAsync();
        const supplySpeed = await iComptrollerContract.compSpeeds(cToken).callAsync();
        const blockNumber = (await web3Wrapper.getBlockNumberAsync()) + 1;
        const deltaBlocks = new BigNumber(blockNumber).minus(supplyState[1]);
        const supplyTokens = await cusdtContract.totalSupply().callAsync();
        const compAccrued = deltaBlocks.multipliedBy(supplySpeed);
        const ratio = compAccrued.multipliedBy(1e36).dividedBy(supplyTokens);
        const index = supplyState[0].plus(ratio);

        const supplierIndex = await iComptrollerContract.compSupplierIndex(cToken, account).callAsync();
        const deltaIndex = index.minus(supplierIndex);
        const supplierTokens = await cusdtContract.balanceOf(account).callAsync();
        const supplierDelta = supplierTokens.multipliedBy(deltaIndex);

        return (await iComptrollerContract.compAccrued(account).callAsync()).plus(supplierDelta).dividedBy(1e54);
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param account The address of the account to get the balance of
     * @return The number of COMP accrued
     */
    public async getCOMPBalanceAsync(account: string): Promise<BigNumber> {
        const erc20TokenContract = new ERC20TokenContract(
            '0xc00e94Cb662C3520282E6f5717214004A7f26888',
            this.providerEngine,
            this.txDefaults,
        );
        return erc20TokenContract.balanceOf(account).callAsync();
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param cToken CToken address
     * @param amount Amount
     * @return The number of COMP accrued
     */
    public async getUSDTTokensFromCUSDTAsync(cToken: string, amount: number): Promise<BigNumber> {
        const cusdtContract = new CUSDTContract(cToken, this.providerEngine, this.txDefaults);
        const exchangeRate = await cusdtContract.exchangeRateStored().callAsync();
        return exchangeRate.times(amount).dividedBy(1e18);
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param cToken CToken address
     * @param amount Amount
     * @return The number of COMP accrued
     */
    public async getUSDTFromCUSDTAsync(cToken: string, amount: number): Promise<BigNumber> {
        const cusdtContract = new CUSDTContract(cToken, this.providerEngine, this.txDefaults);
        const exchangeRate = await cusdtContract.exchangeRateStored().callAsync();
        return exchangeRate.times(amount).dividedBy(1e16);
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param cToken CToken address
     * @param amount Amount
     * @return The number of COMP accrued
     */
    public async getCUSDTFromUSDTAsync(cToken: string, amount: number): Promise<BigNumber> {
        const cusdtContract = new CUSDTContract(cToken, this.providerEngine, this.txDefaults);
        const exchangeRate = await cusdtContract.exchangeRateStored().callAsync();
        return new BigNumber(amount).times(1e16).div(exchangeRate);
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param cToken CToken address
     * @param amount Amount
     * @return The number of COMP accrued
     */
    public async getUSDCFromCUSDCAsync(cToken: string, amount: number): Promise<BigNumber> {
        const cusdcContract = new CUSDCContract(cToken, this.providerEngine, this.txDefaults);
        const exchangeRate = await cusdcContract.exchangeRateStored().callAsync();
        return exchangeRate.times(amount).dividedBy(1e16);
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param cToken CToken address
     * @param amount Amount
     * @return The number of COMP accrued
     */
    public async getCUSDCFromUSDCAsync(cToken: string, amount: number): Promise<BigNumber> {
        const cusdcContract = new CUSDCContract(cToken, this.providerEngine, this.txDefaults);
        const exchangeRate = await cusdcContract.exchangeRateStored().callAsync();
        return new BigNumber(amount).times(1e16).div(exchangeRate);
    }

    /**
     * @notice Claim COMP.
     * @param account The address to claim COMP for.
     */
    public claimComp(account: string): ContractTxFunctionObj<void> {
        const iComptrollerContract = new IComptrollerContract(
            '0x3d9819210a31b4961b30ef54be2aed79b9c9cd3b',
            this.providerEngine,
            this.txDefaults,
        );
        return iComptrollerContract.claimComp(account);
    }

    // *** AAVE *** //
    /**
     * @notice Get the number of COMP tokens accrued.
     * @param account The address of the account to get the balance of
     * @param aToken AToken address
     * @return The number of COMP accrued
     */
    public async getBalanceOfATokenAsync(account: string, aToken: string): Promise<BigNumber> {
        const ausdtContract = new AUSDTContract(aToken, this.providerEngine, this.txDefaults);
        return Web3Wrapper.toUnitAmount(await ausdtContract.balanceOf(account).callAsync(), 6);
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param account The address of the account to get the balance of
     * @param cToken AToken address
     * @return The number of COMP accrued
     */
    public async getBalanceOfCTokenAsync(account: string, cToken: string): Promise<BigNumber> {
        const cusdtContract = new CUSDTContract(cToken, this.providerEngine, this.txDefaults);
        return Web3Wrapper.toUnitAmount(await cusdtContract.balanceOf(account).callAsync(), 6);
    }

    // *** DIFUNDTOKEN *** //
    /**
     * @notice Get the number of COMP tokens accrued.
     * @param collateralName The collateral name.
     * @param account AToken address
     * @param blockNumber AToken address
     * @return The number of COMP accrued
     */
    public async getPriorValuesDIFundTokenAsync(
        collateralName: string,
        account: string,
        blockNumber: number,
    ): Promise<BigNumber> {
        const diFundTokenAddress = (await this.getStakeCollateralByCollateralNameAsync(collateralName)).diFundToken;
        const diFundTokenContract = new DIFundTokenContract(diFundTokenAddress, this.providerEngine, this.txDefaults);
        return Web3Wrapper.toUnitAmount(
            await diFundTokenContract.getPriorValues(account, new BigNumber(blockNumber).minus(1)).callAsync(),
            this.getDecimalsForCollateralType(collateralName),
        );
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param collateralName The address of the account to get the balance of
     * @param blockNumber AToken address
     * @return The number of COMP accrued
     */
    public async getTotalPriorValuesDIFundTokenAsync(collateralName: string, blockNumber: number): Promise<BigNumber> {
        const diFundTokenAddress = (await this.getStakeCollateralByCollateralNameAsync(collateralName)).diFundToken;
        const diFundTokenContract = new DIFundTokenContract(diFundTokenAddress, this.providerEngine, this.txDefaults);
        return Web3Wrapper.toUnitAmount(
            await diFundTokenContract.getTotalPriorValues(new BigNumber(blockNumber).minus(1)).callAsync(),
            this.getDecimalsForCollateralType(collateralName),
        );
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param collateralName The collateral name.
     * @param account AToken address
     * @return The number of COMP accrued
     */
    public async getBalanceOfDIFundTokenAsync(collateralName: string, account: string): Promise<BigNumber> {
        const diFundTokenAddress = (await this.getStakeCollateralByCollateralNameAsync(collateralName)).diFundToken;
        const diFundTokenContract = new DIFundTokenContract(diFundTokenAddress, this.providerEngine, this.txDefaults);
        return Web3Wrapper.toUnitAmount(
            await diFundTokenContract.balanceOf(account).callAsync(),
            this.getDecimalsForCollateralType(collateralName),
        );
    }

    /**
     * @notice Get the number of COMP tokens accrued.
     * @param collateralName The collateral name.
     * @return The number of COMP accrued
     */
    public async getTotalSupplyDIFundTokenAsync(collateralName: string): Promise<BigNumber> {
        const diFundTokenAddress = (await this.getStakeCollateralByCollateralNameAsync(collateralName)).diFundToken;
        const diFundTokenContract = new DIFundTokenContract(diFundTokenAddress, this.providerEngine, this.txDefaults);
        return Web3Wrapper.toUnitAmount(
            await diFundTokenContract.totalSupply().callAsync(),
            this.getDecimalsForCollateralType(collateralName),
        );
    }

    /**
     * @notice Transfer `amount` tokens from `msg.sender` to `dst`
     * @param collateralAddress The address of the destination account
     * @param recipient The address of the destination account
     * @param amount The number of tokens to transfer
     * @return Whether or not the transfer succeeded
     */
    public transferDIFundToken(
        collateralAddress: string,
        recipient: string,
        amount: number,
    ): ContractTxFunctionObj<boolean> {
        const diFundTokenContract = new DIFundTokenContract(collateralAddress, this.providerEngine, this.txDefaults);
        return diFundTokenContract.transfer(recipient, Web3Wrapper.toBaseUnitAmount(amount, 6));
    }

    /**
     * @notice Get decimals for arbitrary token
     * @param tokenAddress The address of the token
     * @return Decimals for token
     */
    public async getDecimalsForArbitraryTokenAsync(tokenAddress: string): Promise<number> {
        const safeERC20WrapperContract = new SafeERC20WrapperContract(
            tokenAddress,
            this.providerEngine,
            this.txDefaults,
        );
        return safeERC20WrapperContract.decimals().callAsync();
    }

    /**
     * @notice Transfer `amount` tokens from `msg.sender` to `dst`
     * @param tokenAddress The address of the token to transfer
     * @param recipient The address of the destination account
     * @param amount The number of tokens to transfer
     * @param decimals The number of decimals for the token contract
     */
    public transferArbitraryTokens(
        tokenAddress: string,
        recipient: string,
        amount: number,
        decimals: number,
    ): ContractTxFunctionObj<void> {
        const safeERC20WrapperContract = new SafeERC20WrapperContract(
            tokenAddress,
            this.providerEngine,
            this.txDefaults,
        );
        return safeERC20WrapperContract.transfer(recipient, Web3Wrapper.toBaseUnitAmount(amount, decimals));
    }

    /**
     * @notice Get the number of tokens held by the `account`
     * @param tokenAddress The address of the token to get balance of
     * @param account The account to get the balance of
     * @return The number of tokens held
     */
    public async getBalanceOfArbitraryTokensAsync(tokenAddress: string, account: string): Promise<BigNumber> {
        const safeERC20WrapperContract = new SafeERC20WrapperContract(
            tokenAddress,
            this.providerEngine,
            this.txDefaults,
        );
        const balance = await safeERC20WrapperContract.balanceOf(account).callAsync();
        const tokenContractDecimals = await safeERC20WrapperContract.decimals().callAsync();
        return Web3Wrapper.toUnitAmount(balance, tokenContractDecimals);
    }
}
