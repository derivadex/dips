import { getContractAddressesForChainOrThrow } from '@0x/contract-addresses';
import { ContractTxFunctionObj, ContractWrappers, ERC20TokenContract } from '@0x/contract-wrappers';
import { BigNumber } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { Account, TraderInfo } from '@derivadex/types';

import { LegacyContractWrapper } from '.';

const UNLIMITED_ALLOWANCE = new BigNumber(2).pow(256).minus(1); // tslint:disable-line:custom-no-magic-numbers

export class TraderBase {
    public account: string;
    public contractWrapper: LegacyContractWrapper;
    private _contractWrappers: ContractWrappers;

    constructor(account: string, derivadex: LegacyContractWrapper) {
        this.account = account;
        this.contractWrapper = derivadex;
        this._contractWrappers = new ContractWrappers(this.contractWrapper.providerEngine, {
            chainId: derivadex.chainId,
        });
    }

    public async getQuoteCurrencyProxyAllowanceAsync(): Promise<BigNumber> {
        const erc20TokenContract = new ERC20TokenContract(
            this.contractWrapper.quoteCurrencyContract.address,
            this.contractWrapper.providerEngine,
        );
        return erc20TokenContract.balanceOf(this.account).callAsync();
    }

    /**
     * Approve the proxy contract of the quote currency of the platform.
     */
    public approveDepositProxy(): ContractTxFunctionObj<boolean> {
        const tokenAddress = this.contractWrapper.quoteCurrencyContract.address;
        const erc20TokenContract = new ERC20TokenContract(tokenAddress, this.contractWrapper.providerEngine);
        return erc20TokenContract.approve(this.contractWrapper.derivaDEXContract.address, UNLIMITED_ALLOWANCE);
    }

    /**
     * Approve the proxy contract of the quote currency of the platform.
     */
    public revokeDepositProxy(): ContractTxFunctionObj<boolean> {
        const tokenAddress = this.contractWrapper.quoteCurrencyContract.address;
        const erc20TokenContract = new ERC20TokenContract(tokenAddress, this.contractWrapper.providerEngine);
        return erc20TokenContract.approve(this.contractWrapper.derivaDEXContract.address, new BigNumber(0));
    }

    /**
     * Get proxy allowance for token
     * @param tokenAddress The token address
     */
    public async getProxyAllowanceAsync(tokenAddress: string): Promise<BigNumber> {
        const erc20TokenContract = new ERC20TokenContract(tokenAddress, this.contractWrapper.providerEngine);
        return erc20TokenContract
            .allowance(this.account, getContractAddressesForChainOrThrow(this.contractWrapper.chainId).erc20BridgeProxy)
            .callAsync();
    }

    /**
     * Get proxy allowance for token
     * @param tokenAddress The token address
     */
    public async getDepositAllowanceAsync(tokenAddress: string): Promise<BigNumber> {
        const erc20TokenContract = new ERC20TokenContract(tokenAddress, this.contractWrapper.providerEngine);
        return erc20TokenContract.allowance(this.account, this.contractWrapper.derivaDEXContract.address).callAsync();
    }

    /**
     * Approve proxy allowance for token
     * @param tokenAddress The token address
     */
    public approveProxy(tokenAddress: string): ContractTxFunctionObj<boolean> {
        const erc20TokenContract = new ERC20TokenContract(tokenAddress, this.contractWrapper.providerEngine);
        return erc20TokenContract.approve(
            getContractAddressesForChainOrThrow(this.contractWrapper.chainId).erc20BridgeProxy,
            UNLIMITED_ALLOWANCE,
        );
    }

    /**
     * Revoke proxy allowance for token
     * @param tokenAddress The token address
     */
    public revokeProxy(tokenAddress: string): ContractTxFunctionObj<boolean> {
        const erc20TokenContract = new ERC20TokenContract(tokenAddress, this.contractWrapper.providerEngine);
        return erc20TokenContract.approve(
            getContractAddressesForChainOrThrow(this.contractWrapper.chainId).erc20BridgeProxy,
            new BigNumber(0),
        );
    }

    /**
     * Fetch token balance
     * @param tokenAddress The token address
     */
    public async getTokenBalanceAsync(tokenAddress: string): Promise<BigNumber> {
        const erc20TokenContract = new ERC20TokenContract(tokenAddress, this.contractWrapper.providerEngine);
        return erc20TokenContract.balanceOf(this.account).callAsync();
    }

    /**
     * Fetch token balance
     */
    public async getTraderInfoAsync(): Promise<TraderInfo> {
        return this.contractWrapper.getTraderInfoAsync(this.account);
    }

    /**
     * Fetch token balance
     */
    public async getAccountAsync(account: string): Promise<Account> {
        return this.contractWrapper.getAccountByTraderAndAccountAsync(account, this.account);
    }

    /**
     * Fetch token balance
     */
    public async depositAsync(amount: number, bucket: string): Promise<string> {
        return this.contractWrapper.deposit(amount, bucket).sendTransactionAsync({ from: this.account });
    }

    /**
     * Fetch token balance
     */
    public async withdrawAsync(amount: number, bucket: string): Promise<string> {
        return this.contractWrapper.withdraw(amount, bucket).sendTransactionAsync({ from: this.account });
    }

    public getWeb3Wrapper(): Web3Wrapper {
        return this.contractWrapper.getWeb3Wrapper();
    }
}
