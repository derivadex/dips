import { ContractAddresses } from '@derivadex/types';
import { SupportedProvider } from 'ethereum-types';

import { ContractWrapper } from './contract_wrapper';

// tslint:disable-next-line:max-classes-per-file
export class Derivadex extends ContractWrapper {
    constructor(contractAddresses: ContractAddresses, provider: SupportedProvider, chainId = 1337) {
        super(contractAddresses, provider, chainId);
    }
}
