/**
 * contract_wrapper helper functions
 * @module contract_utils
 */

import { PositionForFundingRate, PositionToADL, PositionToADLForContract } from '@derivadex/types';
import { Contract, ethers, utils } from 'ethers';
import { Web3Provider } from 'ethers/providers';

/**
 * Create `Ethers` contract instance
 * @param address: The deployed address
 * @param abi: The contract interface object
 */
export function createContract(
    address: string,
    abi: Array<string | utils.ParamType> | string | utils.Interface,
    provider: Web3Provider,
): Contract {
    const checksumAddress = utils.getAddress(address);
    return new ethers.Contract(checksumAddress, abi, provider);
}

export function createPositionToADLForContract(positionToADL: PositionToADL): PositionToADLForContract {
    return {
        ...positionToADL,
        account: ethers.utils.formatBytes32String(positionToADL.account),
    };
}

export function createPositionForFundingRateForContract(
    positionForFundingRate: PositionForFundingRate,
): PositionToADLForContract {
    return {
        ...positionForFundingRate,
        account: ethers.utils.formatBytes32String(positionForFundingRate.account),
    };
}
