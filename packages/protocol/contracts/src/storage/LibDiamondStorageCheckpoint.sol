// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

library LibDiamondStorageCheckpoint {
    struct DiamondStorageCheckpoint {
        // The current epoch ID of the DerivaDEX exchange.
        uint128 currentEpochId;
        // The most recent block number that was published by the DerivaDEX
        // operators. This value is monotonically increasing.
        uint128 latestConfirmedBlockNumber;
        // The most recent state root that was published by the DerivaDEX
        // operators. This root provides a digest of the state for the DerivaDEX
        // exchange.
        bytes32 stateRoot;
        // The most recent transaction root that was published by the DerivaDEX
        // operators. This root provides a digest of the transactions that have
        // been executed on the DerivaDEX exchange.
        bytes32 transactionRoot;
        // Percentage of operators that must report in agreement for consensus
        // to be reached. Valid values range from 51 to 100 inclusive (51% to 100%).
        uint128 consensusThreshold;
        // The minimum quorum necessary for a group of operators to be able to
        // achieve consensus. This value is mostly used to protect the system
        // when only a small number of operators (maybe even just 1) has registered.
        uint128 quorum;
    }

    bytes32 constant DIAMOND_STORAGE_POSITION_CHECKPOINT =
        keccak256("diamond.standard.diamond.storage.DerivaDEX.Checkpoint");

    function diamondStorageCheckpoint() internal pure returns (DiamondStorageCheckpoint storage ds) {
        bytes32 position = DIAMOND_STORAGE_POSITION_CHECKPOINT;
        assembly {
            ds.slot := position
        }
    }
}
