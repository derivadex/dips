// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

library LibDiamondStorageDepositGuarded {
    struct DiamondStorageDepositGuarded {
        // The max number of unique deposited addresses.
        uint128 maxDepositedAddresses;
        // The minimum deposit (USDC).
        uint128 minDeposit;
        // The number of unique deposited addresses.
        uint128 numDepositedAddresses;
        // Mapping from address to whether a deposit has ever been made.
        mapping(address => bool) hasDeposited;
    }

    bytes32 constant DIAMOND_STORAGE_POSITION_DEPOSIT_GUARDED =
        keccak256("diamond.standard.diamond.storage.DerivaDEX.DepositGuarded");

    function diamondStorageDepositGuarded() internal pure returns (DiamondStorageDepositGuarded storage ds) {
        bytes32 position = DIAMOND_STORAGE_POSITION_DEPOSIT_GUARDED;
        assembly {
            ds.slot := position
        }
    }
}
