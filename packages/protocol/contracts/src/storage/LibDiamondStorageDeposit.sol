// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { DepositDefs } from "../libs/defs/DepositDefs.sol";

library LibDiamondStorageDeposit {
    struct DiamondStorageDeposit {
        // Mapping from collateral token to a structure containing additional
        // information about the exchange collateral.
        mapping(address => DepositDefs.ExchangeCollateral) exchangeCollaterals;
        // The amount of blocks that must elapse before the withdrawal rate
        // limit will be reset.
        uint128 rateLimitPeriod;
        // The percentage of the total amount of funds in the system (at the
        // time the rate limit is set) that should be used to increase the
        // rate limit.
        uint128 rateLimitPercentage;
        // A mapping from token addresses to the rate limits for the token.
        mapping(address => DepositDefs.WithdrawalRateLimit) withdrawalRateLimits;
        // The maximum amount of DDX that can be stored within the SMT. This
        // cap is enforced to ensure that enough DDX stays outside of the SMT
        // to allow governance to react to any malicious activity perpetrated
        // by the group of operators.
        uint256 maxDDXCap;
        // A value that tracks the amount of DDX that has been deposited into
        // trader leaves. This is used to prevent trader withdrawals (which can
        // mint DDX) from taking DDX that should be withdrawn through stricter
        // flows.
        uint128 traderDDXDeposited;
        // A mapping from token addresses to the total amount that has been
        // deposited for that token. This mapping can be used to prevent rogue
        // operators from withdrawing funds from other parts of the system that
        // are logically separate from the merklized state. Of course, there are
        // mechanisms in place to prevent rogue operator cartels from being
        // possible/feasible; however, this layer of security doesn't rely on
        // these protections.
        //
        // Address zero represents DDX stored within trader leaves within the
        // sparse merkle tree. Unlike all of the other allowances, this allowance
        // will be increased when epochs are advanced to account for engagement
        // mining programs including trade mining.
        mapping(address => uint128) withdrawalAllowances;
        // A mapping from withdraw address to strategy ID to token address to a
        // pruned linked list representing unprocessed withdrawals. A linked list
        // is used instead of an array since we are interested in which block the
        // the withdrawal took place in. This is sparse data with an increasing
        // index, so a singly linked list is an efficient choice of storage.
        // solhint-disable max-line-length
        mapping(address => mapping(bytes32 => mapping(address => DepositDefs.UnprocessedWithdrawalList))) unprocessedWithdrawals;
        // solhint-enable max-line-length
    }

    bytes32 constant DIAMOND_STORAGE_POSITION_DEPOSIT = keccak256("diamond.standard.diamond.storage.DerivaDEX.Deposit");

    function diamondStorageDeposit() internal pure returns (DiamondStorageDeposit storage ds) {
        bytes32 position = DIAMOND_STORAGE_POSITION_DEPOSIT;
        assembly {
            ds.slot := position
        }
    }
}
