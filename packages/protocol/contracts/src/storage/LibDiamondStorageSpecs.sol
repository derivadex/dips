// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

library LibDiamondStorageSpecs {
    struct DiamondStorageSpecs {
        // The genesis block number.
        uint128 genesisBlock;
    }

    bytes32 constant DIAMOND_STORAGE_POSITION_SPECS = keccak256("diamond.standard.diamond.storage.DerivaDEX.Specs");

    function diamondStorageSpecs() internal pure returns (DiamondStorageSpecs storage ds) {
        bytes32 position = DIAMOND_STORAGE_POSITION_SPECS;
        assembly {
            ds.slot := position
        }
    }
}
