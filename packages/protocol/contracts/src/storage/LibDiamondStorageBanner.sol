// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

library LibDiamondStorageBanner {
    struct DiamondStorageBanner {
        mapping(address => bool) banned;
    }

    bytes32 constant DIAMOND_STORAGE_POSITION_BANNER = keccak256("diamond.standard.diamond.storage.DerivaDEX.Banner");

    function diamondStorageBanner() internal pure returns (DiamondStorageBanner storage ds) {
        bytes32 position = DIAMOND_STORAGE_POSITION_BANNER;
        assembly {
            ds.slot := position
        }
    }
}
