// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

library LibDiamondStorageReentrancyGuard {
    struct DiamondStorageReentrancyGuard {
        // Status of reentrancy guard (entered or not entered)
        uint256 status;
    }

    bytes32 constant DIAMOND_STORAGE_POSITION_REENTRANCY_GUARD =
        keccak256("diamond.standard.diamond.storage.DerivaDEX.ReentrancyGuard");

    function diamondStorageReentrancyGuard() internal pure returns (DiamondStorageReentrancyGuard storage ds) {
        bytes32 position = DIAMOND_STORAGE_POSITION_REENTRANCY_GUARD;
        assembly {
            ds.slot := position
        }
    }
}
