// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { RegistrationDefs } from "../libs/defs/RegistrationDefs.sol";

library LibDiamondStorageRegistration {
    struct DiamondStorageRegistration {
        // The registration metadata hash of the current release. This hash is
        // calculated as `hash(mrEnclave, isvSvn)` and is used as an identifier
        // for releases.
        bytes32 currentReleaseHash;
        // The number of blocks that must elapse before a custodian can
        // withdraw their DDX bond.
        uint128 unbondDelay;
        // Minimum bond required for an operator to be active
        uint128 minimumBond;
        // Accounting for penalties issued.
        uint128 penalties;
        // Count of signers able to submit checkpoints (i.e., are
        // registered, have sufficient bond, and not jailed)
        uint128 validSignersCount;
        // The starting epoch ID of the current release.
        uint128 currentReleaseStartingEpochId;
        // The release schedule. Since the release schedule is sparse, this
        // is stored in the form of a linked list with a key of the starting
        // epoch of a given release and a value of the registration metadata
        // hash used in a given release.
        mapping(uint128 => RegistrationDefs.ReleaseScheduleEntry) releaseSchedule;
        // Mapping of release hash(mrEnclave, isvSvn) to a list of all
        // of the custodians that have registered in that release.
        mapping(bytes32 => address[]) registeredCustodians;
        // Mapping from custodian address to the custodian state. This
        // includes properties like the bonding and jailing status of the
        // custodian.
        mapping(address => RegistrationDefs.Custodian) custodians;
        // Mapping from deployment hash(mrEnclave, isvSvn) to signer
        // to signer state. This state includes the signer's remote
        // attestation report and the signer's custodian.
        mapping(bytes32 => mapping(address => RegistrationDefs.Signer)) signers;
    }

    bytes32 constant DIAMOND_STORAGE_POSITION_REGISTRATION =
        keccak256("diamond.standard.diamond.storage.DerivaDEX.Registration");

    function diamondStorageRegistration() internal pure returns (DiamondStorageRegistration storage ds) {
        bytes32 position = DIAMOND_STORAGE_POSITION_REGISTRATION;
        assembly {
            ds.slot := position
        }
    }
}
