// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { RegistrationDefs } from "../libs/defs/RegistrationDefs.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageRegistration } from "../storage/LibDiamondStorageRegistration.sol";

/**
 * @title LibRelease
 * @author DerivaDEX
 * @dev This library contains logic for scheduling and advancing releases.
 */
library LibRelease {
    /**
     * @dev Attempts to advance the release hash schedule and returns the
     *      current release hash.
     * @param _dsRegistration A pointer to the registration diamond storage.
     * @param _epochId The new epoch ID.
     * @return The current release hash.
     */
    function evaluateReleaseSchedule(
        LibDiamondStorageRegistration.DiamondStorageRegistration storage _dsRegistration,
        uint128 _epochId
    ) internal returns (bytes32) {
        uint128 nextEpochId =
            _dsRegistration.releaseSchedule[_dsRegistration.currentReleaseStartingEpochId].nextEpochId;

        if (nextEpochId == 0 || nextEpochId > _epochId) {
            // If the next epoch ID is zero or the next epoch ID is greater than
            // the current epoch ID...

            // return the current release hash.
            return _dsRegistration.currentReleaseHash;
        }

        // Advance through as many epochs as possible while still staying
        // less than or equal to the new epoch ID.
        uint128 tailEpochId = _dsRegistration.releaseSchedule[nextEpochId].nextEpochId;
        while (tailEpochId <= _epochId && tailEpochId != 0) {
            nextEpochId = tailEpochId;
            tailEpochId = _dsRegistration.releaseSchedule[nextEpochId].nextEpochId;
        }

        bytes32 releaseHash = _dsRegistration.releaseSchedule[nextEpochId].releaseHash;
        _dsRegistration.currentReleaseStartingEpochId = nextEpochId;
        _dsRegistration.currentReleaseHash = releaseHash;

        // Recount the number of valid signers.
        uint128 validSignersCount;
        for (uint256 i = 0; i < _dsRegistration.registeredCustodians[releaseHash].length; i++) {
            RegistrationDefs.Custodian storage custodian =
                _dsRegistration.custodians[_dsRegistration.registeredCustodians[releaseHash][i]];
            if (
                custodian.signers[releaseHash] != address(0) &&
                custodian.balance >= _dsRegistration.minimumBond &&
                custodian.unbondETA == 0 &&
                !custodian.jailed
            ) {
                validSignersCount++;
            }
        }
        _dsRegistration.validSignersCount = validSignersCount;

        // Return the new release hash.
        return releaseHash;
    }

    /**
     * @dev Commits a new release hash to the schedule. A release hash of zero
     *      is used to delete a scheduled release.
     * @param _dsCheckpoint A pointer to the checkpoint diamond storage.
     * @param _dsRegistration A pointer to the registration diamond storage.
     * @param _releaseHash The release hash to add to the release schedule.
     * @param _startingEpochId The epoch ID in which the new release should take
     *        effect.
     * @return A boolean indicating whether the current release was updated.
     */
    function commitReleaseScheduleUpdate(
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage _dsCheckpoint,
        LibDiamondStorageRegistration.DiamondStorageRegistration storage _dsRegistration,
        bytes32 _releaseHash,
        uint128 _startingEpochId
    ) internal returns (bool) {
        // Unless this commit is the first, don't allow updates to be made
        // before the current release.
        require(
            (_dsRegistration.currentReleaseHash == bytes32(0) && _startingEpochId == 0) ||
                (_dsRegistration.currentReleaseHash != bytes32(0) &&
                    _startingEpochId >= _dsRegistration.currentReleaseStartingEpochId),
            "LibRelease: can't commit an update before the current release."
        );

        // If the starting epoch ID is less than or equal to the current
        // epoch ID, this release should become the current release.
        if (_startingEpochId <= _dsCheckpoint.currentEpochId) {
            require(
                _releaseHash != bytes32(0),
                "LibRelease: Can't delete a non-existent release or the current release."
            );

            // Update the current release hash to be the new release hash and
            // link the new entry to the next entry if appropriate. We don't
            // store the new release hash in the new entry since it's being
            // stored as the current release hash and it won't be used after the
            // schedule is updated.
            RegistrationDefs.ReleaseScheduleEntry storage currentEntry =
                _dsRegistration.releaseSchedule[_dsRegistration.currentReleaseStartingEpochId];
            _dsRegistration.currentReleaseStartingEpochId = _startingEpochId;
            _dsRegistration.currentReleaseHash = _releaseHash;
            if (currentEntry.nextEpochId != 0) {
                _dsRegistration.releaseSchedule[_startingEpochId].nextEpochId = currentEntry.nextEpochId;
            }
            return true;
        }

        // Iterate through the release hash schedule until we find an entry that
        // has an earlier starting epoch than the new entry or we exhaust the
        // release hash schedule.
        uint128 previousEpochId = _dsRegistration.currentReleaseStartingEpochId;
        uint128 nextEpochId = _dsRegistration.releaseSchedule[previousEpochId].nextEpochId;
        while (nextEpochId != 0 && nextEpochId < _startingEpochId) {
            previousEpochId = nextEpochId;
            nextEpochId = _dsRegistration.releaseSchedule[previousEpochId].nextEpochId;
        }

        // Commit the new entry to the release hash schedule.
        if (nextEpochId == 0 || nextEpochId > _startingEpochId) {
            // If the next epoch ID is equal to zero or is greater than the
            // starting epoch ID...

            // Reject empty release hashes since there isn't an entry to delete.
            require(_releaseHash != bytes32(0), "LibRelease: Can't delete a non-existent release.");

            // Create the new entry. Ensure that the previous entry is linked to
            // the new entry.
            _dsRegistration.releaseSchedule[previousEpochId].nextEpochId = _startingEpochId;
            _dsRegistration.releaseSchedule[_startingEpochId].releaseHash = _releaseHash;

            // If the next scheduled epoch ID is non-zero, we must ensure that
            // the new entry is linked to the next entry.
            if (nextEpochId != 0) {
                _dsRegistration.releaseSchedule[_startingEpochId].nextEpochId = nextEpochId;
            }
        } else if (nextEpochId == _startingEpochId) {
            // If the next scheduled epoch ID is equal to the starting epoch
            // ID...

            if (_releaseHash == bytes32(0)) {
                // An empty release indicates that the next entry should be
                // deleted. Ensure that we link the previous node to the tail of
                // the deleted entry.
                _dsRegistration.releaseSchedule[previousEpochId].nextEpochId = _dsRegistration.releaseSchedule[
                    _startingEpochId
                ]
                    .nextEpochId;
                delete _dsRegistration.releaseSchedule[_startingEpochId];
            } else {
                // Overwrite the existing entry. We don't need to re-link any
                // entries since this entry already existed.
                _dsRegistration.releaseSchedule[_startingEpochId].releaseHash = _releaseHash;
            }
        }

        return false;
    }

    /**
     * @dev Gets the current release hash.
     * @param _dsRegistration A pointer to the registration diamond storage.
     * @return The current release hash.
     */
    function getCurrentReleaseHash(LibDiamondStorageRegistration.DiamondStorageRegistration storage _dsRegistration)
        internal
        view
        returns (bytes32)
    {
        return _dsRegistration.currentReleaseHash;
    }
}
