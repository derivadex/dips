// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { LibSignature } from "../libs/LibSignature.sol";
import { MathHelpers } from "../libs/MathHelpers.sol";
import { CheckpointDefs } from "../libs/defs/CheckpointDefs.sol";
import { RegistrationDefs } from "../libs/defs/RegistrationDefs.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageRegistration } from "../storage/LibDiamondStorageRegistration.sol";

/**
 * @title LibCheckpoint
 * @author DerivaDEX
 * @dev This library abstracts the core logic required for checkpoints.
 */
library LibCheckpoint {
    using MathHelpers for uint256;

    uint96 internal constant CONSENSUS_PRECISION = 1e18;

    /**
     * @dev Returns the current consensus threshold.
     * @param _validSignersCount The number of valid signers.
     * @return The current consensus threshold.
     */
    function getConsensusThreshold(uint128 _validSignersCount) internal view returns (uint256) {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        return uint256(_validSignersCount).proportion256(dsCheckpoint.consensusThreshold * CONSENSUS_PRECISION, 100);
    }

    /**
     * @dev Hashes checkpoint data with the chain ID and a epoch ID. Adding this
     *      extra data protects against replay attacks.
     * @param _checkpointData The data that should be hashed.
     * @param _epochId The epoch ID to use during hashing.
     * @return hash The hash of the checkpoint, the chain ID, and the epoch ID.
     */
    function hashCheckpointData(CheckpointDefs.CheckpointData memory _checkpointData, uint256 _epochId)
        internal
        view
        returns (bytes32 hash)
    {
        // Assembly for optimization
        // return keccak256(abi.encode(
        //     address(this),
        //     chainId,
        //     _epochId,
        //     _checkpointData.blockNumber,
        //     _checkpointData.blockHash,
        //     _checkpointData.stateRoot,
        //     _checkpointData.transactionRoot
        // ));
        assembly {
            let ptr := mload(0x40)
            mstore(ptr, address()) // verifying contract address
            mstore(add(ptr, 0x20), chainid()) // chain ID
            mstore(add(ptr, 0x40), _epochId) // epoch ID
            mstore(add(ptr, 0x60), mload(_checkpointData)) // block number
            mstore(add(ptr, 0x80), mload(add(_checkpointData, 0x20))) // block hash
            mstore(add(ptr, 0xa0), mload(add(_checkpointData, 0x40))) // state root
            mstore(add(ptr, 0xc0), mload(add(_checkpointData, 0x60))) // transaction root
            hash := keccak256(ptr, 0xe0)
        }
        return hash;
    }

    /**
     * @dev Verify that the provided _consensusCount is sufficient for
     *      consensus to be achieved. This amounts to verifying that
     *      _consensusCount is a large enough percentage of the valid
     *      signer count that it is greater than the consensus threshold
     *      defined by governance.
     * @param _consensusCount The consensus count that should be verified.
     * @param _validSignersCount The number of valid signers.
     */
    function verifyConsensusRule(uint256 _consensusCount, uint128 _validSignersCount) internal view {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();

        // Verify that the consensus count is greater than or equal to the
        // threshold needed for consensus. We add precision in order to
        // prevent 50% from passing the threshold.
        uint256 threshold = getConsensusThreshold(_validSignersCount);
        require(
            _consensusCount * CONSENSUS_PRECISION >= threshold && _consensusCount >= dsCheckpoint.quorum,
            "LibCheckpoint: Consensus was not reached."
        );
    }

    /**
     * @dev Sanitize the checkpoint submission inputs, recover the signer
     *      addresses from the signatures to guarantee uniqueness and
     *      confirm/count validity (those that are not jailed and have
     *      sufficient bond), and jail operators submitting invalid
     *      checkpoints.
     * @param _dsRegistration A storage pointer to the diamond storage entry
     *        for the Registration facet.
     * @param _checkpointSubmission The struct containing data that needs
     *        to be hashed in order to recover signatures. This data can
     *        pertain to either the majority or the minority hashes.
     * @param _epochId The current epoch ID. The network chain ID and a epoch
     *        ID are prepended to the checkpoint data that needs to be hashed
     *        to provide replay protection.
     * @param _releaseHash The current release hash that identifies which
     *        signer can submit checkpoints.
     * @param _referenceHashForInvalidCheckpoints Reference hash. In the
     *        case we're looking at majority checkpoints, this will be
     *        bytes32(0), otherwise will be the reference hash from
     *        the majority checkpoint iteration during minority/invalid
     *        checkpoint consideration.
     * @return The list of custodians that contributed to the checkpoint. If
     *         majority checkpoints are being evaluated, a list of the bonds
     *         of each custodian will also be returned. If minority checkpoints
     *         are being evaluated, the second list will be empty.
     */
    function verifySubmission(
        LibDiamondStorageRegistration.DiamondStorageRegistration storage _dsRegistration,
        CheckpointDefs.CheckpointSubmission memory _checkpointSubmission,
        uint256 _epochId,
        bytes32 _releaseHash,
        bytes32 _referenceHashForInvalidCheckpoints
    ) internal returns (address[] memory, uint128[] memory) {
        // Ensure the list of signatures is non-empty.
        require(_checkpointSubmission.signatures.length > 0, "LibCheckpoint: submission signatures must be non-empty.");

        // We track the last signer address recovered. We do this to
        // guarantee that there are no duplicate submissions and ensure
        // that reporting signers are unique within the submission. In other
        // words, the signatures comprising the checkpoint submission must be
        // arranged upon submission in such a way that the signers that are
        // recovered are arranged in ascending order.
        address lastSignerAddress;

        // Hash the checkpoint data. All signatures for the checkpoint
        // submission in question have presumably signed this same checkpoint
        // data.
        bytes32 hash = hashCheckpointData(_checkpointSubmission.checkpointData, _epochId);

        // Check if we are assessing the minority checkpoint submissions
        // to ensure that the observed hash is not the same as the majority
        // checkpoint submission's hash.
        if (_referenceHashForInvalidCheckpoints != bytes32(0)) {
            // If we are considering minority checkpoints...

            // Ensure this minority hash doesn't agree with the reference hash
            require(
                _referenceHashForInvalidCheckpoints != hash,
                "LibCheckpoint: An invalid submission was the same as a valid submission."
            );
        }

        // Loop through the signatures backing this checkpoint submission.
        uint128 validSignersCountDelta = 0;
        uint128 addressesCount = 0;
        address[] memory addresses = new address[](_checkpointSubmission.signatures.length);
        uint128[] memory bonds;
        if (_referenceHashForInvalidCheckpoints == bytes32(0)) {
            // If we are considering majority checkpoints...

            // Allocate memory for the bonds.
            bonds = new uint128[](_checkpointSubmission.signatures.length);
        } else {
            // If we are considering minority checkpoints...

            // Don't allocate any memory for the bonds.
            bonds = new uint128[](0);
        }
        for (uint256 i = 0; i < _checkpointSubmission.signatures.length; i++) {
            // Recover the current signer address. In order to improve the
            // algorithms used in this smart contract, we require the
            // submitter to provide all of the signer addresses to the
            // contract in ascending order. Verifying that the signer
            // addresses are monotonically increasing ensures that there are
            // no duplicate submissions.
            {
                address recoveredSignerAddress =
                    LibSignature.getEIP191SignerOfHash(hash, _checkpointSubmission.signatures[i]);
                require(
                    lastSignerAddress == address(0) || lastSignerAddress < recoveredSignerAddress,
                    "LibCheckpoint: Signer addresses must be monotonically increasing."
                );
                lastSignerAddress = recoveredSignerAddress;
            }

            // Check if the signer is registered.
            RegistrationDefs.Signer memory signer = _dsRegistration.signers[_releaseHash][lastSignerAddress];
            RegistrationDefs.Custodian storage custodian = _dsRegistration.custodians[signer.custodian];
            if (
                signer.custodian == address(0) ||
                (_referenceHashForInvalidCheckpoints == 0 && custodian.signers[_releaseHash] != lastSignerAddress)
            ) {
                // If the submission is a majority submission and the signer
                // isn't an active signer or if the submission is a minority
                // checkpoint and the signer hasn't been registered...

                // skip the other checks and continue through the loop.
                continue;
            }

            // Check if the custodian has a valid signer. In either the case
            // that this submission is a valid or an invalid submission, we
            // won't do anything with these custodians after this point.
            if (
                custodian.jailed ||
                (_referenceHashForInvalidCheckpoints == 0 && custodian.balance < _dsRegistration.minimumBond) ||
                (_referenceHashForInvalidCheckpoints == 0 && custodian.unbondETA > 0)
            ) {
                // If the submission is the majority submission and the
                // custodian doesn't have a valid signer or if the submission
                // is a minority checkpoint and isn't jailed...

                // Skip the other checks and continue through the loop.
                continue;
            }

            // Check if we're evaluating minority checkpoint submissions in
            // the checkpointing workflow (this won't be called during the
            // reject workflow), jail custodians as appropriate, and decrement
            // the state's count of valid signers accordingly.
            if (_referenceHashForInvalidCheckpoints != bytes32(0)) {
                // If we're looking at minority checkpoints...

                // Jail the custodian.
                custodian.jailed = true;

                if (custodian.balance >= _dsRegistration.minimumBond && custodian.unbondETA == 0) {
                    // If the custodian has a valid signer...

                    // Increment the valid signers count delta.
                    validSignersCountDelta++;
                }
            } else {
                // If we're looking at majority checkpoints...

                // Add to the bonds array.
                bonds[addressesCount] = custodian.balance;
            }

            // Add the custodian to the list of contributors to the checkpoint.
            addresses[addressesCount++] = signer.custodian;
        }

        if (validSignersCountDelta > 0) {
            // If the valid signers count delta is greater than zero...

            // Update the valid signers count.
            _dsRegistration.validSignersCount = _dsRegistration.validSignersCount - validSignersCountDelta;
        }

        // Update the length of the addresses array to equal the number of
        // elements that were added to the array and return the value.
        assembly {
            mstore(addresses, addressesCount)
        }

        // If we are evaluating majority checkpoints, update the length of the
        // bonds array to equal the number of elements that were added to the
        // array and return the value.
        if (_referenceHashForInvalidCheckpoints == bytes32(0)) {
            // If we are looking at majority checkpoints...
            assembly {
                mstore(bonds, addressesCount)
            }
        }

        return (addresses, bonds);
    }
}
