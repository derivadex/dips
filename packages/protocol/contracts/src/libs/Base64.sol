// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

/**
 * @title Base64
 * @author DerivaDEX
 * @notice This library facilitates Base64 decoding of bytes. It's
           inspired by the implementation pattern outlined at:
           http://www.sunshine2k.de/articles/coding/base64/understanding_base64.html.
 */
library Base64 {
    /**
     * @notice Initialize the decimal look-up table. During a base64
     *         encoding, sets of bytes (8 bits) are instead regrouped
     *         into sets of 6 bits. Each set of 6 bits is converted into
     *         its decimal representation, and used to index into an
     *         encoding look up string: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
     *         to obtain its respective base64 character representation.
     *         With that being said, the decoding procedure needs
     *         to go in reverse. Starting with the base64 encoded
     *         character, we need to arrive at its decimal location in
     *         the aforementioned look up string. The decoding look up
     *         table has all 128 ASCII characters defined, with 80 being
     *         used for the invalid base64 encodings, and all other
     *         values being used for the base64 character indices in
     *         the look up string.
     */
    function initDecLookUpTable() internal pure returns (uint8[128] memory) {
        return [
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            62, // + (ASCII); 43 (Decimal); 62 (Base64 lookup index)
            80,
            80,
            80,
            63, // / (ASCII); 47 (Decimal); 63 (Base64 lookup index)
            52, // 0 (ASCII); 48 (Decimal); 52 (Base64 lookup index)
            53, // 1 (ASCII); 49 (Decimal); 53 (Base64 lookup index)
            54, // 2 (ASCII); 50 (Decimal); 54 (Base64 lookup index)
            55, // 3 (ASCII); 51 (Decimal); 55 (Base64 lookup index)
            56, // 4 (ASCII); 52 (Decimal); 56 (Base64 lookup index)
            57, // 5 (ASCII); 53 (Decimal); 57 (Base64 lookup index)
            58, // 6 (ASCII); 54 (Decimal); 58 (Base64 lookup index)
            59, // 7 (ASCII); 55 (Decimal); 59 (Base64 lookup index)
            60, // 8 (ASCII); 56 (Decimal); 60 (Base64 lookup index)
            61, // 9 (ASCII); 57 (Decimal); 61 (Base64 lookup index)
            80,
            80,
            80,
            80,
            80,
            80,
            80,
            0, // A (ASCII); 65 (Decimal); 0 (Base64 lookup index)
            1, // B (ASCII); 66 (Decimal); 1 (Base64 lookup index)
            2, // C (ASCII); 67 (Decimal); 2 (Base64 lookup index)
            3, // D (ASCII); 68 (Decimal); 3 (Base64 lookup index)
            4, // E (ASCII); 69 (Decimal); 4 (Base64 lookup index)
            5, // F (ASCII); 70 (Decimal); 5 (Base64 lookup index)
            6, // G (ASCII); 71 (Decimal); 6 (Base64 lookup index)
            7, // H (ASCII); 72 (Decimal); 7 (Base64 lookup index)
            8, // I (ASCII); 73 (Decimal); 8 (Base64 lookup index)
            9, // J (ASCII); 74 (Decimal); 9 (Base64 lookup index)
            10, // K (ASCII); 75 (Decimal); 10 (Base64 lookup index)
            11, // L (ASCII); 76 (Decimal); 11 (Base64 lookup index)
            12, // M (ASCII); 77 (Decimal); 12 (Base64 lookup index)
            13, // N (ASCII); 78 (Decimal); 13 (Base64 lookup index)
            14, // O (ASCII); 79 (Decimal); 14 (Base64 lookup index)
            15, // P (ASCII); 80 (Decimal); 15 (Base64 lookup index)
            16, // Q (ASCII); 81 (Decimal); 16 (Base64 lookup index)
            17, // R (ASCII); 82 (Decimal); 17 (Base64 lookup index)
            18, // S (ASCII); 83 (Decimal); 18 (Base64 lookup index)
            19, // T (ASCII); 84 (Decimal); 19 (Base64 lookup index)
            20, // U (ASCII); 85 (Decimal); 20 (Base64 lookup index)
            21, // V (ASCII); 86 (Decimal); 21 (Base64 lookup index)
            22, // W (ASCII); 87 (Decimal); 22 (Base64 lookup index)
            23, // X (ASCII); 88 (Decimal); 23 (Base64 lookup index)
            24, // Y (ASCII); 89 (Decimal); 24 (Base64 lookup index)
            25, // Z (ASCII); 90 (Decimal); 25 (Base64 lookup index)
            80,
            80,
            80,
            80,
            80,
            80,
            26, // a (ASCII); 97 (Decimal); 26 (Base64 lookup index)
            27, // b (ASCII); 98 (Decimal); 27 (Base64 lookup index)
            28, // c (ASCII); 99 (Decimal); 28 (Base64 lookup index)
            29, // d (ASCII); 100 (Decimal); 29 (Base64 lookup index)
            30, // e (ASCII); 101 (Decimal); 30 (Base64 lookup index)
            31, // f (ASCII); 102 (Decimal); 31 (Base64 lookup index)
            32, // g (ASCII); 103 (Decimal); 32 (Base64 lookup index)
            33, // h (ASCII); 104 (Decimal); 33 (Base64 lookup index)
            34, // i (ASCII); 105 (Decimal); 34 (Base64 lookup index)
            35, // j (ASCII); 106 (Decimal); 35 (Base64 lookup index)
            36, // k (ASCII); 107 (Decimal); 36 (Base64 lookup index)
            37, // l (ASCII); 108 (Decimal); 37 (Base64 lookup index)
            38, // m (ASCII); 109 (Decimal); 38 (Base64 lookup index)
            39, // n (ASCII); 110 (Decimal); 39 (Base64 lookup index)
            40, // o (ASCII); 111 (Decimal); 40 (Base64 lookup index)
            41, // p (ASCII); 112 (Decimal); 41 (Base64 lookup index)
            42, // q (ASCII); 113 (Decimal); 42 (Base64 lookup index)
            43, // r (ASCII); 114 (Decimal); 43 (Base64 lookup index)
            44, // s (ASCII); 115 (Decimal); 44 (Base64 lookup index)
            45, // t (ASCII); 116 (Decimal); 45 (Base64 lookup index)
            46, // u (ASCII); 117 (Decimal); 46 (Base64 lookup index)
            47, // v (ASCII); 118 (Decimal); 47 (Base64 lookup index)
            48, // w (ASCII); 119 (Decimal); 48 (Base64 lookup index)
            49, // x (ASCII); 120 (Decimal); 49 (Base64 lookup index)
            50, // y (ASCII); 121 (Decimal); 50 (Base64 lookup index)
            51, // z (ASCII); 122 (Decimal); 51 (Base64 lookup index)
            80,
            80,
            80,
            80,
            80
        ];
    }

    /**
     * @notice Decode a Base64-encoded source of bytes. The
     *         implementation here is tightly-coupled with the SGX
     *         remote attestation use case.
     * @param _base64Source Base64 encoded bytes source to be decoded
     */
    function decode(bytes memory _base64Source) internal view returns (bytes memory) {
        // Compute length of original base64 encoded bytes source
        uint256 len = _base64Source.length;

        // Resultant decoded bytes position to insert into
        uint256 outPos = 0;

        // Counter to iterate over the original base64 encoded bytes source
        uint256 i = 0;

        // Decoding look up table described above
        uint8[128] memory decLookUpTable = initDecLookUpTable();

        // A traditional base64 decoding scheme generally handles
        // scenarios where the source is not a multiple of 4, thus
        // can't be divied into 4 6-bit chunks to cleanly decode
        // into 3 byte (8-bit) chunks. However, a valid SGX report
        // is always a multiple of 4 since it has a fixed length, so
        // we validate this length here and implement the decoding
        // scheme without considering any padding scenarios.
        require(len % 4 == 0, "Base64: invalid report body length");

        // Initialize the pre-defined resulting decoded bytes array
        bytes memory res = new bytes((_base64Source.length / 4) * 3);

        // Loop through the encoded bytes source 4 at a time
        for (i; i < len; i += 4) {
            // Obtain the 4 6-bit indices in the look up string
            uint8 b0 = decLookUpTable[uint8(_base64Source[i])];
            uint8 b1 = decLookUpTable[uint8(_base64Source[i + 1])];
            uint8 b2 = decLookUpTable[uint8(_base64Source[i + 2])];
            uint8 b3 = decLookUpTable[uint8(_base64Source[i + 3])];

            // Derive the first byte (8-bit) chunk using all of b0
            // and the first two bits of b1
            res[outPos++] = bytes1(((b0 << 2) | ((b1 & 48) >> 4)));

            // Derive the second byte (8-bit) chunk using the last 4
            // bits of b1 and the first 4 bits of b2
            res[outPos++] = bytes1((((b1 & 15) << 4) | ((b2 & 60) >> 2)));

            // Derive the third byte (8-bit) chunk using the last 2 bits
            // of b2 and all of b3
            res[outPos++] = bytes1((((b2 & 3) << 6) | b3));
        }

        // Return the decoded result
        return res;
    }
}
