// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { LibDiamondStorageDeposit } from "../storage/LibDiamondStorageDeposit.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { IERC20Extended } from "../tokens/interfaces/IERC20Extended.sol";
import { DepositDefs } from "./defs/DepositDefs.sol";
import { SharedDefs } from "./defs/SharedDefs.sol";
import { MathHelpers } from "./MathHelpers.sol";

/**
 * @title LibCollateral
 * @author DerivaDEX
 * @dev This library abstracts the core logic required for dealing with exchange
 *      collateral and rewards. In particular, any logic relating to deposits
 *      and withdrawals should be encapsulated in this library.
 */
library LibCollateral {
    using MathHelpers for uint128;
    using MathHelpers for uint256;

    // The number of decimals that the operator uses internally to store decimal
    // numbers. Tokens that have a different number of decimals will need to be
    // be scaled appropriately so that the operator correctly handles them.
    uint256 internal constant OPERATOR_DECIMALS = 6;

    // The maximum number of block withdrawals that can occur between checkpoints
    // Measured for each withdrawal address, strategy, and token address by
    // counting the depth of the unprocessed withdrawal linked list traversal.
    uint256 internal constant MAX_WITHDRAWALS_PER_PERIOD = 10;

    // The length of the trade mining program, in trade mining epochs
    uint128 internal constant TRADE_MINING_LENGTH = 10950;

    // The frequency of trade mining epochs, in checkpoints
    uint128 internal constant TRADE_MINING_EPOCH_MULTIPLIER = 48;

    // The trade mining reward amount in DDX, per epoch (3196.347031)
    uint128 internal constant TRADE_MINING_REWARD_PER_EPOCH = 3196347031000000000000;

    /**
     * @dev Commits a deposit to the withdrawal allowances after verifying that
     *      it will not violate the maximum DDX capitalization allowed by
     *      governance. This verification ensures that a sufficient quantity of
     *      DDX will be kept outside of the system to ensure that enough DDX is
     *      available to vote outside the system.
     * @param _dsDeposit A pointer to the deposit diamond storage.
     * @param _collateralAddress The address of the collateral being deposited.
     * @param _depositAmount The amount to deposit.
     */
    function commitDeposit(
        LibDiamondStorageDeposit.DiamondStorageDeposit storage _dsDeposit,
        address _collateralAddress,
        uint128 _depositAmount
    ) internal {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();

        // Ensure that the deposit amount is non-zero.
        require(_depositAmount > 0, "LibCollateral: Invalid deposit amount.");

        // If the collateral address represents DDX in the trader or strategy
        // leaf, ensure that the max DDX cap will not be exceeded. The withdrawal
        // allowance for the trader leaf is stored in the entry for address zero
        // in the withdrawal allowances mapping.
        address ddxAddress = address(dsDerivaDEX.ddxToken);
        if (_collateralAddress == address(0) || _collateralAddress == ddxAddress) {
            uint128 remainingTradeMiningRewards =
                calculateTradeMiningRewardsForRange(
                    dsCheckpoint.currentEpochId,
                    TRADE_MINING_LENGTH * TRADE_MINING_EPOCH_MULTIPLIER
                );
            uint128 ddxWithdrawalAllowance =
                _dsDeposit.withdrawalAllowances[address(0)] + _dsDeposit.withdrawalAllowances[ddxAddress];
            require(
                ddxWithdrawalAllowance + remainingTradeMiningRewards + _depositAmount <= _dsDeposit.maxDDXCap,
                "LibCollateral: DDX deposit exceeds the maximum allowed DDX in the SMT."
            );
        }

        // Commit the deposit amount to the withdrawal allowance.
        _dsDeposit.withdrawalAllowances[_collateralAddress] =
            _dsDeposit.withdrawalAllowances[_collateralAddress] +
            _depositAmount;
    }

    /**
     * @dev Verifies a user's withdrawal from a Trader item and then commits the
     *      data that is required to prevent double withdrawals. This function
     *      makes assumptions that are not appropriate for non-DDX withdrawals.
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @param _withdrawalAmount The amount that the trader is attempting to
     *        withdraw.
     * @param _frozenBalance The frozen ddx balance found in the trader leaf.
     */
    function commitTraderWithdrawal(
        address _withdrawAddress,
        uint128 _withdrawalAmount,
        uint128 _frozenBalance
    ) internal {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        uint128 blockNumber = block.number.safe128("LibCollateral: Invalid block number.");

        // Ensure that the epoch ID is non-zero. This ensures that a checkpoint
        // has been registered prior to a withdrawal occuring.
        require(dsCheckpoint.currentEpochId > 0, "LibCollateral: Must have non-zero epoch ID for withdrawal.");

        uint128 scaledWithdrawalAmount = scaleAmountForToken(_withdrawalAmount, address(dsDerivaDEX.ddxToken));

        // We don't allow withdrawal amounts of zero since this will result
        // in the emission of events that do not carry any meaningful
        // information to the operator.
        require(scaledWithdrawalAmount > 0, "LibCollateral: Withdrawal amount of zero.");

        // We use the zero address to disambiguate DDX stored in the trader
        // leaf from tokens held in strategy leaves.
        address tokenAddress = address(0);

        // Apply the withdrawal rate limits to the DDX withdrawal.
        commitWithdrawalAllowance(dsDeposit, tokenAddress, _withdrawalAmount, blockNumber);

        // Commit the DDX withdrawal. To disambiguate withdrawals of DDX
        // from a trader leaf and withdrawals of DDX from a strategy leaf,
        // we use the zero address instead of the DDX address.
        commitWithdrawal(
            dsCheckpoint,
            dsDeposit,
            _withdrawAddress,
            bytes32(0),
            tokenAddress,
            _frozenBalance,
            scaledWithdrawalAmount,
            blockNumber
        );
    }

    /**
     * @dev Verifies a user's withdrawal and then commits the data that is
     *      required to prevent double withdrawals. This function should always
     *      be used to commit withdrawal state for structures that support
     *      multi-collateral (like strategy and insurance fund items).
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @param _strategyId The ID of the strategy that is being withdrawn from.
     * @param _withdrawalData The withdrawal data that specifies the users intended
     *        withdrawal.
     * @param _freeCollateral The free collateral contained in the SMT leaf.
     * @param _frozenCollateral The frozen collateral contained in the SMT leaf.
     */
    function commitStrategyWithdrawal(
        address _withdrawAddress,
        bytes32 _strategyId,
        SharedDefs.Balance128 memory _withdrawalData,
        SharedDefs.Balance256 memory _freeCollateral,
        SharedDefs.Balance128 memory _frozenCollateral
    ) internal {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();

        // Ensure that the epoch ID is non-zero. This ensures that a checkpoint
        // has been registered prior to a withdrawal occuring.
        require(dsCheckpoint.currentEpochId > 0, "LibCollateral: Must have non-zero epoch ID for withdrawal.");

        // Ensure that the withdrawal data is not empty and valid.
        require(
            _withdrawalData.tokens.length > 0 && _withdrawalData.tokens.length == _withdrawalData.amounts.length,
            "LibCollateral: Malformed withdrawal data."
        );

        // Ensure that the strategy is valid. This means that free collateral must
        // be valid and that frozen collateral must be non-empty and valid.
        require(
            _freeCollateral.tokens.length == _freeCollateral.amounts.length,
            "LibCollateral: Malformed free collateral data."
        );
        require(
            _frozenCollateral.tokens.length > 0 && _frozenCollateral.tokens.length == _frozenCollateral.amounts.length,
            "LibCollateral: Malformed frozen collateral data."
        );

        // Ensure that the provided withdrawal and strategy data are not malicous.
        uint256 jStart;
        uint128 blockNumber = block.number.safe128("LibCollateral: Invalid block number.");
        for (uint256 i = 0; i < _withdrawalData.tokens.length; i++) {
            // Scale the withdrawal amount so that it is consistent with
            // the precision used by the DerivaDEX operators.
            uint128 scaledWithdrawalAmount = scaleAmountForToken(_withdrawalData.amounts[i], _withdrawalData.tokens[i]);

            // We don't allow withdrawal amounts of zero since this will result
            // in the emission of events that do not carry any meaningful
            // information to the operator.
            require(scaledWithdrawalAmount > 0, "LibCollateral: Withdrawal amount of zero.");

            // Since j's starting point is monotonically increasing (an informal
            // "proof" for this is provided where jStart is updated below), a
            // frozen collateral could only be used twice if it was duplicated.
            // We check that this is not the case within the following for-loop.
            uint256 addressCount;
            for (uint256 j = jStart; j < _frozenCollateral.tokens.length; j++) {
                if (_withdrawalData.tokens[i] == _frozenCollateral.tokens[j]) {
                    require(addressCount == 0, "LibCollateral: Duplicate collateral data.");

                    // Apply the withdrawal rate limits. We make sure to do
                    // this before the withdrawal allowances are updated to
                    // ensure that the rate limits won't be affected by this
                    // withdrawal.
                    commitWithdrawalAllowance(
                        dsDeposit,
                        _withdrawalData.tokens[i], // token address
                        _withdrawalData.amounts[i], // unscaled withdrawal amount
                        blockNumber
                    );

                    // Commit the withdrawal.
                    commitWithdrawal(
                        dsCheckpoint,
                        dsDeposit,
                        _withdrawAddress,
                        _strategyId,
                        _withdrawalData.tokens[i], // token address
                        _frozenCollateral.amounts[j],
                        scaledWithdrawalAmount,
                        blockNumber
                    );

                    // Since addressCount is incremented after jStart is updated,
                    // the check that addressCount is zero ensures that jStart
                    // will only be updated once. Similarly, the check that
                    // addressCount must be one for each entry in _withdrawalData
                    // ensures that jStart  must be updated during each loop
                    // iteration. Therefore, jStart is monotonically increasing.
                    jStart = j + 1;
                    addressCount++;
                }
            }
            require(addressCount == 1, "LibCollateral: Missing address in frozen collateral.");
        }
    }

    /*
     * @dev Commit the necessary changes to the withdrawal allowance. To keep
     *      the gas costs for deposit function down, we use unscaled withdrawal
     *      amounts in this function.
     * @param _dsDeposit A pointer to the deposit diamond storage.
     * @param _tokenAddress The token that is being withdrawn.
     * @param _unscaledWithdrawalAmount The unscaled withdrawal amount.
     * @param _blockNumber The current block number scaled to uint128.
     */
    function commitWithdrawalAllowance(
        LibDiamondStorageDeposit.DiamondStorageDeposit storage _dsDeposit,
        address _tokenAddress,
        uint128 _unscaledWithdrawalAmount,
        uint128 _blockNumber
    ) internal {
        (uint128 maximumWithdrawalAmount, uint128 rateLimitRemaining, uint128 blocksRemaining) =
            getMaximumWithdrawal(_dsDeposit, _tokenAddress, _blockNumber);

        // Ensure that the unscaled withdrawal amount can be fulfilled.
        require(_unscaledWithdrawalAmount <= maximumWithdrawalAmount, "LibCollateral: Maximum withdrawal exceeded.");

        // Update the rate limit and withdrawal allowance state.
        DepositDefs.WithdrawalRateLimit storage rateLimits = _dsDeposit.withdrawalRateLimits[_tokenAddress];
        if (blocksRemaining == 0) {
            rateLimits.rateLimitForPeriod = rateLimitRemaining;
            rateLimits.consumedInPeriod = _unscaledWithdrawalAmount;
            rateLimits.rateLimitStartingBlock = _blockNumber;
        } else {
            rateLimits.consumedInPeriod = rateLimits.consumedInPeriod + _unscaledWithdrawalAmount;
        }
        _dsDeposit.withdrawalAllowances[_tokenAddress] =
            _dsDeposit.withdrawalAllowances[_tokenAddress] -
            _unscaledWithdrawalAmount;
    }

    /**
     * @dev Commits an individual withdrawal to the linked list storing the
     *      withdrawals that haven't been processed by the operator yet. The
     *      use of this function prevents double withdrawals from occuring
     *      since there is a check to verify that the withdrawal won't bring
     *      the account below the allowed withdrawal amount.
     *
     *      Despite the apparent potential for out-of-gas issues, this function
     *      should be safe from these issues since the gas cost for SLOAD
     *      operations is cheaper than the cost for SSTORE operations. With this
     *      said, any situation that would cause out-of-gas issues would have
     *      caused out-of-gas reverts when the entry was stored. This contradicts
     *      the assumption was stored.
     * @param _dsCheckpoint A pointer to the checkpoint facet's diamond storage state.
     * @param _dsDeposit A pointer to the deposit facet's diamond storage state.
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @param _strategyId The ID of the strategy that is being withdrawn from.
     * @param _tokenAddress The token that is being withdrawn from.
     * @param _frozenCollateralAmount The frozen collateral amount that is
     *        stored within the checkpointed sparse merkle tree. This is the
     *        first constraint that should be placed on a withdrawal.
     * @param _scaledWithdrawalAmount The requested withdrawal amount, scaled
     *        to 6 decimal places to ensure that it is comparable to the
     *        sparse merkle tree's data.
     * @param _blockNumber The current block number.
     */
    function commitWithdrawal(
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage _dsCheckpoint,
        LibDiamondStorageDeposit.DiamondStorageDeposit storage _dsDeposit,
        address _withdrawAddress,
        bytes32 _strategyId,
        address _tokenAddress,
        uint128 _frozenCollateralAmount,
        uint128 _scaledWithdrawalAmount,
        uint128 _blockNumber
    ) internal {
        // Compute the withdrawal reduction amount from the unprocessed
        // withdrawal list. Ensure that the desired withdrawal amount
        // is possible in spite of the withdrawal reduction amount.
        (uint128 withdrawalReductionAmount, uint128 start, uint128 end) =
            getUnprocessedWithdrawals(_dsCheckpoint, _dsDeposit, _withdrawAddress, _strategyId, _tokenAddress);
        require(
            _scaledWithdrawalAmount <= _frozenCollateralAmount - withdrawalReductionAmount,
            "LibCollateral: Invalid withdrawal amount."
        );

        // Update the linked list entry for this block with the scaled
        // withdrawal amount.
        DepositDefs.UnprocessedWithdrawalList storage unprocessedWithdrawalList =
            _dsDeposit.unprocessedWithdrawals[_withdrawAddress][_strategyId][_tokenAddress];
        unprocessedWithdrawalList.committedWithdrawals[_blockNumber].amount =
            unprocessedWithdrawalList.committedWithdrawals[_blockNumber].amount +
            _scaledWithdrawalAmount;

        // If start is zero, then we need to set the head to the block
        // number to ensure that the entry is recorded. If start is
        // non-zero and not equal to the head, we should prune some
        // unprocessed withdrawal entries.
        if (start == 0) {
            unprocessedWithdrawalList.head = _blockNumber;
        } else if (start != 0 && start != unprocessedWithdrawalList.head) {
            unprocessedWithdrawalList.head = start;
        }

        // If the end is non-zero and not equal to the block number,
        // we need to update the tail.
        if (end != 0 && end != _blockNumber) {
            unprocessedWithdrawalList.committedWithdrawals[end].tail = _blockNumber;
        }
    }

    /**
     * @dev Gets the maximum withdrawal amount permitted by the withdrawal
     *      rate limits and allowance.
     * @param _dsDeposit A pointer to the deposit diamond storage.
     * @param _tokenAddress The address of the token being withdrawn.
     * @param _blockNumber The current block number scaled to uint128.
     * @return The maximum withdrawal amount, the rate limit remaining, and the
     *         number of blocks before the rate limit needs to be reassessed.
     */
    function getMaximumWithdrawal(
        LibDiamondStorageDeposit.DiamondStorageDeposit storage _dsDeposit,
        address _tokenAddress,
        uint128 _blockNumber
    )
        internal
        view
        returns (
            uint128,
            uint128,
            uint128
        )
    {
        uint128 rateLimitPercentage = _dsDeposit.rateLimitPercentage;

        // If the rate limits for the period are zero or the rate limit period
        // has elapsed, we must reassess the rate limit for the new period.
        DepositDefs.WithdrawalRateLimit storage rateLimits = _dsDeposit.withdrawalRateLimits[_tokenAddress];
        if (
            rateLimits.rateLimitForPeriod == 0 ||
            _blockNumber >= rateLimits.rateLimitStartingBlock + _dsDeposit.rateLimitPeriod
        ) {
            // The rate limit for the period is the minimum rate limit for the
            // token plus the portion of the withdrawal allowances for the token
            // dictated by the rate limit percentage. We return a value of zero
            // for the number of blocks remaining to indicated that we should
            // update the rate limits.
            uint128 rateLimitRemaining =
                rateLimits.minimumRateLimit +
                    ((_dsDeposit.withdrawalAllowances[_tokenAddress] * rateLimitPercentage) / 100);
            return (rateLimitRemaining.min128(_dsDeposit.withdrawalAllowances[_tokenAddress]), rateLimitRemaining, 0);
        } else {
            uint128 rateLimitRemaining = rateLimits.rateLimitForPeriod - rateLimits.consumedInPeriod;
            return (
                rateLimitRemaining.min128(_dsDeposit.withdrawalAllowances[_tokenAddress]),
                rateLimitRemaining,
                rateLimits.rateLimitStartingBlock + _dsDeposit.rateLimitPeriod - _blockNumber
            );
        }
    }

    /*
     * @dev Gets the unprocessed withdrawals and some accompanying metadata
     *      for a given withdrawal address and token address. This function
     *      aggregates information from an in-storage linked list that is
     *      appended to whenever withdrawals are committed. In order to avoid
     *      another read of the linked list, this function returns the `start`
     *      and `end` of the section of the linked list that pertains to blocks
     *      greater than the last confirmed block.
     * @param _dsCheckpoint A pointer to the checkpoint facet's diamond storage state.
     * @param _dsDeposit A pointer to the deposit facet's diamond storage state.
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @param _strategyId The ID of the strategy that is being withdrawn from.
     * @param _tokenAddress The token that is being withdrawn.
     * @return amount The amount of unprocessed withdrawals found for the specified
     *         withdraw and token addresses.
     * @return start The start of the unprocessed withdrawals in the linked list.
     * @return end The end of the unprocessed withdrawals in the linked list.
     */
    function getUnprocessedWithdrawals(
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage _dsCheckpoint,
        LibDiamondStorageDeposit.DiamondStorageDeposit storage _dsDeposit,
        address _withdrawAddress,
        bytes32 _strategyId,
        address _tokenAddress
    )
        internal
        view
        returns (
            uint128 amount,
            uint128 start,
            uint128 end
        )
    {
        // Get a pointer to the start of the unprocessed withdrawals list. If
        // the current pointer is zero, we short-circuit to save gas.
        uint128 currentPtr = _dsDeposit.unprocessedWithdrawals[_withdrawAddress][_strategyId][_tokenAddress].head;
        if (currentPtr == 0) {
            return (0, 0, 0);
        }

        // While the current entry's amount is non-zero, we must continue the
        // aggregation routine since we are dealing with non-trivial withdrawal
        // entries.
        DepositDefs.UnprocessedWithdrawal memory current =
            _dsDeposit.unprocessedWithdrawals[_withdrawAddress][_strategyId][_tokenAddress].committedWithdrawals[
                currentPtr
            ];
        uint16 traversalCount = 1;
        while (current.amount > 0) {
            // Check whether the traversal depth has exceeded the limit. This prevents
            // the out-of-gas risk from an endless traversal.
            require(
                traversalCount < MAX_WITHDRAWALS_PER_PERIOD,
                "LibCollateral: Maximum withdrawals per period limit reached."
            );

            // If the current pointer is greater than the latest confirmed block
            // number, then the current entry represents an unprocessed withdrawal.
            if (currentPtr > _dsCheckpoint.latestConfirmedBlockNumber) {
                // If we haven't set the starting block, set it to the current
                // pointer so that consumers can prune the linked list.
                if (start == 0) {
                    start = currentPtr;
                }

                // Apply the current entry's amount to the cumulative unprocessed
                // withdrawal amount.
                amount = amount + current.amount;

                // Increment the traversal count
                traversalCount++;
            }

            // If the current tail is zero, there are no more entries to evaluate.
            if (current.tail == 0) {
                break;
            }

            // Update the current pointer and the current entry and continue the
            // aggregation routine.
            currentPtr = current.tail;
            current = _dsDeposit.unprocessedWithdrawals[_withdrawAddress][_strategyId][_tokenAddress]
                .committedWithdrawals[currentPtr];
        }

        // Set the end to the current pointer so that consumers know the entry
        // that needs an updated tail on insert. This is only set if the current
        // pointer represented an unprocessed withdrawal.
        if (currentPtr > _dsCheckpoint.latestConfirmedBlockNumber) {
            end = currentPtr;
        }

        return (amount, start, end);
    }

    /**
     * @dev Gets the processed withdrawals of a trader for a given token for
     *      a state in which the operator has a greater confirmed block number
     *      than the latest checkpointed confirmed block number. These
     *      processed withdrawals reflect the amount that the trader's current
     *      frozen collateral for the specified token will be debited, which
     *      makes it possible to calculate the amount of pending withdrawals
     * @param _dsCheckpoint A pointer to the checkpoint facet's diamond storage state.
     * @param _dsDeposit A pointer to the deposit facet's diamond storage state.
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @param _strategyId The ID of the strategy that is being withdrawn from.
     * @param _tokenAddress The address of the collateral that was withdrawn.
     * @param _blockNumber The confirmed block number to use for the query.
     * @return amount The processed withdrawal amount for the given range.
     */
    function getProcessedWithdrawals(
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage _dsCheckpoint,
        LibDiamondStorageDeposit.DiamondStorageDeposit storage _dsDeposit,
        address _withdrawAddress,
        bytes32 _strategyId,
        address _tokenAddress,
        uint128 _blockNumber
    ) internal view returns (uint128 amount) {
        // Get a pointer to the start of the unprocessed withdrawals list. If
        // the current pointer is zero, we short-circuit to save gas.
        uint128 currentPtr = _dsDeposit.unprocessedWithdrawals[_withdrawAddress][_strategyId][_tokenAddress].head;
        if (currentPtr == 0) {
            return 0;
        }

        // While the current entry's amount is non-zero, we must continue the
        // aggregation routine since we are dealing with non-trivial withdrawal
        // entries.
        DepositDefs.UnprocessedWithdrawal memory current =
            _dsDeposit.unprocessedWithdrawals[_withdrawAddress][_strategyId][_tokenAddress].committedWithdrawals[
                currentPtr
            ];
        while (current.amount > 0 && currentPtr <= _blockNumber) {
            // If the current pointer is greater than the latest confirmed block
            // number, then the current entry represents an unprocessed withdrawal.
            if (currentPtr > _dsCheckpoint.latestConfirmedBlockNumber) {
                // Apply the current entry's amount to the cumulative unprocessed
                // withdrawal amount.
                amount = amount + current.amount;
            }

            // If the current tail is zero, there are no more entries to evaluate.
            if (current.tail == 0) {
                break;
            }

            // Update the current pointer and the current entry and continue the
            // aggregation routine.
            currentPtr = current.tail;
            current = _dsDeposit.unprocessedWithdrawals[_withdrawAddress][_strategyId][_tokenAddress]
                .committedWithdrawals[currentPtr];
        }

        return amount;
    }

    /**
     * @dev Given a starting and an ending epoch ID, this function will calculate
     *      the amount of trade mining rewards that should be distributed for the
     *      range. This function does not verify that `_start` <= `_end` to save
     *      on gas, and it will be higher than the actual distribution by a dust
     *      amount (a small amount of wei) for some epochs.
     * @param _start The starting epoch ID of the range.
     * @param _end The ending epoch ID of the range.
     * @return The rewards that were distributed during the range.
     */
    function calculateTradeMiningRewardsForRange(uint128 _start, uint128 _end) internal pure returns (uint128) {
        // If the start is greater than or equal to the ending epoch of trade mining,
        // there are no more rewards to provide.
        uint128 trade_mining_end = TRADE_MINING_LENGTH * TRADE_MINING_EPOCH_MULTIPLIER;
        if (_start >= trade_mining_end) {
            return 0;
        }

        // We calculate the trade mining rewards for the range as
        // `reward * reward_epochs`. To calculate the `reward_epochs` value,
        // we take the sum of the number of complete epochs contained in
        // `[_start, min(end, TRADE_MINING_END)]` and one extra epoch if the
        // remaining part of the range contains a trade mining epoch. We use
        // `min(end, TRADE_MINING_END)` as the end of the range to avoid
        // crediting trade mining rewards after trade mining ends.
        uint128 end = _end.min128(trade_mining_end);
        uint128 reward_epochs =
            ((end - _start) / TRADE_MINING_EPOCH_MULTIPLIER) +
                ((end % TRADE_MINING_EPOCH_MULTIPLIER) < (_start % TRADE_MINING_EPOCH_MULTIPLIER) ? 1 : 0);
        return TRADE_MINING_REWARD_PER_EPOCH * reward_epochs;
    }

    /**
     * @dev Scales a token amount so that it can be handled appropriately by the
     *      DerivaDEX operators.
     * @param _amount The token amount to scale.
     * @param _tokenAddress The token address.
     * @return The scaled token amount.
     */
    function scaleAmountForToken(uint128 _amount, address _tokenAddress) internal view returns (uint128) {
        uint256 decimals = IERC20Extended(_tokenAddress).decimals();

        if (decimals > OPERATOR_DECIMALS) {
            require(_amount % 10**(decimals - OPERATOR_DECIMALS) == 0, "LibCollateral: Unsupported precision level.");
            _amount =
                _amount /
                (10**(decimals - OPERATOR_DECIMALS)).safe128("LibCollateral: Unsupported decimal amount.");
        } else if (decimals < OPERATOR_DECIMALS) {
            _amount =
                _amount *
                (10**(OPERATOR_DECIMALS - decimals)).safe128("LibCollateral: Unsupported Decimal amount.");
        }

        return _amount;
    }

    /*
     * @dev Generate the merkle path for the specified strategy. Strategy keys
     *      have the following schema:
     *        1. The first byte is the item discriminant.
     *        2. The second byte is the chain discriminant (zero for Ethereum).
     *        3. The next twenty bytes are filled with the trader address.
     *        4. The next four bytes are filled with the first four bytes of
     *           the strategy ID.
     * @param _traderAddress The trader address to use during key generation.
     * @param _strategyId The strategy ID to use when generating the key.
     * @return The generated strategy key.
     */
    function generateStrategyKey(address _traderAddress, bytes32 _strategyId) internal pure returns (bytes32) {
        uint256 strategyKey = uint256(generateTraderGroupKey(SharedDefs.ItemKind.Strategy, _traderAddress));
        strategyKey |= (uint256(keccak256(abi.encode(_strategyId))) >> 224) << 48;
        return bytes32(strategyKey);
    }

    /**
     * @dev Build a trader group key given the specified trader and item type.
     *      Trader keys have the following schema:
     *        1. The first byte is the item discriminant.
     *        2. The second byte is the chain discriminant (zero for Ethereum).
     *        3. The next twenty bytes are filled with the trader address.
     * @param _itemKind The item kind of the key.
     * @param _traderAddress The trader address to use during key generation.
     * @return The generated trader group key.
     */
    function generateTraderGroupKey(SharedDefs.ItemKind _itemKind, address _traderAddress)
        internal
        pure
        returns (bytes32)
    {
        uint256 key = uint256(_itemKind) << 248;
        key |= uint256(uint160(_traderAddress)) << 80;
        return bytes32(key);
    }
}
