// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { LibBytes } from "./LibBytes.sol";
import { LibBytes32 } from "./LibBytes32.sol";
import { LibStack } from "./LibStack.sol";

/**
 * @title LibSMT
 * @author DerivaDEX
 * @dev This library implements several utility functions that are useful for
 *      performing cryptographic computations in correspondance with the SMT
 *      library that is used by the DerivaDEX operator. The source code for this
 *      SMT implementation can be found at https://github.com/jjyr/sparse-merkle-tree.
 */
library LibSMT {
    using LibBytes for bytes;
    using LibBytes32 for bytes32;
    using LibSMT for LibStack.Stack;
    using LibStack for LibStack.Stack;
    using LibSMT for State;

    bytes1 private constant HASH_SIBLINGS_OPCODE = 0x48;
    bytes1 private constant PUSH_LEAF_OPCODE = 0x4C;
    bytes1 private constant HASH_PROOF_ELEMENT_OPCODE = 0x50;

    struct State {
        uint256 proofIndex;
        uint256 leafIndex;
        LibStack.Stack stack;
    }

    struct Leaf {
        bytes32 key;
        bytes32 value;
    }

    /**
     * @dev Computes the merkle root that is specified by the compiled merkle
     *      proof that was provided. Unlike a conventional merkle proof, the
     *      compiled merkle proofs that are used by
     *      https://github.com/jjyr/sparse-merkle-tree are programs of a very
     *      simple language. In order to compute the root, we must execute the
     *      programs. All well-formed programs in this language will end
     *      execution with a single leaf remaining on the stack.
     *
     *      The leaves that are provided to this computation must be sorted.
     *      The SMT library sorts the leaves prior to computing the merkle
     *      proof; however, we avoid sorting leaves on chain as this would be
     *      fairly expensive. Since the encoding used by the SMT library is
     *      little endian, comparisons are also somewhat gas intensive. This
     *      means that verifying that leaves are sorted is more expensive than
     *      it would be if the encoding was big endian, which makes this
     *      validation undesireable. The main rationale that we use to justify
     *      omitting this validation is that an improper ordering of leaves is
     *      itself an invalid merkle proof, so it does not introduce much (if
     *      any) additional risk to the library.
     * @param _leaves The leaves to use when computing the merkle root of the
     *        virtual sparse merkle tree.
     * @param _proof The compiled merkle proof to execute.
     * @return The merkle root that was output by the compiled merkle proof.
     */
    function computeRoot(Leaf[] memory _leaves, bytes memory _proof) internal pure returns (bytes32) {
        State memory state;
        state.stack = LibStack.init();
        while (state.proofIndex < _proof.length) {
            bytes1 opcode = _proof[state.proofIndex];
            state.proofIndex += 1;
            if (opcode == HASH_SIBLINGS_OPCODE) {
                state.operationHashSiblings(_proof);
            } else if (opcode == PUSH_LEAF_OPCODE) {
                state.operationPushLeaf(_leaves);
            } else if (opcode == HASH_PROOF_ELEMENT_OPCODE) {
                state.operationHashProofElement(_proof);
            } else {
                revert("LibSMT: Invalid code.");
            }
        }
        if (state.stack.size() != 0x40) {
            revert("LibSMT: Invalid stack size after execution.");
        }
        bytes32 root = state.stack.popItem();
        return root;
    }

    /**
     * @dev Implements the "hash siblings" opcode for the compiled merkle proof
     *      interpreter.
     * @param _state The compiled merkle proof execution state that should be
     *        operated on.
     * @param _proof The compiled merkle proof that is being executed.
     */
    function operationHashSiblings(State memory _state, bytes memory _proof) internal pure {
        // Ensure that the proof is long enough to hash two siblings.
        if (_state.proofIndex >= _proof.length) {
            revert("LibSMT: Proof length too short for operationHashSiblings.");
        }

        // Get the height of the siblings from the proof.
        uint8 height = uint8(_proof[_state.proofIndex]);
        _state.proofIndex += 1;

        // Pop the top two nodes from the stack.
        (bytes32 keyB, bytes32 valueB) = _state.stack.popNode();
        (bytes32 keyA, bytes32 valueA) = _state.stack.popNode();

        // Get the parent keys of both nodes.
        bytes32 parentKeyA = keyA.copyBits(height);
        bytes32 parentKeyB = keyB.copyBits(height);

        // Get the "set bit" of the keys at the height. This set bit determines
        // which side of the tree the nodes fall at the given height.
        bool setA = keyA.getBit(height);
        bool setB = keyB.getBit(height);

        // Get the sibling key of A. If the nodes are siblings, this should be
        // equal to the parent key of B.
        bytes32 siblingKeyA = parentKeyA;
        if (!setA) {
            siblingKeyA = siblingKeyA.setBit(height);
        }

        // Ensure that the two nodes being hashed are siblings.
        if (siblingKeyA != parentKeyB || setA == setB) {
            revert("LibSMT: Values are not siblings.");
        }

        // Get the parent hash of the two nodes.
        bytes32 parent;
        if (keyA.getBit(height)) {
            parent = merge(valueB, valueA);
        } else {
            parent = merge(valueA, valueB);
        }

        // Push the parent node to the stack.
        pushNode(_state.stack, parentKeyA, parent);
    }

    /**
     * @dev Implements the "push leaf" opcode for the compiled merkle proof
     *      interpreter.
     * @param _state The compiled merkle proof execution state that should be
     *        operated on.
     * @param _leaves The leaves that are being used as input for the merkle
     *        proof execution.
     */
    function operationPushLeaf(State memory _state, Leaf[] memory _leaves) internal pure {
        // Ensure that there are enough leaves to push a new leaf.
        if (_state.leafIndex >= _leaves.length) {
            revert("LibSMT: Corrupted leaf index.");
        }

        // Push the updated leaf to the stack.
        pushNode(_state.stack, _leaves[_state.leafIndex].key, hashLeaf(_leaves[_state.leafIndex]));
        _state.leafIndex += 1;
    }

    /**
     * @dev Implements the "hash proof element" opcode for the compiled merkle
     *      proof interpreter.
     * @param _state The compiled merkle proof execution state that should be
     *        operated on.
     * @param _proof The compiled merkle proof that is being executed.
     */
    function operationHashProofElement(State memory _state, bytes memory _proof) internal pure {
        // Ensure that the proof is long enough to hash a new proof element.
        if (_state.proofIndex + 33 > _proof.length) {
            revert("LibSMT: Proof length is too short for operationHashProofElement");
        }

        // Get the height of the proof elements from the proof.
        uint8 height = uint8(_proof[_state.proofIndex]);
        _state.proofIndex += 1;

        // Get the data to hash from the proof.
        bytes32 data = _proof.readBytes32(_state.proofIndex);
        _state.proofIndex += 32;

        // Pop the top leaf off of the stack.
        (bytes32 key, bytes32 value) = _state.stack.popNode();

        // Get the parent key of the popped leaf.
        bytes32 parentKey = key.parentPath(height);

        // Hash the proof element extracted from the proof and the popped leaf.
        bytes32 parent;
        if (key.getBit(height)) {
            parent = merge(data, value);
        } else {
            parent = merge(value, data);
        }

        // Push the newly hashed value to the stack.
        pushNode(_state.stack, parentKey, parent);
    }

    /**
     * @dev Computes the hash of an SMT leaf.
     * @param _leaf The leaf to hash.
     * @return hash The hash that was computed.
     */
    function hashLeaf(Leaf memory _leaf) internal pure returns (bytes32 hash) {
        if (_leaf.value == 0x0) {
            return 0x0;
        }
        assembly {
            // Use scratch space
            mstore(0x0, mload(_leaf))
            mstore(0x20, mload(add(_leaf, 0x20)))
            hash := keccak256(0x0, 0x40)
        }
        return hash;
    }

    /**
     * @dev Computes the hash of two branch hashes. Implements an optimization that
     *      prevents us from needing to cache a pre-computed hash set. See
     *      https://ethresear.ch/t/optimizing-sparse-merkle-trees/3751/7 for more
     *      details.
     * @param _left The value that should be hashed on the left.
     * @param _right The value that should be hashed on the right.
     * @return hash The hash that was computed.
     */
    function merge(bytes32 _left, bytes32 _right) internal pure returns (bytes32 hash) {
        if (_left == 0x0) {
            return _right;
        }
        if (_right == 0x0) {
            return _left;
        }
        assembly {
            // Use scratch space
            mstore(0x0, _left)
            mstore(0x20, _right)
            hash := keccak256(0x0, 0x40)
        }
        return hash;
    }

    /**
     * @dev Pops a node off of the stack.
     * @param _stack The stack that should be popped from.
     * @return The key and value of the leaf on the stack.
     */
    function popNode(LibStack.Stack memory _stack) internal pure returns (bytes32, bytes32) {
        bytes32 value = _stack.popItem();
        bytes32 key = _stack.popItem();
        return (key, value);
    }

    /**
     * @dev Pushes a node onto the stack.
     * @param _key The key that should be pushed.
     * @param _value The value that should be pushed.
     */
    function pushNode(
        LibStack.Stack memory _stack,
        bytes32 _key,
        bytes32 _value
    ) internal pure {
        LibStack.pushItem(_stack, _key);
        LibStack.pushItem(_stack, _value);
    }
}
