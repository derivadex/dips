// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { LibDiamondStorageReentrancyGuard } from "../storage/LibDiamondStorageReentrancyGuard.sol";

// solhint-disable max-line-length
// Original OpenZeppelin ReentrancyGuard contract copied from
// https://github.com/OpenZeppelin/openzeppelin-contracts/blob/de99bccbfd4ecd19d7369d01b070aa72c64423c9/contracts/utils/ReentrancyGuard.sol
// solhint-enable max-line-length

/**
 * @dev Contract module that helps prevent reentrant calls to a function. Note
 *      that this is a version of OpenZeppelin's ReentrancyGuard contract adapted
 *      for use with the Diamond Standard. The comments have been ported from
 *      the original OpenZeppelin contract.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot's contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler's defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction's gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        LibDiamondStorageReentrancyGuard.DiamondStorageReentrancyGuard storage dsReentrancyGuard =
            LibDiamondStorageReentrancyGuard.diamondStorageReentrancyGuard();

        // On the first call to nonReentrant, _notEntered will be true
        require(dsReentrancyGuard.status != _ENTERED, "ReentrancyGuard: reentrant call");

        // Any calls to nonReentrant after this point will fail
        dsReentrancyGuard.status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        dsReentrancyGuard.status = _NOT_ENTERED;
    }
}
