// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

/**
 * @title LibStack
 * @author DerivaDEX
 * @dev This library provides an implementation of an in memory stack for Solidity.
 *      It can be quite unsafe if it is not used properly. In particular, this libary
 *      assumes that no memory will be allocated while it is being used. Any violation
 *      of this assumption can lead to memory being corrupted.
 */
library LibStack {
    struct Stack {
        uint256 start_ptr;
        uint256 end_ptr;
    }

    /**
     * Initializes a new in-memory stack.
     * @return stack A newly initialized stack.
     */
    function init() internal pure returns (Stack memory stack) {
        uint256 ptr;
        assembly {
            ptr := mload(0x40)
        }
        stack.start_ptr = ptr;
        stack.end_ptr = ptr;
        return stack;
    }

    /**
     * Pushes an item to the stack.
     * @param _item The item to push to the stack.
     */
    function pushItem(Stack memory _stack, bytes32 _item) internal pure {
        uint256 free_ptr;
        assembly {
            free_ptr := mload(0x40)
        }

        if (free_ptr != _stack.end_ptr) {
            revert("LibStack: extra memory allocation detected.");
        }

        assembly {
            // Push the item to the stack
            mstore(free_ptr, _item)

            // Update the free memory pointer
            mstore(0x40, add(free_ptr, 0x20))
        }

        // Update the end pointer in the stack struct
        _stack.end_ptr += 32;
    }

    /**
     * Pops an item from the stack. This function will revert if the stack is too
     * small to pop an item.
     * @param _stack The stack to pop a value from.
     * @return item The item that was popped from the stack.
     */
    function popItem(Stack memory _stack) internal pure returns (bytes32 item) {
        // Get the start pointer of the stack and the free memory pointer.
        uint256 start_ptr = _stack.start_ptr;
        uint256 free_ptr;
        assembly {
            free_ptr := mload(0x40)
        }

        if (free_ptr != _stack.end_ptr) {
            revert("LibStack: extra memory allocation detected.");
        }

        // Ensure that there is enough space on the stack to pop an item.
        if (free_ptr < start_ptr + 32) {
            revert("LibStack: Nothing to pop.");
        }

        // Pop the item off of the stack and update the free memory pointer.
        assembly {
            free_ptr := sub(free_ptr, 0x20)
            item := mload(free_ptr)
            mstore(0x40, free_ptr)
        }

        // Update the end pointer in the stack struct
        _stack.end_ptr -= 32;

        return item;
    }

    /**
     * Returns the size of the stack in bytes.
     * @return The size of the stack.
     */
    function size(Stack memory _stack) internal pure returns (uint256) {
        uint256 free_ptr;
        assembly {
            free_ptr := mload(0x40)
        }
        if (free_ptr != _stack.end_ptr) {
            revert("LibStack: extra memory allocation detected.");
        }
        if (free_ptr < _stack.start_ptr) {
            revert("LibStack: Corrupted stack.");
        }
        return free_ptr - _stack.start_ptr;
    }
}
