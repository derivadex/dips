// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

/**
 * @title LibBytes32
 * @author DerivaDEX
 * @dev This library provides several bit twiddling operations on bytes32 values
 *      that are interpreted as numbers in the little endian encoding used by
 *      https://github.com/jjyr/sparse-merkle-tree. To further elaborate, the
 *      bytes are arranged in ascending significance from left to right, and
 *      within a given byte, the bits are arranged in descending significance
 *      from left to right. For example, the number 5 would be represented would
 *      be represented as 0000010100000000...0000000000000000.
 */
library LibBytes32 {
    uint8 internal constant BYTE_SIZE = 8;

    /**
     * @dev Get the parent path of a bytes32 value that is interpreted as a key
     *      in a binary search tree.
     * @param _key The key to use to get a parent.
     * @param _height The height of the node that is indexed by this key.
     */
    function parentPath(bytes32 _key, uint8 _height) internal pure returns (bytes32) {
        if (_height == 255) {
            return 0x0;
        }
        return copyBits(_key, _height + 1);
    }

    /**
     * Example: setBit(0000010100000000...0000000000000000, 6)
     *
     * bytePosition = 6 / 8 = 0 => Least significant byte
     * bitPosition = 6 % 8 = 6 => 6th least significant bit within byte
     * mask = 1 << (31 - 0) * 8 + 6 = 1 << 254 = 01000000...00000000
     * return 0000010100000000...0000000000000000 | 01000000...00000000
     *     = 0100010100000000...0000000000000000
     *
     * @dev Sets a bit in a provided bytes32 value.
     * @param _value The value.
     * @param _idx The index that the bit should be set at.
     * @return The updated value.
     */
    function setBit(bytes32 _value, uint8 _idx) internal pure returns (bytes32) {
        uint256 bytePosition = _idx / BYTE_SIZE;
        uint256 bitPosition = _idx % BYTE_SIZE;
        uint256 mask = 1 << ((31 - bytePosition) * 8 + bitPosition);
        return bytes32(uint256(_value) | mask);
    }

    /**
     * Example: getBit(0000010100000000...0000000000000000, 2)
     *
     * bytePosition = 2 / 8 = 0 => Least significant byte
     * bitPosition = 2 % 8 = 2 => 2nd least significant bit within byte
     * mask = 1 << (31 - 0) * 8 + 2 = 1 << 250 = 00000100...00000000
     * return 0000010100000000...0000000000000000 & 00000100...00000000 != 0
     *     => 0000010000000000...0000000000000000 != 0 = True
     *
     * @dev Get a bit from a provided bytes32 value at a given index.
     * @param _value The value.
     * @param _idx The index to get the bit from.
     * @return bit The bit that was obtained from the value.
     */
    function getBit(bytes32 _value, uint8 _idx) internal pure returns (bool bit) {
        uint256 bytePosition = _idx / BYTE_SIZE;
        uint256 bitPosition = _idx % BYTE_SIZE;
        uint256 mask = 1 << ((31 - bytePosition) * 8 + bitPosition);
        return uint256(_value) & mask != 0;
    }

    /**
     * Example: copyBits(0100010100000000...0001100010001010, 6)
     *
     * bytePosition = 6 / 8 = 0 => Least significant byte
     * bitPosition = 6 % 8 = 6 => 6th least significant bit within byte
     * mask = 2 ** ((31 - 0) * 8) - 1 = 0000000011111111...11111111
     * result = 0100010100000000...0001100010001010 & 0000000011111111...11111111
     *     = 0000000000000000...0001100010001010
     * mask = (2 ** 8 - 1) - (2 ** 6 - 1) = 11000000
     * byteValue = byte(0, 0100010100000000...0001100010001010) = 01000101
     * return 0000000000000000...0001100010001010 | (01000101 & 11000000) << 248
     *     = 0000000000000000...0001100010001010 | 01000000 << 248
     *     = 0000000000000000...0001100010001010 | 0100000000000000...00000000
     *     = 0100000000000000...0001100010001010
     *
     * @dev Copy the most significant bits of a value starting at a provided index.
     * @param _value The value.
     * @param _start The starting index.
     * @return The copied bits.
     */
    function copyBits(bytes32 _value, uint8 _start) internal pure returns (bytes32) {
        uint256 bytePosition = _start / BYTE_SIZE;
        uint256 bitPosition = _start % BYTE_SIZE;

        // Copy the most significant bytes.
        bytes32 result;
        if (bytePosition != 31) {
            uint256 mask = 2**((31 - bytePosition) * 8) - 1;
            result = bytes32(uint256(_value) & mask);
        }

        // Copy the remaining significant bits.
        uint256 mask = (2**8 - 1) - (2**bitPosition - 1);
        uint256 byteValue;
        assembly {
            byteValue := byte(bytePosition, _value)
        }
        return bytes32(uint256(result) | ((byteValue & mask) << ((31 - bytePosition) * 8)));
    }
}
