// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { SharedDefs } from "./SharedDefs.sol";

/**
 * @title FundedInsuranceFundDefs
 * @author DerivaDEX
 *
 * This library contains the common structs and enums pertaining to
 * the funded insurance fund.
 */
library FundedInsuranceFundDefs {
    struct FundedInsuranceFundPositionData {
        // The collateral that is available to trade on the DerivaDEX exchange.
        SharedDefs.Balance256 freeBalance;
        // The collateral that is available to withdraw from the contracts.
        SharedDefs.Balance128 frozenBalance;
    }

    struct FundedInsuranceFundPositionEncoding {
        SharedDefs.ItemKind discriminant;
        FundedInsuranceFundPositionData positionData;
    }

    enum FundedInsuranceFundUpdateKind { Deposit, Withdraw }
}
