// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

library CheckpointDefs {
    struct CheckpointData {
        // The last confirmed block number processed within the enclave.
        uint128 blockNumber;
        // The last confirmed block hash processed within the enclave. Using
        // the block number and the block hash, we can use the Solidity
        // built-in `blockhash` to verify that the chain that has been
        // processed by the enclave is the same chain that the `checkpoint`
        // transaction is being executed in.
        bytes32 blockHash;
        // The root hash of the sparse merkle tree that is used by the
        // operators to maintain the exchange state.
        bytes32 stateRoot;
        // The root hash of the sparse merkle tree that includes the
        // transaction logs from the last epoch.
        bytes32 transactionRoot;
    }

    struct CheckpointSubmission {
        // The checkpoint data that is being attested to by the signers.
        CheckpointData checkpointData;
        // A list of signatures providing attestation to the checkpoint data.
        bytes[] signatures;
    }

    struct CheckpointSubmissionIndex {
        // The index in the CheckpointSubmission array
        uint256 submissionIndex;
        // The index in the signatures array from a CheckpointSubmission
        uint256 signatureIndex;
    }
}
