// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

/**
 * @title SpecsDefs
 * @author DerivaDEX
 *
 * This library contains the common structs and enums pertaining to
 * specs.
 */
library SpecsDefs {
    enum SpecsUpdateKind { Upsert, Remove }
}
