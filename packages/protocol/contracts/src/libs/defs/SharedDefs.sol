// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

/**
 * @title SharedDefs
 * @author DerivaDEX
 *
 * This library contains common structs and enums that are used by multiple facets.
 */
library SharedDefs {
    struct Balance128 {
        address[] tokens;
        uint128[] amounts;
    }

    struct Balance256 {
        address[] tokens;
        uint256[] amounts;
    }

    enum ItemKind {
        Empty,
        Trader,
        Strategy,
        Position,
        BookOrder,
        Price,
        InsuranceFund,
        Stats,
        Release,
        Specs,
        InsuranceFundContribution,
        Signer
    }

    // Type of collateral
    enum Flavor { Vanilla, Compound, Aave }
}
