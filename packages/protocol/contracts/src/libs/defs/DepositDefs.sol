// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { SharedDefs } from "./SharedDefs.sol";

/**
 * @title DepositDefs
 * @author DerivaDEX
 *
 * This library contains the common structs and enums pertaining to
 * deposits and withdrawals.
 */
library DepositDefs {
    struct ExchangeCollateral {
        address underlyingToken;
        SharedDefs.Flavor flavor;
        bool isListed;
    }

    struct StrategyDataEncoding {
        SharedDefs.ItemKind discriminant;
        StrategyData strategyData;
    }

    struct StrategyData {
        bytes32 strategyId;
        // The collateral that is available to trade on the DerivaDEX exchange.
        SharedDefs.Balance256 freeCollateral;
        // The collateral that is available to withdraw from the contracts.
        SharedDefs.Balance128 frozenCollateral;
        uint64 maxLeverage;
        bool frozen;
    }

    struct TraderDataEncoding {
        SharedDefs.ItemKind discriminant;
        TraderData traderData;
    }

    struct TraderData {
        /// The DDX balance stated / usable for trading
        uint128 freeDDXBalance;
        /// The DDX balance frozen for withdrawal
        uint128 frozenDDXBalance;
        /// The Ethereum account who referred the trader (receives DDX rewards as per referral program)
        address referralAddress;
        /// Whether to use DDX for fees
        bool payFeesInDDX;
    }

    struct UnprocessedWithdrawalList {
        // The first block in the linked list that is unprocessed. This value
        // can be used to prune the withdrawal list by updating the value when
        // the latest confirmed block has been increased.
        uint128 head;
        // A mapping from block number to unprocessed withdrawal entries. This
        // mapping is the storage component of the linked list.
        mapping(uint128 => UnprocessedWithdrawal) committedWithdrawals;
    }

    struct UnprocessedWithdrawal {
        // The amount that was withdrawn in this block.
        uint128 amount;
        // The next block in the linked list. If the next entry hasn't been
        // written, this value will be set to zero.
        uint128 tail;
    }

    struct WithdrawalRateLimit {
        // The calculated rate limit for the period.
        uint128 rateLimitForPeriod;
        // The block in which the withdrawal rate limit was last reset.
        uint128 rateLimitStartingBlock;
        // The rate limit consumed in the period.
        uint128 consumedInPeriod;
        // The minimum amount that should be withdrawable from this token.
        // This amount should not be scaled to 18 decimals and should reflect
        // the minimum rate limit scaled appropriately for the token that this
        // entry refers to.
        uint128 minimumRateLimit;
    }

    enum StrategyUpdateKind { Deposit, Withdrawal }

    enum TraderUpdateKind { Deposit, Withdrawal }
}
