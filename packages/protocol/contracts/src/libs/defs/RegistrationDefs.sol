// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

library RegistrationDefs {
    struct Custodian {
        // The DDX balance of the custodian.
        uint128 balance;
        // The block in which the custodian can unbond themselves.
        uint128 unbondETA;
        // Indicates whether or not the custodian is approved to register
        // signers.
        bool approved;
        // Indicates whether or not the custodian was jailed due to submitting
        // a non-matching hash or at the discretion of governance.
        bool jailed;
        // Mapping from release hash to signer address.
        mapping(bytes32 => address) signers;
    }

    struct CustodianWithoutSigners {
        // The DDX balance of the custodian.
        uint128 balance;
        // The block in which the custodian can unbond themselves.
        uint128 unbondETA;
        // Indicates whether or not the custodian is approved to register
        // signers.
        bool approved;
        // Indicates whether or not the custodian was jailed due to submitting
        // a non-matching hash or at the discretion of governance.
        bool jailed;
    }

    struct RegistrationMetadata {
        // An enclave measurement taken by an SGX chip.
        bytes32 mrEnclave;
        // The security version of the enclave.
        bytes2 isvSvn;
    }

    struct ReleaseScheduleEntry {
        // The registration metadata hash of the release. In the case that the
        // entry was created as the current entry, this field will be empty as
        // the release hash will be stored in the `currentReleaseHash` field
        // of `DiamondStorageRegistration`.
        bytes32 releaseHash;
        // A pointer to the next entry in the release schedule. Since releases,
        // are indexed by their starting epoch ID, this pointer is the
        // starting epoch ID of the next release.
        uint128 nextEpochId;
    }

    struct ScheduledRelease {
        // The registration metadata hash of the release.
        bytes32 releaseHash;
        // The starting epoch ID of the release.
        uint128 startingEpochId;
    }

    struct Signer {
        // The custodian address of the signer.
        address custodian;
        // The RLP encoded remote attestation report that registered this
        // enclave.
        bytes report;
    }
}
