// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { DummyToken } from "./DummyToken.sol";

contract CompetitionDummyToken is DummyToken {
    address public derivaDAOAddress;
    address internal deployer;

    mapping(address => bool) public transferStatuses;

    modifier onlyDeployer {
        require(msg.sender == deployer, "CompetitionDummyToken: sender is not deployer.");
        _;
    }

    constructor(
        string memory name,
        string memory symbol,
        uint8 decimals,
        address derivaDAOAddress_
    ) public payable DummyToken(name, symbol, decimals) {
        deployer = msg.sender;
        derivaDAOAddress = derivaDAOAddress_;
    }

    function setTransferStatuses(address[] memory _addresses, bool[] memory _statuses) external onlyDeployer {
        require(
            _addresses.length > 0 && _addresses.length == _statuses.length,
            "CompetitionDummyToken: malformed input."
        );
        for (uint256 i = 0; i < _addresses.length; i++) {
            transferStatuses[_addresses[i]] = _statuses[i];
        }
    }

    function _mint(address _account, uint256 _amount) internal override {
        require(
            (msg.sender == deployer || transferStatuses[msg.sender] || deployer == address(0)) &&
                _amount < 1 << (128 - 1),
            "CompetitionDummyToken: invalid mint."
        );
        super._mint(_account, _amount);
    }

    function _transfer(
        address from,
        address to,
        uint256 value
    ) internal override {
        require(
            transferStatuses[from] || from == derivaDAOAddress || from == deployer || to == derivaDAOAddress,
            "CompetitionDummyToken: invalid transfer."
        );
        super._transfer(from, to, value);
    }
}
