// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { Base64 } from "../libs/Base64.sol";
import { SolRsaVerify } from "../libs/SolRsaVerify.sol";
import { LibRelease } from "../libs/LibRelease.sol";
import { MathHelpers } from "../libs/MathHelpers.sol";
import { StringUtils } from "../libs/StringUtils.sol";
import { LibBytes } from "../libs/LibBytes.sol";
import { RegistrationDefs } from "../libs/defs/RegistrationDefs.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStorageRegistration } from "../storage/LibDiamondStorageRegistration.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStoragePause } from "../storage/LibDiamondStoragePause.sol";
import { LibDiamondStorageGovernance } from "../storage/LibDiamondStorageGovernance.sol";

/**
 * @title Registration
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to signer registration for the DerivaDEX
 *         diamond.
 */
contract Registration {
    using LibBytes for bytes;
    using MathHelpers for uint256;
    using StringUtils for *;

    event FastPathFunctionSignaturesSet(string[] fastPathSignatures, bool[] statuses);

    event SignerRegistered(bytes32 indexed releaseHash, address indexed custodian, address indexed signer);

    event RegistrationInitialized(address[] initialCustodians, bytes32 mrEnclave, bytes2 isvSvn);

    event ReleaseScheduleUpdated(bytes32 indexed mrEnclave, bytes2 indexed isvSvn, uint128 indexed startingEpochId);

    /**
     * @notice Ensures function can only be called when the contract is not
     *         paused.
     */
    modifier isNotPaused {
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        require(!dsPause.isPaused, "Registration: paused.");
        _;
    }

    /**
     * @notice Limits functions to only be called via governance.
     */
    modifier onlyAdmin {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        require(msg.sender == dsDerivaDEX.admin, "Registration: must be called by Gov.");
        _;
    }

    /**
     * @notice Allows governance to initialize the Registration facet.
     * @dev This function is intended to be the initialization target
     *      of the diamond cut function. This function is not included
     *      in the selectors being added to the diamond, meaning it
     *      cannot be called again.
     * @param _releaseMetadata The release metadata, including the mrEnclave
     *         and isvSvn as well as any other relevant registration metadata.
     * @param _custodians An initial list of custodians to approve for
     *        registration.
     */
    function initialize(RegistrationDefs.RegistrationMetadata memory _releaseMetadata, address[] calldata _custodians)
        external
        onlyAdmin
    {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        LibDiamondStorageGovernance.DiamondStorageGovernance storage dsGovernance =
            LibDiamondStorageGovernance.diamondStorageGovernance();

        // Commit the initial release to the release schedule.
        LibRelease.commitReleaseScheduleUpdate(
            dsCheckpoint,
            dsRegistration,
            hashRegistrationMetadata(_releaseMetadata.mrEnclave, _releaseMetadata.isvSvn),
            0
        );

        // Loop through the provided list of custodians to approve them.
        for (uint256 i = 0; i < _custodians.length; i++) {
            // Approve each custodian for registration.
            dsRegistration.custodians[_custodians[i]].approved = true;
        }

        // Add updateReleaseSchedule to the fast path.
        dsGovernance.fastPathFunctionSignatures["updateReleaseSchedule((bytes32,bytes2),uint128)"] = true;

        emit RegistrationInitialized(_custodians, _releaseMetadata.mrEnclave, _releaseMetadata.isvSvn);
    }

    /**
     * @notice This function sets the fast path function signatures.
     * @param _functionSignatures Function signatures.
     * @param _statuses Fast path statuses.
     */
    function setFastPathFunctionSignatures(string[] memory _functionSignatures, bool[] memory _statuses)
        external
        onlyAdmin
    {
        LibDiamondStorageGovernance.DiamondStorageGovernance storage dsGovernance =
            LibDiamondStorageGovernance.diamondStorageGovernance();

        // Ensure that the input is well-formed.
        require(_functionSignatures.length > 0, "Governance: must have non-zero function signatures.");
        require(_functionSignatures.length == _statuses.length, "Governance: set fast path input parity mismatch.");

        // Loop through the function signatures.
        for (uint256 i = 0; i < _functionSignatures.length; i++) {
            require(
                dsGovernance.fastPathFunctionSignatures[_functionSignatures[i]] != _statuses[i],
                "Governance: must change the status."
            );

            // Set the function signature's fast path status accordingly.
            dsGovernance.fastPathFunctionSignatures[_functionSignatures[i]] = _statuses[i];
        }

        emit FastPathFunctionSignaturesSet(_functionSignatures, _statuses);
    }

    /**
     * @notice Registers a new exchange signer. This grants the signer the
     *         ability to participate in checkpointing updates in-so-far as
     *         it meets other elibility criteria (i.e., its custodian is not
     *         jailed and has posted sufficient bond).
     * @param _report The Intel Attestation Report that was created by the
     *        signer's SGX enclave.
     * @param _signature The Intel Attestation Signature created by the
     *        signer's SGX enclave.
     */
    function register(bytes calldata _report, bytes calldata _signature) external virtual isNotPaused {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Verify the remote attestation report's cryptography.
        require(verifyReportCryptography(_report, _signature) == 0, "Registration: verifying signature failed");

        // Verify the Intel SGX Remote Attestation report of the signer and
        // extract the release hash from the quote body.
        (address signer, address sender, bytes32 releaseHash) = extractMetadataFromReport(_report, _signature);
        // Authenticate the message sender against the custodian address in the report user data.
        require(sender == msg.sender, "Registration: sender authentication failed");

        // Verify that the custodian can register the provided signer.
        verifySignerRegistration(dsRegistration, msg.sender, signer, releaseHash);

        // Update the registered custodians array if appropriate.
        RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[msg.sender];
        if (custodian.signers[releaseHash] == address(0)) {
            // If the custodian does not have a registered signer in this
            // release hash...

            // Add custodian address to the list of registered custodians.
            dsRegistration.registeredCustodians[releaseHash].push(msg.sender);

            // Update the valid signers count if appropriate.
            if (
                releaseHash == LibRelease.getCurrentReleaseHash(dsRegistration) &&
                custodian.balance >= dsRegistration.minimumBond &&
                custodian.unbondETA == 0 &&
                !custodian.jailed
            ) {
                // If the custodian will have a valid signer after
                // registration...

                // increment the valid signers count.
                dsRegistration.validSignersCount++;
            }
        }

        // Associate the signer address with the custodian address.
        custodian.signers[releaseHash] = signer;

        // Set up the signer state.
        dsRegistration.signers[releaseHash][signer].custodian = msg.sender;
        dsRegistration.signers[releaseHash][signer].report = _report;

        emit SignerRegistered(releaseHash, msg.sender, signer);
    }

    /**
     * @notice Allows governance to schedule a new release or update the
     *         existing release schedule.
     * @param _releaseMetadata The release metadata, including the mrEnclave
     *         and isvSvn as well as any other relevant registration metadata.
     *         If both the enclave measurement and the ISV SVN are zero, then
     *         we delete the corresponding entry.
     * @param _startingEpochId The epoch ID in which the release update
     *        should take effect.
     */
    function updateReleaseSchedule(
        RegistrationDefs.RegistrationMetadata memory _releaseMetadata,
        uint128 _startingEpochId
    ) external onlyAdmin {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Ensure the release metadata doesn't have an empty enclave
        // measurement and a non-empty ISV SVN.
        require(
            _releaseMetadata.mrEnclave != bytes32(0) || _releaseMetadata.isvSvn == bytes2(0),
            "Registration: ISV_SVN must be empty if MR_ENCLAVE is empty."
        );

        bytes32 releaseHash =
            _releaseMetadata.mrEnclave == bytes32(0)
                ? bytes32(0)
                : hashRegistrationMetadata(_releaseMetadata.mrEnclave, _releaseMetadata.isvSvn);

        // Commit the update to the release schedule.
        bool didUpdateCurrentRelease =
            LibRelease.commitReleaseScheduleUpdate(dsCheckpoint, dsRegistration, releaseHash, _startingEpochId);

        if (didUpdateCurrentRelease) {
            // If the current release was updated...

            uint128 validSignersCount = 0;
            for (uint256 i = 0; i < dsRegistration.registeredCustodians[releaseHash].length; i++) {
                // Get custodian for the new release hash and custodian address.
                RegistrationDefs.Custodian storage custodian =
                    dsRegistration.custodians[dsRegistration.registeredCustodians[releaseHash][i]];

                if (custodian.balance >= dsRegistration.minimumBond && custodian.unbondETA == 0 && !custodian.jailed) {
                    // If the custodian has a valid signer...

                    // Increment the count of valid signers.
                    validSignersCount++;
                }
            }

            // Update the valid signers count.
            dsRegistration.validSignersCount = validSignersCount;
        }

        // Emit an event that reflects the release schedule update.
        emit ReleaseScheduleUpdated(_releaseMetadata.mrEnclave, _releaseMetadata.isvSvn, _startingEpochId);
    }

    /**
     * @notice Gets some information about the current release.
     * @return The current release hash and the current release starting epoch.
     */
    function getCurrentReleaseInfo() external view returns (bytes32, uint128) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return (dsRegistration.currentReleaseHash, dsRegistration.currentReleaseStartingEpochId);
    }

    /**
     * @notice Gets the list of registered custodians.
     * @return The list of custodians in diamond storage.
     */
    function getRegisteredCustodians() external view returns (address[] memory) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return getRegisteredCustodiansByReleaseHash(LibRelease.getCurrentReleaseHash(dsRegistration));
    }

    /**
     * @notice Gets the list of registered custodians for a release hash.
     * @return The list of registered custodians in diamond storage.
     */
    function getRegisteredCustodiansByReleaseHash(bytes32 _releaseHash) public view returns (address[] memory) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        return dsRegistration.registeredCustodians[_releaseHash];
    }

    /**
     * @notice Gets the release schedule.
     * @return releaseSchedule The release schedule.
     */
    function getReleaseSchedule() external view returns (RegistrationDefs.ScheduledRelease[] memory releaseSchedule) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Get the length of the release schedule. This isn't the most
        // efficient way of doing this (it would be better to use assembly),
        // but since this will primarily be called by eth_call requests, it's
        // not worth the complexity to gas golf.
        uint256 releaseScheduleLength = 1;

        uint128 currentEpochId =
            dsRegistration.releaseSchedule[dsRegistration.currentReleaseStartingEpochId].nextEpochId;
        while (currentEpochId != 0) {
            releaseScheduleLength++;
            currentEpochId = dsRegistration.releaseSchedule[currentEpochId].nextEpochId;
        }

        // Write the initial release schedule entry.
        releaseSchedule = new RegistrationDefs.ScheduledRelease[](releaseScheduleLength);
        releaseSchedule[0].releaseHash = dsRegistration.currentReleaseHash;
        releaseSchedule[0].startingEpochId = dsRegistration.currentReleaseStartingEpochId;

        // Populate the remainder of the release schedule.
        uint256 i = 1;
        currentEpochId = dsRegistration.releaseSchedule[dsRegistration.currentReleaseStartingEpochId].nextEpochId;
        while (currentEpochId != 0) {
            releaseSchedule[i].releaseHash = dsRegistration.releaseSchedule[currentEpochId].releaseHash;
            releaseSchedule[i].startingEpochId = currentEpochId;
            currentEpochId = dsRegistration.releaseSchedule[currentEpochId].nextEpochId;
            i++;
        }

        return releaseSchedule;
    }

    /**
     * @dev Extracts metadata from an Intel remote attestation report and
     *      verifies the quote status. The report must contain an
     *      acceptable isvEnclaveQuoteStatus and quoteBody. The enclave
     *      quote body has the following byte schema (the irrelevant
     *      parts are omitted for conciseness):
     *          [112, 143] => mrEnclave
     *          [306, 307] => isvSvn
     *          [368, 431] => reportData
     *              [432, 451] => signerAddress
     *              [452, 471] => custodianAddress
     * @param _report The Intel remote attestation report.
     * @return The custodian address (to authenticate the sender) and signer
     *         address included in the report, and the hash of the report's
     *         MR_ENCLAVE measurement and ISV_SVN.
     */
    function extractMetadataFromReport(bytes memory _report, bytes memory)
        internal
        view
        virtual
        returns (
            address,
            address,
            bytes32
        )
    {
        // Convert report (bytes) to str repr and obtain report slice.
        StringUtils.slice memory reportSlice = string(_report).toSlice();

        // Check that the isvEnclaveQuoteStatus is OK or GROUP_OUT_OF_DATE.
        bool validIsvEnclaveQuoteStatus =
            reportSlice.contains('"isvEnclaveQuoteStatus":"OK"'.toSlice()) ||
                reportSlice.contains('"isvEnclaveQuoteStatus":"GROUP_OUT_OF_DATE"'.toSlice()) ||
                // FIXME: Temp workaround using SGX SDK 1.1.4
                // solhint-disable-next-line max-line-length
                // This may mean disable hyperthreading: https://community.intel.com/t5/Intel-Software-Guard-Extensions/Remote-attestation-still-returns-quot-configuration-needed-quot/m-p/1186069/highlight/true#M3726
                // It seems to work on the server but fails on my workstation,
                // which would be consistent with hyper-threading config.
                reportSlice.contains('"isvEnclaveQuoteStatus":"CONFIGURATION_NEEDED"'.toSlice()) ||
                reportSlice.contains('"isvEnclaveQuoteStatus":"SW_HARDENING_NEEDED"'.toSlice()) ||
                reportSlice.contains('"isvEnclaveQuoteStatus":"CONFIGURATION_AND_SW_HARDENING_NEEDED"'.toSlice());
        require(validIsvEnclaveQuoteStatus, "Registration: invalid isvEnclaveQuoteStatus in RA report.");

        // Extract the isvEnclaveQuoteBody from the report.
        StringUtils.slice memory part;
        reportSlice.split('"isvEnclaveQuoteBody":"'.toSlice(), part);
        require(reportSlice._len != 0, "Registration: isvEnclaveQuoteBody not found in report");
        reportSlice.split('"}'.toSlice(), part);

        bytes memory quoteBody = bytes(part.toString());

        // Base64 decode quote body.
        bytes memory quoteDecoded = Base64.decode(quoteBody);

        // Extract the mrEnclave from the quote body.
        bytes32 mrEnclave = quoteDecoded.readBytes32(112);

        // Extract the isvSvn from the quote body.
        bytes2 isvSvn = quoteDecoded.readBytes2(306);

        // Extract the reportData (aka user data) fixed-size buffer from the quote body.
        bytes memory reportData = quoteDecoded.slice(368, 432);
        // Return the user data components (custodian and signer addresses) and release hash specified by the report.
        return (reportData.readAddress(0), reportData.readAddress(20), hashRegistrationMetadata(mrEnclave, isvSvn));
    }

    /**
     * @dev Verify that the custodian can register a signer and validate that
     *      the signer address is appropriate. If this validation passes, the
     *      submitted report will be processed.
     * @param _dsRegistration A storage pointer to the diamond storage entry
     *        for the Registration facet.
     * @param _custodian The custodian address of the signer being registered.
     * @param _signer The address that will sign exchange updates.
     * @param _releaseHash The release hash to use when registering the signer.
     */
    function verifySignerRegistration(
        LibDiamondStorageRegistration.DiamondStorageRegistration storage _dsRegistration,
        address _custodian,
        address _signer,
        bytes32 _releaseHash
    ) internal view {
        // Ensure that the signer isn't the zero address.
        require(_signer != address(0), "Registration: can't register with the zero address.");

        // Ensure that the custodian has been approved by governance to run
        // a signer.
        require(
            _dsRegistration.custodians[_custodian].approved,
            "Registration: custodian must be approved to register a signer."
        );

        // Ensure that the signer was never registered. This ensures that
        // a report cannot be re-used after re-registration.
        RegistrationDefs.Signer memory signer = _dsRegistration.signers[_releaseHash][_signer];
        require(
            signer.custodian == _custodian || signer.custodian == address(0),
            "Registration: can't re-use a signing key."
        );
    }

    /**
     * @dev Verify the signature provided for an Intel remote attestation
     *      report.
     * @param _data The Intel remote attestation report data.
     * @param _signature The signature for the remote attestation report.
     * @return The result of verifying the report signature.
     */
    function verifyReportCryptography(bytes memory _data, bytes memory _signature)
        internal
        view
        virtual
        returns (uint256)
    {
        /* solhint-disable max-line-length */
        // This is the modulus and the exponent of intel's certificate, you
        // can extract it using:
        // `openssl x509 -noout -modulus -in intel.cert`
        // and `openssl x509 -in intel.cert  -text`
        bytes memory exponent =
            hex"0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010001";
        bytes memory modulus =
            hex"A97A2DE0E66EA6147C9EE745AC0162686C7192099AFC4B3F040FAD6DE093511D74E802F510D716038157DCAF84F4104BD3FED7E6B8F99C8817FD1FF5B9B864296C3D81FA8F1B729E02D21D72FFEE4CED725EFE74BEA68FBC4D4244286FCDD4BF64406A439A15BCB4CF67754489C423972B4A80DF5C2E7C5BC2DBAF2D42BB7B244F7C95BF92C75D3B33FC5410678A89589D1083DA3ACC459F2704CD99598C275E7C1878E00757E5BDB4E840226C11C0A17FF79C80B15C1DDB5AF21CC2417061FBD2A2DA819ED3B72B7EFAA3BFEBE2805C9B8AC19AA346512D484CFC81941E15F55881CC127E8F7AA12300CD5AFB5742FA1D20CB467A5BEB1C666CF76A368978B5";
        /* solhint-enable max-line-length */

        return SolRsaVerify.pkcs1Sha256VerifyRaw(_data, _signature, exponent, modulus);
    }

    /**
     * @dev Hashes the signer's registration metadata.
     * @param _mrEnclave SGX mrEnclave.
     * @param _isvSvn SGX isvSvn.
     * @return hash The hash of the signer's registration metadata.
     */
    function hashRegistrationMetadata(bytes32 _mrEnclave, bytes2 _isvSvn) internal pure returns (bytes32 hash) {
        return keccak256(abi.encode(_mrEnclave, _isvSvn));
    }
}
