// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { ISanctionsList } from "./interfaces/ISanctionsList.sol";
import { ReentrancyGuard } from "../libs/ReentrancyGuard.sol";
import { LibCollateral } from "../libs/LibCollateral.sol";
import { LibSMT } from "../libs/LibSMT.sol";
import { MathHelpers } from "../libs/MathHelpers.sol";
import { DepositDefs } from "../libs/defs/DepositDefs.sol";
import { SharedDefs } from "../libs/defs/SharedDefs.sol";
import { LibDiamondStorageBanner } from "../storage/LibDiamondStorageBanner.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageDeposit } from "../storage/LibDiamondStorageDeposit.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStoragePause } from "../storage/LibDiamondStoragePause.sol";

/**
 * @title Stake
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles the
 *         logic pertaining to deposits and withdrawals of staked DDX.
 */
contract Stake is ReentrancyGuard {
    using MathHelpers for uint256;

    /// @notice Max number of tokens to be issued (100 million DDX)
    uint96 internal constant MAX_SUPPLY = 100_000_000e18;

    // chainalysis sanctions contract address
    address internal constant _SANCTIONS_CONTRACT = 0x40C57923924B5c5c5455c48D93317139ADDaC8fb;

    event MaxDDXCapSet(uint256 oldMaxCap, uint256 newMaxCap);

    event DDXRateLimitSet(uint128 minimumRateLimit);

    event TraderUpdated(address indexed trader, uint128 amount, DepositDefs.TraderUpdateKind updateKind);

    /**
     * @notice Ensures function can only be called when the contract is not
     *         paused.
     */
    modifier isNotPaused {
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        require(!dsPause.isPaused, "Stake: paused.");
        _;
    }

    /**
     * @notice Limits functions to only be called via governance.
     */
    modifier onlyAdmin {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        require(msg.sender == dsDerivaDEX.admin, "Stake: must be called by Gov.");
        _;
    }

    /**
     * @notice This function initializes the state with some critical
     *         information, including the DDX minimum rate limit and
     *         maximum DDX capitalization that will be allowed in the SMT.
     *         This can only be called via governance.
     * @dev This function is intended to be the initialization
     *      target of the diamond cut function. This function is not included
     *      in the selectors being added to the diamond, meaning it cannot be
     *      called again.
     * @param _ddxMinimumRateLimit The minimum rate limit for DDX withdrawn
     *        from the DDX trader leaves.
     * @param _maxDDXCap The maximum DDX capitalization allowed within the SMT.
     */
    function initialize(uint128 _ddxMinimumRateLimit, uint256 _maxDDXCap) external onlyAdmin {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();

        // Set the DDX minimum rate limit.
        require(_ddxMinimumRateLimit > 0, "Stake: minimum rate limit must be non-zero.");
        dsDeposit.withdrawalRateLimits[address(0)].minimumRateLimit = _ddxMinimumRateLimit;

        // Set the max DDX cap.
        require(_maxDDXCap <= MAX_SUPPLY, "Stake: Max DDX cap must be lower than the max supply.");
        dsDeposit.maxDDXCap = _maxDDXCap;
    }

    /**
     * @notice Allows governance to update rate limits for DDX in the SMT.
     * @param _minimumRateLimit The minimum amount that the rate limit will be
     *        set to.
     */
    function setDDXRateLimits(uint128 _minimumRateLimit) external onlyAdmin isNotPaused {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();

        // Add the minimum rate limit to the rate limiting entry for this token.
        require(_minimumRateLimit > 0, "Stake: minimum rate limit must be non-zero.");
        dsDeposit.withdrawalRateLimits[address(0)].minimumRateLimit = _minimumRateLimit;

        emit DDXRateLimitSet(_minimumRateLimit);
    }

    /**
     * @notice Allows governance to specify the maximum amount of DDX (including
     *         future trade mining rewards) that can be deposited into the system.
     *         This cap can be used to ensure that operator collusion will not
     *         allow for escalation of privilege to get the powers of governance.
     * @dev We intentionally avoid checking to see that the max DDX cap is
     *      greater than the current DDX balance as this check could be front-run
     *      in order to keep the cap at the elevated level. Instead, we simply
     *      set the cap to the chosen value which prevents further deposits from
     *      taking place. The operation is still susceptible to front-running,
     *      but the ability to keep the cap arbitrarily elevated is removed.
     * @param _maxDDXCap The maximum amount of DDX that can be held within the
     *        DerivaDEX SMT.
     */
    function setMaxDDXCap(uint256 _maxDDXCap) external onlyAdmin isNotPaused {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        require(_maxDDXCap <= MAX_SUPPLY, "Stake: Max DDX cap must be lower than the max supply.");
        uint256 oldCap = dsDeposit.maxDDXCap;
        dsDeposit.maxDDXCap = _maxDDXCap;
        emit MaxDDXCapSet(oldCap, _maxDDXCap);
    }

    /**
     * @notice Allows a trader to deposit DDX into the DerivaDEX exchange. This
     *         function ensures that the DDX contained within the SMT does not
     *         exceed the maximum DDX cap set by governance, which ensures that
     *         operator collusion cannot result in an escalation of privilege.
     * @param _amount The amount to deposit.
     */
    function depositDDX(uint128 _amount) external isNotPaused nonReentrant {
        LibDiamondStorageBanner.DiamondStorageBanner storage dsBanner = LibDiamondStorageBanner.diamondStorageBanner();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();

        // Ensure that the sender is not banned.
        require(!dsBanner.banned[msg.sender], "Stake: cannot deposit DDX if banned.");

        // Ensure that the sender is not on the chainalysis sanctions list.
        require(!isSanctionedAddress(msg.sender), "Stake: cannot deposit DDX if on sanctions list.");

        // Commit the deposit to the internal contract state.
        dsDeposit.traderDDXDeposited = dsDeposit.traderDDXDeposited + _amount;
        LibCollateral.commitDeposit(dsDeposit, address(0), _amount);

        // Transfer the DDX tokens and emit an event.
        dsDerivaDEX.ddxToken.transferFrom(msg.sender, address(this), _amount);

        emit TraderUpdated(
            msg.sender,
            LibCollateral.scaleAmountForToken(_amount, address(dsDerivaDEX.ddxToken)),
            DepositDefs.TraderUpdateKind.Deposit
        );
    }

    /**
     * @notice Allows a trader to withdraw merklized DDX.
     * @param _amount The amount of DDX to withdraw.
     * @param _trader The Trader leaf that should represent the sender's trader
     *        account within the DDX state root.
     * @param _proof The merkle proof that will be used to verify whether or not
     *        the trader can withdraw the requested amount.
     */
    function withdrawDDX(
        uint128 _amount,
        DepositDefs.TraderData memory _trader,
        bytes memory _proof
    ) external isNotPaused nonReentrant {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();

        {
            // Compute the key and the leaf hash for the trader entry.
            bytes32 key = LibCollateral.generateTraderGroupKey(SharedDefs.ItemKind.Trader, msg.sender);
            bytes32 value = hashTrader(_trader);
            LibSMT.Leaf[] memory leaves = new LibSMT.Leaf[](1);
            leaves[0] = LibSMT.Leaf({ key: key, value: value });

            // Verify the merkle proof using the key and leaf hash.
            bytes32 root = LibSMT.computeRoot(leaves, _proof);
            require(root == dsCheckpoint.stateRoot, "Stake: Did not recover state root.");
        }

        // Verify that the withdrawal is valid using additional on-chain checks
        // including decreasing the DDX withdrawal allowance and checking past
        // withdrawal state. Once verified, update the relevant on-chain state.
        LibCollateral.commitTraderWithdrawal(msg.sender, _amount, _trader.frozenDDXBalance);

        // Withdraw DDX tokens and emit an event. First check to see if we can
        // transfer DDX from this contract. If there isn't a sufficient balance,
        // we can mint more.
        uint128 traderDDXDeposited = dsDeposit.traderDDXDeposited;
        if (traderDDXDeposited >= _amount) {
            dsDeposit.traderDDXDeposited = dsDeposit.traderDDXDeposited - _amount;
            dsDerivaDEX.ddxToken.transfer(msg.sender, uint256(_amount));
        } else if (traderDDXDeposited > 0) {
            dsDeposit.traderDDXDeposited = 0;
            dsDerivaDEX.ddxToken.transfer(msg.sender, traderDDXDeposited);
            dsDerivaDEX.ddxToken.mint(msg.sender, uint256(_amount) - traderDDXDeposited);
        } else {
            dsDerivaDEX.ddxToken.mint(msg.sender, uint256(_amount));
        }

        emit TraderUpdated(
            msg.sender,
            LibCollateral.scaleAmountForToken(_amount, address(dsDerivaDEX.ddxToken)),
            DepositDefs.TraderUpdateKind.Withdrawal
        );
    }

    /**
     * @dev Checks whether address is on the chainalysis sanctions list.
     * @param _address The address to check.
     * @return Whether the address is sanctioned.
     */
    function isSanctionedAddress(address _address) internal view virtual returns (bool) {
        ISanctionsList sanctionsList = ISanctionsList(_SANCTIONS_CONTRACT);
        return sanctionsList.isSanctioned(_address);
    }

    /**
     * @dev Hash the trader data that will be stored in the merkle tree. This
     *      hash will be hashed with the key when the merkle proof is actually
     *      being validated.
     * @param _traderData The leaf data that needs to be hashed.
     * @return The hash of the leaf data.
     */
    function hashTrader(DepositDefs.TraderData memory _traderData) internal pure returns (bytes32) {
        // ABI encode and hash the trader struct. Prepend the enum identifier
        // of Item::Trader from the Rust codebase.
        bytes memory encodedTrader =
            abi.encode(
                DepositDefs.TraderDataEncoding({ discriminant: SharedDefs.ItemKind.Trader, traderData: _traderData })
            );
        return keccak256(encodedTrader);
    }

    /**
     * @notice Gets the maximum DDX cap.
     * @return The maximum DDX cap.
     */
    function getMaxDDXCap() external view returns (uint256) {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return dsDeposit.maxDDXCap;
    }

    /**
     * @notice Gets the processed DDX withdrawals of a trader for a state in
     *         which the operator has a greater confirmed block number than the
     *         latest checkpointed confirmed block number. These processed
     *         withdrawals reflect the amount that the trader's current frozen
     *         DDX amount will be debited, which makes it possible to calculate
     *         the amount of pending withdrawals.
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @param _blockNumber The confirmed block number to use for the query.
     * @return amount The processed withdrawal amount for the given range.
     */
    function getProcessedDDXWithdrawals(address _withdrawAddress, uint128 _blockNumber)
        external
        view
        returns (uint128 amount)
    {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return
            LibCollateral.getProcessedWithdrawals(
                dsCheckpoint,
                dsDeposit,
                _withdrawAddress,
                bytes32(0),
                address(0),
                _blockNumber
            );
    }

    /**
     * @notice Gets the unprocessed withdrawals of a trader for the DDX token.
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @return amount The withdrawal amount in the provided epoch.
     */
    function getUnprocessedDDXWithdrawals(address _withdrawAddress) external view returns (uint128 amount) {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        (amount, , ) = LibCollateral.getUnprocessedWithdrawals(
            dsCheckpoint,
            dsDeposit,
            _withdrawAddress,
            bytes32(0),
            address(0)
        );
        return amount;
    }
}
