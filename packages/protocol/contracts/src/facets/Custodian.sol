// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { ReentrancyGuard } from "../libs/ReentrancyGuard.sol";
import { LibRelease } from "../libs/LibRelease.sol";
import { MathHelpers } from "../libs/MathHelpers.sol";
import { RegistrationDefs } from "../libs/defs/RegistrationDefs.sol";
import { LibDiamondStorageBanner } from "../storage/LibDiamondStorageBanner.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStorageRegistration } from "../storage/LibDiamondStorageRegistration.sol";
import { LibDiamondStoragePause } from "../storage/LibDiamondStoragePause.sol";

/**
 * @title Custodian
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to custodian bonding, jailing, and penalization
 *         for the DerivaDEX diamond.
 */
contract Custodian is ReentrancyGuard {
    using MathHelpers for uint256;

    event CustodianBondAdded(address indexed custodian, uint128 amount);

    event CustodiansJailed(address[] custodians);

    event CustodianPenalized(address[] custodians, uint128[] penalties);

    event CustodianStatusesSet(address[] custodians, bool[] statuses);

    event CustodianUnbondPrepared(address indexed custodian, uint128 unbondETA);

    event CustodianUnbonded(address indexed custodian, uint128 unbondAmount);

    event CustodiansUnjailed(address[] custodians);

    event MinimumBondSet(uint128 minimumBond);

    event CustodianInitialized(uint128 minimumBond, uint128 unbondDelay);

    event UnbondDelaySet(uint128 unbondDelay);

    /**
     * @notice Ensures function can only be called when the contract is not
     *         paused.
     */
    modifier isNotPaused {
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        require(!dsPause.isPaused, "Registration: paused.");
        _;
    }

    /**
     * @notice Limits functions to only be called via governance.
     */
    modifier onlyAdmin {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        require(msg.sender == dsDerivaDEX.admin, "Registration: must be called by Gov.");
        _;
    }

    /**
     * @notice Allows governance to initialize the Custodian facet.
     * @dev This function is intended to be the initialization target
     *      of the diamond cut function. This function is not included
     *      in the selectors being added to the diamond, meaning it
     *      cannot be called again.
     * @param _minimumBond The minimum bond for a signer to submit checkpoints.
     * @param _unbondDelay The number of blocks that custodians must wait before
     *        unbonding.
     */
    function initialize(uint128 _minimumBond, uint128 _unbondDelay) external onlyAdmin {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Update the minimum bond.
        dsRegistration.minimumBond = _minimumBond;

        // Update the unbond delay.
        dsRegistration.unbondDelay = _unbondDelay;

        emit CustodianInitialized(_minimumBond, _unbondDelay);
    }

    /**
     * @notice Allows governance to change the approval status of custodians.
     * @param _custodians The custodian addresses that should have their approval changed.
     * @param _statuses The target approval statuses corresponding to the custodian addresses.
     */
    function setCustodianStatuses(address[] calldata _custodians, bool[] calldata _statuses) external onlyAdmin {
        // Ensure that the input is well-formed.
        require(_custodians.length > 0, "Registration: empty list of custodians.");
        require(_custodians.length == _statuses.length, "Registration: input parity mismatch.");

        // Change the approval status of the custodians.
        LibDiamondStorageBanner.DiamondStorageBanner storage dsBanner = LibDiamondStorageBanner.diamondStorageBanner();
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Get current release hash.
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);

        // Loop through the custodians for which statuses are being set.
        for (uint256 i = 0; i < _custodians.length; i++) {
            // Ensure that the custodian isn't being approved if it's banned.
            require(
                _statuses[i] == false || !dsBanner.banned[_custodians[i]],
                "Registration: can't approve a banned address for registration."
            );

            // Ensure that the status actually changes the custodian's state.
            RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[_custodians[i]];
            require(_statuses[i] != custodian.approved, "Registration: can't set the same status.");

            // Set the custodian's approval status.
            custodian.approved = _statuses[i];

            uint128 validSignersCountDelta = 0;
            if (!_statuses[i]) {
                // If the custodian is being unapproved...

                if (
                    custodian.signers[releaseHash] != address(0) &&
                    custodian.balance >= dsRegistration.minimumBond &&
                    custodian.unbondETA == 0 &&
                    !custodian.jailed
                ) {
                    // If the custodian had a valid signer...

                    // Increment the valid signers count delta.
                    validSignersCountDelta++;
                }

                if (custodian.balance > 0) {
                    // If the custodian has a balance...

                    // Prepare the custodian to unbond. This ensures that it's
                    // impossible for the custodian to have a valid signer
                    // unless it is re-approved.
                    custodian.unbondETA =
                        block.number.safe128("Registration: invalid block number.") +
                        dsRegistration.unbondDelay;
                }
            }

            if (validSignersCountDelta > 0) {
                // If the valid signers count delta is greater than zero...

                // Update the valid signers count.
                dsRegistration.validSignersCount = dsRegistration.validSignersCount - validSignersCountDelta;
            }
        }

        emit CustodianStatusesSet(_custodians, _statuses);
    }

    /**
     * @notice Allows governance to update the minimum bond. The minimum
     *         bond defines how much a custodian needs to stake in DDX
     *         in order for its signers to participate in checkpointing
     *         consensus.
     * @param _minimumBond The minimum DDX bond needed for the signers of a
     *        custodian to participate in checkpointing consensus.
     */
    function setMinimumBond(uint128 _minimumBond) external onlyAdmin {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Ensure the minimum bond isn't being set to its current value.
        require(_minimumBond != dsRegistration.minimumBond, "Registration: can't set the same minumum bond.");

        // Update the minimum bond.
        dsRegistration.minimumBond = _minimumBond;

        // Track the number of valid signers for the current release hash
        // given the new minimum bond requirement.
        uint128 validSignersCount = 0;

        // Get current release hash.
        bytes32 currentReleaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);

        // Loop through the registered custodians for the current release hash.
        for (uint256 i = 0; i < dsRegistration.registeredCustodians[currentReleaseHash].length; i++) {
            // Get custodian for given release hash and custodian address.
            RegistrationDefs.Custodian storage custodian =
                dsRegistration.custodians[dsRegistration.registeredCustodians[currentReleaseHash][i]];

            if (custodian.balance >= dsRegistration.minimumBond && custodian.unbondETA == 0 && !custodian.jailed) {
                // If the custodian has a valid signer...

                // Increment the count of valid signers.
                validSignersCount++;
            }
        }

        // Store the final count of valid signers
        dsRegistration.validSignersCount = validSignersCount;

        emit MinimumBondSet(_minimumBond);
    }

    /**
     * @notice Allows governance to specify the number of blocks that must
     *         transpire between when a custodian prepares to unbond and is
     *         actually able to unbond.
     * @param _unbondDelay The number of blocks that must elapse for a
     *        custodian to be able to unbond.
     */
    function setUnbondDelay(uint128 _unbondDelay) external onlyAdmin {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Update the unbond delay in storage.
        require(_unbondDelay != dsRegistration.unbondDelay, "Registration: must specify a new unbond delay.");
        dsRegistration.unbondDelay = _unbondDelay;

        emit UnbondDelaySet(_unbondDelay);
    }

    /**
     * @notice Jails a list of custodians.
     * @dev The intention of preventing governance from jailing a custodian
     *      after unapproving them is to provide more clarity to unapproved
     *      custodians and to ensure that governance jails custodians prior to
     *      updating their status.
     * @param _custodians Custodian addresses to be jailed.
     */
    function jail(address[] memory _custodians) external onlyAdmin {
        // Ensure that the input is well-formed.
        require(_custodians.length > 0, "Registration: Empty list of addresses.");

        // Get a reference to the registration diamond storage.
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Get current release hash.
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);

        uint128 validSignersCountDelta = 0;
        uint256 jailedCustodiansCount = 0;
        address[] memory jailedCustodians = new address[](_custodians.length);
        for (uint256 i = 0; i < _custodians.length; i++) {
            // Get the custodian state.
            RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[_custodians[i]];

            if (custodian.jailed || !custodian.approved) {
                // If the custodian is already jailed or isn't an approved custodian...

                // continue to avoid wasting gas.
                continue;
            }

            if (
                custodian.signers[releaseHash] != address(0) &&
                custodian.balance >= dsRegistration.minimumBond &&
                custodian.unbondETA == 0
            ) {
                // If the custodian previously had a valid signer...

                // Increment the delta by 1 to be applied at the end.
                validSignersCountDelta++;
            }

            // Set the operator's status to jailed.
            custodian.jailed = true;

            // Add the custodian address to the list of jailed custodians.
            jailedCustodians[jailedCustodiansCount++] = _custodians[i];
        }

        if (validSignersCountDelta > 0) {
            // If the valid signers count delta is greater than zero...

            // Decrement the count of valid signers by the delta computed above.
            dsRegistration.validSignersCount = dsRegistration.validSignersCount - validSignersCountDelta;
        }

        // Re-size the list of jailed custodians.
        assembly {
            mstore(jailedCustodians, jailedCustodiansCount)
        }

        emit CustodiansJailed(jailedCustodians);
    }

    /**
     * @notice Penalizes jailed custodians.
     * @param _custodians Jailed custodians to penalize.
     * @param _penalties Penalties to be imposed upon the custodians.
     */
    function penalize(address[] memory _custodians, uint128[] memory _penalties) external onlyAdmin {
        // Ensure that the input is well-formed.
        require(_custodians.length > 0, "Registration: Empty list of addresses.");
        require(_penalties.length == _custodians.length, "Registration: penalize input parity mismatch.");

        // Get a reference to the registration diamond storage.
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Loop through the custodians to penalize.
        for (uint256 i = 0; i < _custodians.length; i++) {
            // Obtain custodian to penalize given the custodian address.
            RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[_custodians[i]];

            // Ensure the custodian is jailed before applying penalty.
            require(custodian.jailed, "Registration: Cannot penalize custodians that aren't jailed");

            // Add penalty to contract's penalty pool.
            dsRegistration.penalties = dsRegistration.penalties + _penalties[i];

            // Debit the penalty from the custodian's bond.
            custodian.balance = custodian.balance - _penalties[i];
        }

        emit CustodianPenalized(_custodians, _penalties);
    }

    /**
     * @notice Allows governance to unjail of a list of custodians.
     * @param _custodians Custodian addresses to be unjailed.
     */
    function unjail(address[] memory _custodians) external onlyAdmin {
        // Ensure that the input is well-formed.
        require(_custodians.length > 0, "Registration: Empty list of addresses.");

        // Get a reference to the registration diamond storage.
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Get current release hash.
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);

        uint128 validSignersCountDelta = 0;
        for (uint256 i = 0; i < _custodians.length; i++) {
            // Ensure custodian is jailed in order to be unjailed.
            RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[_custodians[i]];
            require(custodian.jailed, "Registration: Cannot unjail operators that aren't jailed");

            // Check if the custodian controls a valid signer in the current
            // release after being unjailed (i.e., if it has a sufficient bond
            // to participate in consensus), and handle accordingly.
            if (
                custodian.signers[releaseHash] != address(0) &&
                custodian.balance >= dsRegistration.minimumBond &&
                custodian.unbondETA == 0
            ) {
                // If the custodian will have a valid signer...

                // Increment the count of valid signers delta by 1
                // to be applied at the end.
                validSignersCountDelta++;
            }

            // Unjail the custodian's status.
            custodian.jailed = false;
        }

        if (validSignersCountDelta > 0) {
            // If the valid signers count delta is greater than zero...

            // Update the valid signers count by the valid signers count delta.
            dsRegistration.validSignersCount = dsRegistration.validSignersCount + validSignersCountDelta;
        }

        emit CustodiansUnjailed(_custodians);
    }

    /**
     * @notice Allows a custodian to post a DDX bond of any amount.
     * @param _amount DDX bond amount to be posted by the custodian.
     */
    function bond(uint128 _amount) external isNotPaused nonReentrant {
        // Ensure that the amount is greater than zero to avoid emitting extraneous events.
        require(_amount > 0, "Registration: non-zero bond amount.");

        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[msg.sender];

        // Ensure that the custodian is approved.
        require(custodian.approved, "Registration: custodian must be approved to bond.");

        // Ensure that the custodian hasn't already prepared to unbond.
        require(custodian.unbondETA == 0, "Registration: custodian has already starting unbonding.");

        // Transfer DDX from the custodian to the contract.
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        dsDerivaDEX.ddxToken.transferFrom(msg.sender, address(this), _amount);

        // Check if adding the bond necessitates an update in the count
        // of valid signers.
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);
        if (
            custodian.signers[releaseHash] != address(0) &&
            custodian.balance < dsRegistration.minimumBond &&
            custodian.balance + _amount >= dsRegistration.minimumBond &&
            !custodian.jailed
        ) {
            // If an unjailed custodian will now have a valid signer...

            // Increment the count of valid signers by 1.
            dsRegistration.validSignersCount++;
        }

        // Increase the custodian's staked balance.
        custodian.balance = custodian.balance + _amount;

        emit CustodianBondAdded(msg.sender, _amount);
    }

    /**
     * @notice Allows a custodian to signal that they'd like to withdraw
     *         their bond. After the custodian signals their intention
     *         to withdraw their bond, they will no longer be able to
     *         participate in valid checkpoints; however, they will still be
     *         able to be jailed for invalid submissions. The custodian must
     *         wait for the governance specified unbond delay to elapse to
     *         ensure that they can be jailed for past invalid checkpoints
     *         and to give governance time to jail them for past infractions.
     */
    function prepareUnbond() external isNotPaused {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[msg.sender];

        // Ensure the custodian has a bond to withdraw.
        require(custodian.balance > 0, "Registration: custodian isn't bonded.");

        // Ensure the custodian hasn't already prepared to unbond.
        require(custodian.unbondETA == 0, "Registration: custodian has already prepared to unbond.");

        // Set the unbond ETA of the custodian in blocks. We must use blocks
        // for this ETA to avoid making the custodians bond reliant on the
        // existing operator security assumptions to ensure that custodian
        // bonds exist at a layer security layer.
        uint128 unbondETA = block.number.safe128("Registration: invalid block number.") + dsRegistration.unbondDelay;
        custodian.unbondETA = unbondETA;

        // If the custodian previously had a valid signer, decrement the
        // valid signers count to account for the fact that the custodian's
        // signers won't be able to submit checkpoints.
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);
        if (
            custodian.signers[releaseHash] != address(0) &&
            custodian.balance >= dsRegistration.minimumBond &&
            !custodian.jailed
        ) {
            // If the custodian has a valid signer in the current release hash...

            // reduce the valid signers count.
            dsRegistration.validSignersCount--;
        }

        // Emit an event signaling that the custodian has prepared to unbond.
        emit CustodianUnbondPrepared(msg.sender, unbondETA);
    }

    /**
     * @notice Allows custodians to withdraw their bond if they have already
     *         prepared to unbond, a sufficient time has elapsed, and they
     *         aren't jailed.
     */
    function unbond() external isNotPaused nonReentrant {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[msg.sender];

        // Ensure that the custodian isn't jailed
        require(!custodian.jailed, "Registration: jailed custodians can't unbond.");

        // Ensure that the custodian's unbonding waiting period has elapsed.
        require(
            custodian.unbondETA > 0 && custodian.unbondETA <= block.number,
            "Registration: custodian isn't ready to unbond."
        );

        // Return the custodian's bond.
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        uint128 balance = custodian.balance;
        custodian.unbondETA = 0;
        if (balance > 0) {
            // If the custodian's balance is non-zero...

            // Set the balance to zero.
            custodian.balance = 0;

            // Transfer the bond back to the custodian.
            dsDerivaDEX.ddxToken.transfer(msg.sender, balance);
        }

        // Emit an event indicating that the custodian unbonded.
        emit CustodianUnbonded(msg.sender, balance);
    }

    /**
     * @notice Gets the custodian state of a list of custodians.
     * @param _custodians The custodians.
     * @return custodians The custodian information.
     */
    function getCustodianStates(address[] memory _custodians)
        external
        view
        returns (RegistrationDefs.CustodianWithoutSigners[] memory custodians)
    {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        custodians = new RegistrationDefs.CustodianWithoutSigners[](_custodians.length);
        for (uint256 i = 0; i < _custodians.length; i++) {
            custodians[i] = RegistrationDefs.CustodianWithoutSigners({
                balance: dsRegistration.custodians[_custodians[i]].balance,
                unbondETA: dsRegistration.custodians[_custodians[i]].unbondETA,
                approved: dsRegistration.custodians[_custodians[i]].approved,
                jailed: dsRegistration.custodians[_custodians[i]].jailed
            });
        }
        return custodians;
    }

    /**
     * @notice Gets some high level information about the Custodian facet.
     * @return The minimum bond and the unbond delay.
     */
    function getCustodianInfo() external view returns (uint128, uint128) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return (dsRegistration.minimumBond, dsRegistration.unbondDelay);
    }

    /**
     * @notice Gets the total amount of current penalties in DDX
     * @return The current penalties in DDX.
     */
    function getPenalties() external view returns (uint256) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return dsRegistration.penalties;
    }

    /**
     * @notice Gets the list of signer addresses for custodian addresses.
     * @param _custodians Custodian addresses.
     * @return The list of signer addresses.
     */
    function getSignersForCustodians(address[] memory _custodians) external view returns (address[] memory) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return getSignersForCustodiansAndReleaseHash(_custodians, LibRelease.getCurrentReleaseHash(dsRegistration));
    }

    /**
     * @notice Gets the list of signer addresses for custodian addresses.
     * @param _custodians Custodian addresses.
     * @param _releaseHash Release hash(mrEnclave, isvSvn).
     * @return The list of signer addresses.
     */
    function getSignersForCustodiansAndReleaseHash(address[] memory _custodians, bytes32 _releaseHash)
        public
        view
        returns (address[] memory)
    {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        address[] memory signers = new address[](_custodians.length);
        for (uint256 i = 0; i < _custodians.length; i++) {
            signers[i] = dsRegistration.custodians[_custodians[i]].signers[_releaseHash];
        }

        return signers;
    }

    /**
     * @notice Gets the states registered under signer addresses.
     * @param _signers The signer addresses.
     * @return The signer state of the specified signers.
     */
    function getSignerStates(address[] memory _signers) public view returns (RegistrationDefs.Signer[] memory) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return getSignerStatesByReleaseHash(_signers, LibRelease.getCurrentReleaseHash(dsRegistration));
    }

    /**
     * @notice Gets signer states for a set of addresses in a given release hash.
     * @param _signers The signer addresses.
     * @param _releaseHash Release hash(mrEnclave, isvSvn).
     * @return signerStates The signer states.
     */
    function getSignerStatesByReleaseHash(address[] memory _signers, bytes32 _releaseHash)
        public
        view
        returns (RegistrationDefs.Signer[] memory signerStates)
    {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        signerStates = new RegistrationDefs.Signer[](_signers.length);
        for (uint256 i = 0; i < _signers.length; i++) {
            signerStates[i] = dsRegistration.signers[_releaseHash][_signers[i]];
        }
        return signerStates;
    }

    /**
     * @notice Gets the count of valid signers.
     * @return Count of valid signers.
     */
    function getValidSignersCount() external view returns (uint128) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return dsRegistration.validSignersCount;
    }

    /**
     * @notice Returns whether or not the specified custodian has a valid
     *         signer.
     * @param _custodian The address of the custodian to verify.
     * @return A boolean indicating whether or not the custodian has a valid
     *         signer.
     */
    function hasValidSigner(address _custodian) external view returns (bool) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[_custodian];
        return
            custodian.signers[LibRelease.getCurrentReleaseHash(dsRegistration)] != address(0) &&
            custodian.balance >= dsRegistration.minimumBond &&
            custodian.unbondETA == 0 &&
            !custodian.jailed;
    }
}
