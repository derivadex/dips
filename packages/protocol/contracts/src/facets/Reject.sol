// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { LibCheckpoint } from "../libs/LibCheckpoint.sol";
import { LibRelease } from "../libs/LibRelease.sol";
import { LibSignature } from "../libs/LibSignature.sol";
import { MathHelpers } from "../libs/MathHelpers.sol";
import { CheckpointDefs } from "../libs/defs/CheckpointDefs.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStoragePause } from "../storage/LibDiamondStoragePause.sol";
import { LibDiamondStorageRegistration } from "../storage/LibDiamondStorageRegistration.sol";

/**
 * @title Reject
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to checkpoint rejection.
 */
contract Reject {
    using MathHelpers for uint256;

    uint96 internal constant CONSENSUS_PRECISION = 1e18;

    event Rejected(uint128 indexed epochId);

    /**
     * @notice Ensures function can only be called when the contract is not
     *         paused.
     */
    modifier isNotPaused {
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        require(!dsPause.isPaused, "Checkpoint: paused.");
        _;
    }

    /**
     * @notice Allows anyone to submit checkpoint submissions that demonstrate
     *         that consensus is impossible for a given epoch. A successful
     *         submission to this flow will automatically pause the exchange
     *         as a breakdown in consensus to this extent presents a major
     *         problem with the exchange operators.
     * @param _submissions The checkpoint submissions that should be rejected.
     * @param _signatures The signatures which correspond to the submissions in
     *        a monotonically increasing list. This is provided to ensure there
     *        aren't duplicate signers.
     * @param _indexes The index pair (submission, signature) corresponding to each
     *        signature's location in the submissions array. This param should be the same
     *        length as the signatures array.
     * @param _epochId The epoch ID of the submissions.
     */
    function reject(
        CheckpointDefs.CheckpointSubmission[] memory _submissions,
        bytes[] memory _signatures,
        CheckpointDefs.CheckpointSubmissionIndex[] memory _indexes,
        uint128 _epochId
    ) external isNotPaused {
        // Sanitize the inputs.
        require(_submissions.length > 0, "Checkpoint: can't reject an empty list of submissions.");
        require(
            _signatures.length == _indexes.length,
            "Checkpoint: signatures and indexes arrays must be the same length."
        );

        // Get a reference to the checkpoint diamond storage.
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();

        // Ensure that the new epoch ID is larger than the previous epoch ID.
        require(dsCheckpoint.currentEpochId < _epochId, "Checkpoint: Epoch ID must monotonically increase.");

        // Check the signatures and indexes for validity and for no duplicate signers.
        validateSignatureOrdering(_submissions, _signatures, _indexes, _epochId);

        // Process the submissions to validate whether this is in fact a rejection case.
        processSubmissions(dsCheckpoint, _submissions, _signatures, _epochId);

        // Now that a consensus failure has been proven, pause the exchange.
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        dsPause.isPaused = true;

        // Emit an event.
        emit Rejected(_epochId);
    }

    /**
     * @notice Internal function used for processing the list of signatures. The primary
     *         reason for this function is to ensure that there cannot be duplicate signers
     *         since they must appear in monotonically increasing order.
     * @param _submissions The checkpoint submissions that should be rejected.
     * @param _signatures The signatures which correspond to the submissions in
     *        a monotonically increasing list. This is provided to ensure there
     *        aren't duplicate signers.
     * @param _indexes The index pair (submission, signature) corresponding to each
     *        signature's location in the submissions array. This param should be the same
     *        length as the signatures array.
     * @param _epochId The epoch ID of the submissions.
     */
    function validateSignatureOrdering(
        CheckpointDefs.CheckpointSubmission[] memory _submissions,
        bytes[] memory _signatures,
        CheckpointDefs.CheckpointSubmissionIndex[] memory _indexes,
        uint128 _epochId
    ) internal {
        // We track the last signer address recovered. We do this to
        // guarantee that there are no duplicate signers.
        address lastSignerAddress;

        for (uint256 i = 0; i < _signatures.length; i++) {
            require(
                keccak256(_signatures[i]) ==
                    keccak256(_submissions[_indexes[i].submissionIndex].signatures[_indexes[i].signatureIndex]),
                "Checkpoint: signature doesn't match corresponding signature from submissions array"
            );

            // Recover the current signer address. In order to improve the
            // algorithms used in this smart contract, we require the
            // submitter to provide all of the signer addresses to the
            // contract in ascending order. Verifying that the signer
            // addresses are monotonically increasing ensures that there are
            // no duplicate submissions.
            {
                bytes32 hash =
                    LibCheckpoint.hashCheckpointData(
                        _submissions[_indexes[i].submissionIndex].checkpointData,
                        _epochId
                    );
                address recoveredSignerAddress = LibSignature.getEIP191SignerOfHash(hash, _signatures[i]);
                require(
                    lastSignerAddress == address(0) || lastSignerAddress < recoveredSignerAddress,
                    "Checkpoint: Signer addresses must be monotonically increasing."
                );
                lastSignerAddress = recoveredSignerAddress;
            }
        }
    }

    /**
     * @notice Internal function used for actually processing the submissions
     *         as part of the reject flow and determine whether this is a valid
     *         case of rejection.
     * @param _dsCheckpoint Pointer to the diamond storage for Checkpoint.
     * @param _submissions The checkpoint submissions that should be rejected.
     * @param _signatures The signatures which correspond to the submissions in
     *        a monotonically increasing list. These are used in this function
     *        to enforce equality between the total signatures from the submission
     *        and the length of the standalone signatures array.
     * @param _epochId The epoch ID of the submissions.
     */
    function processSubmissions(
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage _dsCheckpoint,
        CheckpointDefs.CheckpointSubmission[] memory _submissions,
        bytes[] memory _signatures,
        uint128 _epochId
    ) internal {
        // Get a reference to the registration diamond storage.
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Attempt to advance the release schedule and get the current release
        // hash that should be used when validating checkpoint submissions.
        bytes32 releaseHash = LibRelease.evaluateReleaseSchedule(dsRegistration, _epochId);

        // Ensure that the each of the submissions are consistent and unique.
        // Then check to see whether or not the submission can be used as a
        // majority checkpoint.
        bytes32 lastReferenceHash = bytes32(0);
        uint256 submissionCount;
        uint256 signaturesInSubmissions;
        uint256[] memory submissionCounts = new uint256[](_submissions.length);

        // Loop through the list of checkpoint submissions.
        for (uint256 i = 0; i < _submissions.length; i++) {
            // Ensure that the submissions don't correspond to an earlier
            // submission list and have non-zero hashes.
            bytes32 referenceHash = LibCheckpoint.hashCheckpointData(_submissions[i].checkpointData, _epochId);
            require(referenceHash != bytes32(0), "Checkpoint: reference hash must be non-zero.");
            require(
                lastReferenceHash < referenceHash,
                "Checkpoint: reference hashes must be monotonically increasing."
            );
            lastReferenceHash = referenceHash;

            // Verify the contents of the checkpoint submission and track
            // the number of valid operators that comprise the submission.
            (address[] memory custodians, ) =
                LibCheckpoint.verifySubmission(dsRegistration, _submissions[i], _epochId, releaseHash, bytes32(0));
            submissionCounts[i] = custodians.length;

            // Increment the total submission count across all
            // checkpoint submissions we are evaluating.
            submissionCount += submissionCounts[i];

            // Increment the total signatures from the submissions struct array.
            signaturesInSubmissions += _submissions[i].signatures.length;
        }

        // Check that the signatures from submissions matches the length of the signatures
        // array that was passed in to ensure caller can't pass in extra signatures
        // to the submissions struct.
        require(
            signaturesInSubmissions == _signatures.length,
            "Checkpoint: signatures in submissions not equal to length of signatures array"
        );

        // Get the maximum number of missing submissions.
        uint256 maximumMissingSubmissions =
            dsRegistration.validSignersCount > submissionCount ? dsRegistration.validSignersCount - submissionCount : 0;

        // Verify that this submission cannot possibly be the majority
        // submission.
        for (uint256 i = 0; i < submissionCounts.length; i++) {
            require(
                (submissionCounts[i] + maximumMissingSubmissions) * CONSENSUS_PRECISION <
                    LibCheckpoint.getConsensusThreshold(dsRegistration.validSignersCount) ||
                    submissionCounts[i] + maximumMissingSubmissions < _dsCheckpoint.quorum,
                "Checkpoint: it's possible for submission to be the majority submission."
            );
        }
    }
}
