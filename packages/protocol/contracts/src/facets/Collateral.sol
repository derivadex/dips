// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { SafeERC20 } from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import { ISanctionsList } from "./interfaces/ISanctionsList.sol";
import { ReentrancyGuard } from "../libs/ReentrancyGuard.sol";
import { LibCollateral } from "../libs/LibCollateral.sol";
import { LibSMT } from "../libs/LibSMT.sol";
import { MathHelpers } from "../libs/MathHelpers.sol";
import { DepositDefs } from "../libs/defs/DepositDefs.sol";
import { SharedDefs } from "../libs/defs/SharedDefs.sol";
import { LibDiamondStorageBanner } from "../storage/LibDiamondStorageBanner.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageDeposit } from "../storage/LibDiamondStorageDeposit.sol";
import { LibDiamondStorageDepositGuarded } from "../storage/LibDiamondStorageDepositGuarded.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStorageGovernance } from "../storage/LibDiamondStorageGovernance.sol";
import { LibDiamondStoragePause } from "../storage/LibDiamondStoragePause.sol";
import { LibDiamondStorageReentrancyGuard } from "../storage/LibDiamondStorageReentrancyGuard.sol";

/**
 * @title Collateral
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles the
 *         logic pertaining to deposits and withdrawals of collateral.
 */
contract Collateral is ReentrancyGuard {
    using MathHelpers for uint256;
    using SafeERC20 for IERC20;

    // Hashed version of 'main' strategy
    bytes32 internal constant _STRATEGY_ID_MAIN = 0x046d61696e000000000000000000000000000000000000000000000000000000;

    // chainalysis sanctions contract address
    address internal constant _SANCTIONS_CONTRACT = 0x40C57923924B5c5c5455c48D93317139ADDaC8fb;

    event ExchangeCollateralAdded(
        address indexed collateralToken,
        address indexed underlyingToken,
        SharedDefs.Flavor flavor,
        uint128 minimumRateLimit
    );

    event ExchangeCollateralUpdated(address indexed collateralToken, uint128 minimumRateLimit);

    event ExchangeCollateralRemoved(address indexed collateralToken);

    event StrategyUpdated(
        address indexed trader,
        address indexed collateralAddress,
        bytes32 indexed strategyId,
        uint128 amount,
        DepositDefs.StrategyUpdateKind updateKind
    );

    event RateLimitParametersSet(uint128 rateLimitPeriod, uint128 rateLimitPercentage);

    event MaxDepositedAddressesSet(uint128 maxDepositedAddresses);

    event MinDepositSet(uint128 minDeposit);

    /**
     * @notice Ensures function can only be called when the contract is not
     *         paused.
     */
    modifier isNotPaused {
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        require(!dsPause.isPaused, "Collateral: paused.");
        _;
    }

    /**
     * @notice Limits functions to only be called via governance.
     */
    modifier onlyAdmin {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        require(msg.sender == dsDerivaDEX.admin, "Collateral: must be called by Gov.");
        _;
    }

    /**
     * @notice This function initializes the state with some critical
     *         information, including the rate limit parameters.
     *         This can only be called via governance.
     * @dev This function is intended to be the initialization target
     *      of the diamond cut function. This function is not included
     *      in the selectors being added to the diamond, meaning it
     *      cannot be called again.
     * @param _rateLimitPeriod The number of blocks before token-specific
     *        rate limits are reassessed.
     * @param _rateLimitPercentage The dynamic component of the withdrawal
     *        rate limits.
     * @param _maxDepositedAddresses The maximum number of deposited addresses.
     * @param _minDeposit The minimum amount of USDC that must be deposited.
     */
    function initialize(
        uint128 _rateLimitPeriod,
        uint128 _rateLimitPercentage,
        uint128 _maxDepositedAddresses,
        uint128 _minDeposit
    ) external onlyAdmin {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibDiamondStorageDepositGuarded.DiamondStorageDepositGuarded storage dsDepositGuarded =
            LibDiamondStorageDepositGuarded.diamondStorageDepositGuarded();
        LibDiamondStorageGovernance.DiamondStorageGovernance storage dsGovernance =
            LibDiamondStorageGovernance.diamondStorageGovernance();
        LibDiamondStorageReentrancyGuard.DiamondStorageReentrancyGuard storage dsReentrancyGuard =
            LibDiamondStorageReentrancyGuard.diamondStorageReentrancyGuard();

        // Set the rate limit period.
        require(_rateLimitPeriod > 0, "Collateral: Rate limit period must be non-zero.");
        dsDeposit.rateLimitPeriod = _rateLimitPeriod;

        // Set the rate limit percentage.
        require(_rateLimitPercentage <= 100, "Collateral: Rate limit percentage must be less than or equal to 100%.");
        dsDeposit.rateLimitPercentage = _rateLimitPercentage;

        // Set the max number of deposited addresses.
        dsDepositGuarded.maxDepositedAddresses = _maxDepositedAddresses;

        // Set the min deposit (USDC grains).
        dsDepositGuarded.minDeposit = _minDeposit;

        // Add setMaxDepositedAddresses and setMinDeposit guarded
        // governance setters to the fast path.
        dsGovernance.fastPathFunctionSignatures["setMaxDepositedAddresses(uint128)"] = true;
        dsGovernance.fastPathFunctionSignatures["setMinDeposit(uint128)"] = true;

        // Sets the initial reentrancy guard to _NOT_ENTERED. Note that this
        // could be done via the initialize methods of any of the new facets.
        dsReentrancyGuard.status = 1;
    }

    /**
     * @notice Allows governance to set the rate limit percentage for
     *         withdrawals.
     * @param _rateLimitPercentage The new rate limit percentage.
     */
    function setRateLimitParameters(uint128 _rateLimitPeriod, uint128 _rateLimitPercentage)
        external
        onlyAdmin
        isNotPaused
    {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();

        // Ensure that the rate limit parameters are valid.
        require(_rateLimitPeriod > 0, "Collateral: Rate limit period must be non-zero.");
        require(_rateLimitPercentage <= 100, "Collateral: Rate limit percentage must be less than or equal to 100%.");

        // Set the rate limit parameters.
        dsDeposit.rateLimitPeriod = _rateLimitPeriod;
        dsDeposit.rateLimitPercentage = _rateLimitPercentage;

        emit RateLimitParametersSet(_rateLimitPeriod, _rateLimitPercentage);
    }

    /**
     * @notice Allows governance to specify the maximum number of unique
               deposited addresses. This is only pertinent during the
               guarded launch.
     * @param _maxDepositedAddresses The maximum number of deposited
              addresses.
     */
    function setMaxDepositedAddresses(uint128 _maxDepositedAddresses) external onlyAdmin isNotPaused {
        LibDiamondStorageDepositGuarded.DiamondStorageDepositGuarded storage dsDepositGuarded =
            LibDiamondStorageDepositGuarded.diamondStorageDepositGuarded();

        // Ensure the max deposited addresses is not the same as current.
        require(
            _maxDepositedAddresses != dsDepositGuarded.maxDepositedAddresses,
            "CollateralGuarded: Max deposited addresses must be different than current."
        );

        // Set the max deposited addresses.
        dsDepositGuarded.maxDepositedAddresses = _maxDepositedAddresses;
        emit MaxDepositedAddressesSet(_maxDepositedAddresses);
    }

    /**
     * @notice Allows governance to specify the minimum amount of USDC
               that must be deposited. This is only pertinent during the
               guarded launch.
     * @param _minDeposit The minimum amount of USDC that must be
              deposited.
     */
    function setMinDeposit(uint128 _minDeposit) external onlyAdmin isNotPaused {
        LibDiamondStorageDepositGuarded.DiamondStorageDepositGuarded storage dsDepositGuarded =
            LibDiamondStorageDepositGuarded.diamondStorageDepositGuarded();

        // Ensure the min deposit is non-zero.
        require(_minDeposit != 0, "CollateralGuarded: Min deposit must be non-zero.");

        // Ensure the min deposit is not the same as current.
        require(
            _minDeposit != dsDepositGuarded.minDeposit,
            "CollateralGuarded: Min deposit must be different than current."
        );

        // Set the min deposit.
        dsDepositGuarded.minDeposit = _minDeposit;
        emit MinDepositSet(_minDeposit);
    }

    /**
     * @notice Allows governance to add exchange collateral.
     * @dev This function purposefully prevents governance from adding collateral
     *      flavors that are not Vanilla. Claiming Compound or Aave rewards has
     *      not been implemented yet, so adding these tokens as collateral will
     *      encourage users to waste their rewards.
     * @param _collateralToken The collateral token to add.
     * @param _minimumRateLimit The minimum amount that the rate limit will be
     *        set to.
     */
    function addExchangeCollateral(address _collateralToken, uint128 _minimumRateLimit) external onlyAdmin isNotPaused {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();

        // Ensure that the collateral is valid and has not already been registered.
        require(_collateralToken != address(0), "Collateral: collateral token must be non-zero.");
        require(!dsDeposit.exchangeCollaterals[_collateralToken].isListed, "Collateral: collateral already added.");

        // Add the collateral to storage.
        dsDeposit.exchangeCollaterals[_collateralToken].isListed = true;

        // Add the minimum rate limit to the rate limiting entry for this token.
        require(_minimumRateLimit > 0, "Collateral: minimum rate limit must be non-zero.");
        dsDeposit.withdrawalRateLimits[_collateralToken].minimumRateLimit = _minimumRateLimit;

        emit ExchangeCollateralAdded(_collateralToken, address(0), SharedDefs.Flavor.Vanilla, _minimumRateLimit);
    }

    /**
     * @notice Allows governance to update exchange collateral.
     * @param _collateralToken The collateral token to update.
     * @param _minimumRateLimit The minimum amount that the rate limit will be
     *        set to.
     */
    function updateExchangeCollateral(address _collateralToken, uint128 _minimumRateLimit)
        external
        onlyAdmin
        isNotPaused
    {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();

        // Ensure that the collateral is valid and has already been registered.
        require(_collateralToken != address(0), "Collateral: collateral token must be non-zero.");
        require(dsDeposit.exchangeCollaterals[_collateralToken].isListed, "Collateral: collateral hasn't been added.");

        // Update the minimum rate limit.
        require(_minimumRateLimit > 0, "Collateral: minimum rate limit must be non-zero.");
        dsDeposit.withdrawalRateLimits[_collateralToken].minimumRateLimit = _minimumRateLimit;

        emit ExchangeCollateralUpdated(_collateralToken, _minimumRateLimit);
    }

    /**
     * @notice Allows governance to remove exchange collateral.
     * @param _collateralToken The collateral token to remove.
     */
    function removeExchangeCollateral(address _collateralToken) external onlyAdmin isNotPaused {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();

        // Ensure that the collateral was previously registered.
        require(dsDeposit.exchangeCollaterals[_collateralToken].isListed, "Collateral: collateral is not listed.");

        // Remove all data associated with the collateral.
        delete dsDeposit.exchangeCollaterals[_collateralToken];

        // Emit an event so the operator can see that the collateral was removed.
        emit ExchangeCollateralRemoved(_collateralToken);
    }

    /**
     * @notice Allows users to deposit funds into the DerivaDEX exchange.
     * @param _collateralAddress The address of the collateral that should be deposited.
     * @param _strategyId The ID of the strategy to deposit into.
     * @param _amount The amount to depost.
     */
    function deposit(
        address _collateralAddress,
        bytes32 _strategyId,
        uint128 _amount
    ) external isNotPaused nonReentrant {
        LibDiamondStorageBanner.DiamondStorageBanner storage dsBanner = LibDiamondStorageBanner.diamondStorageBanner();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibDiamondStorageDepositGuarded.DiamondStorageDepositGuarded storage dsDepositGuarded =
            LibDiamondStorageDepositGuarded.diamondStorageDepositGuarded();

        // Ensure minimum deposit of USDC is met.
        require(_amount >= dsDepositGuarded.minDeposit, "CollateralGuarded: Must deposit minimum amount.");

        if (!dsDepositGuarded.hasDeposited[msg.sender]) {
            // If address has not already deposited before...

            // Ensure the maximum number of deposited addresses has not
            // been met.
            require(
                dsDepositGuarded.numDepositedAddresses++ < dsDepositGuarded.maxDepositedAddresses,
                "CollateralGuarded: Max num of deposited addresses reached."
            );

            // Reflect the fact this address has deposited.
            dsDepositGuarded.hasDeposited[msg.sender] = true;
        }

        // Ensure that the strategy ID is valid.
        verifyStrategyId(_strategyId);

        // Ensure that the sender is not banned.
        require(!dsBanner.banned[msg.sender], "Collateral: cannot deposit if banned.");

        // Ensure that the sender is not on the chainalysis sanctions list.
        require(!isSanctionedAddress(msg.sender), "Collateral: cannot deposit if on sanctions list.");

        // Ensure that the collateral type is listed.
        require(
            _collateralAddress != address(0) && dsDeposit.exchangeCollaterals[_collateralAddress].isListed,
            "Collateral: cannot deposit unlisted collateral."
        );

        // Commit the deposit to the internal contract state.
        LibCollateral.commitDeposit(dsDeposit, _collateralAddress, _amount);

        // Transfer the funds and emit an event. We scale the deposit amount to
        // eighteen decimal places of precision using the tokens decimals.
        IERC20(_collateralAddress).safeTransferFrom(msg.sender, address(this), _amount);
        emit StrategyUpdated(
            msg.sender,
            _collateralAddress,
            _strategyId,
            LibCollateral.scaleAmountForToken(_amount, _collateralAddress),
            DepositDefs.StrategyUpdateKind.Deposit
        );
    }

    /**
     * @notice Allows users to withdraw funds from the DerivaDEX exchange by
     *         proving their balance within the published state root.
     * @dev This withdrawal strategy does not incorporate rebalancing to
     *      ensure that users can always withdraw the tokens that they
     *      specify. This consideration doesn't have an effect on UX when
     *      there is only one type of collateral that is accepted by the
     *      exchange, but it will become more pressing once multi-collateral
     *      support has been implemented.
     * @param _strategyId The id of the strategy to withdraw from.
     * @param _withdrawalData Data that specifies the tokens and amounts to
     *        withdraw. The withdraw data that is provided to this function must
     *        be given in the same order as the `_strategy.frozenCollateral`
     *        field. Misordering this parameter will cause the function to revert.
     * @param _strategy The data that is contained within the strategy.
     * @param _proof A merkle proof that proves that the included strategy is in
     *        the most recent state root.
     */
    function withdraw(
        bytes32 _strategyId,
        SharedDefs.Balance128 memory _withdrawalData,
        DepositDefs.StrategyData memory _strategy,
        bytes memory _proof
    ) external isNotPaused nonReentrant {
        // Compute the key and the leaf hash for the strategy entry.
        bytes32 key = LibCollateral.generateStrategyKey(msg.sender, _strategyId);
        bytes32 value = hashStrategy(_strategy);
        LibSMT.Leaf[] memory leaves = new LibSMT.Leaf[](1);
        leaves[0] = LibSMT.Leaf({ key: key, value: value });

        // Verify the merkle proof using the key and leaf hash.
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        bytes32 root = LibSMT.computeRoot(leaves, _proof);
        require(root == dsCheckpoint.stateRoot, "Collateral: Did not recover state root.");

        // Verify the withdrawal. This will ensure that the withdrawal data and
        // strategy data are well-formed.
        LibCollateral.commitStrategyWithdrawal(
            msg.sender,
            _strategyId,
            _withdrawalData,
            _strategy.freeCollateral,
            _strategy.frozenCollateral
        );

        // Use internal function to transfer tokens to mitigate stack too deep issue
        transferTokens(_strategyId, _withdrawalData);
    }

    /**
     * @dev Checks whether address is on the chainalysis sanctions list.
     * @param _address The address to check.
     * @return Whether the address is sanctioned.
     */
    function isSanctionedAddress(address _address) internal view virtual returns (bool) {
        ISanctionsList sanctionsList = ISanctionsList(_SANCTIONS_CONTRACT);
        return sanctionsList.isSanctioned(_address);
    }

    /**
     * @dev Validates the strategy ID.
     * @param _strategyId The strategy ID to validate.
     */
    function verifyStrategyId(bytes32 _strategyId) internal pure virtual {
        require(_strategyId == _STRATEGY_ID_MAIN, "Collateral: invalid strategy ID.");
    }

    /**
     * @dev Hash the strategy data that will be stored in the merkle tree. This
     *      hash will be hashed with the key when the merkle proof is actually
     *      being validated.
     * @param _strategyData The leaf data that needs to be hashed.
     * @return The hash of the leaf data.
     */
    function hashStrategy(DepositDefs.StrategyData memory _strategyData) internal pure returns (bytes32) {
        // ABI encode and hash the strategy struct. Prepend the enum identifier
        // of Item::Strategy from the Rust codebase.
        bytes memory encodedStrategy =
            abi.encode(
                DepositDefs.StrategyDataEncoding({
                    discriminant: SharedDefs.ItemKind.Strategy,
                    strategyData: _strategyData
                })
            );
        return keccak256(encodedStrategy);
    }

    /**
     * @notice Gets the relevant information stored for a particular collateral.
     * @param _collateralToken The collateral token of the collateral to query.
     * @return The data pertaining to the collateral.
     */
    function getExchangeCollateralInfo(address _collateralToken)
        external
        view
        returns (DepositDefs.ExchangeCollateral memory)
    {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return dsDeposit.exchangeCollaterals[_collateralToken];
    }

    /**
     * @notice Gets the global rate limit parameters.
     * @return The rate limit period and the rate limit percentage.
     */
    function getRateLimitParameters() external view returns (uint128, uint128) {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return (dsDeposit.rateLimitPeriod, dsDeposit.rateLimitPercentage);
    }

    /**
     * @notice Gets the maximum withdrawal amount and the blocks remaining before
     *         the rate limits are reassessed.
     * @param _tokenAddress The address of the token to withdraw.
     * @return maximumWithdrawalAmount The maximum withdrawal amount.
     * @return blocksRemaining The blocks remaining before rate limit
     *         reassessment.
     */
    function getMaximumWithdrawal(address _tokenAddress)
        external
        view
        returns (uint128 maximumWithdrawalAmount, uint128 blocksRemaining)
    {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        (maximumWithdrawalAmount, , blocksRemaining) = LibCollateral.getMaximumWithdrawal(
            dsDeposit,
            _tokenAddress,
            block.number.safe128("Collateral: Invalid block number.")
        );
        return (maximumWithdrawalAmount, blocksRemaining);
    }

    /**
     * @notice Gets the processed withdrawals of a trader for a given token for
     *         a state in which the operator has a greater confirmed block number
     *         than the latest checkpointed confirmed block number. These
     *         processed withdrawals reflect the amount that the trader's current
     *         frozen collateral for the specified token will be debited, which
     *         makes it possible to calculate the amount of pending withdrawals.
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @param _strategyId The ID of the strategy that is being withdrawn from.
     * @param _tokenAddress The address of the collateral that was withdrawn.
     * @param _blockNumber The confirmed block number to use for the query.
     * @return amount The processed withdrawal amount for the given range.
     */
    function getProcessedWithdrawals(
        address _withdrawAddress,
        bytes32 _strategyId,
        address _tokenAddress,
        uint128 _blockNumber
    ) external view returns (uint128 amount) {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return
            LibCollateral.getProcessedWithdrawals(
                dsCheckpoint,
                dsDeposit,
                _withdrawAddress,
                _strategyId,
                _tokenAddress,
                _blockNumber
            );
    }

    /**
     * @notice Gets the unprocessed withdrawals of a trader for a given token.
     *         DDX withdrawals from the trader leaf are special and are indexed
     *         under address zero to disambiguate withdrawals from trader leaves
     *         and withdrawals from strategy leaves.
     * @param _withdrawAddress The address that is attempting to withdraw.
     * @param _strategyId The ID of the strategy that is being withdrawn from.
     * @param _tokenAddress The address of the collateral that was withdrawn.
     * @return amount The withdrawal amount in the provided epoch.
     */
    function getUnprocessedWithdrawals(
        address _withdrawAddress,
        bytes32 _strategyId,
        address _tokenAddress
    ) external view returns (uint128 amount) {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        (amount, , ) = LibCollateral.getUnprocessedWithdrawals(
            dsCheckpoint,
            dsDeposit,
            _withdrawAddress,
            _strategyId,
            _tokenAddress
        );
        return amount;
    }

    /**
     * @notice Gets the withdrawal allowance for a specified token.
     * @param _tokenAddress The specified token.
     * @return The withdrawal allowance for the specified token.
     */
    function getWithdrawalAllowance(address _tokenAddress) external view returns (uint128) {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return dsDeposit.withdrawalAllowances[_tokenAddress];
    }

    /**
     * @notice Gets guarded deposit specific info.
     * @return The number of unique deposited addresses.
     * @return The maximum number of unique deposited addresses.
     * @return The minimum USDC deposit amount.
     */
    function getGuardedDepositInfo()
        external
        view
        returns (
            uint128,
            uint128,
            uint128
        )
    {
        LibDiamondStorageDepositGuarded.DiamondStorageDepositGuarded storage dsDepositGuarded =
            LibDiamondStorageDepositGuarded.diamondStorageDepositGuarded();
        return (
            dsDepositGuarded.numDepositedAddresses,
            dsDepositGuarded.maxDepositedAddresses,
            dsDepositGuarded.minDeposit
        );
    }

    /**
     * @notice Gets whether a list of addresses have deposited already.
     *         This is pertinent for the guarded launch versions.
     * @return Whether the addresses have ever deposited.
     */
    function getAddressesHaveDeposited(address[] memory _traders) external view returns (bool[] memory) {
        LibDiamondStorageDepositGuarded.DiamondStorageDepositGuarded storage dsDepositGuarded =
            LibDiamondStorageDepositGuarded.diamondStorageDepositGuarded();

        bool[] memory hasDeposited = new bool[](_traders.length);
        for (uint256 i = 0; i < _traders.length; i++) {
            hasDeposited[i] = dsDepositGuarded.hasDeposited[_traders[i]];
        }
        return hasDeposited;
    }

    /**
     * @notice Transfer tokens and emit events for each of the withdrawals encoded
     *         in the provided withdrawal data.
     * @param _strategyId The id of the strategy to withdraw from.
     * @param _withdrawalData Data that specifies the tokens and amounts to
     *        withdraw. The withdraw data that is provided to this function must
     *        be given in the same order as the `_strategy.frozenCollateral`
     *        field. Misordering this parameter will cause the function to revert.
     */
    function transferTokens(bytes32 _strategyId, SharedDefs.Balance128 memory _withdrawalData) internal {
        // Transfer tokens and emit events for each of the withdrawals encoded
        // in the provided withdrawal data. These events can be read by the
        // operator to update the merkle tree in the next few checkpoints and
        // are scaled for consistency with the DerivaDEX operators.
        for (uint256 i = 0; i < _withdrawalData.tokens.length; i++) {
            SafeERC20.safeTransfer(IERC20(_withdrawalData.tokens[i]), msg.sender, _withdrawalData.amounts[i]);
            emit StrategyUpdated(
                msg.sender,
                _withdrawalData.tokens[i],
                _strategyId,
                LibCollateral.scaleAmountForToken(_withdrawalData.amounts[i], _withdrawalData.tokens[i]),
                DepositDefs.StrategyUpdateKind.Withdrawal
            );
        }
    }
}
