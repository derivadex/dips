// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { ReentrancyGuard } from "../libs/ReentrancyGuard.sol";
import { SafeERC20 } from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import { LibSMT } from "../libs/LibSMT.sol";
import { LibCollateral } from "../libs/LibCollateral.sol";
import { FundedInsuranceFundDefs } from "../libs/defs/FundedInsuranceFundDefs.sol";
import { InsuranceFundDefs } from "../libs/defs/InsuranceFundDefs.sol";
import { SharedDefs } from "../libs/defs/SharedDefs.sol";
import { LibDiamondStorageBanner } from "../storage/LibDiamondStorageBanner.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageDeposit } from "../storage/LibDiamondStorageDeposit.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStorageInsuranceFund } from "../storage/LibDiamondStorageInsuranceFund.sol";
import { LibDiamondStoragePause } from "../storage/LibDiamondStoragePause.sol";

/**
 * @title FundedInsuranceFund
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy that allows funders to
 *         provide capital to the DerivaDEX exchange. This capital will
 *         not be subject to DDX issuance from the insurance mining program.
 *         As such, its predominate use case is to provide initial capital
 *         for the DerivaDEX exchange.
 */
contract FundedInsuranceFund is ReentrancyGuard {
    event FunderStatusesSet(address[] funders, bool[] statuses);

    event FundedInsuranceFundUpdated(
        address indexed contributor,
        address indexed collateralAddress,
        uint128 amount,
        FundedInsuranceFundDefs.FundedInsuranceFundUpdateKind updateKind
    );

    /**
     * @notice Ensures function can only be called when the contract is not
     *         paused.
     */
    modifier isNotPaused {
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        require(!dsPause.isPaused, "FundedInsuranceFund: paused.");
        _;
    }

    /**
     * @notice Limits functions to only be called via governance.
     */
    modifier onlyAdmin {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        require(msg.sender == dsDerivaDEX.admin, "FundedInsuranceFund: must be called by Gov.");
        _;
    }

    /**
     * @notice Allows a funder to deposit funds into the funded insurance fund.
     *         Users should note that this is not a safe operation in the
     *         sense that it has a negative expectation (in no scenario will the
     *         funder make money and in some they will lose money).
     * @param _collateralAddress The collateral address of the token to deposit.
     * @param _amount The amount to deposit into the funded insurance fund.
     */
    function fund(address _collateralAddress, uint128 _amount) external isNotPaused nonReentrant {
        // Ensure that the sender is not banned.
        LibDiamondStorageBanner.DiamondStorageBanner storage dsBanner = LibDiamondStorageBanner.diamondStorageBanner();
        require(!dsBanner.banned[msg.sender], "FundedInsuranceFund: banned addresses cannot fund.");

        // Commit the fund.
        commitFund(_collateralAddress, uint128(_amount));

        // Transfer the funds from the sender.
        SafeERC20.safeTransferFrom(IERC20(_collateralAddress), msg.sender, address(this), _amount);
    }

    /**
     * @notice Allows the DerivaDAO diamond to deposit funds from the insurance
     *         funds accumulated withdrawal fees.
     * @param _collateralName The name of the collateral to deposit.
     * @param _amount The amount to deposit into the funded insurance fund.
     */
    function fundWithWithdrawalFees(bytes32 _collateralName, uint96 _amount)
        external
        onlyAdmin
        isNotPaused
        nonReentrant
    {
        LibDiamondStorageInsuranceFund.DiamondStorageInsuranceFund storage dsInsuranceFund =
            LibDiamondStorageInsuranceFund.diamondStorageInsuranceFund();

        // Ensure that the collateral name was registered and that the underlying
        // token is listed as exchange collateral.
        InsuranceFundDefs.StakeCollateral storage stakeCollateral = dsInsuranceFund.stakeCollaterals[_collateralName];
        address collateralAddress = stakeCollateral.collateralToken;
        require(collateralAddress != address(0), "FundedInsuranceFund: invalid collateral.");

        // Commit the fund.
        commitFund(collateralAddress, uint128(_amount));

        // Debit funds from the withdrawal fee cap.
        stakeCollateral.withdrawalFeeCap = stakeCollateral.withdrawalFeeCap - _amount;
    }

    /**
     * @dev This function is public because of a limitation of the Solidity
     *      0.6.12 compiler. Do not call this function internally.
     * @notice Allows a funder to prove a proven balance within the last
     *         checkpoint's state root. If the proof is valid, they are allowed
     *         to withdraw up to the full amount of frozen assets.
     * @param _leafData Data that specifies the contents of a leaf.
     * @param _withdrawalData Data that specifies how much should be withdrawn.
     * @param _proof A merkle proof that proves that the leaf data is accurate.
     */
    function unfund(
        FundedInsuranceFundDefs.FundedInsuranceFundPositionData memory _leafData,
        SharedDefs.Balance128 memory _withdrawalData,
        bytes memory _proof
    ) public isNotPaused nonReentrant {
        // Compute the key and the leaf hash for the funded insurance fund entry.
        bytes32 key = LibCollateral.generateTraderGroupKey(SharedDefs.ItemKind.InsuranceFundContribution, msg.sender);
        bytes32 value = hashFundedInsuranceFundPosition(_leafData);
        LibSMT.Leaf[] memory leaves = new LibSMT.Leaf[](1);
        leaves[0] = LibSMT.Leaf({ key: key, value: value });

        // Verify the merkle proof using the key and leaf hash.
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        bytes32 root = LibSMT.computeRoot(leaves, _proof);
        require(root == dsCheckpoint.stateRoot, "FundedInsuranceFund: Did not recover state root.");

        // Verify that the withdraw data and leaf data is valid.
        LibCollateral.commitStrategyWithdrawal(
            msg.sender,
            bytes32(0),
            _withdrawalData,
            _leafData.freeBalance,
            _leafData.frozenBalance
        );

        // Transfer tokens and emit events for each of the withdrawals encoded
        // in the provided withdrawal data. These events can be read by the
        // operator to update the merkle tree in the next few checkpoints.
        for (uint256 i = 0; i < _withdrawalData.tokens.length; i++) {
            SafeERC20.safeTransfer(IERC20(_withdrawalData.tokens[i]), msg.sender, _withdrawalData.amounts[i]);
            emit FundedInsuranceFundUpdated(
                msg.sender,
                _withdrawalData.tokens[i],
                LibCollateral.scaleAmountForToken(_withdrawalData.amounts[i], _withdrawalData.tokens[i]),
                FundedInsuranceFundDefs.FundedInsuranceFundUpdateKind.Withdraw
            );
        }
    }

    /**
     * @dev Commits the deposit state to the contract's internal state.
     * @param _collateralAddress The collateral address to use when funding.
     * @param _amount The amount to fund.
     */
    function commitFund(address _collateralAddress, uint128 _amount) internal {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();

        // Ensure that the exchange collateral is listed.
        require(
            dsDeposit.exchangeCollaterals[_collateralAddress].isListed,
            "FundedInsuranceFund: can't fund an unlisted token."
        );

        // Commit the deposit to the internal contract state.
        LibCollateral.commitDeposit(dsDeposit, _collateralAddress, _amount);

        // Emit a deposit event. This event will be read by the operator in order
        // to update the tracked insurance fund balances.
        emit FundedInsuranceFundUpdated(
            msg.sender,
            _collateralAddress,
            LibCollateral.scaleAmountForToken(_amount, _collateralAddress),
            FundedInsuranceFundDefs.FundedInsuranceFundUpdateKind.Deposit
        );
    }

    /**
     * @dev Hash the insurance fund position data that will be stored in the
     *      merkle tree. This hash will be hashed with the key when the merkle
     *      proof is actually being validated.
     * @param _leafData The leaf data that needs to be hashed.
     * @return The hash of the leaf data.
     */
    function hashFundedInsuranceFundPosition(FundedInsuranceFundDefs.FundedInsuranceFundPositionData memory _leafData)
        internal
        pure
        returns (bytes32)
    {
        // ABI encode and hash the FundedInsuranceFund leaf data. Prepend the
        // enum identifier of Item::InsuranceFund from the Rust codebase.
        bytes memory encodedStrategy =
            abi.encode(
                FundedInsuranceFundDefs.FundedInsuranceFundPositionEncoding({
                    discriminant: SharedDefs.ItemKind.InsuranceFundContribution,
                    positionData: _leafData
                })
            );
        return keccak256(encodedStrategy);
    }
}
