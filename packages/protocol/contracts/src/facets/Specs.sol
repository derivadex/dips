// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { MathHelpers } from "../libs/MathHelpers.sol";
import { SpecsDefs } from "../libs/defs/SpecsDefs.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStorageSpecs } from "../storage/LibDiamondStorageSpecs.sol";

/**
 * @title Specs
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that manages product
 *         specifications.
 */
contract Specs {
    using MathHelpers for uint256;

    event SpecsInitialized(bytes30[] indexed keys, string[] specs);

    event SpecsUpdated(bytes30 indexed key, string specs, SpecsDefs.SpecsUpdateKind op);

    /**
     * @notice Limits functions to only be called via governance.
     */
    modifier onlyAdmin {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        require(msg.sender == dsDerivaDEX.admin, "Specs: must be called by Governance admin.");
        _;
    }

    /**
     * @notice Initializes the facet with an initial list of markets.
     * @dev In order to efficiently validate the price sources included in market
     *      specs, we expect the source names to be provided in order of their
     *      source index. This function is intended to be the initialization
     *      target of the diamond cut function. This function is not included
     *      in the selectors being added to the diamond, meaning it cannot be
     *      called again.
     * @param _keys A list of specs keys to add.
     * @param _specs A list of market specs to add.
     */
    function initialize(bytes30[] memory _keys, string[] memory _specs) external {
        LibDiamondStorageSpecs.DiamondStorageSpecs storage dsSpecs = LibDiamondStorageSpecs.diamondStorageSpecs();

        // Ensure that the input is well-formed.
        require(_keys.length > 0, "Specs: must add at least one specs.");
        require(_keys.length == _specs.length, "Specs: input parity mismatch.");

        dsSpecs.genesisBlock = block.number.safe128("Specs: invalid block number.");

        // Emit an event to signal the initialization.
        emit SpecsInitialized(_keys, _specs);
    }

    /**
     * @notice Allows governance to update or create specs with the given key.
     * @dev Validation delegated to the operator. We simply emit the specs expression as approved by governance.
     * @param _key The ID of the specs.
     * @param _specs The specifications expression.
     */
    function upsertSpecs(bytes30 _key, string memory _specs) external onlyAdmin {
        require(bytes(_specs).length > 0, "Specs: specification expression cannot be empty.");
        // Emit an event to signal to create or update the specs.
        emit SpecsUpdated(_key, _specs, SpecsDefs.SpecsUpdateKind.Upsert);
    }

    /**
     * @notice Allows governance to remove an active specs.
     * @dev Validation delegated to the operator. We simply emit the specs expression as approved by governance.
     * @param _key The ID of the specs.
     */
    function removeSpecs(bytes30 _key) external onlyAdmin {
        // Emit an event to signal to create or update the specs.
        emit SpecsUpdated(_key, "", SpecsDefs.SpecsUpdateKind.Remove);
    }

    /**
     * @notice Get the genesis block number.
     * @return The genesis block number.
     */
    function getGenesisBlock() external view returns (uint128) {
        LibDiamondStorageSpecs.DiamondStorageSpecs storage dsSpecs = LibDiamondStorageSpecs.diamondStorageSpecs();

        return dsSpecs.genesisBlock;
    }
}
