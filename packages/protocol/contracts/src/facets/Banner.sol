// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { LibRelease } from "../libs/LibRelease.sol";
import { MathHelpers } from "../libs/MathHelpers.sol";
import { RegistrationDefs } from "../libs/defs/RegistrationDefs.sol";
import { LibDiamondStorageBanner } from "../storage/LibDiamondStorageBanner.sol";
import { LibDiamondStorageRegistration } from "../storage/LibDiamondStorageRegistration.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStoragePause } from "../storage/LibDiamondStoragePause.sol";

/**
 * @title Banner
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to banning addresses.
 */
contract Banner {
    using MathHelpers for uint256;

    event SetBannedStatuses(address[] addresses, bool[] statuses);

    /**
     * @notice Ensures function can only be called when the contract is not
     *         paused.
     */
    modifier isNotPaused {
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        require(!dsPause.isPaused, "Banner: paused.");
        _;
    }

    /**
     * @notice Limits functions to only be called via governance.
     */
    modifier onlyAdmin {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        require(msg.sender == dsDerivaDEX.admin, "Banner: must be called by Gov.");
        _;
    }

    /**
     * @notice This contract initializes the Banner system by allowing setting
     *         an initial list of addresses to ban.
     * @dev This function is intended to be the initialization target
     *      of the diamond cut function. This function is not included
     *      in the selectors being added to the diamond, meaning it
     *      cannot be called again.
     * @param _addresses An initial list of addresses to ban.
     */
    function initialize(address[] calldata _addresses) external onlyAdmin {
        LibDiamondStorageBanner.DiamondStorageBanner storage dsBanner = LibDiamondStorageBanner.diamondStorageBanner();

        // Ban all of the addresses provided
        for (uint256 i = 0; i < _addresses.length; i++) {
            require(_addresses[i] != address(0), "Banner: cannot set ban status of zero address.");
            dsBanner.banned[_addresses[i]] = true;
        }
    }

    /**
     * @notice Allows governance to ban an address from calling the majority
     *         of functions within the DerivaDEX diamond. It's worth noting
     *         that these addresses will not be prevented from withdrawing
     *         funds that are already in the exchange.
     * @param _addresses The addresses to ban or unban.
     */
    function setBanStatuses(address[] memory _addresses, bool[] memory _statuses) external onlyAdmin isNotPaused {
        // Validate the functions input.
        require(_addresses.length > 0, "Banner: address list to ban must be non-empty.");
        require(_addresses.length == _statuses.length, "Banner: input parity mismatch.");

        // Get a reference to the registration diamond storage.
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Get current release hash
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);

        // Update the ban status of each address in the list.
        uint128 validSignersCountDelta = 0;
        LibDiamondStorageBanner.DiamondStorageBanner storage dsBanner = LibDiamondStorageBanner.diamondStorageBanner();
        for (uint256 i = 0; i < _addresses.length; i++) {
            // Ensure address is non-zero.
            require(_addresses[i] != address(0), "Banner: cannot set ban status of zero address.");

            // Ensure that the ban status actually changes the addresses state.
            require(dsBanner.banned[_addresses[i]] != _statuses[i], "Banner: can't set the same status.");

            // Set ban status.
            dsBanner.banned[_addresses[i]] = _statuses[i];

            RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[_addresses[i]];
            if (_statuses[i] && custodian.approved) {
                // If the address is being banned and the custodian is approved...

                // Unapprove the custodian.
                custodian.approved = false;

                if (
                    custodian.signers[releaseHash] != address(0) &&
                    custodian.balance >= dsRegistration.minimumBond &&
                    custodian.unbondETA == 0 &&
                    !custodian.jailed
                ) {
                    // If the custodian had a valid signer...

                    // Increment the valid signers count delta.
                    validSignersCountDelta++;
                }

                if (custodian.balance > 0) {
                    // If the custodian has a balance...

                    // Prepare the custodian to unbond. This ensures that it's
                    // impossible for the custodian to have a valid signer
                    // unless it is re-approved.
                    custodian.unbondETA =
                        block.number.safe128("Registration: invalid block number.") +
                        dsRegistration.unbondDelay;
                }
            }
        }

        // Update the valid signers count.
        dsRegistration.validSignersCount = dsRegistration.validSignersCount - validSignersCountDelta;

        // Emit an event that communicates new ban statuses to the operator.
        emit SetBannedStatuses(_addresses, _statuses);
    }

    /**
     * @notice Gets the ban status of a given address.
     * @param _address The address to query.
     * @return The ban status.
     */
    function getBanStatus(address _address) external view returns (bool) {
        LibDiamondStorageBanner.DiamondStorageBanner storage dsBanner = LibDiamondStorageBanner.diamondStorageBanner();
        return dsBanner.banned[_address];
    }
}
