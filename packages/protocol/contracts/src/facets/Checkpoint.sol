// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { LibCheckpoint } from "../libs/LibCheckpoint.sol";
import { LibCollateral } from "../libs/LibCollateral.sol";
import { LibRelease } from "../libs/LibRelease.sol";
import { MathHelpers } from "../libs/MathHelpers.sol";
import { CheckpointDefs } from "../libs/defs/CheckpointDefs.sol";
import { LibDiamondStorageCheckpoint } from "../storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageDeposit } from "../storage/LibDiamondStorageDeposit.sol";
import { LibDiamondStorageDerivaDEX } from "../storage/LibDiamondStorageDerivaDEX.sol";
import { LibDiamondStoragePause } from "../storage/LibDiamondStoragePause.sol";
import { LibDiamondStorageRegistration } from "../storage/LibDiamondStorageRegistration.sol";

/**
 * @title Checkpoint
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to exchange checkpointing. This logic handles
 *         checkpoints for the DerivaDEX diamond.
 */
contract Checkpoint {
    using MathHelpers for uint256;

    // The minimum value allowable when validating the consensus threshold.
    uint128 internal constant _CONSENSUS_MINIMUM_THRESHOLD = 50;

    // The maximum value allowable when validating the consensus threshold.
    uint128 internal constant _CONSENSUS_MAXIMUM_THRESHOLD = 100;

    // The minimum value allowable when validating the quorum.
    uint128 internal constant _QUORUM_MINIMUM = 2;

    event CheckpointInitialized(uint128 indexed consensusThreshold, uint128 indexed quorum);

    event Checkpointed(
        bytes32 indexed stateRoot,
        bytes32 indexed transactionRoot,
        uint128 indexed epochId,
        address[] custodians,
        uint128[] bonds,
        address submitter
    );

    event ConsensusThresholdSet(uint128 consensusThreshold);

    event CustodiansJailed(address[] custodians);

    event QuorumSet(uint128 quorum);

    /**
     * @notice Ensures function can only be called when the contract is not
     *         paused.
     */
    modifier isNotPaused {
        LibDiamondStoragePause.DiamondStoragePause storage dsPause = LibDiamondStoragePause.diamondStoragePause();
        require(!dsPause.isPaused, "Checkpoint: paused.");
        _;
    }

    /**
     * @notice Limits functions to only be called via governance.
     */
    modifier onlyAdmin {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        require(msg.sender == dsDerivaDEX.admin, "Checkpoint: must be called by Gov.");
        _;
    }

    /**
     * @notice Allows governance to initialize the Checkpoint facet.
     * @dev This function is intended to be the initialization target
     *      of the diamond cut function. This function is not included
     *      in the selectors being added to the diamond, meaning it
     *      cannot be called again.
     * @param _consensusThreshold The initial consensus threshold.
     * @param _quorum The quorum that will be enforced as a consensus rule.
     */
    function initialize(uint128 _consensusThreshold, uint128 _quorum) external onlyAdmin {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();

        // Update the consensus threshold.
        verifyConsensusThreshold(_consensusThreshold);
        dsCheckpoint.consensusThreshold = _consensusThreshold;

        // Update the quorum.
        verifyQuorum(_quorum);
        dsCheckpoint.quorum = _quorum;

        emit CheckpointInitialized(_consensusThreshold, _quorum);
    }

    /**
     * @notice Allows governance to update the consensus threshold. The
     *         consensus threshold defines a percentage of valid signers that
     *         must create a given submission for the submission to be
     *         accepted by the contracts.
     * @param _consensusThreshold A number between 50 and 100 that represents
     *        the percentage of valid signers that have to agree for a
     *        submission to be considered valid.
     */
    function setConsensusThreshold(uint128 _consensusThreshold) external onlyAdmin {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        verifyConsensusThreshold(_consensusThreshold);

        // Update the consensus threshold.
        dsCheckpoint.consensusThreshold = _consensusThreshold;
        emit ConsensusThresholdSet(_consensusThreshold);
    }

    /**
     * @notice Allows governance to update the quorum. This contract enforces
     *         a quorum for all checkpoint updates in order to provide
     *         stronger guarantees about the decentralization of the system.
     * @param _quorum The quorum value that will be enforced for consensus.
     */
    function setQuorum(uint128 _quorum) external onlyAdmin {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        verifyQuorum(_quorum);

        // Update the quorum
        dsCheckpoint.quorum = _quorum;
        emit QuorumSet(_quorum);
    }

    /**
     * @notice This function allows valid signers to create new "checkpoints".
     *         A checkpoint is a state update to the DerivaDEX diamond that
     *         updates the state and transaction roots in the diamond's
     *         storage. The state and transaction roots contain merklized
     *         state that can be used to prove things about the state of the
     *         DerivaDEX system at a given point in time. Since these roots
     *         will be used to guard withdrawals from the system, they can
     *         only be updated by valid signers (signers that have been
     *         registered and are controlled by a bonded and unjailed
     *         custodian that has been approved by governance to run an
     *         exchange operator). This function verifies that consensus has
     *         been reached by ensuring that there are enough valid checkpoint
     *         submissions that agree and were submitted by valid signers to
     *         meet or exceed the consensus and quorum thresholds.
     *
     *         Several conditions are enforced on the parameters of this
     *         function:
     *
     *         - Each checkpoint submission must correspond to a set of
     *           signatures for signers that have signed off on the checkpoint.
     *           There must be at least one checkpoint submission.
     *         - The signatures for a particular checkpoint submission must
     *           correspond to valid signer addresses that are distinct and
     *           ordered in ascending order.
     *
     *         These pre-conditions enforce strict regularity conditions on
     *         the input to ensure that the checkpoint flow is gas-efficient.
     *
     * @param _majorityCheckpointSubmission Structured data that contains a
     *        state root, a transaction root, block data, and signatures of
     *        signers that have attested to the checkpoint.
     * @param _minorityCheckpointSubmissions Same as the valid checkpoints but
     *        the hashes don't match that of the majority checkpoint.
     * @param _epochId The epoch id of the checkpoint being submitted. This id
     *        must monotonically increase, but we permit skips in the id.
     */
    function checkpoint(
        CheckpointDefs.CheckpointSubmission memory _majorityCheckpointSubmission,
        CheckpointDefs.CheckpointSubmission[] memory _minorityCheckpointSubmissions,
        uint128 _epochId
    ) external isNotPaused {
        // Get references to diamond storage.
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();

        // Ensure that the new epoch ID is larger than the previous epoch ID.
        require(dsCheckpoint.currentEpochId < _epochId, "Checkpoint: Epoch ID must monotonically increase.");

        // Attempt to advance the release schedule and get the current release
        // hash that should be used when validating checkpoint submissions.
        bytes32 releaseHash = LibRelease.evaluateReleaseSchedule(dsRegistration, _epochId);

        // Compute the reference hash as the hash of the majority
        // checkpoint submission's data. We ensure this is non-zero,
        // (which prevents any possible problems in the execution of the
        // function for zero hashes) and will use this to invalidate the
        // minority checkpoint submissions.
        bytes32 referenceHash =
            LibCheckpoint.hashCheckpointData(_majorityCheckpointSubmission.checkpointData, _epochId);
        require(referenceHash != bytes32(0), "Checkpoint: reference hash must be non-zero.");

        // Maintain a reference to the original number of valid signers since
        // jailing custodians will decrease this value, but we need to use the original
        // when checking the majority submission
        uint128 originalValidSignersCount = dsRegistration.validSignersCount;

        // Process minority checkpoint submissions in seperate function to avoid
        // issues with stack too deep
        processMinorityCheckpointSubmissions(
            _minorityCheckpointSubmissions,
            dsRegistration,
            _epochId,
            releaseHash,
            referenceHash
        );

        // Verify the block data against the current chain.
        require(
            _majorityCheckpointSubmission.checkpointData.blockNumber > block.number - 256 &&
                _majorityCheckpointSubmission.checkpointData.blockNumber < block.number &&
                _majorityCheckpointSubmission.checkpointData.blockNumber > dsCheckpoint.latestConfirmedBlockNumber,
            "Checkpoint: Submission references an invalid block number."
        );
        bytes32 blockHash = blockhash(_majorityCheckpointSubmission.checkpointData.blockNumber);
        require(
            blockHash != bytes32(0) && _majorityCheckpointSubmission.checkpointData.blockHash == blockHash,
            "Checkpoint: Submission references an invalid chain."
        );

        // Verify the majority checkpoint submission.
        (address[] memory majorityCustodians, uint128[] memory majorityBonds) =
            LibCheckpoint.verifySubmission(
                dsRegistration,
                _majorityCheckpointSubmission,
                _epochId,
                releaseHash,
                bytes32(0)
            );

        // Verify that consensus was achieved by the submission.
        LibCheckpoint.verifyConsensusRule(majorityCustodians.length, originalValidSignersCount);

        // Increase the DDX withdrawal allowance by the distributed trade
        // mining rewards collected in the checkpointed epochs. The withdrawal
        // allowance for the trader leaf is stored in the entry for address
        // zero in the withdrawal allowances mapping.
        dsDeposit.withdrawalAllowances[address(0)] =
            dsDeposit.withdrawalAllowances[address(0)] +
            LibCollateral.calculateTradeMiningRewardsForRange(dsCheckpoint.currentEpochId, _epochId);

        // Update the block number, state and transaction roots using the new
        // data, increment the epoch ID, and emit an event that records the
        // checkpoint for the last epoch.
        dsCheckpoint.latestConfirmedBlockNumber = _majorityCheckpointSubmission.checkpointData.blockNumber;
        dsCheckpoint.stateRoot = _majorityCheckpointSubmission.checkpointData.stateRoot;
        dsCheckpoint.transactionRoot = _majorityCheckpointSubmission.checkpointData.transactionRoot;
        dsCheckpoint.currentEpochId = _epochId;

        // Emit an event signaling that a new checkpoint has been created.
        emit Checkpointed(
            _majorityCheckpointSubmission.checkpointData.stateRoot,
            _majorityCheckpointSubmission.checkpointData.transactionRoot,
            _epochId,
            majorityCustodians,
            majorityBonds,
            msg.sender
        );
    }

    /**
     * @notice Gets some high level information about the Checkpoint facet.
     * @return The consensus threshold and the quorum.
     */
    function getCheckpointInfo() external view returns (uint128, uint128) {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        return (dsCheckpoint.consensusThreshold, dsCheckpoint.quorum);
    }

    /**
     * @notice Returns important metadata about the most recent checkpoint.
     * @return The most important pieces of metadata about the most recent
     *         checkpoint.
     */
    function getLatestCheckpoint()
        external
        view
        returns (
            uint128,
            bytes32,
            bytes32,
            uint128
        )
    {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        return (
            dsCheckpoint.latestConfirmedBlockNumber,
            dsCheckpoint.stateRoot,
            dsCheckpoint.transactionRoot,
            dsCheckpoint.currentEpochId
        );
    }

    /**
     * @notice Verifies that the provided consensus threshold is valid.
     * @param _consensusThreshold A consensus threshold to verify.
     */
    function verifyConsensusThreshold(uint128 _consensusThreshold) internal pure virtual {
        require(
            _consensusThreshold > _CONSENSUS_MINIMUM_THRESHOLD && _consensusThreshold <= _CONSENSUS_MAXIMUM_THRESHOLD,
            "Checkpoint: consensus must represent a majority opinion of the operators."
        );
    }

    /**
     * @notice Verifies that the provided quorum is valid.
     * @param _quorum A quorum value to verify
     */
    function verifyQuorum(uint128 _quorum) internal pure virtual {
        require(_quorum >= _QUORUM_MINIMUM, "Checkpoint: 2 is the lowest allowed quorum");
    }

    /**
     * @notice Processes the minority checkpoint submissions.
     * @param _minorityCheckpointSubmissions Structured data that contains a
     *        state root, a transaction root, block data, and signatures of
     *        signers that have attested to the checkpoint. The hashes of
     *        minority submissions shouldn't match that of the majority
     *        checkpoint.
     * @param _dsRegistration Pointer to the diamond storage for Registration.
     * @param _epochId The epoch id of the checkpoint being submitted. This id
     *        must monotonically increase, but we permit skips in the id.
     * @param _releaseHash The current release hash that identifies which
     *        signer can submit checkpoints.
     * @param _referenceHash The hash of the majority checkpoint submission's data.
     */
    function processMinorityCheckpointSubmissions(
        CheckpointDefs.CheckpointSubmission[] memory _minorityCheckpointSubmissions,
        LibDiamondStorageRegistration.DiamondStorageRegistration storage _dsRegistration,
        uint128 _epochId,
        bytes32 _releaseHash,
        bytes32 _referenceHash
    ) internal {
        // Loop through minority checkpoint submissions. We validate
        // the minority checkpoint submissions first to rule out the
        // possibility of receiving duplicate submissions from signers.
        uint256 minoritySubmittersCount = 0;
        for (uint256 i = 0; i < _minorityCheckpointSubmissions.length; i++) {
            minoritySubmittersCount += _minorityCheckpointSubmissions[i].signatures.length;
        }
        address[] memory jailedCustodians = new address[](minoritySubmittersCount);
        minoritySubmittersCount = 0;
        for (uint256 i = 0; i < _minorityCheckpointSubmissions.length; i++) {
            // Verify each minority submission.
            (address[] memory jailedCustodians_, ) =
                LibCheckpoint.verifySubmission(
                    _dsRegistration,
                    _minorityCheckpointSubmissions[i],
                    _epochId,
                    _releaseHash,
                    _referenceHash
                );

            // Jail the custodians and add the addresses to the list of all
            // the jailed custodians.
            for (uint256 j = 0; j < jailedCustodians_.length; j++) {
                // Add the custodian to the list of jailed custodians.
                jailedCustodians[minoritySubmittersCount++] = jailedCustodians_[j];
            }
        }

        if (minoritySubmittersCount > 0) {
            // If at least one custodian was jailed...

            // Resize the list of jailed custodians and emit a jailed event.
            assembly {
                mstore(jailedCustodians, minoritySubmittersCount)
            }
            emit CustodiansJailed(jailedCustodians);
        }
    }
}
