// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

interface IInsuranceFund {
    function claimDDXFromInsuranceMining(address _claimant) external;
}
