// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { LibStack } from "../src/libs/LibStack.sol";

contract TestLibStack {
    using LibStack for LibStack.Stack;

    function testPopEmptyStack() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Ensure that the stack starts with an empty length.
        verifyStackSize(stack, 0x0);

        // Pop an item off of the stack. This should cause the function to revert,
        // and the Typescript test will verify that the correct error message is
        // provided.
        stack.popItem();
    }

    function testPushAfterMemoryAllocation() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Push two items to the stack.
        LibStack.pushItem(stack, bytes32(hex"deadbeef"));
        LibStack.pushItem(stack, bytes32(hex"beefdead"));

        // Pop an item from the stack.
        LibStack.popItem(stack);

        // Allocate memory, which will violate the assumptions of LibStack.
        assembly {
            let addr := mload(0x40)
            mstore(0x40, add(addr, 32))
        }

        // Push an item to the stack. This should cause the function to revert,
        // and the Typescript test will verify that the correct error message is
        // provided.
        LibStack.pushItem(stack, bytes32(hex"beefdead"));
    }

    function testPopAfterMemoryAllocation() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Push two items to the stack.
        LibStack.pushItem(stack, bytes32(hex"deadbeef"));
        LibStack.pushItem(stack, bytes32(hex"beefdead"));

        // Pop an item from the stack.
        LibStack.popItem(stack);

        // Allocate memory, which will violate the assumptions of LibStack.
        assembly {
            let addr := mload(0x40)
            mstore(0x40, add(addr, 32))
        }

        // Pop an item from the stack. This should cause the function to revert,
        // and the Typescript test will verify that the correct error message is
        // provided.
        LibStack.popItem(stack);
    }

    function testSizeAfterMemoryAllocation() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Push two items to the stack.
        LibStack.pushItem(stack, bytes32(hex"deadbeef"));
        LibStack.pushItem(stack, bytes32(hex"beefdead"));

        // Pop an item from the stack.
        LibStack.popItem(stack);

        // Allocate memory, which will violate the assumptions of LibStack.
        assembly {
            let addr := mload(0x40)
            mstore(0x40, add(addr, 32))
        }

        // Get size of the stack. This should cause the function to revert,
        // and the Typescript test will verify that the correct error message is
        // provided.
        LibStack.size(stack);
    }

    function testPushOneItem() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Push an item to the stack.
        LibStack.pushItem(stack, bytes32(hex"deadbeef"));

        // Ensure that the stack size reflects the push.
        verifyStackSize(stack, 0x20);

        // Ensure that the item that was pushed onto the stack can be popped off.
        verifyPop(stack, bytes32(hex"deadbeef"));

        // Ensure that the stack size reflects the pop.
        verifyStackSize(stack, 0x0);
    }

    function testPushTwoItems() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Push two items to the stack.
        LibStack.pushItem(stack, bytes32(hex"deadbeef"));
        LibStack.pushItem(stack, bytes32(hex"beefdead"));

        // Ensure that the stack size reflects both pushes.
        verifyStackSize(stack, 0x40);

        // Pop the first item off of the stack and ensure that it is correct.
        verifyPop(stack, bytes32(hex"beefdead"));

        // Ensure that the stack size reflects the pop.
        verifyStackSize(stack, 0x20);

        // Pop the second item off of the stack and ensure that it is correct.
        verifyPop(stack, bytes32(hex"deadbeef"));

        // Ensure that the stack size reflects the pop.
        verifyStackSize(stack, 0x0);
    }

    function testTwoMixedPushAndPop() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Push an item to the stack and then pop it off. Ensure that it has the
        // correct value.
        LibStack.pushItem(stack, bytes32(hex"deadbeef"));
        verifyPop(stack, bytes32(hex"deadbeef"));

        // Push an item to the stack.
        LibStack.pushItem(stack, bytes32(hex"beefdead"));

        // Ensure the stack size reflects the push.
        verifyStackSize(stack, 0x20);

        // Pop the item from the stack and ensure that it is correct.
        verifyPop(stack, bytes32(hex"beefdead"));

        // Ensure the stack size reflects the pop.
        verifyStackSize(stack, 0x0);
    }

    function testRepeatedTwoItemPush() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Push two items to the stack and pop them off. Ensure that the values
        // are correct.
        LibStack.pushItem(stack, bytes32(hex"deadbeef"));
        LibStack.pushItem(stack, bytes32(hex"beefdead"));
        verifyPop(stack, bytes32(hex"beefdead"));
        verifyPop(stack, bytes32(hex"deadbeef"));

        // Push an item to the stack.
        LibStack.pushItem(stack, bytes32(hex"beef"));

        // Ensure that the stack size reflects the push.
        verifyStackSize(stack, 0x20);

        // Push another item onto the stack.
        LibStack.pushItem(stack, bytes32(hex"dead"));

        // Ensure that the stack size reflects both pushes.
        verifyStackSize(stack, 0x40);

        // Pop the first item off of the stack and ensure that it is correct.
        verifyPop(stack, bytes32(hex"dead"));

        // Ensure that the stack size reflects the pop.
        verifyStackSize(stack, 0x20);

        // Pop the second item off of the stack and ensure that it is correct.
        verifyPop(stack, bytes32(hex"beef"));

        // Ensure that the stack size reflects the pop.
        verifyStackSize(stack, 0x0);
    }

    function testMultiLayeredPushAndPop() external pure {
        LibStack.Stack memory stack = LibStack.init();

        // Push two items onto the stack and pop the last item off. Ensure it has
        // the correct value.
        LibStack.pushItem(stack, bytes32(hex"deadbeef"));
        LibStack.pushItem(stack, bytes32(hex"beefdead"));
        verifyPop(stack, bytes32(hex"beefdead"));

        // Push four more items to the stack.
        LibStack.pushItem(stack, bytes32(hex"0a"));
        LibStack.pushItem(stack, bytes32(hex"0b"));
        LibStack.pushItem(stack, bytes32(hex"0c"));
        LibStack.pushItem(stack, bytes32(hex"0d"));

        // Verify that the stack size is correct.
        verifyStackSize(stack, 0xa0);

        // Pop off two items and ensure that the values are correct.
        verifyPop(stack, bytes32(hex"0d"));
        verifyPop(stack, bytes32(hex"0c"));

        // Verify that the stack size is correct.
        verifyStackSize(stack, 0x60);

        // Push five more items onto the stack.
        LibStack.pushItem(stack, bytes32(hex"01"));
        LibStack.pushItem(stack, bytes32(hex"02"));
        LibStack.pushItem(stack, bytes32(hex"03"));
        LibStack.pushItem(stack, bytes32(hex"04"));
        LibStack.pushItem(stack, bytes32(hex"05"));

        // Verify that the stack size is correct.
        verifyStackSize(stack, 0x100);

        // Pop all of the remaining items off of the stack.
        verifyPop(stack, bytes32(hex"05"));
        verifyPop(stack, bytes32(hex"04"));
        verifyPop(stack, bytes32(hex"03"));
        verifyPop(stack, bytes32(hex"02"));
        verifyPop(stack, bytes32(hex"01"));
        verifyPop(stack, bytes32(hex"0b"));
        verifyPop(stack, bytes32(hex"0a"));
        verifyPop(stack, bytes32(hex"deadbeef"));

        // Verify that the stack is empty.
        verifyStackSize(stack, 0x0);
    }

    function verifyPop(LibStack.Stack memory _stack, bytes32 _expectedItem) private pure {
        require(_stack.popItem() == _expectedItem, "Unexpected stack item.");
    }

    function verifyStackSize(LibStack.Stack memory _stack, uint256 _expectedSize) private pure {
        require(_stack.size() == _expectedSize, "Unexpected stack size.");
    }
}
