pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { CheckpointDefs } from "../src/libs/defs/CheckpointDefs.sol";
import { RegistrationDefs } from "../src/libs/defs/RegistrationDefs.sol";
import { LibCheckpoint } from "../src/libs/LibCheckpoint.sol";
import { LibDiamondStorageCheckpoint } from "../src/storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageRegistration } from "../src/storage/LibDiamondStorageRegistration.sol";

contract TestLibCheckpoint {
    function setConsensusThreshold(uint128 _consensusThreshold) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.consensusThreshold = _consensusThreshold;
    }

    function setCurrentReleaseHash(bytes32 _releaseHash) external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        dsRegistration.currentReleaseHash = _releaseHash;
    }

    function setCustodian(
        address _custodianAddress,
        RegistrationDefs.CustodianWithoutSigners memory _custodian,
        bool _shouldUpdateValidSignersCount
    ) external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        dsRegistration.custodians[_custodianAddress].balance = _custodian.balance;
        dsRegistration.custodians[_custodianAddress].unbondETA = _custodian.unbondETA;
        dsRegistration.custodians[_custodianAddress].approved = _custodian.approved;
        dsRegistration.custodians[_custodianAddress].jailed = _custodian.jailed;
        dsRegistration.validSignersCount = dsRegistration.validSignersCount + 1;
    }

    function setCustodianToSigners(address _custodianAddress, address _signerAddress) external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        dsRegistration.custodians[_custodianAddress].signers[dsRegistration.currentReleaseHash] = _signerAddress;
    }

    function setMinimumBond(uint128 _minimumBond) external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        dsRegistration.minimumBond = _minimumBond;
    }

    function setQuorum(uint128 _quorum) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.quorum = _quorum;
    }

    function setSigner(
        address _signerAddress,
        bytes32 _releaseHash,
        RegistrationDefs.Signer memory _signer
    ) external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        dsRegistration.signers[_releaseHash][_signerAddress] = _signer;
    }

    function setValidSignersCount(uint128 _validSignersCount) external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        dsRegistration.validSignersCount = _validSignersCount;
    }

    function verifySubmission(
        CheckpointDefs.CheckpointSubmission memory _checkpointSubmission,
        uint256 _epochId,
        bytes32 _releaseHash,
        bytes32 _referenceHashForInvalidCheckpoints
    ) external returns (address[] memory, uint128[] memory) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return
            LibCheckpoint.verifySubmission(
                dsRegistration,
                _checkpointSubmission,
                _epochId,
                _releaseHash,
                _referenceHashForInvalidCheckpoints
            );
    }

    function verifyConsensusRule(uint256 _consensusCount, uint128 _validSignersCount) external view {
        LibCheckpoint.verifyConsensusRule(_consensusCount, _validSignersCount);
    }

    function getCustodian(address _custodianAddress)
        external
        view
        returns (RegistrationDefs.CustodianWithoutSigners memory)
    {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        RegistrationDefs.Custodian storage custodian = dsRegistration.custodians[_custodianAddress];
        return
            RegistrationDefs.CustodianWithoutSigners({
                balance: custodian.balance,
                unbondETA: custodian.unbondETA,
                approved: custodian.approved,
                jailed: custodian.jailed
            });
    }

    function getValidSignersCount() external view returns (uint128) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return dsRegistration.validSignersCount;
    }

    function hashCheckpointData(CheckpointDefs.CheckpointData memory _checkpointData, uint256 _epochId)
        external
        view
        returns (bytes32)
    {
        return LibCheckpoint.hashCheckpointData(_checkpointData, _epochId);
    }
}
