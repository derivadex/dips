// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { MathHelpers } from "../src/libs/MathHelpers.sol";
import { LibDiamondStorageCheckpoint } from "../src/storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageDeposit } from "../src/storage/LibDiamondStorageDeposit.sol";
import { Checkpoint } from "../src/facets/Checkpoint.sol";

/**
 * @title TestCheckpoint
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to checkpointing. This logic handles operator
 *         checkpointing for the DerivaDEX diamond.
 * @dev This contract should not be used in production. It's useful because it
 *      overrides `register` and does not enforce the Intel SGX cryptography. or
 *      any of the other validation enforced in the production Operator. This
 *      makes it easy to test the core logic of the contract without needing to
 *      use SGX hardware.
 */
contract TestCheckpoint is Checkpoint {
    using MathHelpers for uint256;

    function setEpochId(uint128 _epochId) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.currentEpochId = _epochId;
    }

    function setLastConfirmedBlockNumber(uint128 _lastConfirmedBlockNumber) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.latestConfirmedBlockNumber = _lastConfirmedBlockNumber;
    }

    function setWithdrawalAllowance(address _tokenAddress, uint128 _allowance) external {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        dsDeposit.withdrawalAllowances[_tokenAddress] = _allowance;
    }

    // A test function that returns the operators epoch ID. This is useful for
    // testing that the epoch ID was updated on failures within the `checkpoint`
    // function.
    function getEpochId() external view returns (uint128) {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        return dsCheckpoint.currentEpochId;
    }
}
