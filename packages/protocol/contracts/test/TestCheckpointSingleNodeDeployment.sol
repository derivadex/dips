// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { TestCheckpointMultiNodeDeployment } from "./TestCheckpointMultiNodeDeployment.sol";

/**
 * @title TestCheckpointSingleNodeDeployment
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to exchange operators. This logic handles operator
 *         registration, checkpointing, and operator consensus for the DerivaDEX
 *         diamond.
 * @dev This contract should not be used in production. This makes it easier to
 *      to test the DerivaDEX Exchange end-to-end by providing convenience
 *      setters for the state root.
 */
contract TestCheckpointSingleNodeDeployment is TestCheckpointMultiNodeDeployment {
    // Used to test checkpointing end-to-end with a single operator.
    function verifyConsensusThreshold(uint128 _consensusThreshold) internal pure override {}

    // Used to test checkpointing end-to-end with a single operator.
    function verifyQuorum(uint128 _quorum) internal pure override {}
}
