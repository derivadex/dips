// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { LibBytes32 } from "../src/libs/LibBytes32.sol";

contract TestLibBytes32 {
    function parentPathExternal(bytes32 _key, uint8 _height) external pure returns (bytes32) {
        return LibBytes32.parentPath(_key, _height);
    }

    function setBitExternal(bytes32 _value, uint8 _idx) external pure returns (bytes32) {
        return LibBytes32.setBit(_value, _idx);
    }

    function getBitExternal(bytes32 _value, uint8 _idx) external pure returns (bool) {
        return LibBytes32.getBit(_value, _idx);
    }

    function copyBitsExternal(bytes32 _value, uint8 _start) external pure returns (bytes32) {
        return LibBytes32.copyBits(_value, _start);
    }
}
