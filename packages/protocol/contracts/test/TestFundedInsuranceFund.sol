// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { FundedInsuranceFund } from "../src/facets/FundedInsuranceFund.sol";
import { FundedInsuranceFundDefs } from "../src/libs/defs/FundedInsuranceFundDefs.sol";
import { LibDiamondStorageCheckpoint } from "../src/storage/LibDiamondStorageCheckpoint.sol";

contract TestFundedInsuranceFund is FundedInsuranceFund {
    function setStateRoot(bytes32 _stateRoot) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.stateRoot = _stateRoot;
    }

    function hashFundedInsuranceFundPositionPublic(
        FundedInsuranceFundDefs.FundedInsuranceFundPositionData memory _leafData
    ) public pure returns (bytes32) {
        return hashFundedInsuranceFundPosition(_leafData);
    }
}
