// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { Stake } from "../src/facets/Stake.sol";
import { LibCollateral } from "../src/libs/LibCollateral.sol";
import { DepositDefs } from "../src/libs/defs/DepositDefs.sol";
import { SharedDefs } from "../src/libs/defs/SharedDefs.sol";
import { LibDiamondStorageDeposit } from "../src/storage/LibDiamondStorageDeposit.sol";

contract TestStake is Stake {
    function getTraderDDXDeposited() external view returns (uint128) {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return dsDeposit.traderDDXDeposited;
    }

    function generateTraderGroupKeyExternal(SharedDefs.ItemKind _itemKind, address _traderAddress)
        external
        pure
        returns (bytes32)
    {
        return LibCollateral.generateTraderGroupKey(_itemKind, _traderAddress);
    }

    function hashTraderPublic(DepositDefs.TraderData memory _trader) external pure returns (bytes32) {
        return hashTrader(_trader);
    }

    // Overriding this for test contracts since there is a mainnet-specific constant set
    // for the sanctions list
    function isSanctionedAddress(address _address) internal view override returns (bool) {
        return false;
    }
}
