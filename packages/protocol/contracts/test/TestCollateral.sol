// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { Collateral } from "../src/facets/Collateral.sol";
import { LibDiamondStorageCheckpoint } from "../src/storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageDeposit } from "../src/storage/LibDiamondStorageDeposit.sol";

contract TestCollateral is Collateral {
    function setExchangeCollateralState(address _collateralToken) external {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        dsDeposit.exchangeCollaterals[_collateralToken].isListed = true;
    }

    function setStateRoot(bytes32 _stateRoot) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.stateRoot = _stateRoot;
    }

    // Overriding this function ensures that we can adequately test the contracts
    // while we have a patch in for the trading competition.
    function verifyStrategyId(bytes32) internal pure override {}

    // Overriding this for test contracts since there is a mainnet-specific constant set
    // for the sanctions list
    function isSanctionedAddress(address _address) internal view override returns (bool) {
        return false;
    }
}
