// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { LibSMT } from "../src/libs/LibSMT.sol";
import { LibStack } from "../src/libs/LibStack.sol";

contract TestLibSMT {
    using LibStack for LibStack.Stack;
    using LibSMT for LibStack.Stack;
    using LibSMT for LibSMT.State;

    // This structure encodes the information needed to set up a Solidity test
    // for internal SMT operations.
    struct TestState {
        uint256 proofIndex;
        uint256 leafIndex;
        LibSMT.Leaf[] initialStack;
    }

    function computeRootPublic(LibSMT.Leaf[] memory _leaves, bytes memory _proof) public pure returns (bytes32) {
        return LibSMT.computeRoot(_leaves, _proof);
    }

    function testOperationHashSiblings(TestState memory _state, bytes memory _proof)
        public
        pure
        returns (bytes32, bytes32)
    {
        LibSMT.State memory state = testInit(_state);
        uint256 startStackLength = state.stack.size();
        state.operationHashSiblings(_proof);
        require(state.proofIndex == _state.proofIndex + 1, "testOperationHashSiblings: proofIndex is incorrect");
        require(state.stack.size() == startStackLength - 0x40, "testOperationHashSiblings: Stack size is incorrect.");
        return state.stack.popNode();
    }

    function testOperationPushLeaf(TestState memory _state, LibSMT.Leaf[] memory _leaves)
        public
        pure
        returns (bytes32, bytes32)
    {
        LibSMT.State memory state = testInit(_state);
        uint256 startStackLength = state.stack.size();
        state.operationPushLeaf(_leaves);
        require(state.leafIndex == _state.leafIndex + 1, "testOperationHashSiblings: proofIndex is incorrect");
        require(state.stack.size() == startStackLength + 0x40, "testOperationPushLeaf: Stack size is incorrect.");
        return state.stack.popNode();
    }

    function testOperationHashProofElement(TestState memory _state, bytes memory _proof)
        public
        pure
        returns (bytes32, bytes32)
    {
        LibSMT.State memory state = testInit(_state);
        uint256 startStackLength = state.stack.size();
        state.operationHashProofElement(_proof);
        require(state.proofIndex == _state.proofIndex + 33, "testOperationHashProofElement: proofIndex is incorrect");
        require(state.stack.size() == startStackLength, "testOperationHashProofElement: Stack size is incorrect.");
        return state.stack.popNode();
    }

    function hashLeafExternal(LibSMT.Leaf memory leaf) external pure returns (bytes32) {
        return LibSMT.hashLeaf(leaf);
    }

    function mergeExternal(bytes32 _lhs, bytes32 _rhs) external pure returns (bytes32) {
        return LibSMT.merge(_lhs, _rhs);
    }

    function testPushAndPopLeaves(LibSMT.Leaf[] memory leaves) public pure {
        LibStack.Stack memory stack = LibStack.init();
        for (uint256 i = 0; i < leaves.length; i++) {
            LibSMT.pushNode(stack, leaves[i].key, leaves[i].value);
        }
        for (uint256 i = 0; i < leaves.length; i++) {
            (bytes32 key, bytes32 value) = LibSMT.popNode(stack);
            require(key == leaves[leaves.length - i].key, "testPushAndPopLeaves: Invalid key for leaf.");
            require(value == leaves[leaves.length - i].value, "testPushAndPopLeaves: Invalid value for leaf.");
        }
        require(stack.size() == 0x0, "testPushAndPopLeaves: Invalid ending stack size");
    }

    function testInit(TestState memory _state) internal pure returns (LibSMT.State memory state) {
        state.proofIndex = _state.proofIndex;
        state.leafIndex = _state.leafIndex;
        state.stack = LibStack.init();
        for (uint256 i = 0; i < _state.initialStack.length; i++) {
            LibSMT.pushNode(state.stack, _state.initialStack[i].key, _state.initialStack[i].value);
        }
    }
}
