// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import { SolRsaVerify } from "../src/libs/SolRsaVerify.sol";

contract TestSolRsaVerify {
    function pkcs1Sha256VerifyExternal(
        bytes32 _sha256,
        bytes memory _s,
        bytes memory _e,
        bytes memory _m
    ) external view returns (uint256) {
        return SolRsaVerify.pkcs1Sha256Verify(_sha256, _s, _e, _m);
    }

    function pkcs1Sha256VerifyRawExternal(
        bytes memory _data,
        bytes memory _s,
        bytes memory _e,
        bytes memory _m
    ) external view returns (uint256) {
        return SolRsaVerify.pkcs1Sha256VerifyRaw(_data, _s, _e, _m);
    }
}
