// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { LibCollateral } from "../src/libs/LibCollateral.sol";
import { DepositDefs } from "../src/libs/defs/DepositDefs.sol";
import { SharedDefs } from "../src/libs/defs/SharedDefs.sol";
import { LibDiamondStorageCheckpoint } from "../src/storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageDeposit } from "../src/storage/LibDiamondStorageDeposit.sol";
import { LibDiamondStorageDerivaDEX } from "../src/storage/LibDiamondStorageDerivaDEX.sol";
import { IDDX } from "../src/tokens/interfaces/IDDX.sol";

contract TestLibCollateral {
    // A type used for testing that the `withdraw` flow is protected against
    // frozen collateral and withdrawal amounts that are not uint128 numbers.
    struct StrategyData256 {
        bytes32 strategyId;
        SharedDefs.Balance256 freeCollateral;
        SharedDefs.Balance256 frozenCollateral;
        uint64 maxLeverage;
        bool frozen;
    }

    // A type used for testing that the `withdraw` flow is protected against
    // frozen collateral and withdrawal amounts that are not uint128 numbers.
    struct StrategyDataEncoding256 {
        SharedDefs.ItemKind discriminant;
        StrategyData256 strategyData;
    }

    // This stub is used to generate calldata that can be passed to the normal
    // `withdraw` function to ensure that the `withdraw` function is safe
    // against `uint256` numbers being used in invalid locations during the
    // withdrawal.
    function withdraw256(
        bytes32 _strategyId,
        SharedDefs.Balance256 memory _withdrawalData,
        StrategyData256 memory _strategy,
        bytes memory _proof
    ) external {}

    function setDDXAddress(address _ddxAddress) external {
        LibDiamondStorageDerivaDEX.DiamondStorageDerivaDEX storage dsDerivaDEX =
            LibDiamondStorageDerivaDEX.diamondStorageDerivaDEX();
        dsDerivaDEX.ddxToken = IDDX(_ddxAddress);
    }

    function setEpochId(uint128 _epochId) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.currentEpochId = _epochId;
    }

    function setLastConfirmedBlockNumber(uint128 _lastConfirmedBlockNumber) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.latestConfirmedBlockNumber = _lastConfirmedBlockNumber;
    }

    function setMaxDDXCap(uint256 _maxDDXCap) external {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        dsDeposit.maxDDXCap = _maxDDXCap;
    }

    function setMinimumRateLimitForToken(address _tokenAddress, uint128 _minimumRateLimit) external {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        dsDeposit.withdrawalRateLimits[_tokenAddress].minimumRateLimit = _minimumRateLimit;
    }

    function setRateLimitParameters(uint128 _rateLimitPeriod, uint128 _rateLimitPercentage) external {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        dsDeposit.rateLimitPeriod = _rateLimitPeriod;
        require(_rateLimitPercentage <= 100, "Deposit: Rate limit percentage must be less than 100%.");
        dsDeposit.rateLimitPercentage = _rateLimitPercentage;
    }

    function setWithdrawalAllowance(address _tokenAddress, uint128 _allowance) external {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        dsDeposit.withdrawalAllowances[_tokenAddress] = _allowance;
    }

    function commitWithdrawalPublic(
        address _withdrawAddress,
        bytes32 _strategyId,
        address _tokenAddress,
        uint128 _frozenCollateralAmount,
        uint128 _scaledWithdrawalAmount,
        uint128 _blockNumber
    ) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibCollateral.commitWithdrawal(
            dsCheckpoint,
            dsDeposit,
            _withdrawAddress,
            _strategyId,
            _tokenAddress,
            _frozenCollateralAmount,
            _scaledWithdrawalAmount,
            _blockNumber
        );
    }

    function commitWithdrawalAllowancePublic(
        address _tokenAddress,
        uint128 _unscaledWithdrawalAmount,
        uint128 _blockNumber
    ) external {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibCollateral.commitWithdrawalAllowance(dsDeposit, _tokenAddress, _unscaledWithdrawalAmount, _blockNumber);
    }

    function calculateTradeMiningRewardsForRangePublic(uint128 _start, uint128 _end) external view returns (uint128) {
        return LibCollateral.calculateTradeMiningRewardsForRange(_start, _end);
    }

    function getRateLimitsForToken(address _tokenAddress)
        external
        view
        returns (DepositDefs.WithdrawalRateLimit memory)
    {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return dsDeposit.withdrawalRateLimits[_tokenAddress];
    }

    function getProcessedWithdrawals(
        address _withdrawAddress,
        bytes32 _strategyId,
        address _tokenAddress,
        uint128 _blockNumber
    ) external view returns (uint128 amount) {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return
            LibCollateral.getProcessedWithdrawals(
                dsCheckpoint,
                dsDeposit,
                _withdrawAddress,
                _strategyId,
                _tokenAddress,
                _blockNumber
            );
    }

    function getUnprocessedWithdrawals(
        address _withdrawAddress,
        bytes32 _strategyId,
        address _tokenAddress
    )
        external
        view
        returns (
            uint128 amount,
            uint128 start,
            uint128 end
        )
    {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return
            LibCollateral.getUnprocessedWithdrawals(
                dsCheckpoint,
                dsDeposit,
                _withdrawAddress,
                _strategyId,
                _tokenAddress
            );
    }

    function getWithdrawalAllowance(address _tokenAddress) external view returns (uint128) {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        return dsDeposit.withdrawalAllowances[_tokenAddress];
    }

    function scaleAmountForTokenExternal(uint128 _amount, address _tokenAddress) external view returns (uint128) {
        return LibCollateral.scaleAmountForToken(_amount, _tokenAddress);
    }

    function generateStrategyKeyExternal(address _traderAddress, bytes32 _strategyId) external pure returns (bytes32) {
        return LibCollateral.generateStrategyKey(_traderAddress, _strategyId);
    }

    function hashStrategy256Public(StrategyData256 memory _strategyData) external pure returns (bytes32) {
        // ABI encode and hash the strategy struct. Prepend the enum identifier
        // of Item::Strategy from the Rust codebase.
        bytes memory encodedStrategy =
            abi.encode(
                StrategyDataEncoding256({ discriminant: SharedDefs.ItemKind.Strategy, strategyData: _strategyData })
            );
        return keccak256(encodedStrategy);
    }

    function hashStrategyPublic(DepositDefs.StrategyData memory _strategy) external pure returns (bytes32) {
        // same contents from Collateral
        bytes memory encodedStrategy =
            abi.encode(
                DepositDefs.StrategyDataEncoding({
                    discriminant: SharedDefs.ItemKind.Strategy,
                    strategyData: _strategy
                })
            );
        return keccak256(encodedStrategy);
    }

    function commitDepositPublic(address _collateralAddress, uint128 _depositAmount) public {
        LibDiamondStorageDeposit.DiamondStorageDeposit storage dsDeposit =
            LibDiamondStorageDeposit.diamondStorageDeposit();
        LibCollateral.commitDeposit(dsDeposit, _collateralAddress, _depositAmount);
    }

    function commitTraderWithdrawalPublic(
        address _withdrawAddress,
        uint128 _withdrawalAmount,
        uint128 _frozenBalance
    ) public {
        LibCollateral.commitTraderWithdrawal(_withdrawAddress, _withdrawalAmount, _frozenBalance);
    }

    function commitStrategyWithdrawalPublic(
        address _withdrawalAddress,
        bytes32 _strategyId,
        SharedDefs.Balance128 memory _withdrawalData,
        SharedDefs.Balance256 memory _freeCollateral,
        SharedDefs.Balance128 memory _frozenCollateral
    ) public {
        LibCollateral.commitStrategyWithdrawal(
            _withdrawalAddress,
            _strategyId,
            _withdrawalData,
            _freeCollateral,
            _frozenCollateral
        );
    }
}
