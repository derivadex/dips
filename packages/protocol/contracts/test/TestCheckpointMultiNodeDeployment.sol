// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { StringUtils } from "../src/libs/StringUtils.sol";
import { LibBytes } from "../src/libs/LibBytes.sol";
import { LibDiamondStorageCheckpoint } from "../src/storage/LibDiamondStorageCheckpoint.sol";
import { Checkpoint } from "../src/facets/Checkpoint.sol";

/**
 * @title TestCheckpointMultiNodeDeployment
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to exchange operators. This logic handles operator
 *         registration, checkpointing, and operator consensus for the DerivaDEX
 *         diamond.
 * @dev This contract should not be used in production. This makes it easier to
 *      to test the DerivaDEX Exchange end-to-end by providing convenience
 *      setters for the state root.
 */
contract TestCheckpointMultiNodeDeployment is Checkpoint {
    using LibBytes for bytes;
    using StringUtils for *;

    function setEpochId(uint128 _epochId) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.currentEpochId = _epochId;
    }

    function setLastConfirmedBlockNumber(uint128 _lastConfirmedBlockNumber) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.latestConfirmedBlockNumber = _lastConfirmedBlockNumber;
    }

    // Used to test withdrawals end-to-end.
    function setStateRoot(bytes32 _stateRoot) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.stateRoot = _stateRoot;
    }

    function setTransactionRoot(bytes32 _txRoot) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.transactionRoot = _txRoot;
    }
}
