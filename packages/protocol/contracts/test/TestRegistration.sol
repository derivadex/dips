// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { MathHelpers } from "../src/libs/MathHelpers.sol";
import { RegistrationDefs } from "../src/libs/defs/RegistrationDefs.sol";
import { LibRelease } from "../src/libs/LibRelease.sol";
import { LibDiamondStorageRegistration } from "../src/storage/LibDiamondStorageRegistration.sol";
import { Registration } from "../src/facets/Registration.sol";

/**
 * @title TestRegistration
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to registration. This logic handles operator
 *         registration for the DerivaDEX
 *         diamond.
 * @dev This contract should not be used in production. It's useful because it
 *      overrides `register` and does not enforce the Intel SGX cryptography. or
 *      any of the other validation enforced in the production Operator. This
 *      makes it easy to test the core logic of the contract without needing to
 *      use SGX hardware.
 */
contract TestRegistration is Registration {
    using MathHelpers for uint256;

    // We're using a mapping here to ensure that there isn't conflict between
    // the state of this test facet and other test facets.
    bytes32 constant CRYPTOGRAPHY_CHECK_RESPONSE_KEY =
        keccak256(abi.encode("TestRegistration.cryptographyCheckResponseKey"));
    mapping(bytes32 => uint256) cryptographyCheckResponse;

    function deleteLastCustodian() external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);
        address lastCustodian =
            dsRegistration.registeredCustodians[releaseHash][
                dsRegistration.registeredCustodians[releaseHash].length - 1
            ];
        dsRegistration.registeredCustodians[releaseHash].pop();
        address signer = dsRegistration.custodians[lastCustodian].signers[releaseHash];
        delete dsRegistration.signers[releaseHash][signer];
        delete dsRegistration.custodians[lastCustodian];
    }

    function setCryptographyCheckResponse(uint256 _cryptographyCheckResponse) external {
        cryptographyCheckResponse[CRYPTOGRAPHY_CHECK_RESPONSE_KEY] = _cryptographyCheckResponse;
    }

    function registerForTest(address _signer, bytes memory _report) external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);
        RegistrationDefs.Signer storage signer = dsRegistration.signers[releaseHash][_signer];
        dsRegistration.registeredCustodians[releaseHash].push(msg.sender);
        signer.custodian = msg.sender;
        signer.report = _report;
    }

    // Overriding this function allows us to easily test the majority of the
    // `register` function without Intel SGX hardware.
    function verifyReportCryptography(bytes memory, bytes memory) internal view override returns (uint256) {
        return cryptographyCheckResponse[CRYPTOGRAPHY_CHECK_RESPONSE_KEY];
    }
}
