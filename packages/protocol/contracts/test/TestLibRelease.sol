// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { LibRelease } from "../src/libs/LibRelease.sol";
import { RegistrationDefs } from "../src/libs/defs/RegistrationDefs.sol";
import { LibDiamondStorageCheckpoint } from "../src/storage/LibDiamondStorageCheckpoint.sol";
import { LibDiamondStorageRegistration } from "../src/storage/LibDiamondStorageRegistration.sol";

contract TestLibRelease {
    function evaluateReleaseScheduleExternal(uint128 _epochId) external returns (bytes32) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return LibRelease.evaluateReleaseSchedule(dsRegistration, _epochId);
    }

    function commitReleaseScheduleUpdateExternal(bytes32 _releaseHash, uint128 _startingEpochId)
        external
        returns (bool)
    {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return LibRelease.commitReleaseScheduleUpdate(dsCheckpoint, dsRegistration, _releaseHash, _startingEpochId);
    }

    function setEpochId(uint128 _epochId) external {
        LibDiamondStorageCheckpoint.DiamondStorageCheckpoint storage dsCheckpoint =
            LibDiamondStorageCheckpoint.diamondStorageCheckpoint();
        dsCheckpoint.currentEpochId = _epochId;
    }

    function getCurrentReleaseHashExternal() external view returns (bytes32) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return LibRelease.getCurrentReleaseHash(dsRegistration);
    }

    function getCurrentReleaseStartingEpochExternal() external view returns (uint128) {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return dsRegistration.currentReleaseStartingEpochId;
    }

    function getReleaseScheduleEntry(uint128 _startingEpochId)
        external
        view
        returns (RegistrationDefs.ReleaseScheduleEntry memory)
    {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        return dsRegistration.releaseSchedule[_startingEpochId];
    }
}
