// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;
pragma experimental ABIEncoderV2;

import { Base64 } from "../src/libs/Base64.sol";
import { MathHelpers } from "../src/libs/MathHelpers.sol";
import { StringUtils } from "../src/libs/StringUtils.sol";
import { LibBytes } from "../src/libs/LibBytes.sol";
import { LibRelease } from "../src/libs/LibRelease.sol";
import { LibDiamondStorageRegistration } from "../src/storage/LibDiamondStorageRegistration.sol";
import { LibDiamondStorageSpecs } from "../src/storage/LibDiamondStorageSpecs.sol";
import { Registration } from "../src/facets/Registration.sol";
import { RegistrationDefs } from "../src/libs/defs/RegistrationDefs.sol";

/**
 * @title TestRegistrationWithReset
 * @author DerivaDEX
 * @notice This is a facet to the DerivaDEX proxy contract that handles
 *         the logic pertaining to registration. This logic handles operator
 *         registration for the DerivaDEX diamond.
 * @dev This contract should not be used in production. This makes it easier to
 *      to test the DerivaDEX Exchange end-to-end by including a reset function.
 */
contract TestRegistrationWithReset is Registration {
    using LibBytes for bytes;
    using StringUtils for *;
    using MathHelpers for uint256;

    function reset() external {
        LibDiamondStorageRegistration.DiamondStorageRegistration storage dsRegistration =
            LibDiamondStorageRegistration.diamondStorageRegistration();
        LibDiamondStorageSpecs.DiamondStorageSpecs storage dsSpecs = LibDiamondStorageSpecs.diamondStorageSpecs();
        bytes32 releaseHash = LibRelease.getCurrentReleaseHash(dsRegistration);
        for (uint256 i = 0; i < dsRegistration.registeredCustodians[releaseHash].length; i++) {
            address custodian = dsRegistration.registeredCustodians[releaseHash][i];
            address signer = dsRegistration.custodians[custodian].signers[releaseHash];
            delete dsRegistration.signers[releaseHash][signer];
            delete dsRegistration.custodians[custodian].signers[releaseHash];
            delete dsRegistration.custodians[custodian];
            dsRegistration.custodians[custodian].approved = true;
        }
        delete dsRegistration.registeredCustodians[releaseHash];
        dsRegistration.validSignersCount = 0;
        dsSpecs.genesisBlock = block.number.safe128("Registration: invalid block number.");
    }
}
