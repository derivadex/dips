import { providerUtils } from '@0x/utils';
import { SupportedProvider, Web3Wrapper } from '@0x/web3-wrapper';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { Derivadex } from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, advanceTimeAsync } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { ContractAddresses } from '@derivadex/types';
import * as hardhat from 'hardhat';

import { liftGovernanceCliffAsync } from '../../deployment';
import { FORK_URL, GOVERNANCE_DEPLOYMENT_CONSTANTS } from '../constants';

describe('#Lifting governance cliff', function () {
    let derivadex: Derivadex;
    let ddxAddresses: ContractAddresses;
    let treasury: string;
    let communityProposer: string;
    let anotherLargeVoter: string;
    let proposalId: number;
    let web3Wrapper: Web3Wrapper;
    let accounts: Array<string>;
    let provider: SupportedProvider;

    before(async () => {
        // Reset to a fresh forked state.
        provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [
                {
                    forking: {
                        jsonRpcUrl: FORK_URL,
                    },
                },
            ],
        });
        const chainId = await providerUtils.getChainIdAsync(provider);
        accounts = await web3Wrapper.getAvailableAddressesAsync();
        const { addresses, chainId: chainId_ } = await getContractDeploymentAsync('derivadex');
        ddxAddresses = addresses;
        expect(chainId).to.be.eq(chainId_);

        // NOTE(jalextowle): Since the DDX is primarily held by the GnosisSafeProxy,
        // we will unlock this account and use it as the `owner` account in order
        // to set up the fixture trader accounts.
        treasury = ddxAddresses.gnosisSafeProxyAddress;
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [treasury],
        });
        // Fund the proxy address with ether
        await web3Wrapper.sendTransactionAsync({
            from: accounts[7],
            to: treasury,
            value: Web3Wrapper.toBaseUnitAmount(10, 18),
        });

        communityProposer = '0xC02ad7b9a9121fc849196E844DC869D2250DF3A6';
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [communityProposer],
        });

        anotherLargeVoter = '0x7055ef0557dc6ff56cdf0c36d28b346d40a1b8ed';
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [anotherLargeVoter],
        });

        derivadex = new Derivadex(ddxAddresses, provider, chainId);
        derivadex.setFacetAddressesToProxy();
    });

    after(async () => {
        // Stop impersonating the treasury
        await web3Wrapper.sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: [treasury],
        });
        // Stop impersonating the community proposer
        await web3Wrapper.sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: [communityProposer],
        });
        // Stop impersonating the other large voter
        await web3Wrapper.sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: [anotherLargeVoter],
        });
    });

    it('fails to withdraw DDX', async () => {
        await expect(
            derivadex.withdrawDDXToTrader(1000).awaitTransactionSuccessAsync({ from: communityProposer }),
        ).to.be.rejectedWith('Trader: prior to reward cliff.');
    });

    it('make proposal to lift the governance cliff', async () => {
        const initProposalCount = await derivadex.getProposalCountAsync();
        await liftGovernanceCliffAsync(ddxAddresses, provider, communityProposer);
        await advanceBlocksAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.votingDelay + 1);

        proposalId = (await derivadex.getProposalCountAsync()).toNumber();
        expect(proposalId).to.eq(initProposalCount.toNumber() + 1);
    });

    it('casts votes to succeed', async () => {
        // Cast vote from three addresses that exceed the 50% threshold to skip remaining voting period
        await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: communityProposer });
        await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: treasury });
        await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: anotherLargeVoter });
        await derivadex.queue(proposalId).awaitTransactionSuccessAsync({ from: accounts[0] });
        await advanceTimeAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.timelockDelay);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        await derivadex.execute(proposalId).awaitTransactionSuccessAsync({ from: accounts[0] });
    });

    it('withdraws DDX', async () => {
        const initDDXBalance = await derivadex.getBalanceOfDDXAsync(communityProposer);
        expect(initDDXBalance).to.be.bignumber.eq(0);
        await derivadex.withdrawDDXToTrader(1000).awaitTransactionSuccessAsync({ from: communityProposer });
        const finDDXBalance = await derivadex.getBalanceOfDDXAsync(communityProposer);
        expect(finDDXBalance).to.be.bignumber.eq(1000);
    });
});
