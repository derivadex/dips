import { BigNumber, providerUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { AUSDTContract, CUSDTContract, Derivadex, SafeERC20WrapperContract } from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, advanceTimeAsync, generateCallData } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { StakeFlavor } from '@derivadex/types';
import * as hardhat from 'hardhat';

import { FORK_URL, GOVERNANCE_DEPLOYMENT_CONSTANTS, INSURANCE_MINING_DEPLOYMENT_CONSTANTS } from '../constants';
import { AUSDC, AUSDT, CUSDC, CUSDT, Fixtures, GUSD, HUSD, USDC, USDT, ZERO_ADDRESS } from '../fixtures';

// TODO(jalextowle): De-dupe this code with `../integration/TestInsuranceMining.ts`.
// These two test suites are very similar, and it should be easy to reduce code
// duplication.
describe('#Insurance Mining Deployment', function () {
    let derivadex: Derivadex;
    let fixtures: Fixtures;
    let owner: string;
    let proposalId: number;
    let web3Wrapper: Web3Wrapper;

    before(async () => {
        // Reset to a fresh forked state.
        const provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [
                {
                    forking: {
                        jsonRpcUrl: FORK_URL,
                    },
                },
            ],
        });
        const chainId = await providerUtils.getChainIdAsync(provider);
        const accounts = await web3Wrapper.getAvailableAddressesAsync();
        const { addresses: derivadexAddresses, chainId: chainId_ } = await getContractDeploymentAsync('derivadex');
        expect(chainId).to.be.eq(chainId_);

        // NOTE(jalextowle): Since the DDX is primarily held by the GnosisSafeProxy,
        // we will unlock this account and use it as the `owner` account in order
        // to set up the fixture trader accounts.
        owner = derivadexAddresses.gnosisSafeProxyAddress;
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [owner],
        });

        // Fund the proxy address with ether
        await web3Wrapper.sendTransactionAsync({
            from: accounts[7],
            to: owner,
            value: Web3Wrapper.toBaseUnitAmount(10, 18),
        });

        // Create the insurance fund and immediately set up all of the facet
        // addresses.
        derivadex = new Derivadex(derivadexAddresses, provider, chainId);
        derivadex.setFacetAddressesToProxy();

        // Unlock the accounts that we need to impersonate
        await new Web3Wrapper(provider).sendRawPayloadAsync({
            method: 'hardhat_impersonateAccount',
            params: ['0xD545f6EAf71b8E54aF1F02dAFBa6C0D46C491cc1'],
        });
        await new Web3Wrapper(provider).sendRawPayloadAsync({
            method: 'hardhat_impersonateAccount',
            params: ['0x0182685f547a8335ff7b48264f15e76f346e282e'],
        });
        await new Web3Wrapper(provider).sendRawPayloadAsync({
            method: 'hardhat_impersonateAccount',
            params: ['0xa97bd3094fb9bf8a666228bceffc0648358ee48f'],
        });

        /// HACK(jalextowle) ///
        const usdtAddress = '0xdAC17F958D2ee523a2206206994597C13D831ec7';
        const cusdtAddress = '0xf650c3d88d12db855b8bf7d11be6c55a4e07dcc9';
        const ausdtAddress = '0x71fc860f7d3a592a4a98740e39db31d25db65ae8';
        const unlockedAddressUSDT = '0xD545f6EAf71b8E54aF1F02dAFBa6C0D46C491cc1';
        const unlockedAddressCUSDT = '0x0182685f547a8335ff7b48264f15e76f346e282e';
        const unlockedAddressAUSDT = '0xa97bd3094fb9bf8a666228bceffc0648358ee48f';
        const tokenCount = 500000;
        const cusdtTokenCount = 500;
        const ausdtTokenCount = 5000;

        const usdt = new SafeERC20WrapperContract(usdtAddress, provider);
        const cusdt = new CUSDTContract(cusdtAddress, provider, { gas: 500000 });
        const ausdt = new AUSDTContract(ausdtAddress, provider, { gas: 500000 });
        const amount = Web3Wrapper.toBaseUnitAmount(tokenCount, 6);
        const cusdtAmount = Web3Wrapper.toBaseUnitAmount(cusdtTokenCount, 8);
        const ausdtAmount = Web3Wrapper.toBaseUnitAmount(ausdtTokenCount, 6);
        for (const account of accounts.slice(0, 5)) {
            if (account !== owner) {
                // Distributing 100 tokens to each account (we have 18 decimals)
                await usdt.transfer(account, amount).awaitTransactionSuccessAsync({ from: unlockedAddressUSDT });
                await cusdt.transfer(account, cusdtAmount).awaitTransactionSuccessAsync({ from: unlockedAddressCUSDT });
                await ausdt.transfer(account, ausdtAmount).awaitTransactionSuccessAsync({ from: unlockedAddressAUSDT });
            }
        }

        // Re-lock the accounts that we need to impersonate
        await new Web3Wrapper(provider).sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: ['0xD545f6EAf71b8E54aF1F02dAFBa6C0D46C491cc1'],
        });
        await new Web3Wrapper(provider).sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: ['0x0182685f547a8335ff7b48264f15e76f346e282e'],
        });
        await new Web3Wrapper(provider).sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: ['0xa97bd3094fb9bf8a666228bceffc0648358ee48f'],
        });

        proposalId = (await derivadex.getProposalCountAsync()).toNumber() + 1;
        fixtures = new Fixtures(derivadex, accounts, owner);

        await derivadex
            .transferDDX(fixtures.traderB(), 10000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderC(), 5000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        await advanceBlocksAsync(derivadex.providerEngine, 1);
    });

    after(async () => {
        // Stop impersonating the GnosisSafeProxy
        await web3Wrapper.sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: [owner],
        });
    });

    it('check lock and unlock of stake', async () => {
        // Staking Trader A
        let lockStatus = await derivadex.getLockStatusForStakeToInsuranceFundAsync(
            CUSDT.collateralAddress,
            fixtures.traderA(),
        );
        expect(lockStatus).to.eq(false);
        await derivadex
            .approveForStakeToInsuranceFund(CUSDT.collateralAddress)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        lockStatus = await derivadex.getLockStatusForStakeToInsuranceFundAsync(
            CUSDT.collateralAddress,
            fixtures.traderA(),
        );
        expect(lockStatus).to.eq(true);
        await derivadex
            .lockStakeToInsuranceFund(CUSDT.collateralAddress)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        lockStatus = await derivadex.getLockStatusForStakeToInsuranceFundAsync(
            CUSDT.collateralAddress,
            fixtures.traderA(),
        );
        expect(lockStatus).to.eq(false);
    });

    it('fails to add USDT collateral since name already added', async () => {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.insuranceFundContract.getFunctionSignature('addInsuranceFundCollateral')];
        const calldatas = [
            generateCallData(
                derivadex
                    .addInsuranceFundCollateral(
                        USDT.collateralName,
                        USDT.collateralSymbol,
                        ZERO_ADDRESS,
                        USDT.collateralAddress,
                        USDT.collateralStakingType,
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add USDT again.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await advanceBlocksAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.votingDelay + 1);
        await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalId).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.timelockDelay);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        await expect(
            derivadex.execute(proposalId++).awaitTransactionSuccessAsync({ from: fixtures.traderB(), gas: 7000000 }),
        ).to.be.rejectedWith('Governance: transaction execution reverted.');
    });

    it('fails to add cUSDT collateral since same collateral and underlying token addresses', async () => {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.insuranceFundContract.getFunctionSignature('addInsuranceFundCollateral')];
        const calldatas = [
            generateCallData(
                derivadex
                    .addInsuranceFundCollateral(
                        CUSDT.collateralName,
                        CUSDT.collateralSymbol,
                        CUSDT.collateralAddress,
                        CUSDT.collateralAddress,
                        CUSDT.collateralStakingType,
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add cUSDT';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await advanceBlocksAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.votingDelay + 1);
        await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalId).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.timelockDelay);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        await expect(
            derivadex.execute(proposalId++).awaitTransactionSuccessAsync({ from: fixtures.traderB(), gas: 7000000 }),
        ).to.be.rejectedWith('Governance: transaction execution reverted.');
    });

    it('fails to add USDT collateral with a different name', async () => {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.insuranceFundContract.getFunctionSignature('addInsuranceFundCollateral')];
        const calldatas = [
            generateCallData(
                derivadex
                    .addInsuranceFundCollateral(
                        CUSDT.collateralName,
                        CUSDT.collateralSymbol,
                        ZERO_ADDRESS,
                        USDT.collateralAddress,
                        USDT.collateralStakingType,
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add USDT.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await advanceBlocksAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.votingDelay + 1);
        await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalId).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.timelockDelay);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        await expect(
            derivadex.execute(proposalId++).awaitTransactionSuccessAsync({ from: fixtures.traderB(), gas: 7000000 }),
        ).to.be.rejectedWith('Governance: transaction execution reverted.');
    });

    it('adds aUSDT and aUSDC to insurance fund as valid collateral types ', async () => {
        const targets = [derivadex.derivaDEXContract.address, derivadex.derivaDEXContract.address];
        const values = [0, 0];
        const signatures = [
            derivadex.insuranceFundContract.getFunctionSignature('addInsuranceFundCollateral'),
            derivadex.insuranceFundContract.getFunctionSignature('addInsuranceFundCollateral'),
        ];
        const calldatas = [
            generateCallData(
                derivadex
                    .addInsuranceFundCollateral(
                        AUSDT.collateralName,
                        AUSDT.collateralSymbol,
                        USDT.collateralAddress,
                        AUSDT.collateralAddress,
                        AUSDT.collateralStakingType,
                    )
                    .getABIEncodedTransactionData(),
            ),
            generateCallData(
                derivadex
                    .addInsuranceFundCollateral(
                        AUSDC.collateralName,
                        AUSDC.collateralSymbol,
                        USDC.collateralAddress,
                        AUSDC.collateralAddress,
                        AUSDC.collateralStakingType,
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add aToken family of collaterals.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await advanceBlocksAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.votingDelay + 1);
        await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalId).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.timelockDelay);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        await derivadex.execute(proposalId++).awaitTransactionSuccessAsync({ from: fixtures.traderB(), gas: 9000000 });
    });

    it('checks supported collateral names have been added and addresses and flavors', async () => {
        const collateralNames = (await derivadex.getInsuranceMineInfoAsync()).collateralNames;
        expect(collateralNames).to.deep.eq([
            USDT.collateralName,
            CUSDT.collateralName,
            USDC.collateralName,
            CUSDC.collateralName,
            HUSD.collateralName,
            GUSD.collateralName,
            AUSDT.collateralName,
            AUSDC.collateralName,
        ]);

        // Get addresses (collateral, underlying, and diFund tokens) and flavor
        const addressAndFlavorUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(USDT.collateralName);
        expect(addressAndFlavorUSDT.collateralToken).to.eq(USDT.collateralAddress.toLowerCase());
        expect(addressAndFlavorUSDT.underlyingToken).to.eq(ZERO_ADDRESS);
        expect(addressAndFlavorUSDT.diFundToken).to.not.eq(ZERO_ADDRESS);
        expect(addressAndFlavorUSDT.cap).to.be.bignumber.eq(0);
        expect(addressAndFlavorUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);
        expect(addressAndFlavorUSDT.flavor).to.eq(StakeFlavor.Vanilla);

        const addressAndFlavorCUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(CUSDT.collateralName);
        expect(addressAndFlavorCUSDT.collateralToken).to.eq(CUSDT.collateralAddress.toLowerCase());
        expect(addressAndFlavorCUSDT.underlyingToken).to.eq(USDT.collateralAddress.toLowerCase());
        expect(addressAndFlavorCUSDT.diFundToken).to.not.eq(ZERO_ADDRESS);
        expect(addressAndFlavorCUSDT.cap).to.be.bignumber.eq(0);
        expect(addressAndFlavorCUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);
        expect(addressAndFlavorCUSDT.flavor).to.eq(StakeFlavor.Compound);

        const addressAndFlavorUSDC = await derivadex.getStakeCollateralByCollateralNameAsync(USDC.collateralName);
        expect(addressAndFlavorUSDC.collateralToken).to.eq(USDC.collateralAddress.toLowerCase());
        expect(addressAndFlavorUSDC.underlyingToken).to.eq(ZERO_ADDRESS);
        expect(addressAndFlavorUSDC.diFundToken).to.not.eq(ZERO_ADDRESS);
        expect(addressAndFlavorUSDC.cap).to.be.bignumber.eq(0);
        expect(addressAndFlavorUSDC.withdrawalFeeCap).to.be.bignumber.eq(0);
        expect(addressAndFlavorUSDC.flavor).to.eq(StakeFlavor.Vanilla);

        const addressAndFlavorCUSDC = await derivadex.getStakeCollateralByCollateralNameAsync(CUSDC.collateralName);
        expect(addressAndFlavorCUSDC.collateralToken).to.eq(CUSDC.collateralAddress.toLowerCase());
        expect(addressAndFlavorCUSDC.underlyingToken).to.eq(USDC.collateralAddress.toLowerCase());
        expect(addressAndFlavorCUSDC.diFundToken).to.not.eq(ZERO_ADDRESS);
        expect(addressAndFlavorCUSDC.cap).to.be.bignumber.eq(0);
        expect(addressAndFlavorCUSDC.withdrawalFeeCap).to.be.bignumber.eq(0);
        expect(addressAndFlavorCUSDC.flavor).to.eq(StakeFlavor.Compound);

        const addressAndFlavorHUSD = await derivadex.getStakeCollateralByCollateralNameAsync(HUSD.collateralName);
        expect(addressAndFlavorHUSD.collateralToken).to.eq(HUSD.collateralAddress.toLowerCase());
        expect(addressAndFlavorHUSD.underlyingToken).to.eq(ZERO_ADDRESS);
        expect(addressAndFlavorHUSD.diFundToken).to.not.eq(ZERO_ADDRESS);
        expect(addressAndFlavorHUSD.cap).to.be.bignumber.eq(0);
        expect(addressAndFlavorHUSD.withdrawalFeeCap).to.be.bignumber.eq(0);
        expect(addressAndFlavorHUSD.flavor).to.eq(StakeFlavor.Vanilla);

        const addressAndFlavorGUSD = await derivadex.getStakeCollateralByCollateralNameAsync(GUSD.collateralName);
        expect(addressAndFlavorGUSD.collateralToken).to.eq(GUSD.collateralAddress.toLowerCase());
        expect(addressAndFlavorGUSD.underlyingToken).to.eq(ZERO_ADDRESS);
        expect(addressAndFlavorGUSD.diFundToken).to.not.eq(ZERO_ADDRESS);
        expect(addressAndFlavorGUSD.cap).to.be.bignumber.eq(0);
        expect(addressAndFlavorGUSD.withdrawalFeeCap).to.be.bignumber.eq(0);
        expect(addressAndFlavorGUSD.flavor).to.eq(StakeFlavor.Vanilla);

        const addressAndFlavorAUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(AUSDT.collateralName);
        expect(addressAndFlavorAUSDT.collateralToken).to.eq(AUSDT.collateralAddress.toLowerCase());
        expect(addressAndFlavorAUSDT.underlyingToken).to.eq(USDT.collateralAddress.toLowerCase());
        expect(addressAndFlavorAUSDT.diFundToken).to.not.eq(ZERO_ADDRESS);
        expect(addressAndFlavorAUSDT.cap).to.be.bignumber.eq(0);
        expect(addressAndFlavorAUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);
        expect(addressAndFlavorAUSDT.flavor).to.eq(StakeFlavor.Aave);

        const addressAndFlavorAUSDC = await derivadex.getStakeCollateralByCollateralNameAsync(AUSDC.collateralName);
        expect(addressAndFlavorAUSDC.collateralToken).to.eq(AUSDC.collateralAddress.toLowerCase());
        expect(addressAndFlavorAUSDC.underlyingToken).to.eq(USDC.collateralAddress.toLowerCase());
        expect(addressAndFlavorAUSDC.diFundToken).to.not.eq(ZERO_ADDRESS);
        expect(addressAndFlavorAUSDC.cap).to.be.bignumber.eq(0);
        expect(addressAndFlavorAUSDC.withdrawalFeeCap).to.be.bignumber.eq(0);
        expect(addressAndFlavorAUSDC.flavor).to.eq(StakeFlavor.Aave);
    });

    it('fails to stake 0 USDT', async () => {
        // Staking Trader A
        await expect(
            derivadex
                .stakeToInsuranceFund(USDT.collateralName, 0)
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() }),
        ).to.be.rejectedWith('IFund: non-zero amount.');
    });

    let cusdtToUSDTAmount;
    it('Trader A - stakes USDT and cUSDT in the first interval', async () => {
        // Make approvals for trader A to stake USDT and cUSDT
        await derivadex
            .approveForStakeToInsuranceFund(USDT.collateralAddress)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        await derivadex
            .approveForStakeToInsuranceFund(CUSDT.collateralAddress)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });

        // Get insurance mine info
        let insuranceMineInfoA = await derivadex.getInsuranceMineInfoAsync();
        expect(insuranceMineInfoA.minedAmount).to.be.bignumber.eq(0);
        expect(insuranceMineInfoA.mineRatePerBlock).to.be.bignumber.eq(
            INSURANCE_MINING_DEPLOYMENT_CONSTANTS.mineRatePerBlock,
        );
        expect(insuranceMineInfoA.interval).to.be.bignumber.eq(INSURANCE_MINING_DEPLOYMENT_CONSTANTS.interval);

        // Pre claiming data
        const traderABalancePreClaim = await derivadex.getBalanceOfDDXAsync(fixtures.traderA());
        let traderAAttributes = await derivadex.getTraderAsync(fixtures.traderA());
        expect(traderAAttributes.ddxWalletContract).to.eq(ZERO_ADDRESS);

        let unclaimedLocalCurrentA = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(0),
            new BigNumber(0),
        );
        let unclaimedLocalNextA = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(0),
            new BigNumber(0),
            false,
        );
        let unclaimedDDXRewardsA = await derivadex.getUnclaimedDDXRewardsAsync(fixtures.traderA());
        expect(unclaimedLocalCurrentA.dp(2)).to.be.bignumber.eq(0);
        expect(unclaimedLocalNextA.dp(2)).to.be.bignumber.eq(0);
        expect(unclaimedDDXRewardsA.dp(2)).to.be.bignumber.eq(0);
        expect(unclaimedLocalCurrentA.dp(2)).to.be.bignumber.eq(unclaimedDDXRewardsA.dp(2));

        // Stake to insurance fund
        await derivadex
            .stakeToInsuranceFund(USDT.collateralName, 1000)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });

        unclaimedLocalCurrentA = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(1000000000),
            new BigNumber(1000000000),
        );
        unclaimedLocalNextA = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(1000000000),
            new BigNumber(1000000000),
            false,
        );
        unclaimedDDXRewardsA = await derivadex.getUnclaimedDDXRewardsAsync(fixtures.traderA());
        expect(unclaimedLocalCurrentA.dp(2)).to.be.bignumber.eq(unclaimedDDXRewardsA.dp(2));

        // await derivadex.getCOMPOwedAsync(fixtures.traderA(), CUSDT.collateralAddress);

        await derivadex
            .stakeToInsuranceFund(CUSDT.collateralName, 500)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });

        insuranceMineInfoA = await derivadex.getInsuranceMineInfoAsync();
        expect(insuranceMineInfoA.minedAmount.dp(2)).to.be.bignumber.eq(unclaimedLocalNextA.dp(2));

        const traderABalancePostClaim = await derivadex.getBalanceOfDDXAsync(fixtures.traderA());
        expect(traderABalancePostClaim.minus(traderABalancePreClaim).dp(2)).to.be.bignumber.eq(0);
        traderAAttributes = await derivadex.getTraderAsync(fixtures.traderA());
        const traderAWalletBalancePostClaim = await derivadex.getBalanceOfDDXAsync(traderAAttributes.ddxWalletContract);
        expect(traderAWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(unclaimedLocalNextA.dp(2));
        expect(traderAWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(traderAAttributes.ddxBalance.dp(2));

        await advanceBlocksAsync(derivadex.providerEngine, 1);

        // Get addresses (collateral, underlying, and diFund tokens) and flavor
        const stakeCollateralUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(USDT.collateralName);
        const stakeCollateralCUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(CUSDT.collateralName);

        expect(stakeCollateralUSDT.cap).to.be.bignumber.eq(1000);
        expect(stakeCollateralUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);

        cusdtToUSDTAmount = await derivadex.getUSDTFromCUSDTAsync(CUSDT.collateralAddress, 500);
        expect(stakeCollateralCUSDT.cap).to.be.bignumber.eq(500);
        expect(stakeCollateralCUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);

        // Get current stake details for Trader A and USDT
        const currentStakeByCollateralNameAndStakerAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerAUSDT.stakerStake).to.be.bignumber.eq(1000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.globalCap).to.be.bignumber.eq(1000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedStakerStake).to.be.bignumber.eq(1000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedGlobalCap).to.be.bignumber.eq(1000);

        // Get current stake details for Trader A and cUSDT
        const currentStakeByCollateralNameAndStakerACUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerACUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedStakerStake.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        const currentTotalStakesA = await derivadex.getCurrentTotalStakesAsync(fixtures.traderA());
        expect(currentTotalStakesA.normalizedStakerStakeSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(1000).plus(cusdtToUSDTAmount).dp(2),
        );
        expect(currentTotalStakesA.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(1000).plus(cusdtToUSDTAmount).dp(2),
        );
    });

    it('Trader B - stakes USDT in the first interval', async () => {
        // Make approvals for trader B to stake USDT and cUSDT
        await derivadex
            .approveForStakeToInsuranceFund(USDT.collateralAddress)
            .awaitTransactionSuccessAsync({ from: fixtures.traderB() });

        // Stake to insurance fund
        await derivadex
            .stakeToInsuranceFund(USDT.collateralName, 500)
            .awaitTransactionSuccessAsync({ from: fixtures.traderB() });

        await advanceBlocksAsync(derivadex.providerEngine, 1);

        // Get addresses (collateral, underlying, and diFund tokens) and flavor
        const stakeCollateralUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(USDT.collateralName);
        const stakeCollateralCUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(CUSDT.collateralName);

        expect(stakeCollateralUSDT.cap).to.be.bignumber.eq(1500);
        expect(stakeCollateralUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);

        cusdtToUSDTAmount = await derivadex.getUSDTFromCUSDTAsync(CUSDT.collateralAddress, 500);
        expect(stakeCollateralCUSDT.cap).to.be.bignumber.eq(500);
        expect(stakeCollateralCUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);

        // Get current stake details for Trader A and USDT
        const currentStakeByCollateralNameAndStakerAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerAUSDT.stakerStake).to.be.bignumber.eq(1000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.globalCap).to.be.bignumber.eq(1500);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedStakerStake).to.be.bignumber.eq(1000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedGlobalCap).to.be.bignumber.eq(1500);

        // Get current stake details for Trader A and cUSDT
        const currentStakeByCollateralNameAndStakerACUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerACUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedStakerStake.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        // Get current stake details for Trader B and USDT
        const currentStakeByCollateralNameAndStakerBUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.globalCap).to.be.bignumber.eq(1500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.normalizedStakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.normalizedGlobalCap).to.be.bignumber.eq(1500);

        // Get current stake details for Trader B and cUSDT
        const currentStakeByCollateralNameAndStakerBCUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBCUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        const currentTotalStakesA = await derivadex.getCurrentTotalStakesAsync(fixtures.traderA());
        expect(currentTotalStakesA.normalizedStakerStakeSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(1000).plus(cusdtToUSDTAmount).dp(2),
        );
        expect(currentTotalStakesA.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(1500).plus(cusdtToUSDTAmount).dp(2),
        );

        const currentTotalStakesB = await derivadex.getCurrentTotalStakesAsync(fixtures.traderB());
        expect(currentTotalStakesB.normalizedStakerStakeSum).to.be.bignumber.eq(500);
        expect(currentTotalStakesB.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(1500).plus(cusdtToUSDTAmount).dp(2),
        );
    });

    it('distributes DDX to insurance miner A', async () => {
        await advanceBlocksAsync(derivadex.providerEngine, INSURANCE_MINING_DEPLOYMENT_CONSTANTS.interval);

        const initCompDDX = await derivadex.getCOMPBalanceAsync(derivadex.derivaDEXContract.address);
        await derivadex.advanceOtherRewardsInterval().awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        const finCompDDX = await derivadex.getCOMPBalanceAsync(derivadex.derivaDEXContract.address);

        expect(initCompDDX).to.be.bignumber.eq(0);
        expect(finCompDDX.gt(0)).to.eq(true);

        // Pre claiming data
        const traderABalancePreClaim = await derivadex.getBalanceOfDDXAsync(fixtures.traderA());
        let traderAAttributes = await derivadex.getTraderAsync(fixtures.traderA());
        expect(traderAAttributes.ddxWalletContract).to.not.eq(ZERO_ADDRESS);
        const traderAWalletBalancePreClaim = await derivadex.getBalanceOfDDXAsync(traderAAttributes.ddxWalletContract);
        expect(traderAWalletBalancePreClaim).to.be.bignumber.eq(traderAAttributes.ddxBalance);

        const cusdtToUSDTTokenAmount = await derivadex.getUSDTTokensFromCUSDTAsync(
            CUSDT.collateralAddress,
            50000000000,
        );
        const unclaimedLocalCurrentA = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(1500000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(1000000000).plus(cusdtToUSDTTokenAmount),
        );
        const unclaimedLocalNextA = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(1500000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(1000000000).plus(cusdtToUSDTTokenAmount),
            false,
        );
        const unclaimedDDXRewardsA = await derivadex.getUnclaimedDDXRewardsAsync(fixtures.traderA());
        expect(unclaimedLocalCurrentA.dp(2)).to.be.bignumber.eq(unclaimedDDXRewardsA.dp(2));

        const initInsuranceMineInfoA = await derivadex.getInsuranceMineInfoAsync();
        const initCompA = await derivadex.getCOMPBalanceAsync(fixtures.traderA());
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderA())
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        const finCompA = await derivadex.getCOMPBalanceAsync(fixtures.traderA());

        expect(initCompA).to.be.bignumber.eq(0);
        expect(finCompA.gt(0)).to.eq(true);
        expect(finCompA).to.be.bignumber.eq(finCompDDX);

        const insuranceMineInfoA = await derivadex.getInsuranceMineInfoAsync();
        expect(insuranceMineInfoA.minedAmount.dp(2)).to.be.bignumber.eq(
            initInsuranceMineInfoA.minedAmount.plus(unclaimedLocalNextA).dp(2),
        );

        const traderABalancePostClaim = await derivadex.getBalanceOfDDXAsync(fixtures.traderA());
        expect(traderABalancePostClaim.minus(traderABalancePreClaim).dp(2)).to.be.bignumber.eq(0);
        traderAAttributes = await derivadex.getTraderAsync(fixtures.traderA());
        const traderAWalletBalancePostClaim = await derivadex.getBalanceOfDDXAsync(traderAAttributes.ddxWalletContract);
        expect(traderAWalletBalancePostClaim).to.be.bignumber.eq(traderAAttributes.ddxBalance);
        expect(traderAWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(
            traderAWalletBalancePreClaim.plus(unclaimedLocalNextA).dp(2),
        );
        expect(traderAWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(traderAAttributes.ddxBalance.dp(2));
    });

    it('distributes DDX to insurance miner B', async () => {
        const traderBBalancePreClaim = await derivadex.getBalanceOfDDXAsync(fixtures.traderB());
        let traderBAttributes = await derivadex.getTraderAsync(fixtures.traderB());
        expect(traderBAttributes.ddxWalletContract).to.eq(ZERO_ADDRESS);
        const traderBWalletBalancePreClaim = new BigNumber(0);

        const cusdtToUSDTTokenAmount = await derivadex.getUSDTTokensFromCUSDTAsync(
            CUSDT.collateralAddress,
            50000000000,
        );
        const unclaimedLocalCurrentB = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderB(),
            new BigNumber(1500000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(500000000),
        );
        const unclaimedLocalNextB = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderB(),
            new BigNumber(1500000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(500000000),
            false,
        );
        const unclaimedDDXRewardsB = await derivadex.getUnclaimedDDXRewardsAsync(fixtures.traderB());
        expect(unclaimedLocalCurrentB.dp(2)).to.be.bignumber.eq(unclaimedDDXRewardsB.dp(2));

        // cusdtToUSDTAmount = await derivadex.getUSDTFromCUSDTAsync(CUSDT.collateralAddress, 500);

        const initInsuranceMineInfoB = await derivadex.getInsuranceMineInfoAsync();
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderB())
            .awaitTransactionSuccessAsync({ from: fixtures.traderB() });

        const insuranceMineInfoB = await derivadex.getInsuranceMineInfoAsync();
        expect(insuranceMineInfoB.minedAmount.dp(2)).to.be.bignumber.eq(
            initInsuranceMineInfoB.minedAmount.plus(unclaimedLocalNextB).dp(2),
        );

        const traderBBalancePostClaim = await derivadex.getBalanceOfDDXAsync(fixtures.traderB());
        expect(traderBBalancePostClaim.minus(traderBBalancePreClaim).dp(2)).to.be.bignumber.eq(0);
        traderBAttributes = await derivadex.getTraderAsync(fixtures.traderB());
        const traderBWalletBalancePostClaim = await derivadex.getBalanceOfDDXAsync(traderBAttributes.ddxWalletContract);
        expect(traderBWalletBalancePostClaim).to.be.bignumber.eq(traderBAttributes.ddxBalance);

        expect(traderBWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(
            traderBWalletBalancePreClaim.plus(unclaimedLocalNextB).dp(2),
        );
        expect(traderBWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(traderBAttributes.ddxBalance.dp(2));
    });

    it('Trader A - stakes USDT in the second interval', async () => {
        // Staking Trader A
        // Stake USDT
        await derivadex
            .stakeToInsuranceFund(USDT.collateralName, 5000)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        await advanceBlocksAsync(derivadex.providerEngine, 1);

        const insuranceMineInfoA = await derivadex.getInsuranceMineInfoAsync();
        expect(insuranceMineInfoA.interval).to.be.bignumber.eq(40320);

        // Get addresses (collateral, underlying, and diFund tokens) and flavor
        const stakeCollateralUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(USDT.collateralName);
        const stakeCollateralCUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(CUSDT.collateralName);

        expect(stakeCollateralUSDT.cap).to.be.bignumber.eq(6500);
        expect(stakeCollateralUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);

        cusdtToUSDTAmount = await derivadex.getUSDTFromCUSDTAsync(CUSDT.collateralAddress, 500);
        expect(stakeCollateralCUSDT.cap).to.be.bignumber.eq(500);
        expect(stakeCollateralCUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);

        // **********
        // Get current stake details for Trader A and USDT
        const currentStakeByCollateralNameAndStakerAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerAUSDT.stakerStake).to.be.bignumber.eq(6000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.globalCap).to.be.bignumber.eq(6500);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedStakerStake).to.be.bignumber.eq(6000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedGlobalCap).to.be.bignumber.eq(6500);

        cusdtToUSDTAmount = await derivadex.getUSDTFromCUSDTAsync(CUSDT.collateralAddress, 500);

        // Get current stake details for Trader A and cUSDT
        const currentStakeByCollateralNameAndStakerACUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerACUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedStakerStake.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        // Get current stake details for Trader B and USDT
        const currentStakeByCollateralNameAndStakerBUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.globalCap).to.be.bignumber.eq(6500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.normalizedStakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.normalizedGlobalCap).to.be.bignumber.eq(6500);

        // Get current stake details for Trader B and cUSDT
        const currentStakeByCollateralNameAndStakerBCUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBCUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        const currentTotalStakesA = await derivadex.getCurrentTotalStakesAsync(fixtures.traderA());
        expect(currentTotalStakesA.normalizedStakerStakeSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(6000).plus(cusdtToUSDTAmount).dp(2),
        );
        expect(currentTotalStakesA.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(6500).plus(cusdtToUSDTAmount).dp(2),
        );

        const currentTotalStakesB = await derivadex.getCurrentTotalStakesAsync(fixtures.traderB());
        expect(currentTotalStakesB.normalizedStakerStakeSum).to.be.bignumber.eq(500);
        expect(currentTotalStakesB.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(6500).plus(cusdtToUSDTAmount).dp(2),
        );
    });

    it('Trader C - stakes AUSDT in the second interval', async () => {
        await derivadex
            .approveForStakeToInsuranceFund(AUSDT.collateralAddress)
            .awaitTransactionSuccessAsync({ from: fixtures.traderC() });
        await derivadex
            .stakeToInsuranceFund(AUSDT.collateralName, 2500)
            .awaitTransactionSuccessAsync({ from: fixtures.traderC() });
        await advanceBlocksAsync(derivadex.providerEngine, 1);

        const insuranceMineInfo = await derivadex.getInsuranceMineInfoAsync();
        expect(insuranceMineInfo.interval).to.be.bignumber.eq(40320);

        const stakeCollateralAUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(AUSDT.collateralName);
        expect(stakeCollateralAUSDT.cap).to.be.bignumber.eq(2500);
        expect(stakeCollateralAUSDT.withdrawalFeeCap).to.be.bignumber.eq(0);

        // **********
        // Get current stake details for Trader A and USDT
        const currentStakeByCollateralNameAndStakerAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerAUSDT.stakerStake).to.be.bignumber.eq(6000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.globalCap).to.be.bignumber.eq(6500);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedStakerStake).to.be.bignumber.eq(6000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedGlobalCap).to.be.bignumber.eq(6500);

        cusdtToUSDTAmount = await derivadex.getUSDTFromCUSDTAsync(CUSDT.collateralAddress, 500);

        // Get current stake details for Trader A and cUSDT
        const currentStakeByCollateralNameAndStakerACUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerACUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedStakerStake.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        // Get current stake details for Trader A and aUSDT
        const currentStakeByCollateralNameAndStakerAAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(AUSDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerAAUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerAAUSDT.globalCap).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerAAUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerAAUSDT.normalizedGlobalCap).to.be.bignumber.eq(2500);

        // Get current stake details for Trader B and USDT
        const currentStakeByCollateralNameAndStakerBUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.globalCap).to.be.bignumber.eq(6500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.normalizedStakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.normalizedGlobalCap).to.be.bignumber.eq(6500);

        // Get current stake details for Trader B and cUSDT
        const currentStakeByCollateralNameAndStakerBCUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBCUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        // Get current stake details for Trader B and aUSDT
        const currentStakeByCollateralNameAndStakerBAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(AUSDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBAUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBAUSDT.globalCap).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerBAUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBAUSDT.normalizedGlobalCap).to.be.bignumber.eq(2500);

        // Get current stake details for Trader C and USDT
        const currentStakeByCollateralNameAndStakerCUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderC());
        expect(currentStakeByCollateralNameAndStakerCUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerCUSDT.globalCap).to.be.bignumber.eq(6500);
        expect(currentStakeByCollateralNameAndStakerCUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerCUSDT.normalizedGlobalCap).to.be.bignumber.eq(6500);

        // Get current stake details for Trader C and cUSDT
        const currentStakeByCollateralNameAndStakerCCUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderC());
        expect(currentStakeByCollateralNameAndStakerCCUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerCCUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerCCUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerCCUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        // Get current stake details for Trader C and aUSDT
        const currentStakeByCollateralNameAndStakerCAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(AUSDT.collateralName, fixtures.traderC());
        expect(currentStakeByCollateralNameAndStakerCAUSDT.stakerStake).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerCAUSDT.globalCap).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerCAUSDT.normalizedStakerStake).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerCAUSDT.normalizedGlobalCap).to.be.bignumber.eq(2500);

        const currentTotalStakesA = await derivadex.getCurrentTotalStakesAsync(fixtures.traderA());
        expect(currentTotalStakesA.normalizedStakerStakeSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(6000).plus(cusdtToUSDTAmount).dp(2),
        );
        expect(currentTotalStakesA.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(9000).plus(cusdtToUSDTAmount).dp(2),
        );

        const currentTotalStakesB = await derivadex.getCurrentTotalStakesAsync(fixtures.traderB());
        expect(currentTotalStakesB.normalizedStakerStakeSum).to.be.bignumber.eq(500);
        expect(currentTotalStakesB.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(9000).plus(cusdtToUSDTAmount).dp(2),
        );

        const currentTotalStakesC = await derivadex.getCurrentTotalStakesAsync(fixtures.traderC());
        expect(currentTotalStakesC.normalizedStakerStakeSum).to.be.bignumber.eq(2500);
        expect(currentTotalStakesC.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(9000).plus(cusdtToUSDTAmount).dp(2),
        );
    });

    it('fails to withdraw 0 USDT', async () => {
        // Staking Trader A
        await expect(
            derivadex
                .withdrawFromInsuranceFund(USDT.collateralName, 0)
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() }),
        ).to.be.rejectedWith('IFund: non-zero amount.');
    });

    let unclaimedLocalCurrentA2: BigNumber;
    let unclaimedLocalCurrentB2: BigNumber;
    let unclaimedLocalCurrentC2: BigNumber;
    it('claims a couple times for Traders A, B, C', async () => {
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderA())
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderB())
            .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderC())
            .awaitTransactionSuccessAsync({ from: fixtures.traderC() });

        await advanceBlocksAsync(derivadex.providerEngine, 10);

        const cusdtToUSDTTokenAmount = await derivadex.getUSDTTokensFromCUSDTAsync(
            CUSDT.collateralAddress,
            50000000000,
        );
        const unclaimedLocalCurrentA1 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(9000000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(6000000000).plus(cusdtToUSDTTokenAmount),
        );
        const unclaimedLocalCurrentB1 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderB(),
            new BigNumber(9000000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(500000000),
        );
        const unclaimedLocalCurrentC1 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderC(),
            new BigNumber(9000000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(2500000000),
        );

        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderA())
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderB())
            .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderC())
            .awaitTransactionSuccessAsync({ from: fixtures.traderC() });

        await advanceBlocksAsync(derivadex.providerEngine, 10);

        unclaimedLocalCurrentA2 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(9000000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(6000000000).plus(cusdtToUSDTTokenAmount),
        );
        unclaimedLocalCurrentB2 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderB(),
            new BigNumber(9000000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(500000000),
        );
        unclaimedLocalCurrentC2 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderC(),
            new BigNumber(9000000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(2500000000),
        );

        expect(unclaimedLocalCurrentA1).to.be.bignumber.eq(unclaimedLocalCurrentA2);
        expect(unclaimedLocalCurrentB1).to.be.bignumber.eq(unclaimedLocalCurrentB2);
        expect(unclaimedLocalCurrentC1).to.be.bignumber.eq(unclaimedLocalCurrentC2);
    });

    it('withdraws USDT from insurance fund in third index', async () => {
        // Pre claiming data
        const traderABalancePreClaim = await derivadex.getBalanceOfDDXAsync(fixtures.traderA());
        let traderAAttributes = await derivadex.getTraderAsync(fixtures.traderA());
        expect(traderAAttributes.ddxWalletContract).to.not.eq(ZERO_ADDRESS);
        const traderAWalletBalancePreClaim = await derivadex.getBalanceOfDDXAsync(traderAAttributes.ddxWalletContract);
        expect(traderAWalletBalancePreClaim).to.be.bignumber.eq(traderAAttributes.ddxBalance);

        const cusdtToUSDTTokenAmount = await derivadex.getUSDTTokensFromCUSDTAsync(
            CUSDT.collateralAddress,
            50000000000,
        );
        const unclaimedLocalCurrentA = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(9000000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(6000000000).plus(cusdtToUSDTTokenAmount),
        );
        const unclaimedLocalNextA = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(9000000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(6000000000).plus(cusdtToUSDTTokenAmount),
            false,
        );
        const unclaimedDDXRewardsA = await derivadex.getUnclaimedDDXRewardsAsync(fixtures.traderA());
        expect(unclaimedLocalCurrentA.dp(2)).to.be.bignumber.eq(unclaimedDDXRewardsA.dp(2));

        const initInsuranceMineInfoA = await derivadex.getInsuranceMineInfoAsync();
        // Staking Trader A
        await derivadex
            .withdrawFromInsuranceFund(USDT.collateralName, 3500)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });

        const insuranceMineInfoA = await derivadex.getInsuranceMineInfoAsync();
        expect(insuranceMineInfoA.minedAmount.dp(2)).to.be.bignumber.eq(
            initInsuranceMineInfoA.minedAmount.plus(unclaimedLocalNextA).dp(2),
        );

        const traderABalancePostClaim = await derivadex.getBalanceOfDDXAsync(fixtures.traderA());
        expect(traderABalancePostClaim.minus(traderABalancePreClaim).dp(2)).to.be.bignumber.eq(0);
        traderAAttributes = await derivadex.getTraderAsync(fixtures.traderA());
        const traderAWalletBalancePostClaim = await derivadex.getBalanceOfDDXAsync(traderAAttributes.ddxWalletContract);
        expect(traderAWalletBalancePostClaim).to.be.bignumber.eq(traderAAttributes.ddxBalance);
        expect(traderAWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(
            traderAWalletBalancePreClaim.plus(unclaimedLocalNextA).dp(2),
        );
        expect(traderAWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(traderAAttributes.ddxBalance.dp(2));

        // Get addresses (collateral, underlying, and diFund tokens) and flavor
        const stakeCollateralUSDT = await derivadex.getStakeCollateralByCollateralNameAsync(USDT.collateralName);

        expect(stakeCollateralUSDT.cap).to.be.bignumber.eq(3000);
        expect(stakeCollateralUSDT.withdrawalFeeCap).to.be.bignumber.eq(17.5);

        // Get current stake details for Trader A and USDT
        const currentStakeByCollateralNameAndStakerAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerAUSDT.stakerStake).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerAUSDT.globalCap).to.be.bignumber.eq(3000);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedStakerStake).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerAUSDT.normalizedGlobalCap).to.be.bignumber.eq(3000);

        cusdtToUSDTAmount = await derivadex.getUSDTFromCUSDTAsync(CUSDT.collateralAddress, 500);

        // Get current stake details for Trader A and cUSDT
        const currentStakeByCollateralNameAndStakerACUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerACUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedStakerStake.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );
        expect(currentStakeByCollateralNameAndStakerACUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        // Get current stake details for Trader A and aUSDT
        const currentStakeByCollateralNameAndStakerAAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(AUSDT.collateralName, fixtures.traderA());
        expect(currentStakeByCollateralNameAndStakerAAUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerAAUSDT.globalCap).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerAAUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerAAUSDT.normalizedGlobalCap).to.be.bignumber.eq(2500);

        // Get current stake details for Trader B and USDT
        const currentStakeByCollateralNameAndStakerBUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBUSDT.stakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.globalCap).to.be.bignumber.eq(3000);
        expect(currentStakeByCollateralNameAndStakerBUSDT.normalizedStakerStake).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBUSDT.normalizedGlobalCap).to.be.bignumber.eq(3000);

        // Get current stake details for Trader B and cUSDT
        const currentStakeByCollateralNameAndStakerBCUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBCUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBCUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        // Get current stake details for Trader B and aUSDT
        const currentStakeByCollateralNameAndStakerBAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(AUSDT.collateralName, fixtures.traderB());
        expect(currentStakeByCollateralNameAndStakerBAUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBAUSDT.globalCap).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerBAUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerBAUSDT.normalizedGlobalCap).to.be.bignumber.eq(2500);

        // Get current stake details for Trader C and USDT
        const currentStakeByCollateralNameAndStakerCUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(USDT.collateralName, fixtures.traderC());
        expect(currentStakeByCollateralNameAndStakerCUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerCUSDT.globalCap).to.be.bignumber.eq(3000);
        expect(currentStakeByCollateralNameAndStakerCUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerCUSDT.normalizedGlobalCap).to.be.bignumber.eq(3000);

        // Get current stake details for Trader C and cUSDT
        const currentStakeByCollateralNameAndStakerCCUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(CUSDT.collateralName, fixtures.traderC());
        expect(currentStakeByCollateralNameAndStakerCCUSDT.stakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerCCUSDT.globalCap).to.be.bignumber.eq(500);
        expect(currentStakeByCollateralNameAndStakerCCUSDT.normalizedStakerStake).to.be.bignumber.eq(0);
        expect(currentStakeByCollateralNameAndStakerCCUSDT.normalizedGlobalCap.dp(2)).to.be.bignumber.eq(
            cusdtToUSDTAmount.dp(2),
        );

        // Get current stake details for Trader C and aUSDT
        const currentStakeByCollateralNameAndStakerCAUSDT =
            await derivadex.getCurrentStakeByCollateralNameAndStakerAsync(AUSDT.collateralName, fixtures.traderC());
        expect(currentStakeByCollateralNameAndStakerCAUSDT.stakerStake).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerCAUSDT.globalCap).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerCAUSDT.normalizedStakerStake).to.be.bignumber.eq(2500);
        expect(currentStakeByCollateralNameAndStakerCAUSDT.normalizedGlobalCap).to.be.bignumber.eq(2500);

        const currentTotalStakesA = await derivadex.getCurrentTotalStakesAsync(fixtures.traderA());
        expect(currentTotalStakesA.normalizedStakerStakeSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(2500).plus(cusdtToUSDTAmount).dp(2),
        );
        expect(currentTotalStakesA.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(5500).plus(cusdtToUSDTAmount).dp(2),
        );

        const currentTotalStakesB = await derivadex.getCurrentTotalStakesAsync(fixtures.traderB());
        expect(currentTotalStakesB.normalizedStakerStakeSum).to.be.bignumber.eq(500);
        expect(currentTotalStakesB.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(5500).plus(cusdtToUSDTAmount).dp(2),
        );

        const currentTotalStakesC = await derivadex.getCurrentTotalStakesAsync(fixtures.traderC());
        expect(currentTotalStakesC.normalizedStakerStakeSum).to.be.bignumber.eq(2500);
        expect(currentTotalStakesC.normalizedGlobalCapSum.dp(2)).to.be.bignumber.eq(
            new BigNumber(5500).plus(cusdtToUSDTAmount).dp(2),
        );
    });

    it('claims a couple times for Traders A, B, C again', async () => {
        const diFundTokenAddress = (await derivadex.getStakeCollateralByCollateralNameAsync(USDT.collateralName))
            .diFundToken;
        await derivadex
            .transferDIFundToken(diFundTokenAddress, fixtures.traderD(), 1000)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        const diFundTokenBalanceUSDTA = await derivadex.getBalanceOfDIFundTokenAsync(
            USDT.collateralName,
            fixtures.traderA(),
        );
        const diFundTokenBalanceUSDTD = await derivadex.getBalanceOfDIFundTokenAsync(
            USDT.collateralName,
            fixtures.traderD(),
        );
        expect(diFundTokenBalanceUSDTA).to.be.bignumber.eq(1500);
        expect(diFundTokenBalanceUSDTD).to.be.bignumber.eq(1000);
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderB())
            .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderC())
            .awaitTransactionSuccessAsync({ from: fixtures.traderC() });

        await advanceBlocksAsync(derivadex.providerEngine, 10);

        const cusdtToUSDTTokenAmount = await derivadex.getUSDTTokensFromCUSDTAsync(
            CUSDT.collateralAddress,
            50000000000,
        );
        const unclaimedLocalCurrentA1 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderA(),
            new BigNumber(5500000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(2500000000).plus(cusdtToUSDTTokenAmount),
        );
        const unclaimedLocalCurrentB1 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderB(),
            new BigNumber(5500000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(500000000),
        );
        const unclaimedLocalCurrentC1 = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderC(),
            new BigNumber(5500000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(2500000000),
        );

        expect(unclaimedLocalCurrentA1.lt(unclaimedLocalCurrentA2)).to.eq(true);
        expect(unclaimedLocalCurrentB1.gt(unclaimedLocalCurrentB2)).to.eq(true);
        expect(unclaimedLocalCurrentC1.gt(unclaimedLocalCurrentC2)).to.eq(true);

        const traderAAttributes = await derivadex.getTraderAsync(fixtures.traderA());
        const traderAWalletBalance = await derivadex.getBalanceOfDDXAsync(traderAAttributes.ddxWalletContract);
        const traderAClaimedDDX = await derivadex.getDDXClaimantStateAsync(fixtures.traderA());
        expect(traderAWalletBalance.dp(2)).to.be.bignumber.eq(traderAAttributes.ddxBalance.dp(2));
        expect(traderAWalletBalance.dp(2)).to.be.bignumber.eq(traderAClaimedDDX.claimedDDX.plus(10).dp(2));
    });

    it('insurance miner can claim earned rewards after "mineRateForBlock" has been updated', async () => {
        const traderBAttributes = await derivadex.getTraderAsync(fixtures.traderB());
        expect(traderBAttributes).to.not.be.eq(ZERO_ADDRESS);
        const traderBWalletBalancePreClaim = await derivadex.getBalanceOfDDXAsync(traderBAttributes.ddxWalletContract);
        await advanceBlocksAsync(derivadex.providerEngine, 10);

        // Submit a governance proposal to change the "mineRatePerBlock" to 0.
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.insuranceFundContract.getFunctionSignature('setMineRatePerBlock')];
        const calldatas = [
            generateCallData(
                derivadex.insuranceFundContract.setMineRatePerBlock(new BigNumber(0)).getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Set the mine rate for each block to zero.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await advanceBlocksAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.votingDelay + 1);
        await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalId).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.timelockDelay);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        await derivadex.execute(proposalId++).awaitTransactionSuccessAsync({ from: fixtures.traderB() });

        // Claim the insurance miner's DDX.
        const cusdtToUSDTTokenAmount = await derivadex.getUSDTTokensFromCUSDTAsync(
            CUSDT.collateralAddress,
            50000000000,
        );
        const unclaimedLocalCurrentB = await derivadex.getUnclaimedDDXRewardsLocalAsync(
            fixtures.traderB(),
            new BigNumber(5500000000).plus(cusdtToUSDTTokenAmount),
            new BigNumber(500000000),
        );
        await derivadex
            .claimDDXFromInsuranceMining(fixtures.traderB())
            .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        const traderBWalletBalancePostClaim = await derivadex.getBalanceOfDDXAsync(traderBAttributes.ddxWalletContract);
        expect(traderBWalletBalancePostClaim.dp(2)).to.be.bignumber.eq(
            traderBWalletBalancePreClaim.plus(unclaimedLocalCurrentB).dp(2),
        );
    });
});
// tslint:disable-line:max-file-line-count
