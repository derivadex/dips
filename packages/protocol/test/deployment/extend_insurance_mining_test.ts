import { BigNumber, providerUtils } from '@0x/utils';
import { SupportedProvider, Web3Wrapper } from '@0x/web3-wrapper';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { Derivadex } from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, advanceTimeAsync } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { ContractAddresses } from '@derivadex/types';
import * as hardhat from 'hardhat';

import { extendInsuranceMiningAsync } from '../../deployment';
import { FORK_URL, GOVERNANCE_DEPLOYMENT_CONSTANTS } from '../constants';

describe('Extending insurance mining', () => {
    let derivadex: Derivadex;
    let ddxAddresses: ContractAddresses;
    let anotherLargeVoter: string;
    let communityProposer: string;
    let insuranceMiner: string;
    let treasury: string;
    let proposalId: number;
    let web3Wrapper: Web3Wrapper;
    let accounts: Array<string>;
    let provider: SupportedProvider;
    let finalBlock: BigNumber;

    // 3 month extension based on the average block number found here: https://etherscan.io/chart/blocktime.
    const INSURANCE_MINING_EXTENSION = new BigNumber(31 * 3 * 24 * 60 * 60).dividedToIntegerBy(13);

    before(async () => {
        // Reset to a fresh forked state.
        provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [
                {
                    forking: {
                        jsonRpcUrl: FORK_URL,
                    },
                },
            ],
        });
        const chainId = await providerUtils.getChainIdAsync(provider);
        accounts = await web3Wrapper.getAvailableAddressesAsync();
        const { addresses } = await getContractDeploymentAsync('derivadex');
        ddxAddresses = addresses;

        // NOTE(jalextowle): Since the DDX is primarily held by the GnosisSafeProxy,
        // we will unlock this account and use it as the `owner` account in order
        // to set up the fixture trader accounts.
        treasury = ddxAddresses.gnosisSafeProxyAddress;
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [treasury],
        });
        await web3Wrapper.sendTransactionAsync({
            from: accounts[7],
            to: treasury,
            value: Web3Wrapper.toBaseUnitAmount(10, 18),
        });

        communityProposer = '0x9765adc04fe7dcb8913037dbb3e05fabdd4a11ff';
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [communityProposer],
        });
        await web3Wrapper.sendTransactionAsync({
            from: accounts[7],
            to: communityProposer,
            value: Web3Wrapper.toBaseUnitAmount(10, 18),
        });

        anotherLargeVoter = '0x76472d8c89d5895646b96fb3f2033572303b641a';
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [anotherLargeVoter],
        });
        await web3Wrapper.sendTransactionAsync({
            from: accounts[7],
            to: anotherLargeVoter,
            value: Web3Wrapper.toBaseUnitAmount(10, 18),
        });

        insuranceMiner = '0x962699DB05A9334C5cd1f9C2867d5160C8E37742';
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [insuranceMiner],
        });

        derivadex = new Derivadex(ddxAddresses, provider, chainId);
        derivadex.setFacetAddressesToProxy();

        await derivadex.ddxContract
            .transfer(communityProposer, Web3Wrapper.toBaseUnitAmount(10000000, 18))
            .awaitTransactionSuccessAsync({ from: treasury });

        [, , , , , finalBlock] = await derivadex.insuranceFundContract.getInsuranceMineInfo().callAsync();
    });

    after(async () => {
        // Stop impersonating the treasury
        await web3Wrapper.sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: [treasury],
        });
        // Stop impersonating the community proposer
        await web3Wrapper.sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: [communityProposer],
        });
        // Stop impersonating the large voter
        await web3Wrapper.sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: [anotherLargeVoter],
        });
    });

    describe('failure path', () => {
        let snapshotId: number;

        before(async () => {
            snapshotId = await web3Wrapper.sendRawPayloadAsync<number>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        after(async () => {
            await web3Wrapper.sendRawPayloadAsync({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it.skip('fails to insurance mine after insurance mining has ended', async () => {
            // Advance blocks to after insurance mining has ended.
            const currentBlock = await web3Wrapper.getBlockNumberAsync();
            await advanceBlocksAsync(derivadex.providerEngine, finalBlock.minus(currentBlock).toNumber());

            // Collect the insurance mining rewards of a big insurance miner.
            await derivadex.insuranceFundContract
                .claimDDXFromInsuranceMining(insuranceMiner)
                .awaitTransactionSuccessAsync({ from: insuranceMiner });
            let trader = await derivadex.traderContract.getTrader(insuranceMiner).callAsync();
            await derivadex.traderContract
                .withdrawDDXToTrader(trader.ddxBalance)
                .awaitTransactionSuccessAsync({ from: insuranceMiner });

            // Mine some more blocks.
            await advanceBlocksAsync(derivadex.providerEngine, 100);

            // Ensure that we don't receive insurance mining rewards.
            const balanceBefore = await derivadex.ddxContract.balanceOf(insuranceMiner).callAsync();
            await derivadex.insuranceFundContract
                .claimDDXFromInsuranceMining(insuranceMiner)
                .awaitTransactionSuccessAsync({ from: insuranceMiner });
            trader = await derivadex.traderContract.getTrader(insuranceMiner).callAsync();
            await derivadex.traderContract
                .withdrawDDXToTrader(trader.ddxBalance)
                .awaitTransactionSuccessAsync({ from: insuranceMiner });
            const balanceAfter = await derivadex.ddxContract.balanceOf(insuranceMiner).callAsync();
            expect(balanceAfter).to.be.bignumber.eq(balanceBefore);
        });
    });

    describe('Make proposal to extend insurance mining', () => {
        before(async () => {
            proposalId = (await derivadex.getProposalCountAsync()).toNumber();
        });

        it.skip('make proposal to extend insurance mining', async () => {
            const initProposalCount = await derivadex.getProposalCountAsync();
            await extendInsuranceMiningAsync(ddxAddresses, provider, communityProposer, INSURANCE_MINING_EXTENSION);
            await advanceBlocksAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.votingDelay + 1);

            expect(proposalId).to.eq(initProposalCount.toNumber() + 1);
        });

        it('casts votes to succeed', async () => {
            const [, , , , , initialBlock] = await derivadex.insuranceFundContract.getInsuranceMineInfo().callAsync();

            // Cast vote from three addresses that exceed the 50% threshold to skip remaining voting period
            await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: communityProposer });
            await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: treasury });
            await derivadex.castVote(proposalId, true).awaitTransactionSuccessAsync({ from: anotherLargeVoter });
            await derivadex.queue(proposalId).awaitTransactionSuccessAsync({ from: accounts[0] });
            await advanceTimeAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.timelockDelay);
            await advanceBlocksAsync(derivadex.providerEngine, 1);
            await derivadex.execute(proposalId).awaitTransactionSuccessAsync({ from: accounts[0] });

            // Ensure the insurance mining state was updated.
            const [, , , , , finalBlock] = await derivadex.insuranceFundContract.getInsuranceMineInfo().callAsync();
            expect(finalBlock).to.be.bignumber.eq(initialBlock.plus(INSURANCE_MINING_EXTENSION));
        });

        it('mines more DDX from insurance mining', async () => {
            // Advance blocks to after insurance mining has ended.
            const currentBlock = await web3Wrapper.getBlockNumberAsync();
            await advanceBlocksAsync(derivadex.providerEngine, finalBlock.minus(currentBlock).toNumber());

            // Proactively claim DDX to ensure that any DDX is earned from the new program.
            await derivadex.insuranceFundContract
                .claimDDXFromInsuranceMining(insuranceMiner)
                .awaitTransactionSuccessAsync({ from: insuranceMiner });
            let trader = await derivadex.traderContract.getTrader(insuranceMiner).callAsync();
            await derivadex.traderContract
                .withdrawDDXToTrader(trader.ddxBalance)
                .awaitTransactionSuccessAsync({ from: insuranceMiner });

            // Mine some more blocks.
            await advanceBlocksAsync(derivadex.providerEngine, 100);

            // Collect the insurance mining rewards of a big insurance miner.
            const balanceBefore = await derivadex.ddxContract.balanceOf(insuranceMiner).callAsync();
            await derivadex.insuranceFundContract
                .claimDDXFromInsuranceMining(insuranceMiner)
                .awaitTransactionSuccessAsync({ from: insuranceMiner });
            trader = await derivadex.traderContract.getTrader(insuranceMiner).callAsync();
            await derivadex.traderContract
                .withdrawDDXToTrader(trader.ddxBalance)
                .awaitTransactionSuccessAsync({ from: insuranceMiner });
            const balanceAfter = await derivadex.ddxContract.balanceOf(insuranceMiner).callAsync();
            expect(balanceAfter).to.be.bignumber.gt(balanceBefore);
        });
    });
});
