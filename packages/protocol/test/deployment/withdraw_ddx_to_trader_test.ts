import { providerUtils } from '@0x/utils';
import { SupportedProvider, Web3Wrapper } from '@0x/web3-wrapper';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { Derivadex } from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, advanceTimeAsync } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { ContractAddresses, ProposalState } from '@derivadex/types';
import * as hardhat from 'hardhat';

import { withdrawDDXToTraderAsync } from '../../deployment';
import { FORK_URL, GOVERNANCE_DEPLOYMENT_CONSTANTS } from '../constants';

describe('#Withdrawing DDX to trader', function () {
    let derivadex: Derivadex;
    let ddxAddresses: ContractAddresses;
    let largeVoter: string;
    let proposalId: number;
    let web3Wrapper: Web3Wrapper;
    let accounts: Array<string>;
    let provider: SupportedProvider;

    before(async () => {
        // Reset to a fresh forked state.
        provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [
                {
                    forking: {
                        jsonRpcUrl: FORK_URL,
                    },
                },
            ],
        });
        const chainId = await providerUtils.getChainIdAsync(provider);
        accounts = await web3Wrapper.getAvailableAddressesAsync();
        const { addresses, chainId: chainId_ } = await getContractDeploymentAsync('derivadex');
        ddxAddresses = addresses;
        expect(chainId).to.be.eq(chainId_);

        largeVoter = '0x7055ef0557dc6ff56cdf0c36d28b346d40a1b8ed';
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_impersonateAccount',
            params: [largeVoter],
        });

        derivadex = new Derivadex(ddxAddresses, provider, chainId);
        derivadex.setFacetAddressesToProxy();
    });

    after(async () => {
        // Stop impersonating the other large voter
        await web3Wrapper.sendRawPayloadAsync({
            method: 'hardhat_stopImpersonatingAccount',
            params: [largeVoter],
        });
    });

    it('withdraws DDX', async () => {
        const status = await derivadex.getStateOfProposalAsync(7);
        if (status !== ProposalState.Executed) {
            await withdrawDDXToTraderAsync(ddxAddresses, provider, largeVoter, 2000, false);
            await expect(withdrawDDXToTraderAsync(ddxAddresses, provider, largeVoter, 2000, true)).to.be.rejectedWith(
                'Trader: prior to reward cliff.',
            );
            await advanceTimeAsync(derivadex.providerEngine, GOVERNANCE_DEPLOYMENT_CONSTANTS.timelockDelay);
            await advanceBlocksAsync(derivadex.providerEngine, 1);
            proposalId = (await derivadex.getProposalCountAsync()).toNumber();
            await derivadex.execute(proposalId).awaitTransactionSuccessAsync({ from: accounts[0] });
        }
        const initDDXBalance = await derivadex.getBalanceOfDDXAsync(largeVoter);
        expect(initDDXBalance).to.be.bignumber.eq(0);
        await withdrawDDXToTraderAsync(ddxAddresses, provider, largeVoter, 2000, false);
        await withdrawDDXToTraderAsync(ddxAddresses, provider, largeVoter, 2000, true);
        const finDDXBalance = await derivadex.getBalanceOfDDXAsync(largeVoter);
        expect(finDDXBalance).to.be.bignumber.eq(2000);
    });
});
