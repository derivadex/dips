import { hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import { expect } from '@derivadex/test-utils';
import * as hardhat from 'hardhat';

import { TestLibSMTContract } from '../generated-wrappers';

const WORD_SIZE = 32;
const FUZZ_ITERATIONS = 1000;

const NULL_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000000';

describe('LibSMT Fuzz Tests', () => {
    let tester: TestLibSMTContract;

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });
        const [fromAddress] = await web3Wrapper.getAvailableAddressesAsync();
        tester = await TestLibSMTContract.deployFrom0xArtifactAsync(
            artifacts.LibSMT,
            provider,
            { from: fromAddress },
            artifacts,
        );
    });

    describe('#hashLeaf', () => {
        it('should return 0x0 when leaf.value == 0x0', async () => {
            const key = hexUtils.random(WORD_SIZE);
            const actualHash = await tester.hashLeafExternal({ key, value: NULL_BYTES32 }).callAsync();
            expect(actualHash).to.be.eq(NULL_BYTES32);
        });

        for (let i = 0; i < FUZZ_ITERATIONS; i++) {
            it(`#hashLeaf trial ${i}`, async () => {
                const key = hexUtils.random(WORD_SIZE);
                const value = hexUtils.random(WORD_SIZE);
                const expectedHash = hexUtils.hash(hexUtils.concat(key, value));
                const actualHash = await tester.hashLeafExternal({ key, value }).callAsync();
                expect(actualHash).to.be.eq(expectedHash);
            });
        }
    });

    describe('#merge', () => {
        it('should return rhs when lhs == 0x0', async () => {
            const rhs = hexUtils.random(WORD_SIZE);
            const actualHash = await tester.mergeExternal(NULL_BYTES32, rhs).callAsync();
            expect(actualHash).to.be.eq(rhs);
        });

        it('should return lhs when rhs == 0x0', async () => {
            const lhs = hexUtils.random(WORD_SIZE);
            const actualHash = await tester.mergeExternal(lhs, NULL_BYTES32).callAsync();
            expect(actualHash).to.be.eq(lhs);
        });

        for (let i = 0; i < FUZZ_ITERATIONS; i++) {
            it(`#merge trial ${i}`, async () => {
                const lhs = hexUtils.random(WORD_SIZE);
                const rhs = hexUtils.random(WORD_SIZE);
                const expectedHash = hexUtils.hash(hexUtils.concat(lhs, rhs));
                const actualHash = await tester.mergeExternal(lhs, rhs).callAsync();
                expect(actualHash).to.be.eq(expectedHash);
            });
        }
    });

    describe('#testPushAndPopLeaves', () => {
        const LENGTH_PARAMETER = 20;
        for (let i = 0; i < FUZZ_ITERATIONS; i++) {
            it(`#testPushAndPopLeaves trial ${i}`, async () => {
                // Generate a random number of leaves (length in (0, LENGTH_PARAMETER])
                const leaves = [];
                const randomLength = Math.floor(Math.random() * LENGTH_PARAMETER) + 1;
                for (let i = 0; i < randomLength; i++) {
                    leaves.push({ key: hexUtils.random(WORD_SIZE), value: hexUtils.random(WORD_SIZE) });
                }

                // Run the test. This is a Solidity test, so the bulk of the logic
                // can be found in ../../contracts/test/TestLibSMT.sol.
                await tester.testPushAndPopLeaves(leaves).callAsync();
            });
        }
    });
});
