import { hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import { copyBits, getBit } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import * as hardhat from 'hardhat';

import { TestLibBytes32Contract } from '../generated-wrappers';

const FUZZ_ITERATIONS = 1000;

const NULL_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000000';

describe('LibBytes32 Fuzz Tests', () => {
    let tester: TestLibBytes32Contract;

    function randomIdx(): number {
        return Math.floor(Math.random() * 256);
    }

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });
        const [fromAddress] = await web3Wrapper.getAvailableAddressesAsync();
        tester = await TestLibBytes32Contract.deployFrom0xArtifactAsync(
            artifacts.LibBytes32,
            provider,
            { from: fromAddress },
            artifacts,
        );
    });

    describe('#getBit', () => {
        for (let i = 0; i < FUZZ_ITERATIONS; i++) {
            it(`#getBit trial ${i}`, async () => {
                const value = hexUtils.random(32);
                const idx = randomIdx();
                const expectedBit = getBit(value, idx);
                const actualBit = await tester.getBitExternal(value, idx).callAsync();
                expect(actualBit, `failed with value ${value} and idx ${idx}`).to.be.eq(expectedBit);
            });
        }
    });

    describe('#setBit', () => {
        for (let i = 0; i < FUZZ_ITERATIONS; i++) {
            it(`#setBit trial ${i}`, async () => {
                const value = hexUtils.random(32);
                const idx = randomIdx();
                const updatedValue = await tester.setBitExternal(value, idx).callAsync();
                const actualBit = getBit(updatedValue, idx);
                expect(actualBit, `failed with value ${value} and idx ${idx}`).to.be.eq(true);
            });
        }
    });

    describe('#copyBits', () => {
        for (let i = 0; i < FUZZ_ITERATIONS; i++) {
            it(`#copyBits trial ${i}`, async () => {
                const value = hexUtils.random(32);
                const start = randomIdx();
                const expectedValue = copyBits(value, start);
                const actualValue = await tester.copyBitsExternal(value, start).callAsync();
                expect(actualValue, `failed with value ${value} and start ${start}`).to.be.eq(expectedValue);
            });
        }
    });

    describe('#parentPath', () => {
        for (let i = 0; i < FUZZ_ITERATIONS; i++) {
            it(`#parentPath trial ${i}`, async () => {
                const value = hexUtils.random(32);
                const height = randomIdx();
                let expectedValue = NULL_BYTES32;
                if (height !== 255) {
                    expectedValue = copyBits(value, height + 1);
                }
                const actualValue = await tester.parentPathExternal(value, height).callAsync();
                expect(actualValue, `failed with value ${value} and height ${height}`).to.be.eq(expectedValue);
            });
        }
    });
});
