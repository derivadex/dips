import { BigNumber, hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import { binaryToHex, getBit } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import * as hardhat from 'hardhat';

import { TestLibSMTContract } from '../generated-wrappers';

const WORD_SIZE = 32;
const NULL_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000000';

type Node = { key: string; value: string };

const HASH_SIBLINGS_OPCODE = '0x48';
const PUSH_LEAF_OPCODE = '0x4C';
const HASH_PROOF_ELEMENT_OPCODE = '0x50';

// TODO(jalextowle): All of the unit tests of the operations are necessary but
// not sufficient. Many of the assertions need to be tested at a higher level
// and all need to be fuzzed aggressively in tandem with the operator.
//
// TODO(jalextowle): We could probably build a random utils library to dry up
// some of the random utilities in this test and others.
describe('LibSMT Unit Tests', () => {
    let tester: TestLibSMTContract;
    const STACK_LENGTH_PARAM = 20;
    const PROOF_LENGTH_PARAM = 500;

    function randomNodeArray(): Node[] {
        const stackLength = Math.floor(Math.random() * STACK_LENGTH_PARAM);
        const stack = [];
        for (let i = 0; i < stackLength; i++) {
            stack.push({ key: hexUtils.random(WORD_SIZE), value: hexUtils.random(WORD_SIZE) });
        }
        return stack;
    }

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });
        const [fromAddress] = await web3Wrapper.getAvailableAddressesAsync();
        tester = await TestLibSMTContract.deployFrom0xArtifactAsync(
            artifacts.TestLibSMT,
            provider,
            { from: fromAddress },
            artifacts,
        );
    });

    describe('#computeRoot', () => {
        it('should fail if an empty proof is provided', async () => {
            const tx = tester.computeRootPublic([], '0x').callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Invalid stack size after execution.');
        });

        it('should correctly push a leaf to the stack', async () => {
            const leaf = { key: hexUtils.random(WORD_SIZE), value: hexUtils.random(WORD_SIZE) };
            const proof = PUSH_LEAF_OPCODE;
            const value = await tester.computeRootPublic([leaf], proof).callAsync();
            expect(value, 'invalid value').to.be.eq(hexUtils.hash(hexUtils.concat(leaf.key, leaf.value)));
        });

        it('should correctly push a leaf to the stack and then hash it with proof data', async () => {
            // Create the proof.
            const height = Math.floor(Math.random() * 256);
            const data = hexUtils.random(WORD_SIZE);
            const leaf = { key: hexUtils.random(WORD_SIZE), value: hexUtils.random(WORD_SIZE) };
            const proof = hexUtils.concat(PUSH_LEAF_OPCODE, HASH_PROOF_ELEMENT_OPCODE, hexUtils.toHex(height), data);

            // Construct the expectedValue.
            let expectedValue;
            const leafHash = hexUtils.hash(hexUtils.concat(leaf.key, leaf.value));
            if (getBit(leaf.key, height)) {
                expectedValue = hexUtils.hash(hexUtils.concat(data, leafHash));
            } else {
                expectedValue = hexUtils.hash(hexUtils.concat(leafHash, data));
            }

            // Execute the test.
            const value = await tester.computeRootPublic([leaf], proof).callAsync();
            expect(value, 'invalid value').to.be.eq(expectedValue);
        });

        it('should correctly push two sibling leaves to the stack and then hash them together', async () => {
            // Create the proof.
            const height = 20;
            const keySuffix = hexUtils.random(29);
            const leaf0 = {
                key: hexUtils.concat('0x0000', binaryToHex('11000111'), keySuffix),
                value: hexUtils.random(WORD_SIZE),
            };
            const leaf1 = {
                key: hexUtils.concat('0x0000', binaryToHex('11010111'), keySuffix),
                value: hexUtils.random(WORD_SIZE),
            };
            const proof = hexUtils.concat(
                PUSH_LEAF_OPCODE,
                PUSH_LEAF_OPCODE,
                HASH_SIBLINGS_OPCODE,
                hexUtils.toHex(height),
            );

            // Construct the expectedValue.
            const expectedValue = hexUtils.hash(
                hexUtils.concat(
                    hexUtils.hash(hexUtils.concat(leaf0.key, leaf0.value)),
                    hexUtils.hash(hexUtils.concat(leaf1.key, leaf1.value)),
                ),
            );

            // Execute the test.
            const value = await tester.computeRootPublic([leaf0, leaf1], proof).callAsync();
            expect(value, 'invalid value').to.be.eq(expectedValue);
        });

        it('should correctly push two sibling leaves to the stack, hash them together, and then hash them with another proof element', async () => {
            // Create the proof.
            const height = 20;
            const data = hexUtils.random(WORD_SIZE);
            const keySuffix = hexUtils.random(29);
            const leaf0 = {
                key: hexUtils.concat('0x0000', binaryToHex('11000111'), keySuffix),
                value: hexUtils.random(WORD_SIZE),
            };
            const leaf1 = {
                key: hexUtils.concat('0x0000', binaryToHex('11010111'), keySuffix),
                value: hexUtils.random(WORD_SIZE),
            };
            const proof = hexUtils.concat(
                PUSH_LEAF_OPCODE,
                PUSH_LEAF_OPCODE,
                HASH_SIBLINGS_OPCODE,
                hexUtils.toHex(height),
                HASH_PROOF_ELEMENT_OPCODE,
                hexUtils.toHex(height + 1),
                data,
            );

            // Construct the expectedValue. The parent key is zero at a height
            // of 21, so the first hash should be on the left.
            const expectedValue = hexUtils.hash(
                hexUtils.concat(
                    hexUtils.hash(
                        hexUtils.concat(
                            hexUtils.hash(hexUtils.concat(leaf0.key, leaf0.value)),
                            hexUtils.hash(hexUtils.concat(leaf1.key, leaf1.value)),
                        ),
                    ),
                    data,
                ),
            );

            // Execute the test.
            const value = await tester.computeRootPublic([leaf0, leaf1], proof).callAsync();
            expect(value, 'invalid value').to.be.eq(expectedValue);
        });
    });

    describe('#operationHashSiblings', () => {
        it('should fail if the proof is empty', async () => {
            const tx = tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: randomNodeArray(),
                    },
                    '0x',
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Proof length too short for operationHashSiblings.');
        });

        it('should fail if the proofIndex is equal to the proof length', async () => {
            const randomProofLength = Math.floor(Math.random() * PROOF_LENGTH_PARAM) + 1;
            const randomProof = hexUtils.random(randomProofLength);
            const tx = tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(randomProof.length),
                        leafIndex: new BigNumber(0),
                        initialStack: randomNodeArray(),
                    },
                    randomProof,
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Proof length too short for operationHashSiblings.');
        });

        it('should fail if the proofIndex is greater to the proof length', async () => {
            const randomProofLength = Math.floor(Math.random() * PROOF_LENGTH_PARAM) + 1;
            const randomProof = hexUtils.random(randomProofLength);
            const tx = tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(randomProof.length + 1),
                        leafIndex: new BigNumber(0),
                        initialStack: randomNodeArray(),
                    },
                    randomProof,
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Proof length too short for operationHashSiblings.');
        });

        it('should fail if the stack is empty', async () => {
            const randomProofLength = Math.floor(Math.random() * PROOF_LENGTH_PARAM) + 1;
            const randomProof = hexUtils.random(randomProofLength);
            const tx = tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(randomProofLength - 1),
                        leafIndex: new BigNumber(0),
                        initialStack: [],
                    },
                    randomProof,
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibStack: Nothing to pop.');
        });

        it('should fail if the stack only has one element', async () => {
            const randomProofLength = Math.floor(Math.random() * PROOF_LENGTH_PARAM) + 1;
            const randomProof = hexUtils.random(randomProofLength);
            const tx = tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(randomProofLength - 1),
                        leafIndex: new BigNumber(0),
                        initialStack: [{ key: hexUtils.random(WORD_SIZE), value: hexUtils.random(WORD_SIZE) }],
                    },
                    randomProof,
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibStack: Nothing to pop.');
        });

        it('should fail if the nodes have the same key', async () => {
            const stack = [
                { key: NULL_BYTES32, value: hexUtils.random(WORD_SIZE) },
                { key: NULL_BYTES32, value: hexUtils.random(WORD_SIZE) },
            ];
            const tx = tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    '0x00',
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Values are not siblings.');
        });

        it("should fail if the nodes aren't siblings", async () => {
            const stack = [
                { key: NULL_BYTES32, value: hexUtils.random(WORD_SIZE) },
                { key: hexUtils.rightPad('0x02', WORD_SIZE), value: hexUtils.random(WORD_SIZE) },
            ];
            const tx = tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    '0x00',
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Values are not siblings.');
        });

        it('should fail if the keys are provided in reverse order', async () => {
            const stack = [
                { key: hexUtils.rightPad('0x01', WORD_SIZE), value: hexUtils.random(WORD_SIZE) },
                { key: NULL_BYTES32, value: hexUtils.random(WORD_SIZE) },
            ];
            const tx = tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    '0x00',
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Values are not siblings.');
        });

        it('should push the correct hash to the stack if the nodes are siblings (left value is zero)', async () => {
            const stack = [
                { key: NULL_BYTES32, value: NULL_BYTES32 },
                { key: hexUtils.rightPad('0x01', WORD_SIZE), value: hexUtils.random(WORD_SIZE) },
            ];
            const [key, value] = await tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    '0x00',
                )
                .callAsync();
            expect(key, 'invalid key').to.be.eq(NULL_BYTES32);
            expect(value, 'invalid value').to.be.eq(stack[1].value);
        });

        it('should push the correct hash to the stack if the nodes are siblings (right value is zero)', async () => {
            const stack = [
                { key: NULL_BYTES32, value: hexUtils.random(WORD_SIZE) },
                { key: hexUtils.rightPad('0x01', WORD_SIZE), value: NULL_BYTES32 },
            ];
            const [key, value] = await tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    '0x00',
                )
                .callAsync();
            expect(key, 'invalid key').to.be.eq(NULL_BYTES32);
            expect(value, 'invalid value').to.be.eq(stack[0].value);
        });

        it('should push the correct hash to the stack if the nodes are siblings (both non-zero)', async () => {
            const stack = [
                { key: NULL_BYTES32, value: hexUtils.random(WORD_SIZE) },
                { key: hexUtils.rightPad('0x01', WORD_SIZE), value: hexUtils.random(WORD_SIZE) },
            ];
            const [key, value] = await tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    '0x00',
                )
                .callAsync();
            expect(key, 'invalid key').to.be.eq(NULL_BYTES32);
            expect(value, 'invalid value').to.be.eq(hexUtils.hash(hexUtils.concat(stack[0].value, stack[1].value)));
        });

        it('should push the correct hash to the stack if the nodes are siblings and the height is non-zero (both non-zero)', async () => {
            const keySuffix = hexUtils.random(29);
            const stack = [
                {
                    key: hexUtils.concat('0x0000', binaryToHex('11000111'), keySuffix),
                    value: hexUtils.random(WORD_SIZE),
                },
                {
                    key: hexUtils.concat('0x0000', binaryToHex('11010111'), keySuffix),
                    value: hexUtils.random(WORD_SIZE),
                },
            ];
            const [key, value] = await tester
                .testOperationHashSiblings(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    '0x14',
                )
                .callAsync();
            expect(key, 'invalid key').to.be.eq(hexUtils.concat('0x0000', binaryToHex('11000000'), keySuffix));
            expect(value, 'invalid value').to.be.eq(hexUtils.hash(hexUtils.concat(stack[0].value, stack[1].value)));
        });
    });

    describe('#operationPushLeaf', () => {
        it('should fail if the leaf index is equal to the length of the leaves', async () => {
            const leaves = randomNodeArray();
            const tx = tester
                .testOperationPushLeaf(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(leaves.length),
                        initialStack: randomNodeArray(),
                    },
                    leaves,
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Corrupted leaf index.');
        });

        it('should fail if the leaf index is greater than the length of the leaves', async () => {
            const leaves = randomNodeArray();
            const tx = tester
                .testOperationPushLeaf(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(leaves.length + 1),
                        initialStack: randomNodeArray(),
                    },
                    leaves,
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Corrupted leaf index.');
        });

        it('should push a zero value to the stack if the leaf is null', async () => {
            const leaves = randomNodeArray();
            const leafKey = hexUtils.random(WORD_SIZE);
            leaves.push({ key: leafKey, value: NULL_BYTES32 });
            const [key, value] = await tester
                .testOperationPushLeaf(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(leaves.length - 1),
                        initialStack: randomNodeArray(),
                    },
                    leaves,
                )
                .callAsync();
            expect(key, 'invalid key').to.be.eq(leafKey);
            expect(value, 'invalid value').to.be.eq(NULL_BYTES32);
        });

        it('should push the correct hash to the stack if the leaf is non-null', async () => {
            const leaves = randomNodeArray();
            const leafKey = hexUtils.random(WORD_SIZE);
            const leafValue = hexUtils.random(WORD_SIZE);
            leaves.push({ key: leafKey, value: leafValue });
            const [key, value] = await tester
                .testOperationPushLeaf(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(leaves.length - 1),
                        initialStack: randomNodeArray(),
                    },
                    leaves,
                )
                .callAsync();
            expect(key, 'invalid key').to.be.eq(leafKey);
            expect(value, 'invalid value').to.be.eq(hexUtils.hash(hexUtils.concat(leafKey, leafValue)));
        });
    });

    // TODO(jalextowle): This is low-priority, but it would be nice to DRY up
    // these tests.
    describe('#operationHashProofElement', () => {
        it('should fail if the proof does not contain a height', async () => {
            const tx = tester
                .testOperationHashProofElement(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: [],
                    },
                    '0x',
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Proof length is too short for operationHashProofElement');
        });

        it('should fail if the proof does not contain data', async () => {
            const tx = tester
                .testOperationHashProofElement(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: [],
                    },
                    hexUtils.random(1),
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Proof length is too short for operationHashProofElement');
        });

        it('should fail if the proof does not contain data that is 32 bytes', async () => {
            const randomDataLength = Math.floor(Math.random() * 30) + 1;
            const tx = tester
                .testOperationHashProofElement(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: [],
                    },
                    hexUtils.concat(hexUtils.random(1), hexUtils.random(randomDataLength)),
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibSMT: Proof length is too short for operationHashProofElement');
        });

        it('should fail if the stack is empty', async () => {
            const tx = tester
                .testOperationHashProofElement(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: [],
                    },
                    hexUtils.concat(hexUtils.random(1), hexUtils.random(WORD_SIZE)),
                )
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibStack: Nothing to pop.');
        });

        it('should push the correct item onto the stack when key is "left" and height is 0', async () => {
            // Create the proof snippet.
            const height = '0x00';
            const data = hexUtils.random(WORD_SIZE);

            // Generate random leaf data.
            const keySuffix = hexUtils.random(31);
            const stackKey = hexUtils.concat('0xf0', keySuffix);
            const stackValue = hexUtils.random(WORD_SIZE);

            // Generate a random stack and push the random leaf data to the end.
            const stack = randomNodeArray();
            stack.push({ key: stackKey, value: stackValue });

            const [key, value] = await tester
                .testOperationHashProofElement(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: [{ key: stackKey, value: stackValue }],
                    },
                    hexUtils.concat(height, data),
                )
                .callAsync();
            expect(key, 'incorrect key').to.be.eq(hexUtils.concat('0xf0', keySuffix));
            expect(value, 'incorrect value').to.be.eq(hexUtils.hash(hexUtils.concat(stackValue, data)));
        });

        it('should push the correct item onto the stack when key is "right" and height is 0', async () => {
            // Create the proof snippet.
            const height = '0x00';
            const data = hexUtils.random(WORD_SIZE);

            // Generate random leaf data.
            const keySuffix = hexUtils.random(31);
            const stackKey = hexUtils.concat('0xf1', keySuffix);
            const stackValue = hexUtils.random(WORD_SIZE);

            // Generate a random stack and push the random leaf data to the end.
            const stack = randomNodeArray();
            stack.push({ key: stackKey, value: stackValue });

            const [key, value] = await tester
                .testOperationHashProofElement(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    hexUtils.concat(height, data),
                )
                .callAsync();
            expect(key, 'incorrect key').to.be.eq(hexUtils.concat('0xf0', keySuffix));
            expect(value, 'incorrect value').to.be.eq(hexUtils.hash(hexUtils.concat(data, stackValue)));
        });

        it('should push the correct item onto the stack when key is "left" and height is 20', async () => {
            // Create the proof snippet.
            const height = '0x14';
            const data = hexUtils.random(WORD_SIZE);

            // Generate random leaf data.
            const keySuffix = hexUtils.random(29);
            const stackKey = hexUtils.concat('0x0000', binaryToHex('11000111'), keySuffix);
            const stackValue = hexUtils.random(WORD_SIZE);

            // Generate a random stack and push the random leaf data to the end.
            const stack = randomNodeArray();
            stack.push({ key: stackKey, value: stackValue });

            // Execute the test.
            const [key, value] = await tester
                .testOperationHashProofElement(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    hexUtils.concat(height, data),
                )
                .callAsync();
            expect(key, 'incorrect key').to.be.eq(hexUtils.concat('0x0000', binaryToHex('11000000'), keySuffix));
            expect(value, 'incorrect value').to.be.eq(hexUtils.hash(hexUtils.concat(stackValue, data)));
        });

        it('should push the correct item onto the stack when key is "right" and height is 20', async () => {
            // Create the proof snippet.
            const height = '0x14';
            const data = hexUtils.random(WORD_SIZE);

            // Generate random leaf data.
            const keySuffix = hexUtils.random(29);
            const stackKey = hexUtils.concat('0x0000', binaryToHex('11010111'), keySuffix);
            const stackValue = hexUtils.random(WORD_SIZE);

            // Generate a random stack and push the random leaf data to the end.
            const stack = randomNodeArray();
            stack.push({ key: stackKey, value: stackValue });

            // Execute the test.
            const [key, value] = await tester
                .testOperationHashProofElement(
                    {
                        proofIndex: new BigNumber(0),
                        leafIndex: new BigNumber(0),
                        initialStack: stack,
                    },
                    hexUtils.concat(height, data),
                )
                .callAsync();
            expect(key, 'incorrect key').to.be.eq(hexUtils.concat('0x0000', binaryToHex('11000000'), keySuffix));
            expect(value, 'incorrect value').to.be.eq(hexUtils.hash(hexUtils.concat(data, stackValue)));
        });
    });
});
