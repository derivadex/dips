import { hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { Derivadex, SpecsSpecsUpdatedEventArgs } from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, advanceTimeAsync, generateCallData, getSelectors } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { FacetCutAction } from '@derivadex/types';
import { LogWithDecodedArgs, TransactionReceiptWithDecodedLogs } from 'ethereum-types';
import { ethers } from 'ethers';
import * as hardhat from 'hardhat';

import { setupWithProviderAsync } from '../../deployment/setup';
import { Fixtures } from '../fixtures';
import { filterEventLogs } from '../utils';

enum SpecsUpdateOp {
    Upsert,
    Remove,
}

describe('Specs Unit Tests', () => {
    let derivadex: Derivadex;
    let accounts: string[];
    let owner: string;
    let fixtures: Fixtures;

    /**
     * Add the Specs facet to the DerivaDAO diamond.
     * @param keys The keys to add to the specs facet.
     * @param specs The specs to add to the specs facet.
     */
    async function addSpecsFacetAsync(keys: string[], specs: string[]): Promise<TransactionReceiptWithDecodedLogs> {
        const specsSelectors = getSelectors(derivadex.specsContract, ['initialize']);
        const specsTargets = [derivadex.derivaDEXContract.address];
        const specsValues = [0];
        const specsSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const specsCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.specsContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: specsSelectors,
                            },
                        ],
                        derivadex.specsContract.address,
                        derivadex.specsContract.initialize(keys, specs).getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const specsDescription = 'Add Specs contract as a facet.';
        await derivadex
            .propose(specsTargets, specsValues, specsSignatures, specsCalldatas, specsDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * TODO(jalextowle): This is now used in this file and './TestOperator.ts'.
     * It probably makes sense to make this a little bit more generic and add this
     * to the `DerivaDEX` contract wrapper.
     *
     * Passes a governance proposal that has already been made using the fixtures.
     * @param proposalNumber The index of the proposal to pass.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function passProposalAsync(proposalNumber: number): Promise<TransactionReceiptWithDecodedLogs> {
        await advanceBlocksAsync(derivadex.providerEngine, 2);
        await derivadex
            .castVote(proposalNumber, true)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalNumber).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, 259200);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        return derivadex.execute(proposalNumber).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
    }

    /**
     * Create or update a specs using the Specs facet.
     * @param key The specs bytes30 key.
     * @param expr The specs expression.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function upsertSpecsAsync(key: string, expr: string): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.specsContract.getFunctionSignature('upsertSpecs')];
        const calldatas = [
            generateCallData(derivadex.specsContract.upsertSpecs(key, expr).getABIEncodedTransactionData()),
        ];
        const description = 'Create or update specs';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Removes an active market from the Specs facet.
     * @param key The ID of the market to remove.
     * @param marketIndex The index of the market in the list of active markets.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function removeSpecsAsync(key: string): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.specsContract.getFunctionSignature('removeSpecs')];
        const calldatas = [generateCallData(derivadex.specsContract.removeSpecs(key).getABIEncodedTransactionData())];
        const description = 'Remove an active market.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });

        ({ derivadex, accounts, owner } = await setupWithProviderAsync(provider, {
            isFork: false,
            isGanache: true,
            isLocalSnapshot: false,
            isTest: true,
            useDeployedDDXToken: false,
        }));

        fixtures = new Fixtures(derivadex, accounts, owner);

        await derivadex
            .transferOwnershipToDerivaDEXProxyDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        await derivadex
            .transferDDX(fixtures.traderB(), 10000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderC(), 5000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderD(), 25000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        await derivadex.ddxContract
            .delegate(fixtures.traderE())
            .awaitTransactionSuccessAsync({ from: fixtures.traderD() });
        await advanceBlocksAsync(derivadex.providerEngine, 1);

        const governanceSelectors = getSelectors(derivadex.governanceContract, ['initialize']);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.governanceContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: governanceSelectors,
                    },
                ],
                derivadex.governanceContract.address,
                derivadex.initializeGovernance(10, 1, 17280, 1209600, 259200, 4, 1, 50).getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setGovernanceAddressToProxy();

        await derivadex.transferOwnershipToSelf().awaitTransactionSuccessAsync({ from: owner });
    });

    describe('#initialize', () => {
        it('fails to add Specs facet with empty keys', async () => {
            const tx = addSpecsFacetAsync([], []);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails to add Specs facet with input parity mismatch', async () => {
            const tx = addSpecsFacetAsync([hexUtils.leftPad('0x1', 30)], []);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('successfully adds the specs facet', async () => {
            const keys = [hexUtils.leftPad('0x0', 30), hexUtils.leftPad('0x1', 30), hexUtils.leftPad('0x2', 30)];
            const specs = ['(specs0)', '(specs1)', '(specs2)'];
            const specsSelectors = getSelectors(derivadex.specsContract, ['initialize']);
            const specsTargets = [derivadex.derivaDEXContract.address];
            const specsValues = [0];
            const specsSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
            const specsCalldatas = [
                generateCallData(
                    derivadex
                        .diamondCut(
                            [
                                {
                                    facetAddress: derivadex.specsContract.address,
                                    action: FacetCutAction.Add,
                                    functionSelectors: specsSelectors,
                                },
                            ],
                            derivadex.specsContract.address,
                            derivadex.specsContract.initialize(keys, specs).getABIEncodedTransactionData(),
                        )
                        .getABIEncodedTransactionData(),
                ),
            ];
            const specsDescription = 'Add Specs contract as a facet.';
            const derivadexContract = new ethers.Contract(
                derivadex.derivaDEXContract.address,
                derivadex.governanceContract.abi,
                derivadex.provider.getSigner(),
            );
            await derivadex
                .propose(specsTargets, specsValues, specsSignatures, specsCalldatas, specsDescription)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            await advanceBlocksAsync(derivadex.providerEngine, 2);
            await derivadex
                .castVote(proposalCount, true)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            await derivadex.queue(proposalCount).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            await advanceTimeAsync(derivadex.providerEngine, 259200);
            await advanceBlocksAsync(derivadex.providerEngine, 1);
            const tx = await derivadexContract.execute(proposalCount);
            const receipt = await tx.wait();
            derivadex.setSpecsAddressToProxy();
            const genesisBlock = await derivadex.specsContract.getGenesisBlock().callAsync();
            expect(genesisBlock).to.be.bignumber.eq(receipt.blockNumber);
        });
    });

    describe('#upsertSpecs', () => {
        it('should successfully add specs', async () => {
            const key = hexUtils.leftPad('0x1', 30);
            const specs = '(spec1)';
            const { logs } = await upsertSpecsAsync(key, specs);

            // Ensure that the log included a SpecsUpdated log.
            const updatedLogs = filterEventLogs(logs, 'SpecsUpdated');
            expect(updatedLogs.length).to.be.eq(1);
            const updatedLog = updatedLogs[0] as LogWithDecodedArgs<SpecsSpecsUpdatedEventArgs>;
            expect(updatedLog.args).to.be.deep.eq({
                key,
                specs,
                op: SpecsUpdateOp.Upsert,
            });
        });

        it('should successfully update specs', async () => {
            const key = hexUtils.leftPad('0x1', 30);
            const specs = '(spec1 - updated)';
            const { logs } = await upsertSpecsAsync(key, specs);

            // Ensure that the log included a SpecsUpdated log.
            const updatedLogs = filterEventLogs(logs, 'SpecsUpdated');
            expect(updatedLogs.length).to.be.eq(1);
            const updatedLog = updatedLogs[0] as LogWithDecodedArgs<SpecsSpecsUpdatedEventArgs>;
            expect(updatedLog.args).to.be.deep.eq({
                key,
                specs,
                op: SpecsUpdateOp.Upsert,
            });
        });

        it('fails to upsert Specs facet with empty value', async () => {
            const tx = upsertSpecsAsync(hexUtils.leftPad('0x1', 30), '');
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });
    });

    describe('#removeSpecs', () => {
        it('should successfully remove specs', async () => {
            const key = hexUtils.leftPad('0x1', 30);
            const { logs } = await removeSpecsAsync(key);

            // Ensure that the log included a SpecsUpdated log.
            const updatedLogs = filterEventLogs(logs, 'SpecsUpdated');
            expect(updatedLogs.length).to.be.eq(1);
            const updatedLog = updatedLogs[0] as LogWithDecodedArgs<SpecsSpecsUpdatedEventArgs>;
            expect(updatedLog.args).to.be.deep.eq({
                key,
                specs: '',
                op: SpecsUpdateOp.Remove,
            });
        });
    });
});
