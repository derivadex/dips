import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import { expect } from '@derivadex/test-utils';
import * as hardhat from 'hardhat';

import { TestLibStackContract } from '../generated-wrappers';

// NOTE(jalextowle): Reading the contract 'TestLibStack' (found at
// ../../contracts/test/TestLibStack.sol) is required to fully understand this
// test. A large amount of the logic being tested is tested in Solidity rather
// than Typescript to leverage the memory of a single transaction.
describe('LibStack Unit Tests', () => {
    let tester: TestLibStackContract;

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });
        const [fromAddress] = await web3Wrapper.getAvailableAddressesAsync();
        tester = await TestLibStackContract.deployFrom0xArtifactAsync(
            artifacts.TestLibStack,
            provider,
            { from: fromAddress },
            artifacts,
        );
    });

    it('#testPopEmptyStack', async () => {
        const tx = tester.testPopEmptyStack().callAsync();
        return expect(tx).to.be.rejectedWith('LibStack: Nothing to pop.');
    });

    it('#testPushAfterMemoryAllocation', async () => {
        const tx = tester.testPushAfterMemoryAllocation().callAsync();
        return expect(tx).to.be.rejectedWith('LibStack: extra memory allocation detected.');
    });

    it('#testPopAfterMemoryAllocation', async () => {
        const tx = tester.testPopAfterMemoryAllocation().callAsync();
        return expect(tx).to.be.rejectedWith('LibStack: extra memory allocation detected.');
    });

    it('#testSizeAfterMemoryAllocation', async () => {
        const tx = tester.testSizeAfterMemoryAllocation().callAsync();
        return expect(tx).to.be.rejectedWith('LibStack: extra memory allocation detected.');
    });

    it('#testPushOneItem', async () => {
        await tester.testPushOneItem().callAsync();
    });

    it('#testPushTwoItems', async () => {
        await tester.testPushTwoItems().callAsync();
    });

    it('#testTwoMixedPushAndPop', async () => {
        await tester.testTwoMixedPushAndPop().callAsync();
    });

    it('#testRepeatedTwoItemPush', async () => {
        await tester.testRepeatedTwoItemPush().callAsync();
    });

    it('#testMultiLayeredPushAndPop', async () => {
        await tester.testMultiLayeredPushAndPop().callAsync();
    });
});
