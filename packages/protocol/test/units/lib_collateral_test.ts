import { BigNumber, hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import { DummyTokenContract } from '@derivadex/contract-wrappers';
import { expect } from '@derivadex/test-utils';
import hardhat from 'hardhat';

import {
    TRADE_MINING_END,
    TRADE_MINING_EPOCH_MULTIPLIER,
    TRADE_MINING_LENGTH,
    TRADE_MINING_REWARD_PER_EPOCH,
} from '../constants';
import { ZERO_ADDRESS } from '../fixtures';
import { TestLibCollateralContract } from '../generated-wrappers';

describe('LibCollateral Unit Tests', () => {
    let web3Wrapper: Web3Wrapper;

    let sender: string;
    let tester: TestLibCollateralContract;
    let ddx: DummyTokenContract;
    let tokenWith2Decimals: DummyTokenContract;
    let tokenWith6Decimals: DummyTokenContract;
    let tokenWith18Decimals: DummyTokenContract;

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });
        [sender] = await web3Wrapper.getAvailableAddressesAsync();
        tester = await TestLibCollateralContract.deployFrom0xArtifactAsync(
            artifacts.TestLibCollateral,
            provider,
            { from: sender },
            artifacts,
        );
        ddx = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            provider,
            { from: sender },
            artifacts,
            'DerivaDAO',
            'DDX',
            new BigNumber(18),
        );
        await tester.setDDXAddress(ddx.address).awaitTransactionSuccessAsync();
        tokenWith2Decimals = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            provider,
            { from: sender },
            artifacts,
            '2 Decimal Token',
            'TWO',
            new BigNumber(2),
        );
        tokenWith6Decimals = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            provider,
            { from: sender },
            artifacts,
            '6 Decimal Token',
            'SIX',
            new BigNumber(6),
        );
        tokenWith18Decimals = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            provider,
            { from: sender },
            artifacts,
            '18 Decimal Token',
            'EIGHTEEN',
            new BigNumber(18),
        );
        await tester
            .setWithdrawalAllowance(
                tokenWith2Decimals.address,
                new BigNumber(10_000).multipliedBy(new BigNumber(10).exponentiatedBy(2)),
            )
            .awaitTransactionSuccessAsync();
        await tester
            .setWithdrawalAllowance(
                tokenWith6Decimals.address,
                new BigNumber(10_000).multipliedBy(new BigNumber(10).exponentiatedBy(6)),
            )
            .awaitTransactionSuccessAsync();
        await tester
            .setWithdrawalAllowance(
                tokenWith18Decimals.address,
                new BigNumber(10_000).multipliedBy(new BigNumber(10).exponentiatedBy(18)),
            )
            .awaitTransactionSuccessAsync();
        await tester
            .setWithdrawalAllowance(
                ZERO_ADDRESS,
                new BigNumber(10_000).multipliedBy(new BigNumber(10).exponentiatedBy(18)),
            )
            .awaitTransactionSuccessAsync();
        await tester.setRateLimitParameters(new BigNumber(50), new BigNumber(100)).awaitTransactionSuccessAsync();
        await tester.setEpochId(new BigNumber(1)).awaitTransactionSuccessAsync();
    });

    describe('#commitDeposit', () => {
        let snapshotId: string;
        const tokenAddress = hexUtils.leftPad('0xdeadbeef', 20);

        before(async () => {
            // Set the DDX maximum cap to the trade mining rewards remaining
            // plus ten. This will allow only ten to be deposited in address
            // zero.
            const tradeMiningRewardsRemaining = await tester
                .calculateTradeMiningRewardsForRangePublic(
                    new BigNumber(0),
                    new BigNumber(TRADE_MINING_LENGTH).times(TRADE_MINING_EPOCH_MULTIPLIER),
                )
                .callAsync();
            await tester.setWithdrawalAllowance(ZERO_ADDRESS, new BigNumber(0)).awaitTransactionSuccessAsync();
            await tester.setMaxDDXCap(tradeMiningRewardsRemaining.plus(10)).awaitTransactionSuccessAsync();
        });

        after(async () => {
            await tester
                .setWithdrawalAllowance(
                    ZERO_ADDRESS,
                    new BigNumber(10_000).multipliedBy(new BigNumber(10).exponentiatedBy(18)),
                )
                .awaitTransactionSuccessAsync();
            await tester.setMaxDDXCap(new BigNumber(0)).awaitTransactionSuccessAsync();
        });

        beforeEach(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        afterEach(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('should fail to commit a withdrawal of zero', async () => {
            const tx = tester.commitDepositPublic(tokenAddress, new BigNumber(0)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCollateral: Invalid deposit amount.');
        });

        it('should fail to commit enough with address zero to exceed the max DDX cap', async () => {
            const tx = tester.commitDepositPublic(ZERO_ADDRESS, new BigNumber(11)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                'LibCollateral: DDX deposit exceeds the maximum allowed DDX in the SMT.',
            );
        });

        it('should fail to commit enough with the ddx address to exceed the max DDX cap', async () => {
            const tx = tester.commitDepositPublic(ddx.address, new BigNumber(11)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                'LibCollateral: DDX deposit exceeds the maximum allowed DDX in the SMT.',
            );
        });

        it('should fail to commit enough with a combination of the zero and ddx addresses to exceed the max DDX cap', async () => {
            await tester.commitDepositPublic(ZERO_ADDRESS, new BigNumber(4)).awaitTransactionSuccessAsync();
            const tx = tester.commitDepositPublic(ddx.address, new BigNumber(7)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                'LibCollateral: DDX deposit exceeds the maximum allowed DDX in the SMT.',
            );
        });

        it('should successfully commit with address zero', async () => {
            await tester.commitDepositPublic(ZERO_ADDRESS, new BigNumber(5)).awaitTransactionSuccessAsync();
            await tester.commitDepositPublic(ZERO_ADDRESS, new BigNumber(4)).awaitTransactionSuccessAsync();
            await tester.commitDepositPublic(ZERO_ADDRESS, new BigNumber(1)).awaitTransactionSuccessAsync();
            const allowance = await tester.getWithdrawalAllowance(ZERO_ADDRESS).callAsync();
            expect(allowance).to.be.bignumber.eq(new BigNumber(10));
        });

        it('should successfully commit with the ddx address', async () => {
            await tester.commitDepositPublic(ddx.address, new BigNumber(5)).awaitTransactionSuccessAsync();
            await tester.commitDepositPublic(ddx.address, new BigNumber(4)).awaitTransactionSuccessAsync();
            await tester.commitDepositPublic(ddx.address, new BigNumber(1)).awaitTransactionSuccessAsync();
            const allowance = await tester.getWithdrawalAllowance(ddx.address).callAsync();
            expect(allowance).to.be.bignumber.eq(new BigNumber(10));
        });

        it('should successfully commit with a combination of the zero and ddx addresses', async () => {
            await tester.commitDepositPublic(ddx.address, new BigNumber(5)).awaitTransactionSuccessAsync();
            await tester.commitDepositPublic(ZERO_ADDRESS, new BigNumber(4)).awaitTransactionSuccessAsync();
            await tester.commitDepositPublic(ddx.address, new BigNumber(1)).awaitTransactionSuccessAsync();
            let allowance = await tester.getWithdrawalAllowance(ddx.address).callAsync();
            expect(allowance).to.be.bignumber.eq(new BigNumber(6));
            allowance = await tester.getWithdrawalAllowance(ZERO_ADDRESS).callAsync();
            expect(allowance).to.be.bignumber.eq(new BigNumber(4));
        });

        it('should successfully commit with a non-ddx address', async () => {
            await tester.commitDepositPublic(tokenAddress, new BigNumber(1234)).awaitTransactionSuccessAsync();
            const allowance = await tester.getWithdrawalAllowance(tokenAddress).callAsync();
            expect(allowance).to.be.bignumber.eq(new BigNumber(1234));
        });
    });

    describe('#commitTraderWithdrawal', () => {
        describe('isolated tests', async () => {
            let snapshotId: string;

            beforeEach(async () => {
                // Take a snapshot of the state before the test executes.
                snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                    method: 'evm_snapshot',
                    params: [],
                });
            });

            afterEach(async () => {
                // Revert to the last snapshot.
                await web3Wrapper.sendRawPayloadAsync<void>({
                    method: 'evm_revert',
                    params: [snapshotId],
                });
            });

            it('should fail if the epoch ID is zero', async () => {
                await tester.setEpochId(new BigNumber(0)).awaitTransactionSuccessAsync();
                const tx = tester
                    .commitTraderWithdrawalPublic(sender, new BigNumber(0), new BigNumber(0))
                    .awaitTransactionSuccessAsync({ from: sender });
                await expect(tx).to.be.rejectedWith('LibCollateral: Must have non-zero epoch ID for withdrawal.');
            });

            it('should fail if there is a zero withdrawal amount', async () => {
                const tx = tester
                    .commitTraderWithdrawalPublic(sender, new BigNumber(0), new BigNumber(0))
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Withdrawal amount of zero.');
            });

            it('should fail if the withdrawal exceeds the DDX withdrawal allowance', async () => {
                await tester.setWithdrawalAllowance(ZERO_ADDRESS, new BigNumber(0)).awaitTransactionSuccessAsync();
                const tx = tester
                    .commitTraderWithdrawalPublic(sender, Web3Wrapper.toBaseUnitAmount(1, 12), new BigNumber(2))
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should fail if there is an invalid withdrawal amount', async () => {
                const tx = tester
                    .commitTraderWithdrawalPublic(sender, Web3Wrapper.toBaseUnitAmount(2, 12), new BigNumber(1))
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Invalid withdrawal amount.');
            });

            it('should succeed if the withdrawal is less than the frozen ddx balance', async () => {
                await tester
                    .commitTraderWithdrawalPublic(sender, Web3Wrapper.toBaseUnitAmount(1, 12), new BigNumber(2))
                    .awaitTransactionSuccessAsync({ from: sender });
            });

            it('should succeed if the withdrawal is equal to the frozen ddx balance', async () => {
                await tester
                    .commitTraderWithdrawalPublic(sender, Web3Wrapper.toBaseUnitAmount(2, 12), new BigNumber(2))
                    .awaitTransactionSuccessAsync({ from: sender });
            });
        });

        describe('interacting tests', () => {
            it('should successfully withdraw and update the DDX withdrawal allowance', async () => {
                const ddxWithdrawAllowanceBefore = await tester.getWithdrawalAllowance(ZERO_ADDRESS).callAsync();
                await tester
                    .commitTraderWithdrawalPublic(sender, Web3Wrapper.toBaseUnitAmount(99, 12), new BigNumber(100))
                    .awaitTransactionSuccessAsync({ from: sender });
                const ddxWithdrawAllowanceAfter = await tester.getWithdrawalAllowance(ZERO_ADDRESS).callAsync();
                expect(ddxWithdrawAllowanceAfter).to.be.bignumber.eq(
                    ddxWithdrawAllowanceBefore.minus(Web3Wrapper.toBaseUnitAmount(99, 12)),
                );
            });

            it('should fail to perform a double withdrawal', async () => {
                const tx = tester
                    .commitTraderWithdrawalPublic(sender, Web3Wrapper.toBaseUnitAmount(99, 12), new BigNumber(100))
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Invalid withdrawal amount.');
            });

            it('should successfully withdraw the remaining amount', async () => {
                const ddxWithdrawAllowanceBefore = await tester.getWithdrawalAllowance(ZERO_ADDRESS).callAsync();
                await tester
                    .commitTraderWithdrawalPublic(sender, Web3Wrapper.toBaseUnitAmount(1, 12), new BigNumber(100))
                    .awaitTransactionSuccessAsync({ from: sender });
                const ddxWithdrawAllowanceAfter = await tester.getWithdrawalAllowance(ZERO_ADDRESS).callAsync();
                expect(ddxWithdrawAllowanceAfter).to.be.bignumber.eq(
                    ddxWithdrawAllowanceBefore.minus(Web3Wrapper.toBaseUnitAmount(1, 12)),
                );
            });

            it('should fail to withdraw more DDX than is currently allowed to withdraw', async () => {
                const tx = tester
                    .commitTraderWithdrawalPublic(
                        sender,
                        Web3Wrapper.toBaseUnitAmount(10_000, 18),
                        new BigNumber(10_000),
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });
        });
    });

    describe('#commitStrategyWithdrawal', () => {
        const strategyId = hexUtils.leftPad('0x1');

        describe('isolated tests', async () => {
            let snapshotId: string;

            beforeEach(async () => {
                // Take a snapshot of the state before the test executes.
                snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                    method: 'evm_snapshot',
                    params: [],
                });
            });

            afterEach(async () => {
                // Revert to the last snapshot.
                await web3Wrapper.sendRawPayloadAsync<void>({
                    method: 'evm_revert',
                    params: [snapshotId],
                });
            });

            it('should fail if the epoch ID is zero', async () => {
                await tester.setEpochId(new BigNumber(0)).awaitTransactionSuccessAsync();
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [], amounts: [] },
                        { tokens: [], amounts: [] },
                        { tokens: [], amounts: [] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                await expect(tx).to.be.rejectedWith('LibCollateral: Must have non-zero epoch ID for withdrawal.');
            });

            it('should fail if withdrawal data is empty', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [], amounts: [] },
                        { tokens: [], amounts: [] },
                        { tokens: [], amounts: [] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Malformed withdrawal data.');
            });

            it('should fail if withdrawal data has more tokens than amounts', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [ZERO_ADDRESS], amounts: [] },
                        { tokens: [], amounts: [] },
                        { tokens: [], amounts: [] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Malformed withdrawal data.');
            });

            it('should fail if withdrawal data has more amounts than tokens', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        { tokens: [], amounts: [] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Malformed withdrawal data.');
            });

            it('should fail if free collateral has more tokens than amounts', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [ZERO_ADDRESS], amounts: [new BigNumber(1)] },
                        { tokens: [ZERO_ADDRESS], amounts: [] },
                        { tokens: [], amounts: [] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Malformed free collateral data.');
            });

            it('should fail if free collateral has more amounts than tokens', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [ZERO_ADDRESS], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Malformed free collateral data.');
            });

            it('should fail if frozen collateral is empty', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [ZERO_ADDRESS], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        { tokens: [], amounts: [] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Malformed frozen collateral data.');
            });

            it('should fail if frozen collateral has more tokens than amounts', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [ZERO_ADDRESS], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        { tokens: [ZERO_ADDRESS], amounts: [] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Malformed frozen collateral data.');
            });

            it('should fail if frozen collateral has more amounts than tokens', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [ZERO_ADDRESS], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        { tokens: [], amounts: [new BigNumber(1)] },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Malformed frozen collateral data.');
            });

            it('should fail if there is duplicate collateral data in withdraw data', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        {
                            tokens: [tokenWith6Decimals.address, tokenWith6Decimals.address],
                            amounts: [new BigNumber(1), new BigNumber(1)],
                        },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith6Decimals.address],
                            amounts: [new BigNumber(1)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Missing address in frozen collateral.');
            });

            it('should fail if there is duplicate collateral data in frozen collateral', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith6Decimals.address], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith6Decimals.address, tokenWith6Decimals.address],
                            amounts: [new BigNumber(1), new BigNumber(1)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Duplicate collateral data.');
            });

            it('should fail if there is duplicate collateral data in frozen collateral and withdraw data', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        {
                            tokens: [tokenWith6Decimals.address, tokenWith6Decimals.address],
                            amounts: [new BigNumber(1), new BigNumber(1)],
                        },

                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith6Decimals.address, tokenWith6Decimals.address],
                            amounts: [new BigNumber(1), new BigNumber(1)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Duplicate collateral data.');
            });

            it('should fail if withdraw data is not ordered correctly', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        {
                            tokens: [tokenWith2Decimals.address, tokenWith6Decimals.address],
                            amounts: [new BigNumber(1), new BigNumber(1)],
                        },
                        {
                            tokens: [tokenWith6Decimals.address, tokenWith2Decimals.address],
                            amounts: [new BigNumber(1), new BigNumber(1)],
                        },
                        {
                            tokens: [ZERO_ADDRESS, ZERO_ADDRESS],
                            amounts: [new BigNumber(1), new BigNumber(1)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Missing address in frozen collateral.');
            });

            it('should fail if there is a zero withdrawal amount', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith6Decimals.address], amounts: [new BigNumber(0)] },

                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith6Decimals.address],
                            amounts: [new BigNumber(1)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Withdrawal amount of zero.');
            });

            it('should fail if there is an invalid withdrawal amount', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith6Decimals.address], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith6Decimals.address],
                            amounts: [new BigNumber(0)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Invalid withdrawal amount.');
            });

            it('should succeed if the withdrawal is less than the frozen collateral amount and the token has 6 decimals', async () => {
                await tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith6Decimals.address], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith6Decimals.address],
                            amounts: [new BigNumber(2)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
            });

            it('should succeed if the scaled withdrawal amount is less than the frozen collateral amount and the token has fewer than 6 decimals', async () => {
                await tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith2Decimals.address], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith2Decimals.address],
                            amounts: [new BigNumber(2000000000000)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
            });

            it('should succeed if the withdrawal is equal to the frozen collateral amount and the token has 6 decimals', async () => {
                await tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith6Decimals.address], amounts: [new BigNumber(1)] },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith6Decimals.address],
                            amounts: [new BigNumber(1)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
            });

            it('should succeed if the scaled withdrawal amount is equal to the frozen collateral amount and the token has 18 decimals', async () => {
                await tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith18Decimals.address], amounts: [new BigNumber(1000000000000)] },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith18Decimals.address],
                            amounts: [new BigNumber(1)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
            });
        });

        describe('interacting tests', () => {
            it('should succeed if the withdrawal contains multiple valid withdrawals', async () => {
                await tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        {
                            tokens: [tokenWith2Decimals.address, tokenWith18Decimals.address],
                            amounts: [new BigNumber(1), new BigNumber(4000000000000)],
                        },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith2Decimals.address, tokenWith18Decimals.address],
                            amounts: [new BigNumber(1000000000000), new BigNumber(5)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                const [pastWithdrawalAmountAddress2] = await tester
                    .getUnprocessedWithdrawals(sender, strategyId, tokenWith2Decimals.address)
                    .callAsync();
                expect(pastWithdrawalAmountAddress2).to.be.bignumber.eq(new BigNumber(10000));
                const [pastWithdrawalAmountAddress3] = await tester
                    .getUnprocessedWithdrawals(sender, strategyId, tokenWith18Decimals.address)
                    .callAsync();
                expect(pastWithdrawalAmountAddress3).to.be.bignumber.eq(new BigNumber(4));
            });

            it('should fail to perform a double withdrawal', async () => {
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        {
                            tokens: [tokenWith2Decimals.address, tokenWith18Decimals.address],
                            amounts: [new BigNumber(1), new BigNumber(4000000000000)],
                        },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith2Decimals.address, tokenWith18Decimals.address],
                            amounts: [new BigNumber(1000000000000), new BigNumber(5)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Invalid withdrawal amount.');
            });

            it('should successfully withdraw the remaining amount', async () => {
                await tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith18Decimals.address], amounts: [new BigNumber(1000000000000)] },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith2Decimals.address, tokenWith18Decimals.address],
                            amounts: [new BigNumber(1000000000000), new BigNumber(5)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                const [pastWithdrawalAmountAddress1] = await tester
                    .getUnprocessedWithdrawals(sender, strategyId, tokenWith18Decimals.address)
                    .callAsync();
                expect(pastWithdrawalAmountAddress1).to.be.bignumber.eq(new BigNumber(5));
            });

            it('should fail if the withdrawal exceeds the withdrawal allowance', async () => {
                await tester
                    .setWithdrawalAllowance(tokenWith18Decimals.address, new BigNumber(0))
                    .awaitTransactionSuccessAsync();
                const tx = tester
                    .commitStrategyWithdrawalPublic(
                        sender,
                        strategyId,
                        { tokens: [tokenWith18Decimals.address], amounts: [new BigNumber(1000000000000)] },
                        { tokens: [], amounts: [] },
                        {
                            tokens: [tokenWith2Decimals.address, tokenWith18Decimals.address],
                            amounts: [new BigNumber(1000000000000), new BigNumber(5)],
                        },
                    )
                    .awaitTransactionSuccessAsync({ from: sender });
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });
        });
    });

    describe('#calculateTradeMiningRewardsForRange', () => {
        it('should return zero when the starting epoch ID is equal to the final trade mining epoch ID', async () => {
            // We utilize an epoch that is significantly larger than the start
            // to ensure that no trade mining rewards are attributed after trade
            // mining ends.
            const rewards = await tester
                .calculateTradeMiningRewardsForRangePublic(TRADE_MINING_END, TRADE_MINING_END.times(10))
                .callAsync();
            expect(rewards).to.be.bignumber.eq(new BigNumber(0));
        });

        it('should return zero when the range is contained within a single trade mining epoch', async () => {
            const end = Math.floor(Math.random() * (TRADE_MINING_EPOCH_MULTIPLIER - 2)) + 1;
            const rewards = await tester
                .calculateTradeMiningRewardsForRangePublic(new BigNumber(1), new BigNumber(end))
                .callAsync();
            expect(rewards).to.be.bignumber.eq(new BigNumber(0));
        });

        it('should return the trade mining epoch reward when checkpoint (range = 1) spans a trade mining epoch', async () => {
            const rewards = await tester
                .calculateTradeMiningRewardsForRangePublic(new BigNumber(47), new BigNumber(48))
                .callAsync();
            expect(rewards).to.be.bignumber.eq(TRADE_MINING_REWARD_PER_EPOCH);
        });

        it('should return the trade mining epoch reward when checkpoint (range = trade mining window) spans a trade mining epoch', async () => {
            const rewards = await tester
                .calculateTradeMiningRewardsForRangePublic(new BigNumber(47), new BigNumber(95))
                .callAsync();
            expect(rewards).to.be.bignumber.eq(TRADE_MINING_REWARD_PER_EPOCH);
        });

        for (let i = 0; i < 50; i++) {
            it(`should correctly calculate rewards for ${i} epochs`, async () => {
                // Calculate a random number in the range [i * 10 + 1, i * 10 + 9].
                const end =
                    i * TRADE_MINING_EPOCH_MULTIPLIER +
                    Math.floor(Math.random() * (TRADE_MINING_EPOCH_MULTIPLIER - 2)) +
                    1;
                const rewards = await tester
                    .calculateTradeMiningRewardsForRangePublic(new BigNumber(1), new BigNumber(end))
                    .callAsync();
                expect(rewards, `invalid reward for start ${1} and end ${end}`).to.be.bignumber.eq(
                    TRADE_MINING_REWARD_PER_EPOCH.times(i),
                );
            });
        }

        it('should only attribute rewards during trade mining', async () => {
            const rewards = await tester
                .calculateTradeMiningRewardsForRangePublic(
                    TRADE_MINING_END.minus(TRADE_MINING_EPOCH_MULTIPLIER),
                    TRADE_MINING_END.plus(TRADE_MINING_EPOCH_MULTIPLIER),
                )
                .callAsync();
            expect(rewards).to.be.bignumber.eq(TRADE_MINING_REWARD_PER_EPOCH);
        });
    });

    // NOTE: The block numbers are increasing in this test to simulate what will
    // actually occur on Ethereum. We don't impose validation on the commit
    // function to verify this assertion since it is unnecessary if the function
    // is used correctly.
    describe('#commitWithdrawal', () => {
        let lastConfirmedBlockNumber = new BigNumber(0);
        const highBlockNumber = new BigNumber(10);
        const frozenCollateralAmount = new BigNumber(10_000);
        const strategyId = hexUtils.leftPad('0x1');
        const tokenAddress = hexUtils.leftPad('0x2', 20);
        const withdrawAddress = hexUtils.leftPad('0x1', 20);
        const unprocessedWithdrawals: UnprocessedWithdrawals[] = [];

        interface UnprocessedWithdrawals {
            blockNumber: BigNumber;
            withdrawalAmount: BigNumber;
        }

        async function assertGetProcessedWithdrawalsAsync(): Promise<void> {
            for (
                let blockNumber = lastConfirmedBlockNumber;
                blockNumber.lt(highBlockNumber);
                blockNumber = blockNumber.plus(1)
            ) {
                const expectedProcessedAmount = unprocessedWithdrawals
                    .filter(
                        (commit) =>
                            commit.blockNumber.gt(lastConfirmedBlockNumber) && commit.blockNumber.lte(blockNumber),
                    )
                    .reduce((p, c) => p.plus(c.withdrawalAmount), new BigNumber(0));
                const actualProcessedAmount = await tester
                    .getProcessedWithdrawals(withdrawAddress, strategyId, tokenAddress, blockNumber)
                    .callAsync();
                expect(actualProcessedAmount).to.be.bignumber.eq(expectedProcessedAmount);
            }
        }

        it('should successfully commit an entry in block 1', async () => {
            const blockNumber = new BigNumber(1);
            const withdrawalAmount = new BigNumber(1_354);
            await tester
                .commitWithdrawalPublic(
                    withdrawAddress,
                    strategyId,
                    tokenAddress,
                    frozenCollateralAmount,
                    withdrawalAmount,
                    blockNumber,
                )
                .awaitTransactionSuccessAsync();
            unprocessedWithdrawals.push({ blockNumber, withdrawalAmount });

            // Ensure that the unprocessed withdrawal state was updated correctly.
            const [amount, start, end] = await tester
                .getUnprocessedWithdrawals(withdrawAddress, strategyId, tokenAddress)
                .callAsync();
            expect(amount).to.be.bignumber.eq(withdrawalAmount);
            expect(start).to.be.bignumber.eq(blockNumber);
            expect(end).to.be.bignumber.eq(blockNumber);

            // Test the `getProcessedWithdrawals`.
            await assertGetProcessedWithdrawalsAsync();
        });

        it('should successfully update the entry in block 1', async () => {
            const blockNumber = new BigNumber(1);
            const withdrawalAmount = new BigNumber(2_646);
            await tester
                .commitWithdrawalPublic(
                    withdrawAddress,
                    strategyId,
                    tokenAddress,
                    frozenCollateralAmount,
                    withdrawalAmount,
                    blockNumber,
                )
                .awaitTransactionSuccessAsync();
            unprocessedWithdrawals.push({ blockNumber, withdrawalAmount });

            // Ensure that the unprocessed withdrawal state was updated correctly.
            const [amount, start, end] = await tester
                .getUnprocessedWithdrawals(withdrawAddress, strategyId, tokenAddress)
                .callAsync();
            expect(amount).to.be.bignumber.eq(new BigNumber(4_000));
            expect(start).to.be.bignumber.eq(blockNumber);
            expect(end).to.be.bignumber.eq(blockNumber);

            // Test the `getProcessedWithdrawals`.
            await assertGetProcessedWithdrawalsAsync();
        });

        it('should successfully commit an entry in block 3', async () => {
            const blockNumber = new BigNumber(3);
            const withdrawalAmount = new BigNumber(6_000);
            await tester
                .commitWithdrawalPublic(
                    withdrawAddress,
                    strategyId,
                    tokenAddress,
                    frozenCollateralAmount,
                    withdrawalAmount,
                    blockNumber,
                )
                .awaitTransactionSuccessAsync();
            unprocessedWithdrawals.push({ blockNumber, withdrawalAmount });

            // Ensure that the unprocessed withdrawal state was updated correctly.
            const [amount, start, end] = await tester
                .getUnprocessedWithdrawals(withdrawAddress, strategyId, tokenAddress)
                .callAsync();
            expect(amount).to.be.bignumber.eq(new BigNumber(10_000));
            expect(start).to.be.bignumber.eq(new BigNumber(1));
            expect(end).to.be.bignumber.eq(blockNumber);

            // Test the `getProcessedWithdrawals`.
            await assertGetProcessedWithdrawalsAsync();
        });

        it('should fail to commit an entry when the withdrawal exhausts the allowance', async () => {
            const blockNumber = new BigNumber(4);
            const withdrawalAmount = new BigNumber(1);
            const tx = tester
                .commitWithdrawalPublic(
                    withdrawAddress,
                    strategyId,
                    tokenAddress,
                    frozenCollateralAmount,
                    withdrawalAmount,
                    blockNumber,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCollateral: Invalid withdrawal amount.');
        });

        it('should successfully commit an entry in block 5 after updating the last confirmed block', async () => {
            const confirmedBlockNumber = new BigNumber(1);
            await tester.setLastConfirmedBlockNumber(confirmedBlockNumber).awaitTransactionSuccessAsync();
            lastConfirmedBlockNumber = confirmedBlockNumber;

            // Ensure that the unprocessed withdrawal state was updated correctly.
            let [amount, start, end] = await tester
                .getUnprocessedWithdrawals(withdrawAddress, strategyId, tokenAddress)
                .callAsync();
            expect(amount).to.be.bignumber.eq(new BigNumber(6_000));
            expect(start).to.be.bignumber.eq(new BigNumber(3));
            expect(end).to.be.bignumber.eq(new BigNumber(3));

            // Commit the new withdrawal.
            const blockNumber = new BigNumber(5);
            const withdrawalAmount = new BigNumber(3_000);
            await tester
                .commitWithdrawalPublic(
                    withdrawAddress,
                    strategyId,
                    tokenAddress,
                    frozenCollateralAmount,
                    withdrawalAmount,
                    blockNumber,
                )
                .awaitTransactionSuccessAsync();
            unprocessedWithdrawals.push({ blockNumber, withdrawalAmount });

            // Ensure that the unprocessed withdrawal state was updated correctly.
            [amount, start, end] = await tester
                .getUnprocessedWithdrawals(withdrawAddress, strategyId, tokenAddress)
                .callAsync();
            expect(amount).to.be.bignumber.eq(new BigNumber(9_000));
            expect(start).to.be.bignumber.eq(new BigNumber(3));
            expect(end).to.be.bignumber.eq(blockNumber);

            // Test the `getProcessedWithdrawals`.
            await assertGetProcessedWithdrawalsAsync();
        });

        it('should fail to commit an entry when the withdrawal exhausts the allowance', async () => {
            const blockNumber = new BigNumber(6);
            const scaledWithdrawalAmount = new BigNumber(1_001);
            const tx = tester
                .commitWithdrawalPublic(
                    withdrawAddress,
                    strategyId,
                    tokenAddress,
                    frozenCollateralAmount,
                    scaledWithdrawalAmount,
                    blockNumber,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCollateral: Invalid withdrawal amount.');
        });

        it('should fail to commit an entry when number of withdrawals within a period exceeds max', async () => {
            const confirmedBlockNumber = new BigNumber(6);
            await tester.setLastConfirmedBlockNumber(confirmedBlockNumber).awaitTransactionSuccessAsync();
            lastConfirmedBlockNumber = confirmedBlockNumber;

            for (let i = 7; i < 17; i++) {
                // Commit the new withdrawal.
                const blockNumber = new BigNumber(i);
                const withdrawalAmount = new BigNumber(10);
                await tester
                    .commitWithdrawalPublic(
                        withdrawAddress,
                        strategyId,
                        tokenAddress,
                        frozenCollateralAmount,
                        withdrawalAmount,
                        blockNumber,
                    )
                    .awaitTransactionSuccessAsync();
            }

            const blockNumber = new BigNumber(17);
            const withdrawalAmount = new BigNumber(10);
            const tx = tester
                .commitWithdrawalPublic(
                    withdrawAddress,
                    strategyId,
                    tokenAddress,
                    frozenCollateralAmount,
                    withdrawalAmount,
                    blockNumber,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawals per period limit reached.');
        });
    });

    describe('#commitWithdrawalAllowance', () => {
        const minimumRateLimit = new BigNumber(2_500);
        const rateLimitPeriod = new BigNumber(50);

        describe('rate limit percentage = 100', () => {
            const rateLimitPercentage = new BigNumber(100);
            const tokenAddress = hexUtils.leftPad('0xdeadbeef', 20);

            before(async () => {
                await tester.setMinimumRateLimitForToken(tokenAddress, minimumRateLimit).awaitTransactionSuccessAsync();
                await tester
                    .setRateLimitParameters(rateLimitPeriod, rateLimitPercentage)
                    .awaitTransactionSuccessAsync();
                await tester.setWithdrawalAllowance(tokenAddress, new BigNumber(10_000)).awaitTransactionSuccessAsync();
            });

            it('should fail to commit more than the withdrawal allowance', async () => {
                const blockNumber = new BigNumber(1);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(10_001), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should successfully commit less than the withdrawal allowance', async () => {
                const blockNumber = new BigNumber(1);
                await tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(2_500), blockNumber)
                    .awaitTransactionSuccessAsync();
            });

            it('should fail to commit more than the remaining withdrawal allowance', async () => {
                const blockNumber = new BigNumber(1);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(7_501), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should successfully commit the remaining withdrawal allowance', async () => {
                const blockNumber = new BigNumber(1);
                await tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(7_500), blockNumber)
                    .awaitTransactionSuccessAsync();
            });

            it('should fail to commit any amount in the same block', async () => {
                const blockNumber = new BigNumber(1);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(1), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should fail to commit any amount after the rate limit period has ended', async () => {
                const blockNumber = new BigNumber(52);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(1), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });
        });

        describe('rate limit percentage = 16', () => {
            const rateLimitPercentage = new BigNumber(16);
            const tokenAddress = hexUtils.leftPad('0xdecaf0c0ffee', 20);

            async function rateLimitRemainingAsync(blockNumber: BigNumber): Promise<BigNumber> {
                const { rateLimitForPeriod, consumedInPeriod, rateLimitStartingBlock, minimumRateLimit } = await tester
                    .getRateLimitsForToken(tokenAddress)
                    .callAsync();
                if (
                    rateLimitStartingBlock.plus(rateLimitPeriod).gt(blockNumber) &&
                    !rateLimitForPeriod.eq(new BigNumber(0))
                ) {
                    return rateLimitForPeriod.minus(consumedInPeriod);
                } else {
                    return minimumRateLimit.plus(
                        (await tester.getWithdrawalAllowance(tokenAddress).callAsync())
                            .multipliedBy(rateLimitPercentage)
                            .dividedToIntegerBy(new BigNumber(100)),
                    );
                }
            }

            before(async () => {
                await tester.setMinimumRateLimitForToken(tokenAddress, minimumRateLimit).awaitTransactionSuccessAsync();
                await tester
                    .setRateLimitParameters(rateLimitPeriod, rateLimitPercentage)
                    .awaitTransactionSuccessAsync();
                await tester.setWithdrawalAllowance(tokenAddress, new BigNumber(10_000)).awaitTransactionSuccessAsync();
            });

            it('should fail to commit more than the withdrawal rate limit', async () => {
                const blockNumber = new BigNumber(1);
                const rateLimitRemaining = await rateLimitRemainingAsync(blockNumber);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, rateLimitRemaining.plus(1), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should successfully commit less than the withdrawal rate limit', async () => {
                const blockNumber = new BigNumber(1);
                const rateLimitRemaining = await rateLimitRemainingAsync(blockNumber);
                await tester
                    .commitWithdrawalAllowancePublic(
                        tokenAddress,
                        rateLimitRemaining.dividedToIntegerBy(2),
                        blockNumber,
                    )
                    .awaitTransactionSuccessAsync();
            });

            it('should fail to commit more than the remaining withdrawal rate limit', async () => {
                const blockNumber = new BigNumber(1);
                const rateLimitRemaining = await rateLimitRemainingAsync(blockNumber);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, rateLimitRemaining.plus(1), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should successfully commit the remaining withdrawal rate limit in a new block', async () => {
                const blockNumber = new BigNumber(10);
                const rateLimitRemaining = await rateLimitRemainingAsync(blockNumber);
                await tester
                    .commitWithdrawalAllowancePublic(tokenAddress, rateLimitRemaining, blockNumber)
                    .awaitTransactionSuccessAsync();
            });

            it('should fail to commit any more of the withdrawal rate limit in the same block', async () => {
                const blockNumber = new BigNumber(10);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(1), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should fail to commit any more of the withdrawal rate limit in the period', async () => {
                const blockNumber = new BigNumber(50);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(1), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should successfully commit the entire withdrawal rate limit in the new period', async () => {
                const blockNumber = new BigNumber(51);
                const rateLimitRemaining = await rateLimitRemainingAsync(blockNumber);
                await tester
                    .commitWithdrawalAllowancePublic(tokenAddress, rateLimitRemaining, blockNumber)
                    .awaitTransactionSuccessAsync();
            });

            it('should fail to commit any more of the withdrawal rate limit in the new period', async () => {
                const blockNumber = new BigNumber(100);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(1), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should fail to consume the remaining rate limit in the next period due to allowance issues', async () => {
                const blockNumber = new BigNumber(101);
                const rateLimitRemaining = await rateLimitRemainingAsync(blockNumber);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, rateLimitRemaining, blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });

            it('should successfully consume the remaining rate limit in the next period', async () => {
                const blockNumber = new BigNumber(101);
                const allowanceRemaining = await tester.getWithdrawalAllowance(tokenAddress).callAsync();
                expect(allowanceRemaining).to.be.bignumber.lt(await rateLimitRemainingAsync(blockNumber));
                await tester
                    .commitWithdrawalAllowancePublic(tokenAddress, allowanceRemaining, blockNumber)
                    .awaitTransactionSuccessAsync();
            });

            it('should fail to commit any more in the next period', async () => {
                const blockNumber = new BigNumber(151);
                const tx = tester
                    .commitWithdrawalAllowancePublic(tokenAddress, new BigNumber(1), blockNumber)
                    .awaitTransactionSuccessAsync();
                return expect(tx).to.be.rejectedWith('LibCollateral: Maximum withdrawal exceeded.');
            });
        });
    });

    describe('#scaleAmountForToken', () => {
        it('should fail when scaling down number with too many significant digits', async () => {
            const tx = tester
                .scaleAmountForTokenExternal(new BigNumber(100000000000), tokenWith18Decimals.address)
                .callAsync();
            return expect(tx).to.be.rejectedWith('LibCollateral: Unsupported precision level.');
        });

        it('should correctly scale when decimals are greater than 18', async () => {
            const scaledAmount = await tester
                .scaleAmountForTokenExternal(new BigNumber(1000000000000), tokenWith18Decimals.address)
                .callAsync();
            expect(scaledAmount).to.be.bignumber.eq(new BigNumber(1));
        });

        it('should correctly scale when decimals are equal to 6', async () => {
            const scaledAmount = await tester
                .scaleAmountForTokenExternal(new BigNumber(1), tokenWith6Decimals.address)
                .callAsync();
            expect(scaledAmount).to.be.bignumber.eq(new BigNumber(1));
        });

        it('should correctly scale when decimals are less 6', async () => {
            const scaledAmount = await tester
                .scaleAmountForTokenExternal(new BigNumber(1), tokenWith2Decimals.address)
                .callAsync();
            expect(scaledAmount).to.be.bignumber.eq(new BigNumber(10000));
        });
    });
});
