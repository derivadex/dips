import { BigNumber, hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import {
    CollateralExchangeCollateralAddedEventArgs,
    CollateralExchangeCollateralRemovedEventArgs,
    CollateralExchangeCollateralUpdatedEventArgs,
    CollateralMaxDepositedAddressesSetEventArgs,
    CollateralMinDepositSetEventArgs,
    CollateralRateLimitParametersSetEventArgs,
    CollateralStrategyUpdatedEventArgs,
    DDXContract,
    DDXDelegateVotesChangedEventArgs,
    DDXWalletCloneableContract,
    Derivadex,
    DummyTokenContract,
    ERC20ApprovalEventArgs,
    ERC20TransferEventArgs,
    SafeERC20WrapperContract,
    StakeDDXRateLimitSetEventArgs,
    StakeMaxDDXCapSetEventArgs,
    StakeTraderUpdatedEventArgs,
    TestCheckpointContract,
    TestRegistrationContract,
} from '@derivadex/contract-wrappers';
import {
    advanceBlocksAsync,
    advanceTimeAsync,
    generateCallData,
    generateStrategyId,
    generateStrategyKey,
    generateTraderGroupKey,
    getBlockNumberAsync,
    getSelectors,
    ItemKind,
} from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { FacetCutAction } from '@derivadex/types';
import { LogWithDecodedArgs, TransactionReceiptWithDecodedLogs } from 'ethereum-types';
import { ContractArtifact } from 'ethereum-types';
import * as hardhat from 'hardhat';
import _ from 'lodash';

import { setupWithProviderAsync } from '../../deployment/setup';
import { Fixtures, NULL_BYTES32, ZERO_ADDRESS } from '../fixtures';
import { TestCollateralContract, TestLibCollateralContract, TestStakeContract } from '../generated-wrappers';
import { filterEventLogs } from '../utils';

enum StrategyUpdateKind {
    Deposit,
    Withdrawal,
}

enum TraderUpdateKind {
    Deposit,
    Withdrawal,
}

interface Strategy {
    strategyId: string;
    freeCollateral: { tokens: string[]; amounts: BigNumber[] };
    frozenCollateral: { tokens: string[]; amounts: BigNumber[] };
    maxLeverage: BigNumber;
    frozen: boolean;
}

interface Trader {
    freeDDXBalance: BigNumber;
    frozenDDXBalance: BigNumber;
    referralAddress: string;
    payFeesInDDX: boolean;
}

const artifactAbis = _.mapValues(artifacts, (artifact: ContractArtifact) => artifact.compilerOutput.abi);

const MAX_SUPPLY = new BigNumber('100000000e18');

// const WORD_SIZE = 32;
const PUSH_LEAF_OPCODE = '0x4C';

describe('#Trader', () => {
    let web3Wrapper: Web3Wrapper;

    let derivadex: Derivadex;
    let collateral: TestCollateralContract;
    let testLibCollateral: TestLibCollateralContract;
    let stake: TestStakeContract;
    let registration: TestRegistrationContract;
    let checkpoint: TestCheckpointContract;
    let accounts: string[];
    let owner: string;
    let fixtures: Fixtures;
    let usdcContract: DummyTokenContract | SafeERC20WrapperContract;
    let usdtContract: DummyTokenContract | SafeERC20WrapperContract;

    const OPERATOR_DECIMALS = 6;

    async function scaleAmountForTokenAsync(
        token: DummyTokenContract | SafeERC20WrapperContract | DDXContract,
        amount: BigNumber,
    ): Promise<BigNumber> {
        let scaledAmount = amount;
        const decimals = await token.decimals().callAsync();
        if (decimals > OPERATOR_DECIMALS) {
            scaledAmount = scaledAmount.dividedToIntegerBy(
                new BigNumber(10).exponentiatedBy(decimals - OPERATOR_DECIMALS),
            );
        } else if (decimals < OPERATOR_DECIMALS) {
            scaledAmount = scaledAmount.multipliedBy(new BigNumber(10).exponentiatedBy(OPERATOR_DECIMALS - decimals));
        }
        return scaledAmount;
    }

    /**
     * TODO(jalextowle): This is now used in this file and './TestOperator.ts'.
     * It probably makes sense to make this a little bit more generic and add this
     * to the `DerivaDEX` contract wrapper.
     *
     * Passes a governance proposal that has already been made using the fixtures.
     * @param proposalNumber The index of the proposal to pass.
     * @param isFastPath Whether proposal qualifies for the fast path.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function passProposalAsync(
        proposalNumber: number,
        isFastPath = false,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        await advanceBlocksAsync(derivadex.providerEngine, 2);
        await derivadex
            .castVote(proposalNumber, true)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalNumber).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, isFastPath ? 1 : 259200);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        return derivadex.execute(proposalNumber).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
    }

    /**
     * Attempts to set the DDX rate limits.
     * @param minimumRateLimit The minimum rate limit that should be used.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function setDDXRateLimitsAsync(minimumRateLimit: BigNumber): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [stake.getFunctionSignature('setDDXRateLimits')];
        const calldatas = [generateCallData(stake.setDDXRateLimits(minimumRateLimit).getABIEncodedTransactionData())];
        const description = 'Set the minimum DDX rate limits.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Attempts to set the maximum DDX cap.
     * @param maxDDXCap The maximum amount of DDX that can be held in the SMT.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function setMaxDDXCapAsync(maxDDXCap: BigNumber): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [stake.getFunctionSignature('setMaxDDXCap')];
        const calldatas = [generateCallData(stake.setMaxDDXCap(maxDDXCap).getABIEncodedTransactionData())];
        const description = 'Set the maximum DDX cap.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Attempts to set the rate limit parameters.
     * @param rateLimitPeriod The rate limit period.
     * @param rateLimitPercentage The rate limit percentage.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function setRateLimitParametersAsync(
        rateLimitPeriod: BigNumber,
        rateLimitPercentage: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [collateral.getFunctionSignature('setRateLimitParameters')];
        const calldatas = [
            generateCallData(
                collateral.setRateLimitParameters(rateLimitPeriod, rateLimitPercentage).getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Set the rate limit parameters.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Attempts to set the maximum number of deposited addresses.
     * @param maxDepositedAddresses The maximum number of deposited addresses.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function setMaxDepositedAddressesAsync(
        maxDepositedAddresses: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [collateral.getFunctionSignature('setMaxDepositedAddresses')];
        const calldatas = [
            generateCallData(collateral.setMaxDepositedAddresses(maxDepositedAddresses).getABIEncodedTransactionData()),
        ];
        const description = 'Set the maximum deposited addresses.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        const proposal = await derivadex.getProposalAsync(proposalCount);
        expect(proposal.delay).to.be.bignumber.eq(1);
        return passProposalAsync(proposalCount, true);
    }

    /**
     * Attempts to set the minimum USDC deposit amount.
     * @param minDeposit The minimum USDC deposit amount.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function setMinDepositAsync(minDeposit: BigNumber): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [collateral.getFunctionSignature('setMinDeposit')];
        const calldatas = [generateCallData(collateral.setMinDeposit(minDeposit).getABIEncodedTransactionData())];
        const description = 'Set the minimum deposit amount.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        const proposal = await derivadex.getProposalAsync(proposalCount);
        expect(proposal.delay).to.be.bignumber.eq(1);
        return passProposalAsync(proposalCount, true);
    }

    /**
     * Attempts to add exchange collateral.
     * @param collateralAddress The address of the collateral.
     * @param minimumRateLimit The minimum rate limit that should be used.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function addExchangeCollateralAsync(
        collateralAddress: string,
        minimumRateLimit: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [collateral.getFunctionSignature('addExchangeCollateral')];
        const calldatas = [
            generateCallData(
                collateral.addExchangeCollateral(collateralAddress, minimumRateLimit).getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add exchange collateral.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Attempts to update exchange collateral.
     * @param collateralAddress The address of the collateral.
     * @param minimumRateLimit The minimum rate limit that should be used.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function updateExchangeCollateralAsync(
        collateralAddress: string,
        minimumRateLimit: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [collateral.getFunctionSignature('updateExchangeCollateral')];
        const calldatas = [
            generateCallData(
                collateral.updateExchangeCollateral(collateralAddress, minimumRateLimit).getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Update exchange collateral.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Attempts to remove exchange collateral.
     * @param collateralAddress The address of the collateral.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function removeExchangeCollateralAsync(
        collateralAddress: string,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [collateral.getFunctionSignature('removeExchangeCollateral')];
        const calldatas = [
            generateCallData(collateral.removeExchangeCollateral(collateralAddress).getABIEncodedTransactionData()),
        ];
        const description = 'Remove exchange collateral.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });

        ({ derivadex, accounts, owner } = await setupWithProviderAsync(provider, {
            isFork: false,
            isGanache: true,
            isLocalSnapshot: false,
            isTest: true,
            useDeployedDDXToken: false,
        }));
        expect(derivadex.usdcContract).to.not.be.undefined;
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        usdcContract = derivadex.usdcContract!;

        expect(derivadex.usdtContract).to.not.be.undefined;
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        usdtContract = derivadex.usdtContract!;

        fixtures = new Fixtures(derivadex, accounts, owner);
        testLibCollateral = await TestLibCollateralContract.deployFrom0xArtifactAsync(
            artifacts.TestLibCollateral,
            provider,
            { from: fixtures.derivaDEXWallet() },
            artifacts,
        );

        await derivadex
            .transferOwnershipToDerivaDEXProxyDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        // Add Pause facet to the diamond
        const pauseSelectors = getSelectors(derivadex.pauseContract);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.pauseContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: pauseSelectors,
                    },
                ],
                derivadex.pauseContract.address,
                derivadex.initializePause().getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setPauseAddressToProxy();

        const governanceSelectors = getSelectors(derivadex.governanceContract, ['initialize']);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.governanceContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: governanceSelectors,
                    },
                ],
                derivadex.governanceContract.address,
                derivadex.initializeGovernance(10, 1, 17280, 1209600, 259200, 4, 1, 50).getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setGovernanceAddressToProxy();

        await derivadex.transferOwnershipToSelf().awaitTransactionSuccessAsync({ from: owner });

        // Added the Banner facet to the DerivaDEX proxy.
        const bannerSelectors = getSelectors(derivadex.bannerContract, ['initialize']);
        const bannerTargets = [derivadex.derivaDEXContract.address];
        const bannerValues = [0];
        const bannerSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const bannerCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.bannerContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: bannerSelectors,
                            },
                        ],
                        derivadex.bannerContract.address,
                        derivadex.bannerContract
                            .initialize([accounts[accounts.length - 1]])
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const bannerDescription = 'Add Banner contract as a facet.';
        await derivadex
            .propose(bannerTargets, bannerValues, bannerSignatures, bannerCalldatas, bannerDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        let proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setBannerAddressToProxy();

        // Added the Custodian facet to the DerivaDEX proxy.
        const custodianSelectors = getSelectors(derivadex.custodianContract, ['initialize']);
        const custodianTargets = [derivadex.derivaDEXContract.address];
        const custodianValues = [0];
        const custodianSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const custodianCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.custodianContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: custodianSelectors,
                            },
                        ],
                        derivadex.custodianContract.address,
                        derivadex.custodianContract
                            .initialize(new BigNumber(2), new BigNumber(200))
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const custodianDescription = 'Add Custodian contract as a facet.';
        await derivadex
            .propose(custodianTargets, custodianValues, custodianSignatures, custodianCalldatas, custodianDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);

        // Added the Registration facet to the DerivaDEX proxy.
        registration = await TestRegistrationContract.deployFrom0xArtifactAsync(
            artifacts.TestRegistration,
            derivadex.providerEngine,
            {
                from: fixtures.derivaDEXWallet(),
            },
            artifacts,
        );
        const registrationSelectors = getSelectors(registration, ['initialize']);
        const registrationTargets = [derivadex.derivaDEXContract.address];
        const registrationValues = [0];
        const registrationSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const registrationCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: registration.address,
                                action: FacetCutAction.Add,
                                functionSelectors: registrationSelectors,
                            },
                        ],
                        registration.address,
                        registration
                            .initialize({ mrEnclave: '0xdeadbeef', isvSvn: '0x0000' }, [])
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const registrationDescription = 'Add Registration contract as a facet.';
        await derivadex
            .propose(
                registrationTargets,
                registrationValues,
                registrationSignatures,
                registrationCalldatas,
                registrationDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        registration = new TestRegistrationContract(derivadex.derivaDEXContract.address, derivadex.providerEngine, {
            from: fixtures.derivaDEXWallet(),
        });

        // Added the Checkpoint facet to the DerivaDEX proxy.
        checkpoint = await TestCheckpointContract.deployFrom0xArtifactAsync(
            artifacts.TestCheckpoint,
            derivadex.providerEngine,
            {
                from: fixtures.derivaDEXWallet(),
            },
            artifacts,
        );
        const checkpointSelectors = getSelectors(checkpoint, ['initialize']);
        const checkpointTargets = [derivadex.derivaDEXContract.address];
        const checkpointValues = [0];
        const checkpointSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const checkpointCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: checkpoint.address,
                                action: FacetCutAction.Add,
                                functionSelectors: checkpointSelectors,
                            },
                        ],
                        checkpoint.address,
                        checkpoint.initialize(new BigNumber(51), new BigNumber(2)).getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const checkpointDescription = 'Add Checkpoint contract as a facet.';
        await derivadex
            .propose(
                checkpointTargets,
                checkpointValues,
                checkpointSignatures,
                checkpointCalldatas,
                checkpointDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        checkpoint = new TestCheckpointContract(derivadex.derivaDEXContract.address, derivadex.providerEngine, {
            from: fixtures.derivaDEXWallet(),
        });
        await checkpoint.setEpochId(new BigNumber(1)).awaitTransactionSuccessAsync();

        await derivadex
            .transferDDX(fixtures.traderB(), 10000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderC(), 5000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderD(), 25000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        await derivadex.ddxContract
            .delegate(fixtures.traderE())
            .awaitTransactionSuccessAsync({ from: fixtures.traderD() });
        await advanceBlocksAsync(derivadex.providerEngine, 1);
    });

    it('adds Collateral facet', async () => {
        collateral = new TestCollateralContract(
            derivadex.derivaDEXContract.address,
            derivadex.providerEngine,
            { from: fixtures.derivaDEXWallet() },
            artifactAbis,
        );
        const selectors = getSelectors(collateral, ['initialize']);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.collateralContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.collateralContract.address,
                        derivadex.collateralContract
                            .initialize(
                                new BigNumber(50),
                                new BigNumber(10),
                                new BigNumber(1_000),
                                Web3Wrapper.toBaseUnitAmount(1_000, 6),
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add Collateral contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setCollateralAddressToProxy();
    });

    it('adds Stake facet', async () => {
        stake = new TestStakeContract(
            derivadex.derivaDEXContract.address,
            derivadex.providerEngine,
            { from: fixtures.derivaDEXWallet() },
            artifactAbis,
        );
        const selectors = getSelectors(stake, ['initialize']);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.stakeContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.stakeContract.address,
                        derivadex.stakeContract
                            .initialize(
                                Web3Wrapper.toBaseUnitAmount(10_000, 18),
                                Web3Wrapper.toBaseUnitAmount(40_000_000, 18),
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add Stake contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setStakeAddressToProxy();
    });

    it('adds Trader facet', async () => {
        const selectors = getSelectors(derivadex.traderContract, ['initialize']);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.traderContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.traderContract.address,
                        derivadex
                            .initializeTrader(derivadex.ddxWalletCloneableContract.address)
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add Trader contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setFacetAddressesToProxy();
    });

    it('stakes DDX properly', async () => {
        let traderBAttributes = await derivadex.getTraderAsync(fixtures.traderB());
        let traderCAttributes = await derivadex.getTraderAsync(fixtures.traderC());
        let traderDAttributes = await derivadex.getTraderAsync(fixtures.traderD());
        expect(traderBAttributes.ddxBalance).to.be.bignumber.eq(0);
        expect(traderBAttributes.ddxWalletContract).to.eq(ZERO_ADDRESS);
        expect(traderCAttributes.ddxBalance).to.be.bignumber.eq(0);
        expect(traderCAttributes.ddxWalletContract).to.eq(ZERO_ADDRESS);
        expect(traderDAttributes.ddxBalance).to.be.bignumber.eq(0);
        expect(traderDAttributes.ddxWalletContract).to.eq(ZERO_ADDRESS);

        let derivaDEXProxyBalance = await derivadex.getBalanceOfDDXAsync(derivadex.derivaDEXContract.address);
        const derivaDEXWalletBalance = await derivadex.getBalanceOfDDXAsync(fixtures.derivaDEXWallet());
        let traderBBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderB());
        let traderCBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderC());
        let traderDBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderD());
        let traderEBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderE());
        expect(derivaDEXProxyBalance).to.be.bignumber.eq(0);
        expect(derivaDEXWalletBalance).to.be.bignumber.eq(49960000);
        expect(traderBBalance).to.be.bignumber.eq(10000);
        expect(traderCBalance).to.be.bignumber.eq(5000);
        expect(traderDBalance).to.be.bignumber.eq(25000);
        expect(traderEBalance).to.be.bignumber.eq(0);

        let blockNumber = (await getBlockNumberAsync(derivadex.providerEngine)) - 1;
        let derivaDEXWalletPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.derivaDEXWallet(), blockNumber);
        let traderBPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderB(), blockNumber);
        let traderCPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderC(), blockNumber);
        let traderDPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderD(), blockNumber);
        let traderEPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderE(), blockNumber);
        expect(derivaDEXWalletPriorVotes).to.be.bignumber.eq(49960000);
        expect(traderBPriorVotes).to.be.bignumber.eq(10000);
        expect(traderCPriorVotes).to.be.bignumber.eq(5000);
        expect(traderDPriorVotes).to.be.bignumber.eq(0);
        expect(traderEPriorVotes).to.be.bignumber.eq(25000);

        // Ensure that a banned address can't stakeDDXFromTrader or sendDDXFromTraderToTraderWallet.
        const banned = accounts[accounts.length - 1];
        let tx = derivadex.stakeDDXFromTrader(2000).awaitTransactionSuccessAsync({ from: banned });
        await expect(tx).to.be.rejectedWith("Trader: Can't transfer DDX to wallet when banned.");
        tx = derivadex
            .sendDDXFromTraderToTraderWallet(fixtures.traderB(), 100)
            .awaitTransactionSuccessAsync({ from: banned });
        await expect(tx).to.be.rejectedWith("Trader: Can't transfer DDX to wallet when banned.");

        await derivadex
            .approveUnlimitedDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await derivadex.stakeDDXFromTrader(2000).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await derivadex
            .approveUnlimitedDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.traderC() });
        await derivadex
            .sendDDXFromTraderToTraderWallet(fixtures.traderB(), 100)
            .awaitTransactionSuccessAsync({ from: fixtures.traderC() });
        await derivadex.stakeDDXFromTrader(3000).awaitTransactionSuccessAsync({ from: fixtures.traderC() });
        await derivadex
            .approveUnlimitedDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.traderD() });
        await derivadex.stakeDDXFromTrader(10000).awaitTransactionSuccessAsync({ from: fixtures.traderD() });
        await advanceBlocksAsync(derivadex.providerEngine, 1);

        traderBAttributes = await derivadex.getTraderAsync(fixtures.traderB());
        traderCAttributes = await derivadex.getTraderAsync(fixtures.traderC());
        traderDAttributes = await derivadex.getTraderAsync(fixtures.traderD());
        expect(traderBAttributes.ddxBalance).to.be.bignumber.eq(2100);
        expect(traderBAttributes.ddxWalletContract).to.not.eq(ZERO_ADDRESS);
        expect(traderCAttributes.ddxBalance).to.be.bignumber.eq(3000);
        expect(traderCAttributes.ddxWalletContract).to.not.eq(ZERO_ADDRESS);
        expect(traderDAttributes.ddxBalance).to.be.bignumber.eq(10000);
        expect(traderDAttributes.ddxWalletContract).to.not.eq(ZERO_ADDRESS);

        derivaDEXProxyBalance = await derivadex.getBalanceOfDDXAsync(derivadex.derivaDEXContract.address);
        traderBBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderB());
        traderCBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderC());
        traderDBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderD());
        traderEBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderE());
        const ddxWalletBBalance = await derivadex.getBalanceOfDDXAsync(traderBAttributes.ddxWalletContract);
        const ddxWalletCBalance = await derivadex.getBalanceOfDDXAsync(traderCAttributes.ddxWalletContract);
        const ddxWalletDDBalance = await derivadex.getBalanceOfDDXAsync(traderDAttributes.ddxWalletContract);
        expect(derivaDEXProxyBalance).to.be.bignumber.eq(0);
        expect(traderBBalance).to.be.bignumber.eq(8000);
        expect(traderCBalance).to.be.bignumber.eq(1900);
        expect(traderDBalance).to.be.bignumber.eq(15000);
        expect(traderEBalance).to.be.bignumber.eq(0);
        expect(ddxWalletBBalance).to.be.bignumber.eq(2100);
        expect(ddxWalletCBalance).to.be.bignumber.eq(3000);
        expect(ddxWalletDDBalance).to.be.bignumber.eq(10000);

        blockNumber = (await getBlockNumberAsync(derivadex.providerEngine)) - 1;
        derivaDEXWalletPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.derivaDEXWallet(), blockNumber);
        traderBPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderB(), blockNumber);
        traderCPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderC(), blockNumber);
        traderDPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderD(), blockNumber);
        traderEPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderE(), blockNumber);
        const ddxWalletBPriorVotes = await derivadex.getPriorVotesDDXAsync(
            traderBAttributes.ddxWalletContract,
            blockNumber,
        );
        const ddxWalletCPriorVotes = await derivadex.getPriorVotesDDXAsync(
            traderCAttributes.ddxWalletContract,
            blockNumber,
        );
        const ddxWalletDPriorVotes = await derivadex.getPriorVotesDDXAsync(
            traderDAttributes.ddxWalletContract,
            blockNumber,
        );
        expect(derivaDEXWalletPriorVotes).to.be.bignumber.eq(49960000);
        expect(traderBPriorVotes).to.be.bignumber.eq(10100);
        expect(traderCPriorVotes).to.be.bignumber.eq(4900);
        expect(traderDPriorVotes).to.be.bignumber.eq(10000);
        expect(traderEPriorVotes).to.be.bignumber.eq(15000);
        expect(ddxWalletBPriorVotes).to.be.bignumber.eq(0);
        expect(ddxWalletCPriorVotes).to.be.bignumber.eq(0);
        expect(ddxWalletDPriorVotes).to.be.bignumber.eq(0);
    });

    it('fails to maliciously reinitialize onchain DDX wallet', async () => {
        const traderBAttributes = await derivadex.getTraderAsync(fixtures.traderB());
        const ddxWalletCloneableContract = new DDXWalletCloneableContract(
            traderBAttributes.ddxWalletContract,
            derivadex.providerEngine,
        );
        await expect(
            ddxWalletCloneableContract
                .initialize(fixtures.traderF(), derivadex.ddxContract.address, fixtures.traderF())
                .awaitTransactionSuccessAsync({ from: fixtures.traderF() }),
        ).to.be.rejectedWith('DDXWalletCloneable: already init.');
    });

    it('lifts governance cliff', async () => {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.traderContract.getFunctionSignature('setRewardCliff')];
        const calldatas = [generateCallData(derivadex.setRewardCliff(true).getABIEncodedTransactionData())];
        const description = 'Lifting reward cliff.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
    });

    it('withdraws DDX properly', async () => {
        await derivadex.withdrawDDXToTrader(1000).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await derivadex.withdrawDDXToTrader(2000).awaitTransactionSuccessAsync({ from: fixtures.traderC() });
        await derivadex.withdrawDDXToTrader(5000).awaitTransactionSuccessAsync({ from: fixtures.traderD() });
        await advanceBlocksAsync(derivadex.providerEngine, 1);

        const traderBAttributes = await derivadex.getTraderAsync(fixtures.traderB());
        const traderCAttributes = await derivadex.getTraderAsync(fixtures.traderC());
        const traderDAttributes = await derivadex.getTraderAsync(fixtures.traderD());
        expect(traderBAttributes.ddxBalance).to.be.bignumber.eq(1100);
        expect(traderBAttributes.ddxWalletContract).to.not.eq(ZERO_ADDRESS);
        expect(traderCAttributes.ddxBalance).to.be.bignumber.eq(1000);
        expect(traderCAttributes.ddxWalletContract).to.not.eq(ZERO_ADDRESS);
        expect(traderDAttributes.ddxBalance).to.be.bignumber.eq(5000);
        expect(traderDAttributes.ddxWalletContract).to.not.eq(ZERO_ADDRESS);

        const derivaDEXProxyBalance = await derivadex.getBalanceOfDDXAsync(derivadex.derivaDEXContract.address);
        const traderBBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderB());
        const traderCBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderC());
        const traderDBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderD());
        const traderEBalance = await derivadex.getBalanceOfDDXAsync(fixtures.traderE());
        const ddxWalletBBalance = await derivadex.getBalanceOfDDXAsync(traderBAttributes.ddxWalletContract);
        const ddxWalletCBalance = await derivadex.getBalanceOfDDXAsync(traderCAttributes.ddxWalletContract);
        const ddxWalletDDBalance = await derivadex.getBalanceOfDDXAsync(traderDAttributes.ddxWalletContract);
        expect(derivaDEXProxyBalance).to.be.bignumber.eq(0);
        expect(traderBBalance).to.be.bignumber.eq(9000);
        expect(traderCBalance).to.be.bignumber.eq(3900);
        expect(traderDBalance).to.be.bignumber.eq(20000);
        expect(traderEBalance).to.be.bignumber.eq(0);
        expect(ddxWalletBBalance).to.be.bignumber.eq(1100);
        expect(ddxWalletCBalance).to.be.bignumber.eq(1000);
        expect(ddxWalletDDBalance).to.be.bignumber.eq(5000);

        const blockNumber = (await getBlockNumberAsync(derivadex.providerEngine)) - 1;
        const derivaDEXWalletPriorVotes = await derivadex.getPriorVotesDDXAsync(
            fixtures.derivaDEXWallet(),
            blockNumber,
        );
        const traderBPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderB(), blockNumber);
        const traderCPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderC(), blockNumber);
        const traderDPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderD(), blockNumber);
        const traderEPriorVotes = await derivadex.getPriorVotesDDXAsync(fixtures.traderE(), blockNumber);
        const ddxWalletBPriorVotes = await derivadex.getPriorVotesDDXAsync(
            traderBAttributes.ddxWalletContract,
            blockNumber,
        );
        const ddxWalletCPriorVotes = await derivadex.getPriorVotesDDXAsync(
            traderCAttributes.ddxWalletContract,
            blockNumber,
        );
        const ddxWalletDPriorVotes = await derivadex.getPriorVotesDDXAsync(
            traderDAttributes.ddxWalletContract,
            blockNumber,
        );
        expect(derivaDEXWalletPriorVotes).to.be.bignumber.eq(49960000);
        expect(traderBPriorVotes).to.be.bignumber.eq(10100);
        expect(traderCPriorVotes).to.be.bignumber.eq(4900);
        expect(traderDPriorVotes).to.be.bignumber.eq(5000);
        expect(traderEPriorVotes).to.be.bignumber.eq(20000);
        expect(ddxWalletBPriorVotes).to.be.bignumber.eq(0);
        expect(ddxWalletCPriorVotes).to.be.bignumber.eq(0);
        expect(ddxWalletDPriorVotes).to.be.bignumber.eq(0);
    });

    describe('#setDDXRateLimits', () => {
        it('should fail when called directly', async () => {
            const tx = stake.setDDXRateLimits(new BigNumber(1)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Stake: must be called by Gov.');
        });

        it('should fail when the minimum rate limit is zero', async () => {
            const tx = setDDXRateLimitsAsync(new BigNumber(0));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully update the minimum rate limit to 100,000', async () => {
            const minimumRateLimit = Web3Wrapper.toBaseUnitAmount(100_000, 12);
            const { logs } = await setDDXRateLimitsAsync(minimumRateLimit);
            // Ensure the logs are correct.
            const setDDXRateLimitsLogs = filterEventLogs(logs, 'DDXRateLimitSet');
            expect(setDDXRateLimitsLogs.length).to.be.eq(1);
            const setDDXRateLimitsLog = setDDXRateLimitsLogs[0] as LogWithDecodedArgs<StakeDDXRateLimitSetEventArgs>;
            expect(setDDXRateLimitsLog.args).to.be.deep.eq({
                minimumRateLimit,
            });
        });
    });

    describe('#setMaxDDXCap', () => {
        it('should fail when called directly', async () => {
            const tx = stake.setMaxDDXCap(new BigNumber(1)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Stake: must be called by Gov.');
        });

        it('should fail if the cap is greater than the maximum DDX supply', async () => {
            const tx = setMaxDDXCapAsync(MAX_SUPPLY.plus(1));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully set the max DDX cap to the max DDX supply', async () => {
            const { logs } = await setMaxDDXCapAsync(MAX_SUPPLY);
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<StakeMaxDDXCapSetEventArgs>;
            expect(log.event).to.be.eq('MaxDDXCapSet');
            expect(log.args).to.be.deep.eq({
                oldMaxCap: new BigNumber('40000000e18'),
                newMaxCap: MAX_SUPPLY,
            });
            const maxDDXCap = await stake.getMaxDDXCap().callAsync();
            expect(maxDDXCap).to.be.bignumber.eq(MAX_SUPPLY);
        });

        it('should successfully set the max DDX cap to a value less than the max supply', async () => {
            const cap = MAX_SUPPLY.dividedToIntegerBy(2);
            const { logs } = await setMaxDDXCapAsync(cap);
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<StakeMaxDDXCapSetEventArgs>;
            expect(log.event).to.be.eq('MaxDDXCapSet');
            expect(log.args).to.be.deep.eq({
                oldMaxCap: MAX_SUPPLY,
                newMaxCap: cap,
            });
            const maxDDXCap = await stake.getMaxDDXCap().callAsync();
            expect(maxDDXCap).to.be.bignumber.eq(cap);
        });
    });

    describe('#setMaxDepositedAddresses', () => {
        let snapshotId: string;

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('should fail when called directly', async () => {
            const tx = collateral.setMaxDepositedAddresses(new BigNumber(2_000)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Collateral: must be called by Gov.');
        });

        it('should fail if the max deposited addresses is same as current', async () => {
            const tx = setMaxDepositedAddressesAsync(new BigNumber(1_000));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully set the max deposited addresses', async () => {
            const { logs } = await setMaxDepositedAddressesAsync(new BigNumber(2_000));
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<CollateralMaxDepositedAddressesSetEventArgs>;
            expect(log.event).to.be.eq('MaxDepositedAddressesSet');
            expect(log.args).to.be.deep.eq({
                maxDepositedAddresses: new BigNumber(2_000),
            });

            const [, maxDepositedAddresses] = await collateral.getGuardedDepositInfo().callAsync();
            expect(maxDepositedAddresses).to.be.bignumber.eq(2_000);
        });
    });

    describe('#setMinDeposit', () => {
        let snapshotId: string;

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('should fail when called directly', async () => {
            const tx = collateral.setMinDeposit(Web3Wrapper.toBaseUnitAmount(2_000, 6)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Collateral: must be called by Gov.');
        });

        it('should fail if the min deposit is zero', async () => {
            const tx = setMinDepositAsync(Web3Wrapper.toBaseUnitAmount(0, 6));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should fail if the min deposit is same as current', async () => {
            const tx = setMinDepositAsync(Web3Wrapper.toBaseUnitAmount(1_000, 6));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully set the min deposit', async () => {
            const { logs } = await setMinDepositAsync(Web3Wrapper.toBaseUnitAmount(500, 6));
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<CollateralMinDepositSetEventArgs>;
            expect(log.event).to.be.eq('MinDepositSet');
            expect(log.args).to.be.deep.eq({
                minDeposit: Web3Wrapper.toBaseUnitAmount(500, 6),
            });

            const [, , minDeposit] = await collateral.getGuardedDepositInfo().callAsync();
            expect(minDeposit).to.be.bignumber.eq(Web3Wrapper.toBaseUnitAmount(500, 6));
        });
    });

    describe('#addExchangeCollateral', () => {
        before(async () => {
            await collateral.setExchangeCollateralState(usdtContract.address).awaitTransactionSuccessAsync();
        });

        it('should fail when called directly', async () => {
            const tx = collateral
                .addExchangeCollateral(usdtContract.address, new BigNumber(0))
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Collateral: must be called by Gov.');
        });

        it('should fail when the collateral token is zero', async () => {
            const tx = addExchangeCollateralAsync(ZERO_ADDRESS, new BigNumber(0));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should fail when the collateral has already been added', async () => {
            const tx = addExchangeCollateralAsync(usdtContract.address, new BigNumber(0));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should fail when the minimum rate limit is zero', async () => {
            const tx = addExchangeCollateralAsync(usdtContract.address, new BigNumber(0));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully add the collateral', async () => {
            const minimumRateLimit = new BigNumber(Web3Wrapper.toBaseUnitAmount(10_000, 6));
            const { logs } = await addExchangeCollateralAsync(usdcContract.address, minimumRateLimit);
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<CollateralExchangeCollateralAddedEventArgs>;
            expect(log.event).to.be.eq('ExchangeCollateralAdded');
            expect(log.args).to.be.deep.eq({
                collateralToken: usdcContract.address,
                underlyingToken: ZERO_ADDRESS,
                flavor: 0,
                minimumRateLimit,
            });
            const collateralInfo = await collateral.getExchangeCollateralInfo(usdcContract.address).callAsync();
            expect(collateralInfo).to.be.deep.eq({
                underlyingToken: ZERO_ADDRESS,
                flavor: 0,
                isListed: true,
            });
        });
    });

    describe('#updateExchangeCollateral', () => {
        it('should fail to call directly', async () => {
            const tx = collateral
                .updateExchangeCollateral(usdtContract.address, new BigNumber(0))
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Collateral: must be called by Gov.');
        });

        it("should fail when the collateral address hasn't been added", async () => {
            const tx = updateExchangeCollateralAsync(hexUtils.leftPad('0x1', 20), new BigNumber(0));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should fail when the minimum rate limit is zero', async () => {
            const tx = updateExchangeCollateralAsync(usdcContract.address, new BigNumber(0));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully update exchange collateral', async () => {
            const minimumRateLimit = new BigNumber(Web3Wrapper.toBaseUnitAmount(100_000, 6));
            const { logs } = await updateExchangeCollateralAsync(usdcContract.address, minimumRateLimit);
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<CollateralExchangeCollateralUpdatedEventArgs>;
            expect(log.event).to.be.eq('ExchangeCollateralUpdated');
            expect(log.args).to.be.deep.eq({
                collateralToken: usdcContract.address,
                minimumRateLimit,
            });
            const collateralInfo = await collateral.getExchangeCollateralInfo(usdcContract.address).callAsync();
            expect(collateralInfo).to.be.deep.eq({
                underlyingToken: ZERO_ADDRESS,
                flavor: 0,
                isListed: true,
            });
        });
    });

    describe('#removeExchangeCollateral', () => {
        before(async () => {
            await collateral.setExchangeCollateralState(usdtContract.address).awaitTransactionSuccessAsync();
        });

        it('should fail when called directly', async () => {
            const tx = collateral.removeExchangeCollateral(usdtContract.address).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Collateral: must be called by Gov.');
        });

        it("should fail when the collateral hasn't been registered", async () => {
            const tx = removeExchangeCollateralAsync(accounts[0]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully remove the collateral', async () => {
            const { logs } = await removeExchangeCollateralAsync(usdcContract.address);
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<CollateralExchangeCollateralRemovedEventArgs>;
            expect(log.event).to.be.eq('ExchangeCollateralRemoved');
            expect(log.args).to.be.deep.eq({
                collateralToken: usdcContract.address,
            });
            const collateralInfo = await collateral.getExchangeCollateralInfo(usdcContract.address).callAsync();
            expect(collateralInfo).to.be.deep.eq({
                underlyingToken: ZERO_ADDRESS,
                flavor: 0,
                isListed: false,
            });
        });
    });

    describe('#setRateLimitParameters', () => {
        it('should fail to call directly', async () => {
            const rateLimitPeriod = new BigNumber(50);
            const rateLimitPercentage = new BigNumber(1);
            const tx = collateral
                .setRateLimitParameters(rateLimitPeriod, rateLimitPercentage)
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Collateral: must be called by Gov.');
        });

        it('should fail to set the rate limit period to 0', async () => {
            const rateLimitPeriod = new BigNumber(0);
            const rateLimitPercentage = new BigNumber(1);
            const tx = collateral
                .setRateLimitParameters(rateLimitPeriod, rateLimitPercentage)
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Collateral: must be called by Gov.');
        });

        it('should fail to set the rate limit percentage to 101%', async () => {
            const rateLimitPeriod = new BigNumber(50);
            const rateLimitPercentage = new BigNumber(101);
            const tx = setRateLimitParametersAsync(rateLimitPeriod, rateLimitPercentage);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully set the rate limit percentage to 1%', async () => {
            const rateLimitPeriod = new BigNumber(50);
            const rateLimitPercentage = new BigNumber(1);
            const { logs } = await setRateLimitParametersAsync(rateLimitPeriod, rateLimitPercentage);

            // Ensure the logs are correct.
            const rateLimitParametersSetLogs = filterEventLogs(logs, 'RateLimitParametersSet');
            expect(rateLimitParametersSetLogs.length).to.be.eq(1);
            const rateLimitParametersSetLog =
                rateLimitParametersSetLogs[0] as LogWithDecodedArgs<CollateralRateLimitParametersSetEventArgs>;
            expect(rateLimitParametersSetLog.event).to.be.eq('RateLimitParametersSet');
            expect(rateLimitParametersSetLog.args).to.be.deep.eq({
                rateLimitPeriod,
                rateLimitPercentage,
            });

            // Ensure the state was updated correctly.
            const [updatedRateLimitPeriod, updatedRateLimitPercentage] = await collateral
                .getRateLimitParameters()
                .callAsync();
            expect(updatedRateLimitPeriod).to.be.bignumber.eq(rateLimitPeriod);
            expect(updatedRateLimitPercentage).to.be.bignumber.eq(rateLimitPercentage);
        });

        it('should successfully set the rate limit percentage to 100%', async () => {
            const rateLimitPeriod = new BigNumber(50);
            const rateLimitPercentage = new BigNumber(100);
            const { logs } = await setRateLimitParametersAsync(rateLimitPeriod, rateLimitPercentage);

            // Ensure the logs are correct.
            const rateLimitParametersSetLogs = filterEventLogs(logs, 'RateLimitParametersSet');
            expect(rateLimitParametersSetLogs.length).to.be.eq(1);
            const rateLimitParametersSetLog =
                rateLimitParametersSetLogs[0] as LogWithDecodedArgs<CollateralRateLimitParametersSetEventArgs>;
            expect(rateLimitParametersSetLog.event).to.be.eq('RateLimitParametersSet');
            expect(rateLimitParametersSetLog.args).to.be.deep.eq({
                rateLimitPeriod,
                rateLimitPercentage,
            });

            // Ensure the state was updated correctly.
            const [updatedRateLimitPeriod, updatedRateLimitPercentage] = await collateral
                .getRateLimitParameters()
                .callAsync();
            expect(updatedRateLimitPeriod).to.be.bignumber.eq(rateLimitPeriod);
            expect(updatedRateLimitPercentage).to.be.bignumber.eq(rateLimitPercentage);
        });
    });

    // TODO(jalextowle): Add test cases for DDX collateral.
    describe('#deposit', () => {
        it('should fail if the sender is banned', async () => {
            const amount = Web3Wrapper.toBaseUnitAmount(1_000, 6);
            const sender = accounts[accounts.length - 1];
            const tx = collateral
                .deposit(usdtContract.address, NULL_BYTES32, amount)
                .awaitTransactionSuccessAsync({ from: sender });
            return expect(tx).to.be.rejectedWith('Collateral: cannot deposit if banned.');
        });

        it('should fail if `amount` less than the minimum', async () => {
            const amount = Web3Wrapper.toBaseUnitAmount(999, 6);
            const tx = collateral
                .deposit(usdtContract.address, NULL_BYTES32, amount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('CollateralGuarded: Must deposit minimum amount');
        });

        it('should fail if the collateral has not been registered yet', async () => {
            const amount = Web3Wrapper.toBaseUnitAmount(1_000, 6);
            const tx = collateral
                .deposit(usdcContract.address, NULL_BYTES32, amount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Collateral: cannot deposit unlisted collateral.');
        });

        it('should fail if msg.sender has not approved the DerivaDEX address', async () => {
            const amount = Web3Wrapper.toBaseUnitAmount(1_000, 6);
            const tx = collateral
                .deposit(usdtContract.address, NULL_BYTES32, amount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('ERC20: insufficient allowance');
        });

        it('should successfully deposit tokens to DerivaDEX contract', async () => {
            let hasDeposited = await collateral.getAddressesHaveDeposited([fixtures.derivaDEXWallet()]).callAsync();
            expect(hasDeposited).to.be.deep.eq([false]);

            const amount = Web3Wrapper.toBaseUnitAmount(1_000, 6);
            const balanceBefore = await usdtContract.balanceOf(derivadex.derivaDEXContract.address).callAsync();
            await usdtContract
                .approve(derivadex.derivaDEXContract.address, amount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const { logs } = await collateral
                .deposit(usdtContract.address, NULL_BYTES32, amount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const balanceAfter = await usdtContract.balanceOf(derivadex.derivaDEXContract.address).callAsync();

            // Verify the event logs.
            expect(logs.length).to.be.eq(3);
            const approveLog = logs[0] as LogWithDecodedArgs<ERC20ApprovalEventArgs>;
            expect(approveLog.event).to.be.eq('Approval');
            expect(approveLog.args).to.be.deep.eq({
                owner: fixtures.derivaDEXWallet(),
                spender: derivadex.derivaDEXContract.address,
                value: new BigNumber(0),
            });
            const transferLog = logs[1] as LogWithDecodedArgs<ERC20TransferEventArgs>;
            expect(transferLog.event).to.be.eq('Transfer');
            expect(transferLog.args).to.be.deep.eq({
                from: fixtures.derivaDEXWallet(),
                to: derivadex.derivaDEXContract.address,
                value: amount,
            });
            const strategyUpdatedLog = logs[2] as LogWithDecodedArgs<CollateralStrategyUpdatedEventArgs>;
            expect(strategyUpdatedLog.event).to.be.eq('StrategyUpdated');
            expect(strategyUpdatedLog.args).to.be.deep.eq({
                trader: fixtures.derivaDEXWallet(),
                collateralAddress: usdtContract.address,
                strategyId: NULL_BYTES32,
                amount: amount,
                updateKind: StrategyUpdateKind.Deposit,
            });

            // Ensure that the balance of the contract was increased.
            expect(balanceAfter).to.be.bignumber.eq(balanceBefore.plus(amount));

            const [numDepositedAddresses, ,] = await collateral.getGuardedDepositInfo().callAsync();
            expect(numDepositedAddresses).to.be.bignumber.eq(1);

            hasDeposited = await collateral.getAddressesHaveDeposited([fixtures.derivaDEXWallet()]).callAsync();
            expect(hasDeposited).to.be.deep.eq([true]);
        });

        it('should fail to deposit if max deposited addresses has been reached', async () => {
            const depositAmount = Web3Wrapper.toBaseUnitAmount(1_000, 6);

            // Temporarily set max deposited addresses to 1.
            await setMaxDepositedAddressesAsync(new BigNumber(1));
            await usdtContract
                .approve(derivadex.derivaDEXContract.address, depositAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            const tx = collateral
                .deposit(usdtContract.address, NULL_BYTES32, depositAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            await expect(tx).to.be.rejectedWith('CollateralGuarded: Max num of deposited addresses reached.');

            // Clean up the max deposited addresses.
            await setMaxDepositedAddressesAsync(new BigNumber(1_000));
        });

        it('should fail to deposit DDX as collateral when the amount exhausted the DDX max cap', async () => {
            await addExchangeCollateralAsync(
                derivadex.ddxContract.address,
                new BigNumber(Web3Wrapper.toBaseUnitAmount(10_000, 18)),
            );

            const depositAmount = Web3Wrapper.toBaseUnitAmount(1_000, 6);
            await setMaxDDXCapAsync(depositAmount);
            const tx = collateral
                .deposit(derivadex.ddxContract.address, NULL_BYTES32, depositAmount)
                .awaitTransactionSuccessAsync();
            await expect(tx).to.be.rejectedWith(
                'LibCollateral: DDX deposit exceeds the maximum allowed DDX in the SMT.',
            );

            // Clean up the max DDX cap.
            await setMaxDDXCapAsync(MAX_SUPPLY.dividedToIntegerBy(2));
        });

        it('should successfully deposit DDX as collateral', async () => {
            let hasDeposited = await collateral.getAddressesHaveDeposited([fixtures.traderB()]).callAsync();
            expect(hasDeposited).to.be.deep.eq([false]);

            // Get the balances before the transfer.
            const ddxWithdrawalAllowanceBefore = await collateral
                .getWithdrawalAllowance(derivadex.ddxContract.address)
                .callAsync();
            const traderBalanceBefore = await derivadex.ddxContract.balanceOf(fixtures.traderB()).callAsync();
            const traderVotesBefore = await derivadex.ddxContract.getCurrentVotes(fixtures.traderB()).callAsync();
            const diamondBalanceBefore = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();

            // Complete the deposit.
            const depositAmount = Web3Wrapper.toBaseUnitAmount(1, 12);
            const { logs } = await collateral
                .deposit(derivadex.ddxContract.address, NULL_BYTES32, depositAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });

            // Verify the logs.
            expect(logs.length).to.be.eq(4);
            const transferLog = logs[0] as LogWithDecodedArgs<ERC20TransferEventArgs>;
            expect(transferLog.event).to.be.eq('Transfer');
            expect(transferLog.args).to.be.deep.eq({
                from: fixtures.traderB(),
                to: derivadex.derivaDEXContract.address,
                value: depositAmount,
            });
            const traderDelegateVotesChangedLog = logs[1] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
            expect(traderDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
            expect(traderDelegateVotesChangedLog.args).to.be.deep.eq({
                delegate: fixtures.traderB(),
                previousBalance: traderVotesBefore,
                newBalance: traderVotesBefore.minus(depositAmount),
            });
            const diamondDelegateVotesChangedLog = logs[2] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
            expect(diamondDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
            expect(diamondDelegateVotesChangedLog.args).to.be.deep.eq({
                delegate: derivadex.derivaDEXContract.address,
                previousBalance: diamondBalanceBefore,
                newBalance: diamondBalanceBefore.plus(depositAmount),
            });
            const log = logs[3] as LogWithDecodedArgs<CollateralStrategyUpdatedEventArgs>;
            expect(log.event).to.be.eq('StrategyUpdated');
            expect(log.args).to.be.deep.eq({
                trader: fixtures.traderB(),
                collateralAddress: derivadex.ddxContract.address,
                strategyId: NULL_BYTES32,
                amount: new BigNumber(1),
                updateKind: StrategyUpdateKind.Deposit,
            });

            // Verify the DDX balances after the deposit.
            const diamondBalanceAfter = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const traderBalanceAfter = await derivadex.ddxContract.balanceOf(fixtures.traderB()).callAsync();
            expect(diamondBalanceAfter).to.be.bignumber.eq(diamondBalanceBefore.plus(depositAmount));
            expect(traderBalanceAfter).to.be.bignumber.eq(traderBalanceBefore.minus(depositAmount));

            // Ensure that the DDX withdrawal allowance was increased.
            const ddxWithdrawalAllowanceAfter = await collateral
                .getWithdrawalAllowance(derivadex.ddxContract.address)
                .callAsync();
            expect(ddxWithdrawalAllowanceAfter).to.be.bignumber.eq(ddxWithdrawalAllowanceBefore.plus(depositAmount));

            const [numDepositedAddresses, ,] = await collateral.getGuardedDepositInfo().callAsync();
            expect(numDepositedAddresses).to.be.bignumber.eq(2);

            hasDeposited = await collateral.getAddressesHaveDeposited([fixtures.traderB()]).callAsync();
            expect(hasDeposited).to.be.deep.eq([true]);
        });
    });

    // TODO(jalextowle): Add test cases for withdrawals of multiple collateral
    // types.
    describe('#withdraw', () => {
        const strategyId = generateStrategyId('main');
        let committedStrategy: Strategy;
        let traderAddress: string;
        let firstWithdrawalBlockNumber: number;

        before(async () => {
            traderAddress = fixtures.traderA();
            committedStrategy = {
                strategyId,
                freeCollateral: { tokens: [], amounts: [] },
                frozenCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, Web3Wrapper.toBaseUnitAmount(10_000, 6))],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };

            // List USDC as collateral.
            await addExchangeCollateralAsync(
                usdcContract.address,
                new BigNumber(Web3Wrapper.toBaseUnitAmount(100_000, 6)),
            );

            // Deposit USDC.
            const depositAmount = Web3Wrapper.toBaseUnitAmount(100_000, 6);
            await usdcContract
                .approve(derivadex.derivaDEXContract.address, depositAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            await collateral
                .deposit(usdcContract.address, generateStrategyId('main'), depositAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        });

        async function updateStateRootAsync(strategyId: string, strategy: Strategy): Promise<void> {
            const strategyKey = generateStrategyKey(traderAddress, strategyId);
            const strategyHash = await testLibCollateral.hashStrategyPublic(strategy).callAsync();
            const stateRoot = hexUtils.hash(hexUtils.concat(strategyKey, strategyHash));
            await collateral.setStateRoot(stateRoot).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        }

        async function assertFailedWithdrawalAsync(
            strategyId: string,
            strategy: Strategy,
            withdrawalAmount: BigNumber,
            revertReason: string,
        ): Promise<void> {
            const proof = PUSH_LEAF_OPCODE;
            const tx = collateral
                .withdraw(
                    strategyId,
                    {
                        tokens: [usdcContract.address],
                        amounts: [withdrawalAmount],
                    },
                    strategy,
                    proof,
                )
                .awaitTransactionSuccessAsync({ from: traderAddress });
            return expect(tx).to.be.rejectedWith(revertReason);
        }

        async function assertSuccessfulWithdrawalAsync(
            strategyId: string,
            strategy: Strategy,
            withdrawalAmount: BigNumber,
        ): Promise<void> {
            // Get the starting unprocessed withdrawals.
            const unprocessedWithdrawalsBefore = await collateral
                .getUnprocessedWithdrawals(traderAddress, strategyId, usdcContract.address)
                .callAsync();

            // Get the starting token balances.
            const derivadexUSDCBalanceBefore = await usdcContract.balanceOf(collateral.address).callAsync();
            const traderUSDCBalanceBefore = await usdcContract.balanceOf(traderAddress).callAsync();

            // Execute the withdrawal.
            const proof = PUSH_LEAF_OPCODE;
            const { logs, blockNumber } = await collateral
                .withdraw(
                    strategyId,
                    {
                        tokens: [usdcContract.address],
                        amounts: [withdrawalAmount],
                    },
                    strategy,
                    proof,
                )
                .awaitTransactionSuccessAsync({ from: traderAddress });
            if (firstWithdrawalBlockNumber === undefined) {
                firstWithdrawalBlockNumber = blockNumber;
            }

            // Ensure that the processed withdrawals up to the block before the
            // withdrawal is equal to the unprocessed withdrawals before the
            // withdrawal.
            const processedWithdrawals = await collateral
                .getProcessedWithdrawals(
                    traderAddress,
                    strategyId,
                    usdcContract.address,
                    new BigNumber(blockNumber - 1),
                )
                .callAsync();
            expect(processedWithdrawals).to.be.bignumber.eq(unprocessedWithdrawalsBefore);

            // Ensure that the unprocessed withdrawals were updated correctly.
            const unprocessedWithdrawalsAfter = await collateral
                .getUnprocessedWithdrawals(traderAddress, strategyId, usdcContract.address)
                .callAsync();
            expect(unprocessedWithdrawalsAfter).to.be.bignumber.eq(
                unprocessedWithdrawalsBefore.plus(await scaleAmountForTokenAsync(usdcContract, withdrawalAmount)),
            );

            // Ensure that the ending token balances are correct.
            const derivadexUSDCBalanceAfter = await usdcContract.balanceOf(collateral.address).callAsync();
            const traderUSDCBalanceAfter = await usdcContract.balanceOf(traderAddress).callAsync();
            expect(derivadexUSDCBalanceAfter).to.be.bignumber.eq(derivadexUSDCBalanceBefore.minus(withdrawalAmount));
            expect(traderUSDCBalanceAfter).to.be.bignumber.eq(traderUSDCBalanceBefore.plus(withdrawalAmount));

            // Ensure that the logs and the state are correct after the withdrawal.
            expect(logs.length).to.be.eq(2);
            const transferLog = logs[0] as LogWithDecodedArgs<ERC20TransferEventArgs>;
            expect(transferLog.event).to.be.eq('Transfer');
            expect(transferLog.args).to.be.deep.eq({
                from: derivadex.derivaDEXContract.address,
                to: traderAddress,
                value: withdrawalAmount,
            });
            const strategyUpdatedLog = logs[1] as LogWithDecodedArgs<CollateralStrategyUpdatedEventArgs>;
            expect(strategyUpdatedLog.event).to.be.eq('StrategyUpdated');
            expect(strategyUpdatedLog.args).to.be.deep.eq({
                trader: traderAddress,
                collateralAddress: usdcContract.address,
                strategyId,
                amount: await scaleAmountForTokenAsync(usdcContract, withdrawalAmount),
                updateKind: StrategyUpdateKind.Withdrawal,
            });
        }

        it('should fail if the state root is not recovered', async () => {
            await assertFailedWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(3_000, 6),
                'Collateral: Did not recover state root.',
            );
        });

        it('should successfully partially withdraw 3,000', async () => {
            await updateStateRootAsync(strategyId, committedStrategy);
            await assertSuccessfulWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(3_000, 6),
            );
        });

        it('should successfully partially withdraw 5', async () => {
            await assertSuccessfulWithdrawalAsync(strategyId, committedStrategy, Web3Wrapper.toBaseUnitAmount(5, 6));
        });

        it('should successfully partially withdraw 6,995', async () => {
            await assertSuccessfulWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(6_995, 6),
            );
        });

        it('should fail to withdraw 1 after fully withdrawing', async () => {
            await assertFailedWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(1, 6),
                'LibCollateral: Invalid withdrawal amount.',
            );
        });

        it('should fail to withdraw 1 after fully withdrawing if all of the withdrawals are unprocessed', async () => {
            await checkpoint
                .setLastConfirmedBlockNumber(new BigNumber(firstWithdrawalBlockNumber - 1))
                .awaitTransactionSuccessAsync();
            await assertFailedWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(1, 6),
                'LibCollateral: Invalid withdrawal amount.',
            );
        });

        it('should successfully withdraw 1 in a different strategy', async () => {
            const strategyId = generateStrategyId('other');
            committedStrategy = {
                strategyId,
                freeCollateral: { tokens: [], amounts: [] },
                frozenCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, Web3Wrapper.toBaseUnitAmount(9_000, 6))],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };
            await updateStateRootAsync(strategyId, committedStrategy);
            await assertSuccessfulWithdrawalAsync(strategyId, committedStrategy, Web3Wrapper.toBaseUnitAmount(1, 6));
        });

        it('should fail to withdraw 3,000 if the last two withdrawals are unprocessed and the frozen collateral is 9,000', async () => {
            committedStrategy = {
                strategyId,
                freeCollateral: { tokens: [], amounts: [] },
                frozenCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, Web3Wrapper.toBaseUnitAmount(9_000, 6))],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };
            await updateStateRootAsync(strategyId, committedStrategy);
            await checkpoint
                .setLastConfirmedBlockNumber(new BigNumber(firstWithdrawalBlockNumber))
                .awaitTransactionSuccessAsync();
            await assertFailedWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(3_000, 6),
                'LibCollateral: Invalid withdrawal amount.',
            );
        });

        it('should successfully withdraw 2,000', async () => {
            await assertSuccessfulWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(2_000, 6),
            );
        });

        it('should fail to withdraw when the withdrawal allowance is exhausted', async () => {
            committedStrategy = {
                strategyId,
                freeCollateral: { tokens: [], amounts: [] },
                frozenCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, Web3Wrapper.toBaseUnitAmount(100_000, 6))],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };
            await updateStateRootAsync(strategyId, committedStrategy);
            await assertFailedWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(100_000, 6),
                'LibCollateral: Maximum withdrawal exceeded.',
            );
        });

        it('should successfully withdraw less than the rate limit', async () => {
            // Update the USDC rate limits.
            await setRateLimitParametersAsync(new BigNumber(50), new BigNumber(1));
            await updateExchangeCollateralAsync(usdcContract.address, Web3Wrapper.toBaseUnitAmount(1_000, 6));

            // Mine enough blocks to advance the period.
            await advanceBlocksAsync(derivadex.providerEngine, 50);

            // Successfully withdraw less than the rate limit.
            committedStrategy = {
                strategyId,
                freeCollateral: { tokens: [], amounts: [] },
                frozenCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, Web3Wrapper.toBaseUnitAmount(15_000, 6))],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };
            await updateStateRootAsync(strategyId, committedStrategy);
            await assertSuccessfulWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(1_000, 6),
            );
        });

        it('should fail to withdraw more than the rate limit', async () => {
            await assertFailedWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(3_000, 6),
                'LibCollateral: Maximum withdrawal exceeded.',
            );

            // Update the global rate limits for future tests.
            await setRateLimitParametersAsync(new BigNumber(50), new BigNumber(100));

            // Mine enough blocks to advance the period.
            await advanceBlocksAsync(derivadex.providerEngine, 50);
        });

        it('should successfully withdraw when the free collateral is larger than 128 bits', async () => {
            // Successfully withdraw less than the rate limit.
            committedStrategy = {
                strategyId,
                freeCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [
                        (await scaleAmountForTokenAsync(usdcContract, Web3Wrapper.toBaseUnitAmount(100_000, 6))).times(
                            new BigNumber(2).exponentiatedBy(128),
                        ),
                    ],
                },
                frozenCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, Web3Wrapper.toBaseUnitAmount(15_000, 6))],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };
            await updateStateRootAsync(strategyId, committedStrategy);
            await assertSuccessfulWithdrawalAsync(
                strategyId,
                committedStrategy,
                Web3Wrapper.toBaseUnitAmount(1_000, 6),
            );
        });

        it('should fail to withdraw when the frozen collateral is larger than 128 bits', async () => {
            committedStrategy = {
                strategyId,
                freeCollateral: {
                    tokens: [],
                    amounts: [],
                },
                frozenCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [
                        (await scaleAmountForTokenAsync(usdcContract, Web3Wrapper.toBaseUnitAmount(100_000, 6))).times(
                            new BigNumber(2).exponentiatedBy(128),
                        ),
                    ],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };

            // Update the state root. Note that we must use 256 functions
            // to avoid issues with generating the correct hash.
            const strategyKey = generateStrategyKey(traderAddress, strategyId);
            const strategyHash = await testLibCollateral.hashStrategy256Public(committedStrategy).callAsync();
            const stateRoot = hexUtils.hash(hexUtils.concat(strategyKey, strategyHash));
            await collateral.setStateRoot(stateRoot).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

            // Generate calldata using the withdraw256 stub.
            const proof = PUSH_LEAF_OPCODE;
            const calldata = hexUtils.concat(
                collateral.getSelector('withdraw'),
                generateCallData(
                    testLibCollateral
                        .withdraw256(
                            strategyId,
                            {
                                tokens: [usdcContract.address],
                                amounts: [Web3Wrapper.toBaseUnitAmount(1_000, 6)],
                            },
                            committedStrategy,
                            proof,
                        )
                        .getABIEncodedTransactionData(),
                ),
            );

            // Call the withdraw function using the calldata generated above.
            const web3Wrapper = new Web3Wrapper(derivadex.providerEngine);
            const tx = web3Wrapper.sendTransactionAsync({
                from: traderAddress,
                to: derivadex.derivaDEXContract.address,
                value: 0,
                gas: 100_000,
                data: calldata,
            });
            return expect(tx).to.be.rejectedWith("Transaction reverted and Hardhat couldn't infer the reason.");
        });

        it('should fail to withdraw when the withdrawal amount is larger than 128 bits', async () => {
            committedStrategy = {
                strategyId,
                freeCollateral: {
                    tokens: [],
                    amounts: [],
                },
                frozenCollateral: {
                    tokens: [usdcContract.address],
                    amounts: [Web3Wrapper.toBaseUnitAmount(10_000, 6)],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };

            // Update the state root. Note that we must use 256 functions
            // to avoid issues with generating the correct hash.
            const strategyKey = generateStrategyKey(traderAddress, strategyId);
            const strategyHash = await testLibCollateral.hashStrategy256Public(committedStrategy).callAsync();
            const stateRoot = hexUtils.hash(hexUtils.concat(strategyKey, strategyHash));
            await collateral.setStateRoot(stateRoot).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

            // Generate calldata using the withdraw256 stub.
            const proof = PUSH_LEAF_OPCODE;
            const calldata = hexUtils.concat(
                collateral.getSelector('withdraw'),
                generateCallData(
                    testLibCollateral
                        .withdraw256(
                            strategyId,
                            {
                                tokens: [usdcContract.address],
                                amounts: [
                                    (await scaleAmountForTokenAsync(usdcContract, new BigNumber(100_000))).times(
                                        new BigNumber(2).exponentiatedBy(128),
                                    ),
                                ],
                            },
                            committedStrategy,
                            proof,
                        )
                        .getABIEncodedTransactionData(),
                ),
            );

            // Call the withdraw function using the calldata generated above.
            const web3Wrapper = new Web3Wrapper(derivadex.providerEngine);
            const tx = web3Wrapper.sendTransactionAsync({
                from: traderAddress,
                to: derivadex.derivaDEXContract.address,
                value: 0,
                gas: 100_000,
                data: calldata,
            });
            return expect(tx).to.be.rejectedWith("Transaction reverted and Hardhat couldn't infer the reason.");
        });
    });

    describe('#depositDDX', () => {
        const depositAmount = Web3Wrapper.toBaseUnitAmount(100, 12);

        before(async () => {
            await derivadex.ddxContract
                .approve(derivadex.derivaDEXContract.address, new BigNumber(2).pow(256).minus(1))
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        });

        it('should fail if the trader is banned', async () => {
            const bannedAddress = accounts[accounts.length - 1];
            await derivadex.ddxContract
                .transfer(bannedAddress, depositAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            const tx = stake.depositDDX(depositAmount).awaitTransactionSuccessAsync({ from: bannedAddress });
            return expect(tx).to.be.rejectedWith('Stake: cannot deposit DDX if banned.');
        });

        it('should fail if the deposit amount is zero', async () => {
            const tx = stake.depositDDX(new BigNumber(0)).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            return expect(tx).to.be.rejectedWith('LibCollateral: Invalid deposit amount.');
        });

        it('should fail to deposit if the trader has insufficient balance', async () => {
            const tx = stake.depositDDX(depositAmount).awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            return expect(tx).to.be.rejectedWith('SafeMath: subtraction overflow');
        });

        it('should fail if the deposit exceeds the max DDX cap (cap = deposit_amount - 1)', async () => {
            await setMaxDDXCapAsync(depositAmount.minus(1));
            const tx = stake.depositDDX(depositAmount).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            return expect(tx).to.be.rejectedWith(
                'LibCollateral: DDX deposit exceeds the maximum allowed DDX in the SMT.',
            );
        });

        it('should fail if the deposit exceeds the max DDX cap (cap = 10 * deposit_amount)', async () => {
            await setMaxDDXCapAsync(depositAmount.times(10));
            const tx = stake.depositDDX(depositAmount).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            await expect(tx).to.be.rejectedWith(
                'LibCollateral: DDX deposit exceeds the maximum allowed DDX in the SMT.',
            );

            // Clean up the max DDX cap.
            await setMaxDDXCapAsync(MAX_SUPPLY.dividedToIntegerBy(2));
        });

        it('should emit a TraderUpdate event for a valid deposit', async () => {
            // Get the preliminary balances of the trader and diamond.
            const ddxWithdrawalAllowanceBefore = await collateral.getWithdrawalAllowance(ZERO_ADDRESS).callAsync();
            const diamondBalanceBefore = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const traderBalanceBefore = await derivadex.ddxContract.balanceOf(fixtures.traderB()).callAsync();
            const traderVotesBefore = await derivadex.ddxContract.getCurrentVotes(fixtures.traderB()).callAsync();

            // Deposit DDX into the diamond.
            const { logs } = await stake
                .depositDDX(depositAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });

            // Verify the logs.
            expect(logs.length).to.be.eq(4);
            const transferLog = logs[0] as LogWithDecodedArgs<ERC20TransferEventArgs>;
            expect(transferLog.event).to.be.eq('Transfer');
            expect(transferLog.args).to.be.deep.eq({
                from: fixtures.traderB(),
                to: derivadex.derivaDEXContract.address,
                value: depositAmount,
            });
            const traderDelegateVotesChangedLog = logs[1] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
            expect(traderDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
            expect(traderDelegateVotesChangedLog.args).to.be.deep.eq({
                delegate: fixtures.traderB(),
                previousBalance: traderVotesBefore,
                newBalance: traderVotesBefore.minus(depositAmount),
            });
            const diamondDelegateVotesChangedLog = logs[2] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
            expect(diamondDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
            expect(diamondDelegateVotesChangedLog.args).to.be.deep.eq({
                delegate: derivadex.derivaDEXContract.address,
                previousBalance: diamondBalanceBefore,
                newBalance: diamondBalanceBefore.plus(depositAmount),
            });
            const log = logs[3] as LogWithDecodedArgs<StakeTraderUpdatedEventArgs>;
            expect(log.event).to.be.eq('TraderUpdated');
            expect(log.args).to.be.deep.eq({
                trader: fixtures.traderB(),
                amount: new BigNumber(100),
                updateKind: TraderUpdateKind.Deposit,
            });

            // Verify the DDX balances after the deposit.
            const diamondBalanceAfter = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const traderBalanceAfter = await derivadex.ddxContract.balanceOf(fixtures.traderB()).callAsync();
            expect(diamondBalanceAfter).to.be.bignumber.eq(diamondBalanceBefore.plus(depositAmount));
            expect(traderBalanceAfter).to.be.bignumber.eq(traderBalanceBefore.minus(depositAmount));

            // Ensure that the DDX withdrawal allowance was increased.
            const ddxWithdrawalAllowanceAfter = await collateral.getWithdrawalAllowance(ZERO_ADDRESS).callAsync();
            expect(ddxWithdrawalAllowanceAfter).to.be.bignumber.eq(ddxWithdrawalAllowanceBefore.plus(depositAmount));
        });
    });

    describe('#withdrawDDX', () => {
        let committedTrader: Trader;
        let traderAddress: string;
        const withdrawalBlockNumbers: number[] = [];

        before(async () => {
            traderAddress = fixtures.traderA();
            committedTrader = {
                freeDDXBalance: new BigNumber(0),
                frozenDDXBalance: new BigNumber(10_000),
                referralAddress: ZERO_ADDRESS,
                payFeesInDDX: false,
            };
            await checkpoint
                .setWithdrawalAllowance(ZERO_ADDRESS, Web3Wrapper.toBaseUnitAmount(100, 12))
                .awaitTransactionSuccessAsync({ from: traderAddress });
            await derivadex.ddxContract
                .transfer(derivadex.derivaDEXContract.address, Web3Wrapper.toBaseUnitAmount(10_000, 12))
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        });

        async function updateStateRootAsync(traderData: Trader): Promise<void> {
            const traderHash = await stake.hashTraderPublic(traderData).callAsync();
            const traderKey = generateTraderGroupKey(ItemKind.Trader, traderAddress);
            const stateRoot = hexUtils.hash(hexUtils.concat(traderKey, traderHash));
            await collateral.setStateRoot(stateRoot).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        }

        async function assertFailedWithdrawalAsync(
            traderData: Trader,
            withdrawalAmount: BigNumber,
            revertReason: string,
        ): Promise<void> {
            const proof = PUSH_LEAF_OPCODE;
            const tx = stake
                .withdrawDDX(withdrawalAmount, traderData, proof)
                .awaitTransactionSuccessAsync({ from: traderAddress });
            return expect(tx).to.be.rejectedWith(revertReason);
        }

        async function assertSuccessfulWithdrawalAsync(traderData: Trader, withdrawalAmount: BigNumber): Promise<void> {
            // Get the starting unprocessed withdrawals.
            const unprocessedWithdrawalsBefore = await stake.getUnprocessedDDXWithdrawals(traderAddress).callAsync();
            const traderDDXDepositedBefore = await stake.getTraderDDXDeposited().callAsync();

            // Get the starting token balances.
            const diamondBalanceBefore = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const traderBalanceBefore = await derivadex.ddxContract.balanceOf(traderAddress).callAsync();
            const traderVotesBefore = await derivadex.ddxContract.getCurrentVotes(traderAddress).callAsync();

            // Execute the withdrawal.
            const proof = PUSH_LEAF_OPCODE;
            const { logs, blockNumber } = await stake
                .withdrawDDX(withdrawalAmount, traderData, proof)
                .awaitTransactionSuccessAsync({ from: traderAddress });
            withdrawalBlockNumbers.push(blockNumber);

            // Ensure that the processed withdrawals up to the block before the
            // withdrawal is equal to the unprocessed withdrawals before the
            // withdrawal.
            const processedWithdrawals = await stake
                .getProcessedDDXWithdrawals(traderAddress, new BigNumber(blockNumber - 1))
                .callAsync();
            expect(processedWithdrawals).to.be.bignumber.eq(unprocessedWithdrawalsBefore);

            // Ensure that the unprocessed withdrawals were updated correctly.
            const unprocessedWithdrawalsAfter = await collateral
                .getUnprocessedWithdrawals(traderAddress, NULL_BYTES32, ZERO_ADDRESS)
                .callAsync();
            expect(unprocessedWithdrawalsAfter).to.be.bignumber.eq(
                unprocessedWithdrawalsBefore.plus(
                    await scaleAmountForTokenAsync(derivadex.ddxContract, withdrawalAmount),
                ),
            );

            const traderDDXDepositedAfter = await stake.getTraderDDXDeposited().callAsync();
            const diamondBalanceAfter = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const traderBalanceAfter = await derivadex.ddxContract.balanceOf(traderAddress).callAsync();
            const traderVotesAfter = await derivadex.ddxContract.getCurrentVotes(traderAddress).callAsync();
            if (traderDDXDepositedBefore.gt(0) && traderDDXDepositedBefore.gte(withdrawalAmount)) {
                // Verify the logs.
                expect(logs.length).to.be.eq(4);
                const transferLog = logs[0] as LogWithDecodedArgs<ERC20TransferEventArgs>;
                expect(transferLog.event).to.be.eq('Transfer');
                expect(transferLog.args).to.be.deep.eq({
                    from: derivadex.derivaDEXContract.address,
                    to: traderAddress,
                    value: withdrawalAmount,
                });
                const diamondDelegateVotesChangedLog = logs[1] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
                expect(diamondDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
                expect(diamondDelegateVotesChangedLog.args).to.be.deep.eq({
                    delegate: derivadex.derivaDEXContract.address,
                    previousBalance: diamondBalanceBefore,
                    newBalance: diamondBalanceBefore.minus(withdrawalAmount),
                });
                const traderDelegateVotesChangedLog = logs[2] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
                expect(traderDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
                expect(traderDelegateVotesChangedLog.args).to.be.deep.eq({
                    delegate: traderAddress,
                    previousBalance: traderBalanceBefore,
                    newBalance: traderBalanceBefore.plus(withdrawalAmount),
                });
                const log = logs[3] as LogWithDecodedArgs<StakeTraderUpdatedEventArgs>;
                expect(log.event).to.be.eq('TraderUpdated');
                expect(log.args).to.be.deep.eq({
                    trader: traderAddress,
                    amount: await scaleAmountForTokenAsync(derivadex.ddxContract, withdrawalAmount),
                    updateKind: TraderUpdateKind.Withdrawal,
                });

                // Ensure the token balances were updated correctly.
                expect(diamondBalanceAfter).to.be.bignumber.eq(diamondBalanceBefore.minus(withdrawalAmount));
                expect(traderBalanceAfter).to.be.bignumber.eq(traderBalanceBefore.plus(withdrawalAmount));
                expect(traderVotesAfter).to.be.bignumber.eq(traderVotesBefore.plus(withdrawalAmount));
                expect(traderDDXDepositedAfter).to.be.bignumber.eq(traderDDXDepositedBefore.minus(withdrawalAmount));
            } else if (traderDDXDepositedBefore.gt(0)) {
                // Verify the logs.
                expect(logs.length).to.be.eq(6);
                let transferLog = logs[0] as LogWithDecodedArgs<ERC20TransferEventArgs>;
                expect(transferLog.event).to.be.eq('Transfer');
                expect(transferLog.args).to.be.deep.eq({
                    from: derivadex.derivaDEXContract.address,
                    to: traderAddress,
                    value: traderDDXDepositedBefore,
                });
                const diamondDelegateVotesChangedLog = logs[1] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
                expect(diamondDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
                expect(diamondDelegateVotesChangedLog.args).to.be.deep.eq({
                    delegate: derivadex.derivaDEXContract.address,
                    previousBalance: diamondBalanceBefore,
                    newBalance: diamondBalanceBefore.minus(traderDDXDepositedBefore),
                });
                let traderDelegateVotesChangedLog = logs[2] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
                expect(traderDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
                expect(traderDelegateVotesChangedLog.args).to.be.deep.eq({
                    delegate: traderAddress,
                    previousBalance: traderVotesBefore,
                    newBalance: traderVotesBefore.plus(traderDDXDepositedBefore),
                });
                transferLog = logs[3] as LogWithDecodedArgs<ERC20TransferEventArgs>;
                expect(transferLog.event).to.be.eq('Transfer');
                expect(transferLog.args).to.be.deep.eq({
                    from: ZERO_ADDRESS,
                    to: traderAddress,
                    value: withdrawalAmount.minus(traderDDXDepositedBefore),
                });
                traderDelegateVotesChangedLog = logs[4] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
                expect(traderDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
                expect(traderDelegateVotesChangedLog.args).to.be.deep.eq({
                    delegate: traderAddress,
                    previousBalance: traderVotesBefore.plus(diamondBalanceBefore),
                    newBalance: traderVotesBefore.plus(withdrawalAmount),
                });
                const log = logs[5] as LogWithDecodedArgs<StakeTraderUpdatedEventArgs>;
                expect(log.event).to.be.eq('TraderUpdated');
                expect(log.args).to.be.deep.eq({
                    trader: traderAddress,
                    amount: await scaleAmountForTokenAsync(derivadex.ddxContract, withdrawalAmount),
                    updateKind: TraderUpdateKind.Withdrawal,
                });

                // Ensure the token balances were updated correctly.
                expect(diamondBalanceAfter).to.be.bignumber.eq(diamondBalanceBefore.minus(traderDDXDepositedBefore));
                expect(traderBalanceAfter).to.be.bignumber.eq(traderBalanceBefore.plus(withdrawalAmount));
                expect(traderVotesAfter).to.be.bignumber.eq(traderVotesBefore.plus(withdrawalAmount));
                expect(traderDDXDepositedAfter).to.be.bignumber.eq(0);
            } else {
                // Verify the logs.
                expect(logs.length).to.be.eq(3);
                const transferLog = logs[0] as LogWithDecodedArgs<ERC20TransferEventArgs>;
                expect(transferLog.event).to.be.eq('Transfer');
                expect(transferLog.args).to.be.deep.eq({
                    from: ZERO_ADDRESS,
                    to: traderAddress,
                    value: withdrawalAmount,
                });
                const traderDelegateVotesChangedLog = logs[1] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
                expect(traderDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
                expect(traderDelegateVotesChangedLog.args).to.be.deep.eq({
                    delegate: traderAddress,
                    previousBalance: traderVotesBefore,
                    newBalance: traderVotesBefore.plus(withdrawalAmount),
                });
                const traderUpdatedLog = logs[2] as LogWithDecodedArgs<StakeTraderUpdatedEventArgs>;
                expect(traderUpdatedLog.event).to.be.eq('TraderUpdated');
                expect(traderUpdatedLog.args).to.be.deep.eq({
                    trader: traderAddress,
                    amount: await scaleAmountForTokenAsync(derivadex.ddxContract, withdrawalAmount),
                    updateKind: TraderUpdateKind.Withdrawal,
                });

                // Ensure the token balances were updated correctly.
                expect(diamondBalanceAfter).to.be.bignumber.eq(diamondBalanceBefore);
                expect(traderBalanceAfter).to.be.bignumber.eq(traderBalanceBefore.plus(withdrawalAmount));
                expect(traderVotesAfter).to.be.bignumber.eq(traderVotesBefore.plus(withdrawalAmount));
                expect(traderDDXDepositedAfter).to.be.bignumber.eq(traderDDXDepositedBefore);
            }
        }

        it('should fail if the state root is not recovered', async () => {
            await assertFailedWithdrawalAsync(
                committedTrader,
                Web3Wrapper.toBaseUnitAmount(100, 12),
                'Stake: Did not recover state root.',
            );
        });

        it('should fail to withdraw more than the DDX withdrawal allowance', async () => {
            await updateStateRootAsync(committedTrader);
            await assertFailedWithdrawalAsync(
                committedTrader,
                Web3Wrapper.toBaseUnitAmount(101, 12),
                'LibCollateral: Maximum withdrawal exceeded.',
            );
        });

        it('should successfully withdraw 100 (with 12 zeros)', async () => {
            await assertSuccessfulWithdrawalAsync(committedTrader, Web3Wrapper.toBaseUnitAmount(100, 12));
        });

        it('should fail to withdraw when the DDX withdrawal allowance is exhausted', async () => {
            await assertFailedWithdrawalAsync(
                committedTrader,
                Web3Wrapper.toBaseUnitAmount(1, 12),
                'LibCollateral: Maximum withdrawal exceeded.',
            );
        });

        it('should successfully withdraw 7,000 (with 12 zeros)', async () => {
            await checkpoint
                .setWithdrawalAllowance(ZERO_ADDRESS, Web3Wrapper.toBaseUnitAmount(100_000, 12))
                .awaitTransactionSuccessAsync({ from: traderAddress });
            await assertSuccessfulWithdrawalAsync(committedTrader, Web3Wrapper.toBaseUnitAmount(7_000, 12));
        });

        it('should successfully withdraw 2,900 (with 12 zeros)', async () => {
            await assertSuccessfulWithdrawalAsync(committedTrader, Web3Wrapper.toBaseUnitAmount(2_900, 12));
        });

        it('should fail to withdraw when the trader leaf is exhausted', async () => {
            await assertFailedWithdrawalAsync(
                committedTrader,
                Web3Wrapper.toBaseUnitAmount(1, 12),
                'LibCollateral: Invalid withdrawal amount.',
            );
        });

        it('should fail to withdraw when the trader leaf is exhausted', async () => {
            await checkpoint
                .setLastConfirmedBlockNumber(new BigNumber(withdrawalBlockNumbers[0] - 1))
                .awaitTransactionSuccessAsync();
            await assertFailedWithdrawalAsync(
                committedTrader,
                Web3Wrapper.toBaseUnitAmount(1, 12),
                'LibCollateral: Invalid withdrawal amount.',
            );
        });

        it('should fail to withdraw more than the trader leaf permits', async () => {
            await checkpoint
                .setLastConfirmedBlockNumber(new BigNumber(withdrawalBlockNumbers[1]))
                .awaitTransactionSuccessAsync();
            committedTrader = {
                freeDDXBalance: new BigNumber(0),
                frozenDDXBalance: new BigNumber(4_000),
                referralAddress: ZERO_ADDRESS,
                payFeesInDDX: false,
            };
            await updateStateRootAsync(committedTrader);
            await assertFailedWithdrawalAsync(
                committedTrader,
                Web3Wrapper.toBaseUnitAmount(1_200, 12),
                'LibCollateral: Invalid withdrawal amount.',
            );
        });

        it('should successfully withdraw the remaining balance from the trader leaf', async () => {
            await assertSuccessfulWithdrawalAsync(committedTrader, Web3Wrapper.toBaseUnitAmount(1_100, 12));
        });

        it('should fail to withdraw after the trader leaf is exhausted', async () => {
            await assertFailedWithdrawalAsync(
                committedTrader,
                Web3Wrapper.toBaseUnitAmount(1, 12),
                'LibCollateral: Invalid withdrawal amount.',
            );
        });

        it('should successfully withdraw less than the rate limit', async () => {
            // Update the application rate limits.
            await setDDXRateLimitsAsync(Web3Wrapper.toBaseUnitAmount(1_000, 12));
            await setRateLimitParametersAsync(new BigNumber(50), new BigNumber(1));

            // Mine enough blocks to advance the period.
            await advanceBlocksAsync(derivadex.providerEngine, 50);

            committedTrader = {
                freeDDXBalance: new BigNumber(0),
                frozenDDXBalance: new BigNumber(15_000),
                referralAddress: ZERO_ADDRESS,
                payFeesInDDX: false,
            };
            await updateStateRootAsync(committedTrader);
            await assertSuccessfulWithdrawalAsync(committedTrader, Web3Wrapper.toBaseUnitAmount(1_000, 12));
        });

        it('should fail to withdraw more than the rate limit', async () => {
            await assertFailedWithdrawalAsync(
                committedTrader,
                Web3Wrapper.toBaseUnitAmount(3_000, 12),
                'LibCollateral: Maximum withdrawal exceeded.',
            );

            // Update the global rate limits for future tests.
            await setRateLimitParametersAsync(new BigNumber(50), new BigNumber(100));

            // Mine enough blocks to advance the period.
            await advanceBlocksAsync(derivadex.providerEngine, 50);
        });
    });

    describe('#generateStrategyKey', () => {
        const RUNS = 10;

        for (let i = 0; i < RUNS; i++) {
            it(`should encode the correct strategy key trial ${i} of ${RUNS}`, async () => {
                const traderAddress = hexUtils.random(20);
                const strategyId = hexUtils.random(32);
                const key = await testLibCollateral.generateStrategyKeyExternal(traderAddress, strategyId).callAsync();
                expect(key).to.be.eq(generateStrategyKey(traderAddress, strategyId));
            });
        }
    });

    describe('#generateTraderGroupKey', () => {
        const RUNS = 10;

        for (let i = 0; i < RUNS; i++) {
            it(`should encode the correct trader key trial ${i} of ${RUNS}`, async () => {
                const traderAddress = hexUtils.random(20);
                const key = await stake.generateTraderGroupKeyExternal(ItemKind.Trader, traderAddress).callAsync();
                expect(key).to.be.eq(generateTraderGroupKey(ItemKind.Trader, traderAddress));
            });
        }
    });

    describe('#isNotPaused', () => {
        before(async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.pauseContract.getFunctionSignature('setIsPaused')];
            const calldatas = [
                generateCallData(derivadex.pauseContract.setIsPaused(true).getABIEncodedTransactionData()),
            ];
            const description = 'Pause the DerivaDEX diamond.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            await passProposalAsync(proposalCount);
        });

        after(async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.pauseContract.getFunctionSignature('setIsPaused')];
            const calldatas = [
                generateCallData(derivadex.pauseContract.setIsPaused(false).getABIEncodedTransactionData()),
            ];
            const description = 'Unpause the DerivaDEX diamond.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            await passProposalAsync(proposalCount);
        });

        it('should fail to call "setRewardCliff" when paused', async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.traderContract.getFunctionSignature('setRewardCliff')];
            const calldatas = [generateCallData(derivadex.setRewardCliff(true).getABIEncodedTransactionData())];
            const description = 'Lifting reward cliff.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            const tx = passProposalAsync(proposalCount);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted');
        });

        it('should fail to call "setMaxDDXCap" when paused', async () => {
            const tx = setMaxDDXCapAsync(new BigNumber(1));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted');
        });

        it('should fail to call "addExchangeCollateral" when paused', async () => {
            const tx = addExchangeCollateralAsync(accounts[0], Web3Wrapper.toBaseUnitAmount(100_000, 6));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted');
        });

        it('should fail to call "updateExchangeCollateral" when paused', async () => {
            const tx = updateExchangeCollateralAsync(usdcContract.address, Web3Wrapper.toBaseUnitAmount(100_000, 6));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted');
        });

        it('should fail to call "removeExchangeCollateral" when paused', async () => {
            const tx = removeExchangeCollateralAsync(usdtContract.address);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted');
        });

        it('should fail to call "issueDDXReward" when paused', async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.traderContract.getFunctionSignature('issueDDXReward')];
            const calldatas = [
                generateCallData(
                    derivadex.traderContract
                        .issueDDXReward(new BigNumber(50), fixtures.traderA())
                        .getABIEncodedTransactionData(),
                ),
            ];
            const description = 'Issuing DDX reward.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            const tx = passProposalAsync(proposalCount);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted');
        });

        it('should fail to call "issueDDXToRecipient" when paused', async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.traderContract.getFunctionSignature('issueDDXToRecipient')];
            const calldatas = [
                generateCallData(
                    derivadex.traderContract
                        .issueDDXToRecipient(new BigNumber(50), fixtures.traderA())
                        .getABIEncodedTransactionData(),
                ),
            ];
            const description = 'Issuing DDX reward to recipient.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            const tx = passProposalAsync(proposalCount);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted');
        });

        it('should fail to call "deposit" when paused', async () => {
            const tx = collateral
                .deposit(ZERO_ADDRESS, NULL_BYTES32, new BigNumber(0))
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Collateral: paused.');
        });

        it('should fail to call "withdraw" when paused', async () => {
            // Update the state root to be equal to the leaf hash of the strategy.
            const traderAddress = fixtures.traderA();
            const strategyId = generateStrategyId('main');
            const strategyKey = generateStrategyKey(traderAddress, strategyId);
            const strategy = {
                strategyId,
                freeCollateral: { tokens: [], amounts: [] },
                frozenCollateral: {
                    tokens: [usdtContract.address],
                    amounts: [new BigNumber(1000000000000)],
                },
                maxLeverage: new BigNumber(0),
                frozen: false,
            };
            const strategyHash = await testLibCollateral.hashStrategyPublic(strategy).callAsync();
            const stateRoot = hexUtils.hash(hexUtils.concat(strategyKey, strategyHash));
            await collateral.setStateRoot(stateRoot).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

            // Execute the withdrawal.
            const proof = PUSH_LEAF_OPCODE;
            const tx = collateral
                .withdraw(
                    NULL_BYTES32,
                    { tokens: [usdtContract.address], amounts: [new BigNumber(1)] },
                    strategy,
                    proof,
                )
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            return expect(tx).to.be.rejectedWith('Collateral: paused.');
        });

        it('should fail to call "stakeDDXFromTrader" when paused', async () => {
            const tx = derivadex.stakeDDXFromTrader(10000).awaitTransactionSuccessAsync({ from: fixtures.traderD() });
            return expect(tx).to.be.rejectedWith('Trader: paused.');
        });

        it('should fail to call "sendDDXFromTraderToTraderWallet" when paused', async () => {
            const tx = derivadex
                .sendDDXFromTraderToTraderWallet(fixtures.traderB(), 100)
                .awaitTransactionSuccessAsync({ from: fixtures.traderC() });
            return expect(tx).to.be.rejectedWith('Trader: paused.');
        });

        it('should fail to call "withdrawDDXToTrader" when paused', async () => {
            const tx = derivadex.withdrawDDXToTrader(5000).awaitTransactionSuccessAsync({ from: fixtures.traderD() });
            return expect(tx).to.be.rejectedWith('Trader: paused.');
        });

        it('should fail to call "depositDDX" when paused', async () => {
            const tx = stake.depositDDX(new BigNumber(100)).awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            return expect(tx).to.be.rejectedWith('Stake: paused.');
        });

        it('should fail to call "withdrawDDX" when paused', async () => {
            const withdrawalAmount = new BigNumber(100);

            // Update the state root to be equal to the leaf hash of the strategy.
            const traderAddress = fixtures.traderA();
            const traderData = {
                freeDDXBalance: new BigNumber(0),
                frozenDDXBalance: withdrawalAmount,
                referralAddress: ZERO_ADDRESS,
                payFeesInDDX: false,
            };
            const traderHash = await stake.hashTraderPublic(traderData).callAsync();
            const traderKey = generateTraderGroupKey(ItemKind.Trader, traderAddress);
            const stateRoot = hexUtils.hash(hexUtils.concat(traderKey, traderHash));
            await collateral.setStateRoot(stateRoot).awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

            // Execute the withdrawal.
            const proof = PUSH_LEAF_OPCODE;
            const tx = stake
                .withdrawDDX(withdrawalAmount, traderData, proof)
                .awaitTransactionSuccessAsync({ from: traderAddress });
            return expect(tx).to.be.rejectedWith('Stake: paused.');
        });
    });

    describe('#transferArbitraryTokens', () => {
        it('should transfer arbitrary tokens to recipient', async () => {
            const initialBalanceOfTraderA = await usdtContract.balanceOf(fixtures.traderA()).callAsync();
            const initialBalanceOfTraderB = await usdtContract.balanceOf(fixtures.traderB()).callAsync();
            const decimals = await derivadex.getDecimalsForArbitraryTokenAsync(usdtContract.address);
            await derivadex
                .transferArbitraryTokens(usdtContract.address, fixtures.traderB(), 5, decimals)
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            const finalBalanceOfTraderA = await usdtContract.balanceOf(fixtures.traderA()).callAsync();
            const finalBalanceOfTraderB = await usdtContract.balanceOf(fixtures.traderB()).callAsync();
            expect(finalBalanceOfTraderA.minus(initialBalanceOfTraderA)).to.be.bignumber.eq(
                -Web3Wrapper.toBaseUnitAmount(5, decimals),
            );
            expect(finalBalanceOfTraderB.minus(initialBalanceOfTraderB)).to.be.bignumber.eq(
                Web3Wrapper.toBaseUnitAmount(5, decimals),
            );
        });
    });
});
