import { BigNumber, hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import { expect } from '@derivadex/test-utils';
import hardhat from 'hardhat';

import { NULL_BYTES32 } from '../fixtures';
import { TestLibCheckpointContract } from '../generated-wrappers';

describe('LibCheckpoint unit tests', () => {
    let accounts: string[];
    let chainId: number;
    let tester: TestLibCheckpointContract;
    let web3Wrapper: Web3Wrapper;

    const releaseHash = hexUtils.leftPad('0xdeadbeef');

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        chainId = await web3Wrapper.getChainIdAsync();
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });
        accounts = await web3Wrapper.getAvailableAddressesAsync();
        tester = await TestLibCheckpointContract.deployFrom0xArtifactAsync(
            artifacts.TestLibCheckpoint,
            provider,
            { from: accounts[0] },
            artifacts,
        );
        await tester.setCurrentReleaseHash(releaseHash).awaitTransactionSuccessAsync();
    });

    describe('#verifyConsensusRule', () => {
        let snapshotId: string;

        before(async () => {
            await tester.setQuorum(new BigNumber(2)).awaitTransactionSuccessAsync();
            await tester.setConsensusThreshold(new BigNumber(51)).awaitTransactionSuccessAsync();
        });

        beforeEach(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        afterEach(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it("fails if the signers don't reach the quorum", async () => {
            const validSignersCount = await tester.getValidSignersCount().callAsync();
            const tx = tester.verifyConsensusRule(new BigNumber(1), validSignersCount).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCheckpoint: Consensus was not reached.');
        });

        it("fails if the signers don't reach the consensus threshold", async () => {
            // Update the valid signers count.
            await tester.setValidSignersCount(new BigNumber(5)).awaitTransactionSuccessAsync();

            // Verify the consensus threshold.
            const validSignersCount = await tester.getValidSignersCount().callAsync();
            const tx = tester.verifyConsensusRule(new BigNumber(2), validSignersCount).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCheckpoint: Consensus was not reached.');
        });

        it('verifies the consensus threshold when the signers reach the quorum and the consensus threshold', async () => {
            // Update the valid signers count.
            await tester.setValidSignersCount(new BigNumber(5)).awaitTransactionSuccessAsync();

            // Verify the consensus threshold.
            const validSignersCount = await tester.getValidSignersCount().callAsync();
            await tester.verifyConsensusRule(new BigNumber(3), validSignersCount).awaitTransactionSuccessAsync();
        });
    });

    describe('#verifySubmission', () => {
        let snapshotId: string;

        const blockNumber = new BigNumber(1);
        const blockHash = hexUtils.leftPad('0xbeefdead');
        const epochId = new BigNumber(1);
        const stateRoot = hexUtils.leftPad('0xdeadbabe');
        const transactionRoot = hexUtils.leftPad('0xdeadfeed');

        interface Checkpoint {
            blockNumber: BigNumber;
            blockHash: string;
            stateRoot: string;
            transactionRoot: string;
        }

        function hashCheckpointData(checkpoint: Checkpoint, epochId: BigNumber): string {
            const hashContents = [
                tester.address,
                chainId,
                epochId,
                checkpoint.blockNumber,
                checkpoint.blockHash,
                checkpoint.stateRoot,
                checkpoint.transactionRoot,
            ];
            const hashContentsBlob = hexUtils.concat(...hashContents.map((content) => hexUtils.leftPad(content)));
            const checkpointHash = hexUtils.hash(hashContentsBlob);
            return checkpointHash;
        }

        async function signCheckpointAsync(
            checkpoint: Checkpoint,
            signer: string,
            epochId: BigNumber,
        ): Promise<string> {
            const checkpointHash = hashCheckpointData(checkpoint, epochId);
            const signature = await web3Wrapper.signMessageAsync(signer, checkpointHash);
            const vrsSignature = hexUtils.concat(hexUtils.slice(signature, 64), hexUtils.slice(signature, 0, 64));
            return vrsSignature;
        }

        function sortSignerAddresses(signerAddresses: string[]): string[] {
            return signerAddresses.sort((a, b) => {
                const numA = new BigNumber(a);
                const numB = new BigNumber(b);
                if (numA.lt(numB)) {
                    return -1;
                } else if (numA.eq(numB)) {
                    return 0;
                } else {
                    return 1;
                }
            });
        }

        before(async () => {
            // Set a minimum bond.
            await tester.setMinimumBond(new BigNumber(10)).awaitTransactionSuccessAsync();
        });

        beforeEach(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        afterEach(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it("fails if the submission doesn't contain any signatures", async () => {
            const tx = tester
                .verifySubmission(
                    {
                        checkpointData: {
                            blockNumber,
                            blockHash,
                            stateRoot,
                            transactionRoot,
                        },
                        signatures: [],
                    },
                    epochId,
                    releaseHash,
                    NULL_BYTES32,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCheckpoint: submission signatures must be non-empty.');
        });

        it('fails if the reference hash is the same as the submission hash', async () => {
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(checkpointData, epochId);
            const signature = await signCheckpointAsync(checkpointData, accounts[0], epochId);
            const tx = tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [signature],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                'LibCheckpoint: An invalid submission was the same as a valid submission.',
            );
        });

        it('fails if a duplicate signature is used', async () => {
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(checkpointData, epochId);
            const signature = await signCheckpointAsync(checkpointData, accounts[0], epochId);
            const tx = tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [signature, signature],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                'LibCheckpoint: An invalid submission was the same as a valid submission.',
            );
        });

        it("fails if the signatures aren't ordered", async () => {
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(checkpointData, epochId);
            const signers = sortSignerAddresses([accounts[0], accounts[1], accounts[2]]);
            const signatures = [
                await signCheckpointAsync(checkpointData, signers[1], epochId),
                await signCheckpointAsync(checkpointData, signers[0], epochId),
                await signCheckpointAsync(checkpointData, signers[2], epochId),
            ];
            const tx = tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures,
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                'LibCheckpoint: An invalid submission was the same as a valid submission.',
            );
        });

        it('fails if the signatures are ordered in descending order', async () => {
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(checkpointData, epochId);
            const signers = sortSignerAddresses([accounts[0], accounts[1], accounts[2]]);
            const signatures = [
                await signCheckpointAsync(checkpointData, signers[0], epochId),
                await signCheckpointAsync(checkpointData, signers[1], epochId),
                await signCheckpointAsync(checkpointData, signers[2], epochId),
            ];
            const tx = tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures,
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                'LibCheckpoint: An invalid submission was the same as a valid submission.',
            );
        });

        it('leaves a jailed custodian unchanged (referenceHash != 0)', async () => {
            // Set up a jailed custodian.
            await tester
                .setCustodian(
                    accounts[0],
                    {
                        approved: true,
                        balance: new BigNumber(10),
                        jailed: true,
                        unbondETA: new BigNumber(0),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[0], releaseHash, {
                    custodian: accounts[0],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[0], accounts[0]).awaitTransactionSuccessAsync();

            // Get any relevant state before verifying the submission.
            const validSignersCountBefore = await tester.getValidSignersCount().callAsync();

            // Verify the invalid submission.
            const referenceCheckpointData = {
                blockNumber: blockNumber.plus(1),
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(referenceCheckpointData, epochId);
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const { logs } = await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();

            // Ensure there were no logs emitted.
            expect(logs.length).to.be.eq(0);

            // Ensure the state wasn't changed.
            const validSignersCountAfter = await tester.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore);
            const custodian = await tester.getCustodian(accounts[0]).callAsync();
            expect(custodian.approved).to.be.eq(true);
            expect(custodian.balance).to.be.bignumber.eq(10);
            expect(custodian.jailed).to.be.eq(true);
            expect(custodian.unbondETA).to.be.bignumber.eq(0);
        });

        it('jails a custodian with a valid signer (referenceHash != 0)', async () => {
            // Set up a custodian with a valid signer.
            await tester
                .setCustodian(
                    accounts[0],
                    {
                        approved: true,
                        balance: new BigNumber(10),
                        jailed: false,
                        unbondETA: new BigNumber(0),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[0], releaseHash, {
                    custodian: accounts[0],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[0], accounts[0]).awaitTransactionSuccessAsync();

            // Get any relevant state before verifying the submission.
            const validSignersCountBefore = await tester.getValidSignersCount().callAsync();

            // Verify the invalid submission.
            const referenceCheckpointData = {
                blockNumber: blockNumber.plus(1),
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(referenceCheckpointData, epochId);
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const [jailedCustodians, bonds] = await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .callAsync();
            await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();

            // Ensure the correct bonds were returned.
            expect(bonds).to.be.deep.eq([]);

            // Ensure the correct jailed custodians were returned.
            expect(jailedCustodians).to.be.deep.eq([accounts[0]]);

            // Ensure the state was updated correctly.
            const validSignersCountAfter = await tester.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore.minus(1));
            const custodian = await tester.getCustodian(accounts[0]).callAsync();
            expect(custodian.approved).to.be.eq(true);
            expect(custodian.balance).to.be.bignumber.eq(10);
            expect(custodian.jailed).to.be.eq(true);
            expect(custodian.unbondETA).to.be.bignumber.eq(0);
        });

        it('jails a custodian that has re-registered their signer (referenceHash != 0)', async () => {
            // Set up a custodian with a valid signer.
            await tester
                .setCustodian(
                    accounts[0],
                    {
                        approved: true,
                        balance: new BigNumber(10),
                        jailed: false,
                        unbondETA: new BigNumber(0),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[0], releaseHash, {
                    custodian: accounts[0],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[0], accounts[1]).awaitTransactionSuccessAsync();

            // Get any relevant state before verifying the submission.
            const validSignersCountBefore = await tester.getValidSignersCount().callAsync();

            // Verify the invalid submission.
            const referenceCheckpointData = {
                blockNumber: blockNumber.plus(1),
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(referenceCheckpointData, epochId);
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const [jailedCustodians, bonds] = await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .callAsync();
            await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();

            // Ensure the correct bonds were returned.
            expect(bonds).to.be.deep.eq([]);

            // Ensure the correct jailed custodians were returned.
            expect(jailedCustodians).to.be.deep.eq([accounts[0]]);

            // Ensure the state was updated correctly.
            const validSignersCountAfter = await tester.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore.minus(1));
            const custodian = await tester.getCustodian(accounts[0]).callAsync();
            expect(custodian.approved).to.be.eq(true);
            expect(custodian.balance).to.be.bignumber.eq(10);
            expect(custodian.jailed).to.be.eq(true);
            expect(custodian.unbondETA).to.be.bignumber.eq(0);
        });

        it("jails a custodian that didn't meet the required bond (referenceHash != 0)", async () => {
            // Set up a custodian with a valid signer.
            await tester
                .setCustodian(
                    accounts[0],
                    {
                        approved: true,
                        balance: new BigNumber(9),
                        jailed: false,
                        unbondETA: new BigNumber(0),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[0], releaseHash, {
                    custodian: accounts[0],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[0], accounts[0]).awaitTransactionSuccessAsync();

            // Get any relevant state before verifying the submission.
            const validSignersCountBefore = await tester.getValidSignersCount().callAsync();

            // Verify the invalid submission.
            const referenceCheckpointData = {
                blockNumber: blockNumber.plus(1),
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(referenceCheckpointData, epochId);
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const [jailedCustodians, bonds] = await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .callAsync();
            await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();

            // Ensure the correct bonds were returned.
            expect(bonds).to.be.deep.eq([]);

            // Ensure the correct jailed custodians were returned.
            expect(jailedCustodians).to.be.deep.eq([accounts[0]]);

            // Ensure the state was updated correctly.
            const validSignersCountAfter = await tester.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore);
            const custodian = await tester.getCustodian(accounts[0]).callAsync();
            expect(custodian.approved).to.be.eq(true);
            expect(custodian.balance).to.be.bignumber.eq(9);
            expect(custodian.jailed).to.be.eq(true);
            expect(custodian.unbondETA).to.be.bignumber.eq(0);
        });

        it('jails a custodian that has prepared to unbond (referenceHash != 0)', async () => {
            // Set up a custodian with a valid signer.
            await tester
                .setCustodian(
                    accounts[0],
                    {
                        approved: true,
                        balance: new BigNumber(10),
                        jailed: false,
                        unbondETA: new BigNumber(1),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[0], releaseHash, {
                    custodian: accounts[0],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[0], accounts[0]).awaitTransactionSuccessAsync();

            // Get any relevant state before verifying the submission.
            const validSignersCountBefore = await tester.getValidSignersCount().callAsync();

            // Verify the invalid submission.
            const referenceCheckpointData = {
                blockNumber: blockNumber.plus(1),
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const referenceHash = hashCheckpointData(referenceCheckpointData, epochId);
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const [jailedCustodians, bonds] = await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .callAsync();
            await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
                    },
                    epochId,
                    releaseHash,
                    referenceHash,
                )
                .awaitTransactionSuccessAsync();

            // Ensure the correct bonds were returned.
            expect(bonds).to.be.deep.eq([]);

            // Ensure the correct jailed custodians were returned.
            expect(jailedCustodians).to.be.deep.eq([accounts[0]]);

            // Ensure the state was updated correctly.
            const validSignersCountAfter = await tester.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore);
            const custodian = await tester.getCustodian(accounts[0]).callAsync();
            expect(custodian.approved).to.be.eq(true);
            expect(custodian.balance).to.be.bignumber.eq(10);
            expect(custodian.jailed).to.be.eq(true);
            expect(custodian.unbondETA).to.be.bignumber.eq(1);
        });

        it('returns the list of valid signers (referenceHash == 0)', async () => {
            // Sets up several custodians with signers. Only some of these will
            // have valid signers, and the rest should be filtered out of the
            // signer list that is returned.
            await tester
                .setCustodian(
                    accounts[0],
                    {
                        approved: true,
                        balance: new BigNumber(10),
                        jailed: false,
                        unbondETA: new BigNumber(0),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[0], releaseHash, {
                    custodian: accounts[0],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[0], accounts[0]).awaitTransactionSuccessAsync();
            await tester
                .setCustodian(
                    accounts[1],
                    {
                        approved: true,
                        balance: new BigNumber(10),
                        jailed: false,
                        unbondETA: new BigNumber(1),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[1], accounts[1]).awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[1], releaseHash, {
                    custodian: accounts[1],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester
                .setCustodian(
                    accounts[2],
                    {
                        approved: true,
                        balance: new BigNumber(5),
                        jailed: false,
                        unbondETA: new BigNumber(0),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[2], releaseHash, {
                    custodian: accounts[2],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[2], accounts[2]).awaitTransactionSuccessAsync();
            await tester
                .setCustodian(
                    accounts[3],
                    {
                        approved: true,
                        balance: new BigNumber(10),
                        jailed: true,
                        unbondETA: new BigNumber(0),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[3], releaseHash, {
                    custodian: accounts[3],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[3], accounts[3]).awaitTransactionSuccessAsync();
            await tester
                .setCustodian(
                    accounts[4],
                    {
                        approved: true,
                        balance: new BigNumber(10),
                        jailed: false,
                        unbondETA: new BigNumber(0),
                    },
                    true,
                )
                .awaitTransactionSuccessAsync();
            await tester
                .setSigner(accounts[4], releaseHash, {
                    custodian: accounts[4],
                    report: '0xdeadbeef',
                })
                .awaitTransactionSuccessAsync();
            await tester.setCustodianToSigners(accounts[4], accounts[4]).awaitTransactionSuccessAsync();

            // Get any relevant state before verifying the submission.
            const validSignersCountBefore = await tester.getValidSignersCount().callAsync();

            // Verify the submission.
            const checkpointData = {
                blockNumber,
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const signers = sortSignerAddresses([accounts[0], accounts[1], accounts[2], accounts[3], accounts[4]]);
            const signatures = [
                await signCheckpointAsync(checkpointData, signers[0], epochId),
                await signCheckpointAsync(checkpointData, signers[1], epochId),
                await signCheckpointAsync(checkpointData, signers[2], epochId),
                await signCheckpointAsync(checkpointData, signers[3], epochId),
                await signCheckpointAsync(checkpointData, signers[4], epochId),
            ];
            const [validSigners, validBonds] = await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures,
                    },
                    epochId,
                    releaseHash,
                    NULL_BYTES32,
                )
                .callAsync();
            await tester
                .verifySubmission(
                    {
                        checkpointData,
                        signatures,
                    },
                    epochId,
                    releaseHash,
                    NULL_BYTES32,
                )
                .awaitTransactionSuccessAsync();

            // Ensure the correct bonds were returned.
            expect(validBonds).to.be.deep.eq([new BigNumber(10), new BigNumber(10)]);

            // Ensure that the correct list of signers was returned.
            expect(validSigners).to.be.deep.eq(sortSignerAddresses([accounts[0], accounts[4]]));

            // Ensure that the state wasn't updated.
            const validSignersCountAfter = await tester.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.deep.eq(validSignersCountBefore);
        });
    });
});
