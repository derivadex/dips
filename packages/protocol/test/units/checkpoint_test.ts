import { NULL_BYTES } from '@0x/utils';
import { BigNumber, hexUtils, Numberish } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import {
    CheckpointCheckpointedEventArgs,
    CheckpointCheckpointInitializedEventArgs,
    CheckpointConsensusThresholdSetEventArgs,
    CheckpointQuorumSetEventArgs,
    CustodianCustodiansJailedEventArgs,
    Derivadex,
    RejectRejectedEventArgs,
    TestCheckpointContract,
    TestRegistrationContract,
} from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, generateCallData, getSelectors } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { FacetCutAction } from '@derivadex/types';
import { LogWithDecodedArgs, TransactionReceiptWithDecodedLogs } from 'ethereum-types';
import { ContractArtifact } from 'ethereum-types';
import * as hardhat from 'hardhat';
import _ from 'lodash';

import { setupWithProviderAsync } from '../../deployment/setup';
import { TRADE_MINING_EPOCH_MULTIPLIER, TRADE_MINING_LENGTH, TRADE_MINING_REWARD_PER_EPOCH } from '../constants';
import { Fixtures, NULL_BYTES32, ZERO_ADDRESS } from '../fixtures';
import { passProposalAsync, setIsPausedAsync, TestContext } from '../utils';

const OPERATOR_MINIMUM_BOND = new BigNumber(2);

const artifactAbis = _.mapValues(artifacts, (artifact: ContractArtifact) => artifact.compilerOutput.abi);

interface Checkpoint {
    blockNumber: BigNumber;
    blockHash: string;
    stateRoot: string;
    transactionRoot: string;
}

interface CheckpointSubmission {
    signatures: string[];
    checkpointData: Checkpoint;
}

describe('Checkpoint unit test', () => {
    let web3Wrapper: Web3Wrapper;
    let chainId: Numberish;

    let derivadex: Derivadex;
    let registration: TestRegistrationContract;
    let checkpoint: TestCheckpointContract;
    let accounts: string[];
    let owner: string;
    let fixtures: Fixtures;
    let proposalCount: number;
    let ctx: TestContext;

    const mrEnclave = hexUtils.leftPad('0xdeadbeef');
    const isvSvn = '0x0000';
    const signature = '0xdeadbabe';

    function encodeRAReport(
        quoteStatus: string,
        mrEnclave: string,
        isvSvn: string,
        custodian: string,
        signer: string,
    ): string {
        const quoteBody = Buffer.from(
            hexUtils
                .concat(
                    hexUtils.leftPad('0x0', 112),
                    mrEnclave,
                    hexUtils.leftPad('0x0', 162),
                    isvSvn,
                    hexUtils.leftPad('0x0', 60),
                    signer,
                    hexUtils.rightPad(custodian, 44),
                )
                .slice(2),
            'hex',
        ).toString('base64');
        const report = `{"id":"313676330620876491165606016975323788285","timestamp":"2019-04-16T18:26:24.798739","version":3,"isvEnclaveQuoteStatus":"${quoteStatus}","platformInfoBlob":"1502006504000E00000808020401010000000000000000000008000009000000020000000000000AF2A5ABAAA8988CDD680D705C023EA95EC943616ED98CB7789F6052C8AECBA5BABFC1E07A6953D3B7D85B3B53E3170ADD0930B9D110F0A765728F60237DD22325CF","isvEnclaveQuoteBody":"${quoteBody}"}`;
        return `0x${Array.from(report)
            .map((c) => c.charCodeAt(0).toString(16))
            .join('')}`;
    }

    function hashRegistrationMetadata(mrEnclave: string, isvSvn: string): string {
        return hexUtils.hash(hexUtils.concat(mrEnclave, hexUtils.rightPad(isvSvn, 32)));
    }

    async function getBlockInfoAsync(blockNumber: number): Promise<string> {
        const block = await web3Wrapper.getBlockIfExistsAsync(blockNumber);
        if (block === undefined || block.hash === null) {
            throw new Error('Block and block hash should never be undefined or null.');
        }
        return block.hash;
    }

    async function getRandomBlockInfoAsync(): Promise<{
        blockNumber: BigNumber;
        blockHash: string;
    }> {
        const latestBlockNumber = await web3Wrapper.getBlockNumberAsync();
        const blockNumber = latestBlockNumber - Math.round(Math.random() * 127) - 1;
        const block = await web3Wrapper.getBlockIfExistsAsync(blockNumber);
        if (block === undefined || block.hash === null) {
            throw new Error('Block and block hash should never be undefined or null.');
        }
        return { blockNumber: new BigNumber(blockNumber), blockHash: block.hash };
    }

    async function getLatestBlockInfoAsync(): Promise<{
        blockNumber: BigNumber;
        blockHash: string;
    }> {
        let blockHash;
        let [blockNumber] = await checkpoint.getLatestCheckpoint().callAsync();
        if (blockNumber.isZero()) {
            ({ blockNumber, blockHash } = await getRandomBlockInfoAsync());
        } else {
            blockNumber = blockNumber.plus(1);
            blockHash = await getBlockInfoAsync(blockNumber.toNumber());
        }
        return { blockHash, blockNumber };
    }

    function hashCheckpointData(checkpoint: Checkpoint, epochId: BigNumber, verifyingContractAddress?: string): string {
        const hashContents = [
            verifyingContractAddress || derivadex.derivaDEXContract.address,
            chainId,
            epochId,
            checkpoint.blockNumber,
            checkpoint.blockHash,
            checkpoint.stateRoot,
            checkpoint.transactionRoot,
        ];
        const hashContentsBlob = hexUtils.concat(...hashContents.map((content) => hexUtils.leftPad(content)));
        const checkpointHash = hexUtils.hash(hashContentsBlob);
        return checkpointHash;
    }

    function sortSignerAddresses(signerAddresses: string[]): string[] {
        return signerAddresses.sort((a, b) => {
            const numA = new BigNumber(a);
            const numB = new BigNumber(b);
            if (numA.lt(numB)) {
                return -1;
            } else if (numA.eq(numB)) {
                return 0;
            } else {
                return 1;
            }
        });
    }

    function sortSubmissions(submissions: CheckpointSubmission[], epochId: BigNumber): CheckpointSubmission[] {
        return _.unzip(
            _.zip(
                submissions.map((submission) => hashCheckpointData(submission.checkpointData, epochId)),
                submissions,
            ).sort((a, b) => {
                const numA = new BigNumber(a[0] || ZERO_ADDRESS);
                const numB = new BigNumber(b[0] || ZERO_ADDRESS);
                if (numA.lt(numB)) {
                    return -1;
                } else if (numA.eq(numB)) {
                    return 0;
                } else {
                    return 1;
                }
            }),
        )[1] as CheckpointSubmission[];
    }

    function getSortedSignaturesAndIndexesForSubmissions(
        sortedSubmissions: CheckpointSubmission[],
        // Note: the mapping of signers to signatures is probably unnecessary
        // since we should be able to retrieve the signer from the signature
        // using ethers.utils.verifyMessage, but this doesn't seem to work at
        // the moment so providing the explicit mapping to move forward
        signaturesAndSigners: Array<{ signature: string; signer: string }>,
    ): {
        signatures: string[];
        indexes: Array<{ submissionIndex: BigNumber; signatureIndex: BigNumber }>;
    } {
        const sortedSignatures = signaturesAndSigners
            .sort((a, b) => {
                const numA = new BigNumber(a.signer);
                const numB = new BigNumber(b.signer);
                if (numA.lt(numB)) {
                    return -1;
                } else if (numA.eq(numB)) {
                    return 0;
                } else {
                    return 1;
                }
            })
            .map((signaturesAndSigner) => signaturesAndSigner.signature);

        const indexes = sortedSignatures.map((signature) => {
            const submissionIndex = sortedSubmissions.findIndex((submission) =>
                submission.signatures.includes(signature),
            );
            return {
                submissionIndex: new BigNumber(submissionIndex),
                signatureIndex: new BigNumber(
                    sortedSubmissions[submissionIndex].signatures.findIndex((s) => s === signature),
                ),
            };
        });
        return {
            signatures: sortedSignatures,
            indexes,
        };
    }

    async function signCheckpointAsync(
        checkpoint: Checkpoint,
        signer: string,
        epochId: BigNumber,
        verifyingContractAddress?: string,
    ): Promise<string> {
        const checkpointHash = hashCheckpointData(checkpoint, epochId, verifyingContractAddress);
        const signature = await web3Wrapper.signMessageAsync(signer, checkpointHash);
        const vrsSignature = hexUtils.concat(hexUtils.slice(signature, 64), hexUtils.slice(signature, 0, 64));
        return vrsSignature;
    }

    async function prepareSubmissionAsync(
        checkpoint: Checkpoint,
        signers: string[],
        epochId: BigNumber,
    ): Promise<CheckpointSubmission> {
        const signatures = [];
        for (const signer of sortSignerAddresses(signers)) {
            signatures.push(await signCheckpointAsync(checkpoint, signer, epochId));
        }
        return {
            checkpointData: checkpoint,
            signatures,
        };
    }

    async function bondAsync(custodians: string[], bond: BigNumber = OPERATOR_MINIMUM_BOND): Promise<void> {
        for (const custodian of custodians) {
            await derivadex.ddxContract
                .transfer(custodian, bond)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            await derivadex.ddxContract
                .approve(derivadex.derivaDEXContract.address, bond)
                .awaitTransactionSuccessAsync({ from: custodian });
            await derivadex.custodianContract.bond(bond).awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    async function jailAsync(custodians: string[]): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.custodianContract.getFunctionSignature('jail')];
        const calldatas = [
            generateCallData(derivadex.custodianContract.jail(custodians).getABIEncodedTransactionData()),
        ];
        const description = 'Jail a set of custodians.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function prepareUnbondAsync(custodians: string[]): Promise<void> {
        for (const custodian of custodians) {
            await derivadex.custodianContract.prepareUnbond().awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    async function registerAsync(custodians: string[], mrEnclave: string, isvSvn: string): Promise<void> {
        for (const custodian of custodians) {
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, custodian);
            await registration.register(report, signature).awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    async function setConsensusThresholdAsync(
        consensusThreshold: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [checkpoint.getFunctionSignature('setConsensusThreshold')];
        const calldatas = [
            generateCallData(checkpoint.setConsensusThreshold(consensusThreshold).getABIEncodedTransactionData()),
        ];
        const description = `Update consensus threshold to ${consensusThreshold}.`;
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function setQuorumAsync(quorum: BigNumber): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [checkpoint.getFunctionSignature('setQuorum')];
        const calldatas = [generateCallData(checkpoint.setQuorum(quorum).getABIEncodedTransactionData())];
        const description = `Update quorum to ${quorum}.`;
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function updateReleaseScheduleAsync(
        releaseMetadata: { mrEnclave: string; isvSvn: string },
        startingEpochId: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.registrationContract.getFunctionSignature('updateReleaseSchedule')];
        const calldatas = [
            generateCallData(
                derivadex.registrationContract
                    .updateReleaseSchedule(releaseMetadata, startingEpochId)
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Update the release schedule.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        chainId = await web3Wrapper.getChainIdAsync();
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });

        ({ derivadex, accounts, owner } = await setupWithProviderAsync(provider, {
            isFork: false,
            isGanache: true,
            isLocalSnapshot: false,
            isTest: true,
            useDeployedDDXToken: false,
        }));
        fixtures = new Fixtures(derivadex, accounts, owner);
        ctx = { derivadex, fixtures };

        await derivadex
            .transferOwnershipToDerivaDEXProxyDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        // Add Pause facet to the diamond
        const pauseSelectors = getSelectors(derivadex.pauseContract);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.pauseContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: pauseSelectors,
                    },
                ],
                derivadex.pauseContract.address,
                derivadex.initializePause().getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setPauseAddressToProxy();

        const governanceSelectors = getSelectors(derivadex.governanceContract, ['initialize']);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.governanceContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: governanceSelectors,
                    },
                ],
                derivadex.governanceContract.address,
                derivadex.initializeGovernance(10, 1, 17280, 1209600, 259200, 4, 1, 50).getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setGovernanceAddressToProxy();
        await derivadex.transferOwnershipToSelf().awaitTransactionSuccessAsync({ from: owner });
        registration = new TestRegistrationContract(
            derivadex.derivaDEXContract.address,
            derivadex.providerEngine,
            { from: fixtures.derivaDEXWallet() },
            artifactAbis,
        );
        checkpoint = new TestCheckpointContract(
            derivadex.derivaDEXContract.address,
            derivadex.providerEngine,
            { from: fixtures.derivaDEXWallet() },
            artifactAbis,
        );

        const initialBanList = [accounts[accounts.length - 2]];
        let selectors = getSelectors(derivadex.bannerContract, ['initialize']);
        let targets = [derivadex.derivaDEXContract.address];
        let values = [0];
        let signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        let calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.bannerContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.bannerContract.address,
                        derivadex.bannerContract.initialize(initialBanList).getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        let description = 'Add Banner contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        let proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount);
        derivadex.setBannerAddressToProxy();

        // Add the Collateral facet.
        selectors = getSelectors(derivadex.collateralContract, ['initialize']);
        targets = [derivadex.derivaDEXContract.address];
        values = [0];
        signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.collateralContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.collateralContract.address,
                        derivadex.collateralContract
                            .initialize(
                                new BigNumber(50),
                                new BigNumber(10),
                                new BigNumber(1_000),
                                Web3Wrapper.toBaseUnitAmount(1_000, 6),
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        description = 'Add Collateral contract as a facet.';

        console.log('Adding the collateral facet');
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount);
        derivadex.setCollateralAddressToProxy();

        // Add the Stake facet.
        selectors = getSelectors(derivadex.stakeContract, ['initialize']);
        targets = [derivadex.derivaDEXContract.address];
        values = [0];
        signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.stakeContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.stakeContract.address,
                        derivadex.stakeContract
                            .initialize(
                                Web3Wrapper.toBaseUnitAmount(10_000, 18),
                                Web3Wrapper.toBaseUnitAmount(40_000_000, 18),
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        description = 'Add Stake contract as a facet.';

        console.log('Adding the stake facet');
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount);
        derivadex.setStakeAddressToProxy();

        const custodianSelectors = getSelectors(derivadex.custodianContract, ['initialize']);
        const custodianTargets = [derivadex.derivaDEXContract.address];
        const custodianValues = [0];
        const custodianSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const custodianCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.custodianContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: custodianSelectors,
                            },
                        ],
                        derivadex.custodianContract.address,
                        derivadex.custodianContract
                            .initialize(OPERATOR_MINIMUM_BOND, new BigNumber(200))
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const custodianDescription = 'Add Custodian contract as a facet.';
        await derivadex
            .propose(custodianTargets, custodianValues, custodianSignatures, custodianCalldatas, custodianDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount++);
        derivadex.setCustodianAddressToProxy();

        const initialCustodians = accounts.slice(0, 20);
        const registrationSelectors = getSelectors(registration, ['initialize']);
        const registrationTargets = [derivadex.derivaDEXContract.address];
        const registrationValues = [0];
        const registrationSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const registrationCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.registrationContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: registrationSelectors,
                            },
                        ],
                        derivadex.registrationContract.address,
                        // Set the consensus threshold to 51, the quorum to two,
                        // and initial custodians to approve.
                        derivadex.registrationContract
                            .initialize({ mrEnclave, isvSvn }, initialCustodians)
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const registrationDescription = 'Add Registration contract as a facet.';
        await derivadex
            .propose(
                registrationTargets,
                registrationValues,
                registrationSignatures,
                registrationCalldatas,
                registrationDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount++);
        derivadex.setRegistrationAddressToProxy();

        // Advance 257 blocks to ensure the success of our tests that rely on
        // the existence of old blocks.
        await advanceBlocksAsync(derivadex.providerEngine, 257);
    });

    it('adds Checkpoint facet', async () => {
        const selectors = getSelectors(checkpoint, ['initialize']);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.checkpointContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.checkpointContract.address,
                        // Set the consensus threshold to 51, the quorum to two,
                        // and initial custodians to approve.
                        derivadex.checkpointContract
                            .initialize(new BigNumber(51), new BigNumber(2))
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add Checkpoint contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        const { logs } = await passProposalAsync(ctx, proposalCount++);
        derivadex.setCheckpointAddressToProxy();

        // Ensure that the correct CheckpointInitialized event was emitted.
        expect(logs.length).to.be.eq(4);
        const log = logs[1] as LogWithDecodedArgs<CheckpointCheckpointInitializedEventArgs>;
        expect(log.event).to.be.eq('CheckpointInitialized');
        expect(log.args.consensusThreshold).to.be.bignumber.eq(51);
        expect(log.args.quorum).to.be.bignumber.eq(2);

        const [consensusThreshold, quorum] = await checkpoint.getCheckpointInfo().callAsync();
        expect(consensusThreshold).to.be.bignumber.eq(51);
        expect(quorum).to.be.bignumber.eq(2);
    });

    it('adds Reject facet', async () => {
        const selectors = getSelectors(derivadex.rejectContract);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.rejectContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        ZERO_ADDRESS,
                        NULL_BYTES,
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add Reject contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount++);
        derivadex.setRejectAddressToProxy();
    });

    describe('#setConsensusThreshold', () => {
        let snapshotId: string;

        async function assertSuccessfulConsensusThresholdSetAsync(consensusThreshold: number): Promise<void> {
            const { logs } = await setConsensusThresholdAsync(new BigNumber(consensusThreshold));

            // Ensure the correct logs were emitted.
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<CheckpointConsensusThresholdSetEventArgs>;
            expect(log.event).to.be.eq('ConsensusThresholdSet');
            expect(log.args.consensusThreshold).to.be.bignumber.eq(consensusThreshold);

            // Ensure the state was updated correctly.
            const [consensusThreshold_] = await checkpoint.getCheckpointInfo().callAsync();
            expect(consensusThreshold_).to.be.bignumber.eq(consensusThreshold);
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to set consensus threshold to number n (n < 51)', async () => {
            return expect(setConsensusThresholdAsync(new BigNumber(50))).to.be.rejectedWith(
                'Governance: transaction execution reverted.',
            );
        });

        it('fails to set consensus threshold to number n (n > 100)', async () => {
            return expect(setConsensusThresholdAsync(new BigNumber(150))).to.be.rejectedWith(
                'Governance: transaction execution reverted.',
            );
        });

        it('successfully sets consensus threshold to number n (n == 100)', async () => {
            await assertSuccessfulConsensusThresholdSetAsync(100);
        });

        it('successfully sets consensus threshold to number n (51 < n < 100)', async () => {
            await assertSuccessfulConsensusThresholdSetAsync(75);
        });
    });

    describe('#setQuorum', () => {
        let snapshotId: string;

        async function assertSuccessfulQuorumSetAsync(quorum: number): Promise<void> {
            const { logs } = await setQuorumAsync(new BigNumber(quorum));

            // Ensure that the correct logs were emitted.
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<CheckpointQuorumSetEventArgs>;
            expect(log.event).to.be.eq('QuorumSet');
            expect(log.args.quorum).to.be.bignumber.eq(quorum);

            // Ensure the state was updated correctly.
            const [, quorum_] = await checkpoint.getCheckpointInfo().callAsync();
            expect(quorum_).to.be.bignumber.eq(quorum);
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to set quorum to number n (n < 2)', async () => {
            const tx = setQuorumAsync(new BigNumber(1));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('successfully sets quorum to number n (n > 2)', async () => {
            await assertSuccessfulQuorumSetAsync(5);
        });

        it('successfully sets quorum to number n (n == 2)', async () => {
            await assertSuccessfulQuorumSetAsync(2);
        });
    });

    describe('#checkpoint', () => {
        let snapshotId: string;

        const newMrEnclave = hexUtils.leftPad('0xf00d');
        const newIsvSvn = '0x0000';

        async function assertSuccessfulCheckpointAsync(testCase: {
            majorityCheckpointData: Checkpoint;
            majoritySigners: string[];
            minorityCheckpointData: Checkpoint[];
            minoritySigners: string[][];
            epochId: BigNumber;
            releaseHash: string;
            ddxWithdrawalAllowanceDelta: BigNumber;
        }): Promise<void> {
            if (testCase.minorityCheckpointData.length !== testCase.minoritySigners.length) {
                throw new Error('Minority data input parity mismatch.');
            }

            // Get state relating to the majority signers before submitting
            // the checkpoint.
            const sortedMajoritySigners = sortSignerAddresses(testCase.majoritySigners);
            const majorityCustodians = (
                await derivadex.custodianContract
                    .getSignerStatesByReleaseHash(sortedMajoritySigners, testCase.releaseHash)
                    .callAsync()
            ).map((signer) => signer.custodian);
            const majorityCustodianStatesBefore = await derivadex.custodianContract
                .getCustodianStates(majorityCustodians)
                .callAsync();
            const validBonds = [];
            const validCustodians = [];
            for (let i = 0; i < sortedMajoritySigners.length; i++) {
                if (
                    majorityCustodianStatesBefore[i].approved &&
                    majorityCustodianStatesBefore[i].balance >= OPERATOR_MINIMUM_BOND &&
                    !majorityCustodianStatesBefore[i].jailed &&
                    majorityCustodianStatesBefore[i].unbondETA.eq(0)
                ) {
                    validBonds.push(majorityCustodianStatesBefore[i].balance);
                    validCustodians.push(majorityCustodians[i]);
                }
            }

            // Get state relating to the minority signers before submitting
            // the checkpoint.
            const flattenedMinoritySigners = _.uniq(
                testCase.minoritySigners.reduce((acc, signers) => acc.concat(sortSignerAddresses(signers)), []),
            );
            const minorityCustodians = (
                await derivadex.custodianContract
                    .getSignerStatesByReleaseHash(flattenedMinoritySigners, testCase.releaseHash)
                    .callAsync()
            ).map((signer) => signer.custodian);
            const minorityCustodianStatesBefore = await derivadex.custodianContract
                .getCustodianStates(minorityCustodians)
                .callAsync();
            const jailedCustodians = [];
            for (let i = 0; i < minorityCustodians.length; i++) {
                if (minorityCustodianStatesBefore[i].approved && !minorityCustodianStatesBefore[i].jailed) {
                    jailedCustodians.push(minorityCustodians[i]);
                }
            }

            // Get the state relating to the DDX withdrawal allowance before
            // submitting the checkpoint.
            const ddxWithdrawalAllowanceBefore = await derivadex.collateralContract
                .getWithdrawalAllowance(ZERO_ADDRESS)
                .callAsync();

            // Submit the checkpoint.
            const majoritySubmission = await prepareSubmissionAsync(
                testCase.majorityCheckpointData,
                sortSignerAddresses(testCase.majoritySigners),
                testCase.epochId,
            );
            const minoritySubmissions = [];
            for (let i = 0; i < testCase.minorityCheckpointData.length; i++) {
                minoritySubmissions.push(
                    await prepareSubmissionAsync(
                        testCase.minorityCheckpointData[i],
                        testCase.minoritySigners[i],
                        testCase.epochId,
                    ),
                );
            }
            const { logs } = await checkpoint
                .checkpoint(majoritySubmission, minoritySubmissions, testCase.epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

            // Ensure that the correct logs were emitted.
            expect(logs.length).to.be.eq(jailedCustodians.length > 0 ? 2 : 1);
            if (jailedCustodians.length > 0) {
                const jailedLog = logs[0] as LogWithDecodedArgs<CustodianCustodiansJailedEventArgs>;
                expect(jailedLog.event).to.be.eq('CustodiansJailed');
                expect(jailedLog.args).to.be.deep.eq({
                    custodians: jailedCustodians,
                });
            }
            const checkpointedLog = logs[logs.length - 1] as LogWithDecodedArgs<CheckpointCheckpointedEventArgs>;
            expect(checkpointedLog.event).to.be.eq('Checkpointed');
            expect(checkpointedLog.args).to.be.deep.eq({
                stateRoot: testCase.majorityCheckpointData.stateRoot,
                transactionRoot: testCase.majorityCheckpointData.transactionRoot,
                epochId: testCase.epochId,
                custodians: validCustodians,
                bonds: validBonds,
                submitter: fixtures.derivaDEXWallet(),
            });

            // Ensure the valid signers state was updated correctly.
            let expectedValidSignersCount = 0;
            const registeredCustodians = await registration.getRegisteredCustodians().callAsync();
            const registeredSigners = await derivadex.custodianContract
                .getSignersForCustodians(registeredCustodians)
                .callAsync();
            const registeredCustodianStates = await derivadex.custodianContract
                .getCustodianStates(registeredCustodians)
                .callAsync();
            for (let i = 0; i < registeredCustodians.length; i++) {
                if (
                    registeredSigners[i] !== ZERO_ADDRESS &&
                    registeredCustodianStates[i].approved &&
                    registeredCustodianStates[i].balance.gte(OPERATOR_MINIMUM_BOND) &&
                    !registeredCustodianStates[i].jailed &&
                    registeredCustodianStates[i].unbondETA.eq(0)
                ) {
                    expectedValidSignersCount++;
                }
            }
            const validSignersCount = await derivadex.custodianContract.getValidSignersCount().callAsync();
            expect(validSignersCount).to.be.bignumber.eq(expectedValidSignersCount);

            // Ensure the release schedule was advanced correctly.
            const [currentReleaseHash] = await registration.getCurrentReleaseInfo().callAsync();
            expect(currentReleaseHash).to.be.eq(testCase.releaseHash);

            // Ensure the checkpoint state was updated correctly.
            const [actualBlockNumber, actualStateRoot, actualTransactionRoot, actualCurrentEpochId] = await checkpoint
                .getLatestCheckpoint()
                .callAsync();
            expect(actualBlockNumber).to.be.bignumber.eq(testCase.majorityCheckpointData.blockNumber);
            expect(actualStateRoot).to.be.eq(testCase.majorityCheckpointData.stateRoot);
            expect(actualTransactionRoot).to.be.eq(testCase.majorityCheckpointData.transactionRoot);
            expect(actualCurrentEpochId).to.be.bignumber.eq(testCase.epochId);

            // Ensure the correct custodians were jailed.
            let i = 0;
            const minorityCustodianStatesAfter = await derivadex.custodianContract
                .getCustodianStates(minorityCustodians)
                .callAsync();
            for (let j = 0; j < minorityCustodians.length; j++) {
                if (i < jailedCustodians.length && jailedCustodians[i] === minorityCustodians[j]) {
                    expect(minorityCustodianStatesAfter[j].approved).to.be.eq(true);
                    expect(minorityCustodianStatesAfter[j].balance).to.be.bignumber.eq(
                        minorityCustodianStatesBefore[j].balance,
                    );
                    expect(minorityCustodianStatesAfter[j].jailed).to.be.eq(true);
                    expect(minorityCustodianStatesAfter[j].unbondETA).to.be.bignumber.eq(
                        minorityCustodianStatesBefore[j].unbondETA,
                    );
                    i++;
                } else {
                    expect(minorityCustodianStatesAfter[j]).to.be.deep.eq(minorityCustodianStatesBefore[j]);
                }
            }

            // Ensure the DDX withdrawal allowance was updated correctly.
            const ddxWithdrawalAllowanceAfter = await derivadex.collateralContract
                .getWithdrawalAllowance(ZERO_ADDRESS)
                .callAsync();
            expect(ddxWithdrawalAllowanceAfter).to.be.bignumber.eq(
                ddxWithdrawalAllowanceBefore.plus(testCase.ddxWithdrawalAllowanceDelta),
            );
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Advances the current epoch ID a few epochs for the epoch ID
            // failure tests.
            await checkpoint.setEpochId(new BigNumber(1)).awaitTransactionSuccessAsync();

            // Advances the latest confirmed block a few blocks for the latest
            // confirmed block failure tests.
            await checkpoint.setLastConfirmedBlockNumber(new BigNumber(1)).awaitTransactionSuccessAsync();

            // Register some signers.
            await registerAsync(accounts.slice(0, 9), mrEnclave, isvSvn);

            // Bond some custodians.
            await bondAsync(accounts.slice(0, 8).concat(accounts.slice(9, 15)));
            await bondAsync(accounts.slice(0, 3), OPERATOR_MINIMUM_BOND.times(3));

            // Prepare to unbond some custodians.
            await prepareUnbondAsync([accounts[7]]);

            // Jail some custodians.
            await jailAsync([accounts[6]]);

            // Schedules a release to start in epoch 7.
            await updateReleaseScheduleAsync(
                {
                    mrEnclave: hexUtils.leftPad('0xbad'),
                    isvSvn: '0x0000',
                },
                new BigNumber(7),
            );

            // Schedules a release to start in epoch 8.
            await updateReleaseScheduleAsync(
                {
                    mrEnclave: hexUtils.leftPad('0xbed'),
                    isvSvn: '0x0000',
                },
                new BigNumber(8),
            );

            // Schedules a release to start in epoch 9.
            await updateReleaseScheduleAsync(
                {
                    mrEnclave: newMrEnclave,
                    isvSvn: newIsvSvn,
                },
                new BigNumber(9),
            );

            // Register some signers for the new release.
            await registerAsync(accounts.slice(5, 15), newMrEnclave, newIsvSvn);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails if the epoch ID is less than the current epoch ID', async () => {
            const epochId = new BigNumber(0);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    ...(await getLatestBlockInfoAsync()),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Epoch ID must monotonically increase.');
        });

        it('fails if the epoch ID is equal to the current epoch ID', async () => {
            const epochId = new BigNumber(1);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    ...(await getLatestBlockInfoAsync()),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Epoch ID must monotonically increase.');
        });

        it('fails when the operator signature is too short', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const submission = {
                checkpointData: {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot,
                    transactionRoot,
                },
                signatures: ['0xdeadbeef'],
            };
            const tx = checkpoint.checkpoint(submission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibSignature: Signature length must be 65 bytes.');
        });

        it('fails when the operator signature is too long', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const submission = {
                checkpointData: {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot,
                    transactionRoot,
                },
                signatures: [hexUtils.concat(NULL_BYTES32, NULL_BYTES32, NULL_BYTES32)],
            };
            const tx = checkpoint.checkpoint(submission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibSignature: Signature length must be 65 bytes.');
        });

        const bigSecp256k1RValue = new BigNumber('0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141');

        it('fails when the operator signature has an r value that is too high', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const submission = {
                checkpointData: {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot,
                    transactionRoot,
                },
                signatures: [
                    hexUtils.concat('0x00', hexUtils.leftPad(hexUtils.toHex(bigSecp256k1RValue)), NULL_BYTES32),
                ],
            };
            const tx = checkpoint.checkpoint(submission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibSignature: r parameter of signature is invalid.');
        });

        it('fails when the operator signature has an s value that is too high', async () => {
            const bigSecp256k1SValue = new BigNumber(bigSecp256k1RValue).dividedToIntegerBy(2).plus(1);
            const bigSecp256k1SHex = hexUtils.leftPad(hexUtils.toHex(bigSecp256k1SValue));

            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const submission = {
                checkpointData: {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot,
                    transactionRoot,
                },
                signatures: [hexUtils.concat('0x00', NULL_BYTES32, bigSecp256k1SHex)],
            };
            const tx = checkpoint.checkpoint(submission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibSignature: s parameter of signature is invalid.');
        });

        it('fails when the operator signature has a v value that is not 27 or 28', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const submission = {
                checkpointData: {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot: stateRoot,
                    transactionRoot: transactionRoot,
                },
                signatures: [
                    // 0x1D == 29
                    hexUtils.concat('0x1D', NULL_BYTES32, NULL_BYTES32),
                ],
            };
            const tx = checkpoint.checkpoint(submission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibSignature: v parameter of signature is invalid.');
        });

        it("fails when the operator's signature is invalid", async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const submission = {
                checkpointData: {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot: stateRoot,
                    transactionRoot: transactionRoot,
                },
                signatures: [hexUtils.concat('0x1C', NULL_BYTES32, NULL_BYTES32)],
            };
            const tx = checkpoint.checkpoint(submission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibSignature: Bad signature data.');
        });

        it('fails if the signers are not monotonically increasing', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const checkpointData = {
                blockNumber: new BigNumber(blockNumber),
                blockHash,
                stateRoot,
                transactionRoot,
            };
            const signatures = [];
            for (const signer of sortSignerAddresses(accounts.slice(0, 6)).reverse()) {
                signatures.push(await signCheckpointAsync(checkpointData, signer, epochId));
            }
            const tx = checkpoint
                .checkpoint(
                    {
                        checkpointData,
                        signatures,
                    },
                    [],
                    epochId,
                )
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCheckpoint: Signer addresses must be monotonically increasing.');
        });

        it('fails if the checkpoint block number is less than block_number - 256', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 257;
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash: await getBlockInfoAsync(blockNumber),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Submission references an invalid block number.');
        });

        it('fails if the checkpoint block number is equal to block_number - 256', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 256;
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash: await getBlockInfoAsync(blockNumber),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Submission references an invalid block number.');
        });

        it('fails if the checkpoint block number is equal to block_number', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) + 1;
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash: hexUtils.leftPad('0xdeadbeef'),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Submission references an invalid block number.');
        });

        it('fails if the checkpoint block number is greater than block_number', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) + 5;
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash: hexUtils.leftPad('0xdeadbeef'),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Submission references an invalid block number.');
        });

        it('fails if the checkpoint block number is less than the latest confirmed block number', async () => {
            const blockNumber = new BigNumber(0);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash: hexUtils.leftPad('0xdeadbeef'),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Submission references an invalid block number.');
        });

        it('fails if the checkpoint block number is equal to the latest confirmed block number', async () => {
            const blockNumber = new BigNumber(1);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash: hexUtils.leftPad('0xdeadbeef'),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Submission references an invalid block number.');
        });

        it('fails when the block hash is zero', async () => {
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: (await getLatestBlockInfoAsync()).blockNumber,
                    blockHash: NULL_BYTES32,
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Submission references an invalid block number.');
        });

        it("fails when the block hash doesn't reference the chain", async () => {
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: (await getLatestBlockInfoAsync()).blockNumber,
                    blockHash: hexUtils.leftPad('0xdeadbeef'),
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 6),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: Submission references an invalid block number.');
        });

        it('fails if consensus is not achieved', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const majoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 1),
                epochId,
            );
            const tx = checkpoint.checkpoint(majoritySubmission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCheckpoint: Consensus was not reached.');
        });

        it('fails if minority group maliciously swaps majority submission to be minority submission', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const stateRoot = hexUtils.leftPad('0xdecaf');
            const transactionRoot = hexUtils.leftPad('0xc0ffee');
            const maliciousMinoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot,
                    transactionRoot,
                },
                accounts.slice(0, 2),
                epochId,
            );
            const actualMajoritySubmission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot: hexUtils.leftPad('0xdeadbeef'),
                    transactionRoot: hexUtils.leftPad('0xdeadbabe'),
                },
                accounts.slice(3, 6),
                epochId,
            );
            const tx = checkpoint
                .checkpoint(maliciousMinoritySubmission, [actualMajoritySubmission], epochId)
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('LibCheckpoint: Consensus was not reached.');
        });

        it('checkpoints with a full contingent of valid signers', async () => {
            const blockNumber = (await web3Wrapper.getBlockNumberAsync()) - 1;
            const blockHash = await getBlockInfoAsync(blockNumber);
            await assertSuccessfulCheckpointAsync({
                majorityCheckpointData: {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot: hexUtils.leftPad('0xdecaf'),
                    transactionRoot: hexUtils.leftPad('0xc0ffee'),
                },
                majoritySigners: accounts.slice(0, 6),
                minorityCheckpointData: [],
                minoritySigners: [],
                epochId: new BigNumber(2),
                releaseHash: hashRegistrationMetadata(mrEnclave, isvSvn),
                ddxWithdrawalAllowanceDelta: new BigNumber(0),
            });
        });

        it('checkpoints with a partial contingent of valid signers', async () => {
            await assertSuccessfulCheckpointAsync({
                majorityCheckpointData: {
                    ...(await getLatestBlockInfoAsync()),
                    stateRoot: hexUtils.leftPad('0xdeadbeef'),
                    transactionRoot: hexUtils.leftPad('0xdeadbabe'),
                },
                majoritySigners: accounts.slice(0, 4),
                minorityCheckpointData: [],
                minoritySigners: [],
                epochId: new BigNumber(3),
                releaseHash: hashRegistrationMetadata(mrEnclave, isvSvn),
                ddxWithdrawalAllowanceDelta: new BigNumber(0),
            });
        });

        it("checkpoints if some of the signers in the majority submission aren't valid", async () => {
            await assertSuccessfulCheckpointAsync({
                majorityCheckpointData: {
                    ...(await getLatestBlockInfoAsync()),
                    stateRoot: hexUtils.leftPad('0xdead'),
                    transactionRoot: hexUtils.leftPad('0xbeef'),
                },
                majoritySigners: accounts.slice(0, 10),
                minorityCheckpointData: [],
                minoritySigners: [],
                epochId: new BigNumber(4),
                releaseHash: hashRegistrationMetadata(mrEnclave, isvSvn),
                ddxWithdrawalAllowanceDelta: new BigNumber(0),
            });
        });

        it('checkpoints if there are multiple minority submissions', async () => {
            const blockInfo = await getLatestBlockInfoAsync();
            const stateRoot = hexUtils.leftPad('0xbeef');
            const transactionRoot = hexUtils.leftPad('0xdead');
            await assertSuccessfulCheckpointAsync({
                majorityCheckpointData: {
                    ...blockInfo,
                    stateRoot,
                    transactionRoot,
                },
                majoritySigners: accounts.slice(0, 5),
                minorityCheckpointData: [
                    {
                        ...{
                            blockNumber: blockInfo.blockNumber.minus(1),
                            blockHash: await getBlockInfoAsync(blockInfo.blockNumber.minus(1).toNumber()),
                        },
                        stateRoot,
                        transactionRoot,
                    },
                    {
                        ...{
                            blockNumber: blockInfo.blockNumber.minus(2),
                            blockHash: await getBlockInfoAsync(blockInfo.blockNumber.minus(2).toNumber()),
                        },
                        stateRoot,
                        transactionRoot,
                    },
                ],
                minoritySigners: [accounts.slice(5, 8), accounts.slice(8, 10)],
                epochId: new BigNumber(5),
                releaseHash: hashRegistrationMetadata(mrEnclave, isvSvn),
                ddxWithdrawalAllowanceDelta: new BigNumber(0),
            });
        });

        it('checkpoints an epoch that advances the release schedule by several entries', async () => {
            const blockInfo = await getLatestBlockInfoAsync();
            const stateRoot = hexUtils.leftPad('0xcede');
            const transactionRoot = hexUtils.leftPad('0xdec0');
            await assertSuccessfulCheckpointAsync({
                majorityCheckpointData: {
                    ...blockInfo,
                    stateRoot,
                    transactionRoot,
                },
                majoritySigners: accounts.slice(0, 15),
                minorityCheckpointData: [],
                minoritySigners: [],
                epochId: new BigNumber(9),
                releaseHash: hashRegistrationMetadata(newMrEnclave, newIsvSvn),
                ddxWithdrawalAllowanceDelta: new BigNumber(0),
            });
        });

        it('checkpoints enough epochs to advance a trade mining epoch', async () => {
            const blockInfo = await getLatestBlockInfoAsync();
            const stateRoot = hexUtils.leftPad('0xcede');
            const transactionRoot = hexUtils.leftPad('0xdec0');
            await assertSuccessfulCheckpointAsync({
                majorityCheckpointData: {
                    ...blockInfo,
                    stateRoot,
                    transactionRoot,
                },
                majoritySigners: accounts.slice(0, 15),
                minorityCheckpointData: [],
                minoritySigners: [],
                epochId: new BigNumber(TRADE_MINING_EPOCH_MULTIPLIER),
                releaseHash: hashRegistrationMetadata(newMrEnclave, newIsvSvn),
                ddxWithdrawalAllowanceDelta: TRADE_MINING_REWARD_PER_EPOCH,
            });
        });

        it('checkpoints enough epochs to advance several trade mining epoch', async () => {
            const blockInfo = await getLatestBlockInfoAsync();
            const stateRoot = hexUtils.leftPad('0xcede');
            const transactionRoot = hexUtils.leftPad('0xdec0');
            await assertSuccessfulCheckpointAsync({
                majorityCheckpointData: {
                    ...blockInfo,
                    stateRoot,
                    transactionRoot,
                },
                majoritySigners: accounts.slice(0, 15),
                minorityCheckpointData: [],
                minoritySigners: [],
                epochId: new BigNumber(TRADE_MINING_EPOCH_MULTIPLIER).times(11),
                releaseHash: hashRegistrationMetadata(newMrEnclave, newIsvSvn),
                ddxWithdrawalAllowanceDelta: TRADE_MINING_REWARD_PER_EPOCH.times(10),
            });
        });

        it('checkpoints enough epochs to advance past the end of trade mining', async () => {
            const blockInfo = await getLatestBlockInfoAsync();
            const stateRoot = hexUtils.leftPad('0xcede');
            const transactionRoot = hexUtils.leftPad('0xdec0');
            await assertSuccessfulCheckpointAsync({
                majorityCheckpointData: {
                    ...blockInfo,
                    stateRoot,
                    transactionRoot,
                },
                majoritySigners: accounts.slice(0, 15),
                minorityCheckpointData: [],
                minoritySigners: [],
                epochId: new BigNumber(TRADE_MINING_LENGTH).times(TRADE_MINING_EPOCH_MULTIPLIER).times(10),
                releaseHash: hashRegistrationMetadata(newMrEnclave, newIsvSvn),
                ddxWithdrawalAllowanceDelta: TRADE_MINING_REWARD_PER_EPOCH.times(TRADE_MINING_LENGTH - 11),
            });
        });
    });

    describe('#reject', () => {
        let snapshotId: string;
        const stateRoots = [NULL_BYTES32, hexUtils.leftPad(1), hexUtils.leftPad(2), hexUtils.leftPad(3)];
        const transactionRoots = [NULL_BYTES32, hexUtils.leftPad(1), hexUtils.leftPad(2), hexUtils.leftPad(3)];

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Advances the current epoch ID a few epochs for the epoch ID
            // failure tests.
            await checkpoint.setEpochId(new BigNumber(1)).awaitTransactionSuccessAsync();

            // Register some signers.
            await registerAsync(accounts.slice(0, 3), mrEnclave, isvSvn);

            // Bond some custodians.
            await bondAsync(accounts.slice(0, 3));
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        afterEach(async () => {
            await setIsPausedAsync(ctx, false);
        });

        it('fails to reject with an empty submissions list', async () => {
            const tx = derivadex.rejectContract
                .reject([], [], [], new BigNumber(2))
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith("Checkpoint: can't reject an empty list of submissions.");
        });

        it('fails to reject with an epoch ID less than the last', async () => {
            const epochId = new BigNumber(0);
            const checkpointData = {
                ...(await getRandomBlockInfoAsync()),
                stateRoot: NULL_BYTES32,
                transactionRoot: NULL_BYTES32,
            };
            const submission = {
                checkpointData,
                signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
            };
            const tx = derivadex.rejectContract
                .reject(
                    [submission],
                    submission.signatures,
                    [{ submissionIndex: new BigNumber(0), signatureIndex: new BigNumber(0) }],
                    epochId,
                )
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Checkpoint: Epoch ID must monotonically increase.');
        });

        it('fails to reject with an epoch ID equal to the last', async () => {
            const epochId = new BigNumber(1);
            const checkpointData = {
                ...(await getRandomBlockInfoAsync()),
                stateRoot: NULL_BYTES32,
                transactionRoot: NULL_BYTES32,
            };
            const submission = {
                checkpointData,
                signatures: [await signCheckpointAsync(checkpointData, accounts[0], epochId)],
            };
            const tx = derivadex.rejectContract
                .reject(
                    [submission],
                    submission.signatures,
                    [{ submissionIndex: new BigNumber(0), signatureIndex: new BigNumber(0) }],
                    epochId,
                )
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Checkpoint: Epoch ID must monotonically increase.');
        });

        it('fails to reject when signatures from array do not match submissions signatures', async () => {
            const epochId = new BigNumber(2);
            const block = await getRandomBlockInfoAsync();
            const checkpointData0 = {
                ...block,
                stateRoot: stateRoots[0],
                transactionRoot: transactionRoots[0],
            };
            const checkpointData1 = {
                ...block,
                stateRoot: stateRoots[1],
                transactionRoot: transactionRoots[1],
            };
            const [signer0, signature0] = [
                accounts[0],
                await signCheckpointAsync(checkpointData0, accounts[0], epochId),
            ];
            const [signer1, signature1] = [
                accounts[1],
                await signCheckpointAsync(checkpointData1, accounts[1], epochId),
            ];
            const submissions = sortSubmissions(
                [
                    {
                        checkpointData: checkpointData0,
                        signatures: [signature0],
                    },
                    {
                        checkpointData: checkpointData1,
                        signatures: [signature1],
                    },
                ],
                epochId,
            );
            const { signatures, indexes } = getSortedSignaturesAndIndexesForSubmissions(submissions, [
                { signer: signer0, signature: signature0 },
                { signer: signer1, signature: signature1 },
            ]);

            const tx = derivadex.rejectContract
                .reject(submissions, signatures, indexes.reverse(), epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith(
                "Checkpoint: signature doesn't match corresponding signature from submissions array",
            );
        });

        it('fails to reject when signatures array is not monotonically increasing', async () => {
            const epochId = new BigNumber(2);
            const block = await getRandomBlockInfoAsync();
            const checkpointData0 = {
                ...block,
                stateRoot: stateRoots[0],
                transactionRoot: transactionRoots[0],
            };
            const checkpointData1 = {
                ...block,
                stateRoot: stateRoots[1],
                transactionRoot: transactionRoots[1],
            };
            const [signer0, signature0] = [
                accounts[0],
                await signCheckpointAsync(checkpointData0, accounts[0], epochId),
            ];
            const [signer1, signature1] = [
                accounts[1],
                await signCheckpointAsync(checkpointData1, accounts[1], epochId),
            ];
            const submissions = sortSubmissions(
                [
                    {
                        checkpointData: checkpointData0,
                        signatures: [signature0],
                    },
                    {
                        checkpointData: checkpointData1,
                        signatures: [signature1],
                    },
                ],
                epochId,
            );
            const { signatures, indexes } = getSortedSignaturesAndIndexesForSubmissions(submissions, [
                { signer: signer0, signature: signature0 },
                { signer: signer1, signature: signature1 },
            ]);

            const tx = derivadex.rejectContract
                .reject(submissions, signatures.reverse(), indexes.reverse(), epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Checkpoint: Signer addresses must be monotonically increasing.');
        });

        it('fails to reject with an empty submissions sub-list', async () => {
            const epochId = new BigNumber(2);
            const checkpointData = {
                ...(await getRandomBlockInfoAsync()),
                stateRoot: NULL_BYTES32,
                transactionRoot: NULL_BYTES32,
            };
            const submission = {
                checkpointData,
                signatures: [],
            };
            const tx = derivadex.rejectContract
                .reject([submission], [], [], epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Checkpoint: submission signatures must be non-empty.');
        });

        it('fails to reject with decreasing reference hashes', async () => {
            const epochId = new BigNumber(2);
            const block = await getRandomBlockInfoAsync();
            const checkpointData0 = {
                ...block,
                stateRoot: stateRoots[0],
                transactionRoot: transactionRoots[0],
            };
            const checkpointData1 = {
                ...block,
                stateRoot: stateRoots[1],
                transactionRoot: transactionRoots[1],
            };
            const [signer0, signature0] = [
                accounts[0],
                await signCheckpointAsync(checkpointData0, accounts[0], epochId),
            ];
            const [signer1, signature1] = [
                accounts[1],
                await signCheckpointAsync(checkpointData1, accounts[1], epochId),
            ];
            const submissions = sortSubmissions(
                [
                    {
                        checkpointData: checkpointData0,
                        signatures: [signature0],
                    },
                    {
                        checkpointData: checkpointData1,
                        signatures: [signature1],
                    },
                ],
                epochId,
            ).reverse();
            const { signatures, indexes } = getSortedSignaturesAndIndexesForSubmissions(submissions, [
                { signer: signer0, signature: signature0 },
                { signer: signer1, signature: signature1 },
            ]);

            const tx = derivadex.rejectContract
                .reject(submissions, signatures, indexes, epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Checkpoint: reference hashes must be monotonically increasing.');
        });

        it('fails to reject with duplicated reference hashes', async () => {
            const epochId = new BigNumber(2);
            const checkpointData0 = {
                ...(await getRandomBlockInfoAsync()),
                stateRoot: stateRoots[0],
                transactionRoot: transactionRoots[0],
            };
            const submission0 = {
                signatures: [await signCheckpointAsync(checkpointData0, fixtures.traderD(), epochId)],
                checkpointData: checkpointData0,
            };
            const signatures = submission0.signatures;
            const indexes = [{ submissionIndex: new BigNumber(0), signatureIndex: new BigNumber(0) }];
            const tx = derivadex.rejectContract
                .reject([submission0, submission0], signatures, indexes, epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Checkpoint: reference hashes must be monotonically increasing.');
        });

        it('fails to reject with submission sub-list that could be the majority', async () => {
            const epochId = new BigNumber(2);
            const checkpointData = {
                ...(await getRandomBlockInfoAsync()),
                stateRoot: stateRoots[1],
                transactionRoot: transactionRoots[1],
            };
            const submission = {
                signatures: [await signCheckpointAsync(checkpointData, fixtures.traderD(), epochId)],
                checkpointData: checkpointData,
            };
            const signatures = submission.signatures;
            const indexes = [{ submissionIndex: new BigNumber(0), signatureIndex: new BigNumber(0) }];
            const tx = derivadex.rejectContract
                .reject([submission], signatures, indexes, epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith(
                "Checkpoint: it's possible for submission to be the majority submission.",
            );
        });

        it('fails to reject when submission signatures outnumber signatures array', async () => {
            const epochId = new BigNumber(2);
            const checkpointData = {
                ...(await getRandomBlockInfoAsync()),
                stateRoot: stateRoots[1],
                transactionRoot: transactionRoots[1],
            };
            const submission = {
                signatures: [await signCheckpointAsync(checkpointData, fixtures.traderD(), epochId)],
                checkpointData: checkpointData,
            };
            const tx = derivadex.rejectContract
                .reject([submission], [], [], epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith(
                'Checkpoint: signatures in submissions not equal to length of signatures array',
            );
        });

        it('successfully rejects a submission and pauses the DerivaDAO', async () => {
            const epochId = new BigNumber(2);
            const block = await getRandomBlockInfoAsync();
            const checkpointData0 = {
                ...block,
                stateRoot: stateRoots[0],
                transactionRoot: transactionRoots[0],
            };
            const [signer0, signature0] = [
                accounts[0],
                await signCheckpointAsync(checkpointData0, accounts[0], epochId),
            ];
            const submission0 = {
                signatures: [await signCheckpointAsync(checkpointData0, accounts[0], epochId)],
                checkpointData: checkpointData0,
            };

            const checkpointData1 = {
                ...block,
                stateRoot: stateRoots[1],
                transactionRoot: transactionRoots[1],
            };
            const [signer1, signature1] = [
                accounts[1],
                await signCheckpointAsync(checkpointData1, accounts[1], epochId),
            ];
            const submission1 = {
                signatures: [await signCheckpointAsync(checkpointData1, accounts[1], epochId)],
                checkpointData: checkpointData1,
            };

            const checkpointData2 = {
                ...block,
                stateRoot: stateRoots[2],
                transactionRoot: transactionRoots[2],
            };
            const [signer2, signature2] = [
                accounts[2],
                await signCheckpointAsync(checkpointData2, accounts[2], epochId),
            ];
            const submission2 = {
                signatures: [await signCheckpointAsync(checkpointData2, accounts[2], epochId)],
                checkpointData: checkpointData2,
            };

            const checkpointData3 = {
                ...block,
                stateRoot: stateRoots[3],
                transactionRoot: transactionRoots[3],
            };
            const [signer3, signature3] = [
                accounts[3],
                await signCheckpointAsync(checkpointData3, accounts[3], epochId),
            ];
            const submission3 = {
                signatures: [await signCheckpointAsync(checkpointData3, accounts[3], epochId)],
                checkpointData: checkpointData3,
            };
            const sortedSubmissions = sortSubmissions([submission0, submission1, submission2, submission3], epochId);
            const { signatures, indexes } = getSortedSignaturesAndIndexesForSubmissions(sortedSubmissions, [
                { signer: signer0, signature: signature0 },
                { signer: signer1, signature: signature1 },
                { signer: signer2, signature: signature2 },
                { signer: signer3, signature: signature3 },
            ]);

            const { logs } = await derivadex.rejectContract
                .reject(sortedSubmissions, signatures, indexes, epochId)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

            // Ensure that the exchange was paused.
            const isPaused = await derivadex.pauseContract.getIsPaused().callAsync();
            expect(isPaused).to.be.eq(true);

            // Verify that the correct events were emitted.
            expect(logs.length).to.be.eq(1);
            const log = logs[0] as LogWithDecodedArgs<RejectRejectedEventArgs>;
            expect(log.event).to.be.eq('Rejected');
            expect(log.args).to.be.deep.eq({ epochId });
        });

        describe('Rejection fuzz tests', () => {
            const TEST_COUNT = 50;

            let snapshotId: number;
            let bondedCustodians: string[];
            let bondedSigners: string[];
            let unbondedCustodians: string[];
            let unbondedSigners: string[];

            /**
             * Ensure that a random rejection fails to be processed.
             * @param bondedCustodians A list of bonded custodians.
             * @param bondedSigners A list of bonded signers.
             * @param unbondedCustodians A list of unbonded custodians.
             * @param unbondedSigners A list of unbonded signers.
             */
            async function assertRejectionFailureAsync(
                bondedCustodians: string[],
                bondedSigners: string[],
                unbondedCustodians: string[],
                unbondedSigners: string[],
            ): Promise<void> {
                if (
                    bondedCustodians.length !== bondedSigners.length ||
                    unbondedCustodians.length !== unbondedSigners.length
                ) {
                    throw new Error('Input parity mismatch.');
                }
                let bondedSubmissionCount = bondedCustodians.length;
                let unbondedSubmissionCount = unbondedCustodians.length;
                let [, , , epochId] = await derivadex.checkpointContract.getLatestCheckpoint().callAsync();
                epochId = epochId.plus(1);
                const block = await getRandomBlockInfoAsync();
                const submissions: (CheckpointSubmission & { signers: string[] })[] = [];
                let cohortIdx = 0;
                let bondedCustodianIdx = 0;
                let unbondedCustodianIdx = 0;
                const quorum = Math.floor(bondedCustodians.length / 2 + 1);

                // Create an initial submission with a random number of submissions
                // in the range [quorum, submissionCount].
                const initialBondedSubmissionSize =
                    Math.round(Math.random() * (bondedSubmissionCount - quorum)) + quorum;
                bondedSubmissionCount -= initialBondedSubmissionSize;
                let initialSigners: string[] = [];
                let initialSignatures: string[] = [];
                const missingSubmissionSize = Math.floor(Math.random() * initialBondedSubmissionSize);
                const checkpointData = {
                    ...block,
                    stateRoot: hexUtils.leftPad(cohortIdx),
                    transactionRoot: hexUtils.leftPad(cohortIdx),
                };
                initialSigners = initialSigners.concat(
                    bondedSigners.slice(0, initialBondedSubmissionSize - missingSubmissionSize),
                );
                bondedCustodianIdx += initialBondedSubmissionSize - missingSubmissionSize;

                // Add some unbonded submissions to the initial submission.
                // Sample in the range [0, unbondedSubmissionCount].
                const initialUnbondedSubmissionSize = Math.round(Math.random() * unbondedSubmissionCount);
                unbondedSubmissionCount -= initialUnbondedSubmissionSize;
                initialSigners = initialSigners.concat(unbondedSigners.slice(0, initialUnbondedSubmissionSize));
                unbondedCustodianIdx += initialUnbondedSubmissionSize;

                initialSigners = sortSignerAddresses(initialSigners);
                initialSignatures = initialSignatures.concat(
                    await Promise.all(
                        initialSigners.map(
                            async (signer) => await signCheckpointAsync(checkpointData, signer, epochId),
                        ),
                    ),
                );

                // Add the initial submission to the list of submissions.
                submissions.push({
                    signers: initialSigners,
                    signatures: initialSignatures,
                    checkpointData: checkpointData,
                });
                cohortIdx++;

                // Create minority submissions for the remaining signers.
                while (bondedSubmissionCount > 0) {
                    // Since we've already created a checkpoint that satisfies
                    // the quorum, we can submit a failed rejection with just
                    // the initial checkpoint. With this in mind, we break out
                    // of this loop with a probability of 1/4.
                    const breakSample = Math.round(Math.random() * 3);
                    if (breakSample === 0) {
                        break;
                    }

                    let innerSigners: string[] = [];
                    let innerSignatures: string[] = [];

                    // Add some bonded submissions to the inner submission.
                    const bondedSubmissionSize = Math.min(
                        Math.round(Math.random() * (quorum - 2)) + 1,
                        bondedSubmissionCount,
                    );
                    bondedSubmissionCount -= bondedSubmissionSize;
                    const checkpointData = {
                        ...block,
                        stateRoot: hexUtils.leftPad(cohortIdx),
                        transactionRoot: hexUtils.leftPad(cohortIdx),
                    };
                    innerSigners = innerSigners.concat(
                        bondedSigners.slice(bondedCustodianIdx, bondedCustodianIdx + bondedSubmissionSize),
                    );
                    bondedCustodianIdx += bondedSubmissionSize;

                    // Add some unbonded submissions to the inner submission.
                    // Sample in the range [0, unbondedSubmissionCount].
                    const unbondedSubmissionSize = Math.round(Math.random() * unbondedSubmissionCount);
                    unbondedSubmissionCount -= unbondedSubmissionSize;
                    innerSigners = innerSigners.concat(
                        unbondedSigners.slice(unbondedCustodianIdx, unbondedCustodianIdx + unbondedSubmissionSize),
                    );
                    unbondedCustodianIdx += unbondedSubmissionSize;

                    innerSigners = sortSignerAddresses(innerSigners);
                    innerSignatures = innerSignatures.concat(
                        await Promise.all(
                            innerSigners.map(
                                async (signer) => await signCheckpointAsync(checkpointData, signer, epochId),
                            ),
                        ),
                    );

                    // Add the inner submission to the list of submissions.
                    submissions.push({
                        signers: innerSigners,
                        signatures: innerSignatures,
                        checkpointData: checkpointData,
                    });
                    cohortIdx++;
                }

                const sortedSubmissions = sortSubmissions(submissions, epochId);
                const { signatures, indexes } = getSortedSignaturesAndIndexesForSubmissions(
                    sortedSubmissions,
                    _.flatten(
                        submissions.map((submission) =>
                            submission.signers.map((signer, i) => ({ signer, signature: submission.signatures[i] })),
                        ),
                    ),
                );

                // Submit the rejection and verify that it is rejected with the
                // correct error message.
                const tx = derivadex.rejectContract
                    .reject(sortedSubmissions, signatures, indexes, epochId)
                    .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
                return expect(tx).to.be.rejectedWith(
                    "Checkpoint: it's possible for submission to be the majority submission.",
                );
            }

            /**
             * Ensure that a random rejection is successfully processed.
             * @param bondedCustodians A list of bonded custodians.
             * @param bondedSigners A list of bonded signers.
             * @param unbondedCustodians A list of unbonded custodians.
             * @param unbondedSigners A list of unbonded signers.
             */
            async function assertRejectionSuccessAsync(
                bondedCustodians: string[],
                bondedSigners: string[],
                unbondedCustodians: string[],
                unbondedSigners: string[],
            ): Promise<void> {
                if (
                    bondedCustodians.length !== bondedSigners.length ||
                    unbondedCustodians.length !== unbondedSigners.length
                ) {
                    throw new Error('Input parity mismatch.');
                }
                let bondedSubmissionCount = bondedCustodians.length;
                let unbondedSubmissionCount = unbondedCustodians.length;

                let [, , , epochId] = await derivadex.checkpointContract.getLatestCheckpoint().callAsync();
                epochId = epochId.plus(1);
                const block = await getRandomBlockInfoAsync();
                const submissions: (CheckpointSubmission & { signers: string[] })[] = [];
                let cohortIdx = 0;
                let bondedCustodianIdx = 0;
                let unbondedCustodianIdx = 0;
                const quorum = Math.floor(bondedCustodians.length / 2 + 1);
                while (bondedSubmissionCount > 0) {
                    let innerSigners: string[] = [];
                    let innerSignatures: string[] = [];

                    // Add a randomly sampled number of bonded submissions
                    // to the inner submissions. If the sample consumes the
                    // remaining number of submissions, we sample an amount
                    // that we will use to reduce the submission that we
                    // create. Despite only applying to the last submission,
                    // this should be sufficient to test the calculation of
                    // missing checkpoints.
                    let bondedSubmissionSize = Math.min(
                        Math.round(Math.random() * (quorum - 2)) + 1,
                        bondedSubmissionCount,
                    );
                    if (bondedSubmissionCount - bondedSubmissionSize === 0) {
                        const missingSubmissionSize = Math.max(
                            0,
                            quorum -
                                Math.round(Math.max(...submissions.map((innerVal) => innerVal.signatures.length))) -
                                1,
                        );
                        bondedSubmissionSize -= missingSubmissionSize;
                        bondedSubmissionCount = 0;
                    } else {
                        bondedSubmissionCount -= bondedSubmissionSize;
                    }
                    const checkpointData = {
                        ...block,
                        stateRoot: hexUtils.leftPad(cohortIdx),
                        transactionRoot: hexUtils.leftPad(cohortIdx),
                    };
                    innerSigners = innerSigners.concat(
                        bondedSigners.slice(bondedCustodianIdx, bondedCustodianIdx + bondedSubmissionSize),
                    );
                    bondedCustodianIdx += bondedSubmissionSize;

                    // Add a randomly sampled amount of unbonded submissions
                    // to the inner submissions.
                    const unbondedSubmissionSize = Math.round(Math.random() * unbondedSubmissionCount);
                    unbondedSubmissionCount -= unbondedSubmissionSize;
                    innerSigners = innerSigners.concat(
                        unbondedSigners.slice(unbondedCustodianIdx, unbondedCustodianIdx + unbondedSubmissionSize),
                    );
                    unbondedCustodianIdx += unbondedSubmissionSize;

                    // Add the inner submissions and signatures to the list.
                    if (innerSigners.length !== 0) {
                        innerSigners = sortSignerAddresses(innerSigners);
                        innerSignatures = innerSignatures.concat(
                            await Promise.all(
                                innerSigners.map(
                                    async (signer) => await signCheckpointAsync(checkpointData, signer, epochId),
                                ),
                            ),
                        );

                        // Add the inner submission to the list of submissions.
                        submissions.push({
                            signers: innerSigners,
                            signatures: innerSignatures,
                            checkpointData: checkpointData,
                        });
                        cohortIdx++;
                    }
                }
                const sortedSubmissions = sortSubmissions(submissions, epochId);
                const { signatures, indexes } = getSortedSignaturesAndIndexesForSubmissions(
                    sortedSubmissions,
                    _.flatten(
                        submissions.map((submission) =>
                            submission.signers.map((signer, i) => ({ signer, signature: submission.signatures[i] })),
                        ),
                    ),
                );

                const { logs } = await derivadex.rejectContract
                    .reject(sortedSubmissions, signatures, indexes, epochId)
                    .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

                // Ensure that the exchange was paused.
                const isPaused = await derivadex.pauseContract.getIsPaused().callAsync();
                expect(isPaused).to.be.eq(true);

                // Verify that the correct events were emitted.
                expect(logs.length).to.be.eq(1);
                const log = logs[0] as LogWithDecodedArgs<RejectRejectedEventArgs>;
                expect(log.event).to.be.eq('Rejected');
                expect(log.args).to.be.deep.eq({ epochId });
            }

            before(async () => {
                snapshotId = await web3Wrapper.sendRawPayloadAsync<number>({
                    method: 'evm_snapshot',
                    params: [],
                });

                bondedCustodians = accounts.slice(3, 15);
                bondedSigners = accounts.slice(3, 15);
                unbondedCustodians = accounts.slice(15, 20);
                unbondedSigners = accounts.slice(15, 20);

                // Register some signers.
                await registerAsync(bondedCustodians.concat(unbondedCustodians), mrEnclave, isvSvn);

                // Bond some custodians.
                await bondAsync(bondedCustodians);

                // Add the remaining addresses to the bonded lists.
                bondedCustodians = accounts.slice(0, 15);
                bondedSigners = accounts.slice(0, 15);
            });

            after(async () => {
                await web3Wrapper.sendRawPayloadAsync({
                    method: 'evm_revert',
                    params: [snapshotId],
                });
            });

            for (let i = 0; i < TEST_COUNT; i++) {
                it(`#reject failure fuzz test ${i}/${TEST_COUNT}`, async () => {
                    await assertRejectionFailureAsync(
                        bondedCustodians,
                        bondedSigners,
                        unbondedCustodians,
                        unbondedSigners,
                    );
                });
            }

            for (let i = 0; i < TEST_COUNT; i++) {
                it(`#reject success fuzz test ${i}/${TEST_COUNT}`, async () => {
                    await assertRejectionSuccessAsync(
                        bondedCustodians,
                        bondedSigners,
                        unbondedCustodians,
                        unbondedSigners,
                    );
                });
            }
        });

        // describe('Checkpointing study', () => {
        //     const TEST_COUNT = 50;
        //
        //     let snapshotId: number;
        //     let bondedCustodians: string[];
        //     let bondedSigners: string[];
        //     let unbondedCustodians: string[];
        //     let unbondedSigners: string[];
        //
        //     /**
        //      * Ensure that a random rejection fails to be processed.
        //      * @param bondedCustodians A list of bonded custodians.
        //      * @param bondedSigners A list of bonded signers.
        //      * @param unbondedCustodians A list of unbonded custodians.
        //      * @param unbondedSigners A list of unbonded signers.
        //      * @param submissionSize Submission size.
        //      */
        //     async function assertSubmissionSuccessBenchmarkAsync(
        //         bondedCustodians: string[],
        //         bondedSigners: string[],
        //         unbondedCustodians: string[],
        //         unbondedSigners: string[],
        //         submissionSize: number,
        //     ): Promise<void> {
        //         // Advance 257 blocks to ensure the success of our tests that rely on
        //         // the existence of old blocks.
        //         await advanceBlocksAsync(derivadex.providerEngine, submissionSize * 257);
        //         if (
        //             bondedCustodians.length !== bondedSigners.length ||
        //             unbondedCustodians.length !== unbondedSigners.length
        //         ) {
        //             throw new Error('Input parity mismatch.');
        //         }
        //         const custodians = bondedCustodians.concat(unbondedCustodians);
        //         const signers = bondedSigners.concat(unbondedSigners);
        //         if (submissionSize > 5) {
        //             await registration
        //                 .register(REPORT, SIGNATURE)
        //                 .awaitTransactionSuccessAsync({
        //                     from: custodians[submissionSize - 1],
        //                 });
        //             await derivadex
        //                 .transferDDX(custodians[submissionSize - 1], OPERATOR_MINIMUM_BOND.toNumber())
        //                 .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        //             await derivadex
        //                 .approveUnlimitedDDX(derivadex.derivaDEXContract.address)
        //                 .awaitTransactionSuccessAsync({ from: custodians[submissionSize - 1] });
        //             await registration.bond(OPERATOR_MINIMUM_BOND).awaitTransactionSuccessAsync({
        //                 from: custodians[submissionSize - 1],
        //             });
        //         }
        //         let [, , , epochId] = await derivadex.checkpointContract.getLatestCheckpoint().callAsync();
        //         epochId = epochId.plus(1);
        //         const block = await getRandomBlockInfoAsync();
        //         let submissions = [];
        //         let signatures = [];
        //
        //         const initialSubmissions = [];
        //         const initialSignatures = [];
        //         for (let i = 0; i < submissionSize; i++) {
        //             const checkpoint = {
        //                 ...block,
        //                 stateRoot: hexUtils.leftPad(0),
        //                 transactionRoot: hexUtils.leftPad(0),
        //                 custodianAddress: custodians[i],
        //             };
        //             initialSubmissions.push(checkpoint);
        //             initialSignatures.push(await signCheckpointAsync(checkpoint, signers[i], epochId));
        //         }
        //
        //         // Add the initial submission to the list of submissions.
        //         submissions.push(initialSubmissions);
        //         signatures.push(initialSignatures);
        //
        //         // Sort the checkpoint submissions to ensure that we don't
        //         // encounter errors unrelated to the combinatorics of the
        //         // submitted checkpoint.
        //         ({ submissions, signatures } = sortCheckpointSubmissions(submissions, signatures, epochId, true));
        //
        //         // Submit the rejection and verify that it is rejected with the
        //         // correct error message.
        //         const txA = await derivadex.checkpointContract
        //             .checkpointCalldataA(submissions[0], [], signatures[0], [], epochId)
        //             .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        //         console.log(
        //             `A: Submission size (${submissionSize} => gas used (${txA.gasUsed}; cumulative gas used (${txA.cumulativeGasUsed})`,
        //         );
        //
        //         const submissionsB: CheckpointSubmissionA = {
        //             custodians: submissions[0].map((submission) => submission.custodianAddress),
        //             signatures: signatures[0].map((signature) => signature),
        //             checkpointData: {
        //                 blockNumber: submissions[0][0].blockNumber,
        //                 blockHash: submissions[0][0].blockHash,
        //                 stateRoot: submissions[0][0].stateRoot,
        //                 transactionRoot: submissions[0][0].transactionRoot,
        //             },
        //         };
        //         // Submit the rejection and verify that it is rejected with the
        //         // correct error message.
        //         const txB = await derivadex.checkpointContract
        //             .checkpointCalldataB(submissionsB, [], epochId)
        //             .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        //         console.log(
        //             `B: Submission size (${submissionSize} => gas used (${txB.gasUsed}; cumulative gas used (${txB.cumulativeGasUsed})`,
        //         );
        //
        //         const submissionsC: CheckpointSubmissionB = {
        //             signatures: signatures[0].map((signature) => signature),
        //             checkpointData: {
        //                 blockNumber: submissions[0][0].blockNumber,
        //                 blockHash: submissions[0][0].blockHash,
        //                 stateRoot: submissions[0][0].stateRoot,
        //                 transactionRoot: submissions[0][0].transactionRoot,
        //             },
        //         };
        //         // Submit the rejection and verify that it is rejected with the
        //         // correct error message.
        //         const txC = await derivadex.checkpointContract
        //             .checkpointCalldataC(submissionsC, [], epochId)
        //             .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        //         console.log(
        //             `C: Submission size (${submissionSize} => gas used (${txC.gasUsed}; cumulative gas used (${txC.cumulativeGasUsed})`,
        //         );
        //     }
        //
        //     before(async () => {
        //         snapshotId = await web3Wrapper.sendRawPayloadAsync<number>({
        //             method: 'evm_snapshot',
        //             params: [],
        //         });
        //
        //         // Register 15 accounts. Bond 10 of them so that the rest can
        //         // be used to test the logic that disregards checkpoints
        //         // submitted by unbonded operators.
        //         bondedCustodians = accounts.slice(10, 20);
        //         bondedSigners = accounts.slice(10, 20);
        //         unbondedCustodians = accounts.slice(20, 26);
        //         unbondedSigners = accounts.slice(20, 26);
        //         await setCustodianStatusesAsync(
        //             bondedCustodians.concat(unbondedCustodians),
        //             bondedCustodians.concat(unbondedCustodians).map(() => true),
        //         );
        //
        //         // Add the set of previously registered accounts to the list
        //         // of bonded custodians and signers.
        //         bondedCustodians = [
        //             fixtures.traderA(),
        //             fixtures.traderB(),
        //             fixtures.traderC(),
        //             fixtures.traderE(),
        //             accounts[accounts.length - 1],
        //         ].concat(bondedCustodians);
        //         bondedSigners = [
        //             fixtures.traderD(),
        //             fixtures.traderB(),
        //             fixtures.traderC(),
        //             fixtures.traderE(),
        //             accounts[accounts.length - 1],
        //         ].concat(bondedSigners);
        //         //
        //         // // TraderA => (balance = 50; jailed = false) => REGISTERED / VALID
        //         // // TraderB => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // TraderC => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // TraderD => UNREGISTERED
        //         // // TraderE => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // TraderF => UNREGISTERED
        //         // // accounts[10] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[11] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[12] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[13] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[14] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[15] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[16] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[17] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[18] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[19] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         // // accounts[20] => (balance = 0; jailed = false) => REGISTERED / INVALID
        //         // // accounts[21] => (balance = 0; jailed = false) => REGISTERED / INVALID
        //         // // accounts[22] => (balance = 0; jailed = false) => REGISTERED / INVALID
        //         // // accounts[23] => (balance = 0; jailed = false) => REGISTERED / INVALID
        //         // // accounts[24] => (balance = 0; jailed = false) => REGISTERED / INVALID
        //         // // accounts[25] => (balance = 0; jailed = false) => REGISTERED / INVALID
        //         // // accounts[-2] => UNREGISTERED
        //         // // accounts[-1] => (balance = 2; jailed = false) => REGISTERED / VALID
        //         //
        //         // await assertValidOperatorsCountAsync(15);
        //     });
        //
        //     after(async () => {
        //         await web3Wrapper.sendRawPayloadAsync({
        //             method: 'evm_revert',
        //             params: [snapshotId],
        //         });
        //     });
        //
        //     for (let i = 0; i < 18; i++) {
        //         it(`#benchmark ${i}/21`, async () => {
        //             await assertSubmissionSuccessBenchmarkAsync(
        //                 bondedCustodians,
        //                 bondedSigners,
        //                 unbondedCustodians,
        //                 unbondedSigners,
        //                 i + 3,
        //             );
        //         });
        //     }
        // });
    });

    describe('#isNotPaused', () => {
        let snapshotId: string;

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync(accounts.slice(0, 5), mrEnclave, isvSvn);

            // Bond some custodians.
            await bondAsync(accounts.slice(0, 5));

            // Set the paused status to true.
            await setIsPausedAsync(ctx, true);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('calls setConsensusThreshold when paused', async () => {
            await setConsensusThresholdAsync(new BigNumber(75));
        });

        it('calls setQuorum when paused', async () => {
            await setQuorumAsync(new BigNumber(3));
        });

        it('fails to call checkpoint when paused', async () => {
            const blockNumber = await web3Wrapper.getBlockNumberAsync();
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const submission = await prepareSubmissionAsync(
                {
                    blockNumber: new BigNumber(blockNumber),
                    blockHash,
                    stateRoot: NULL_BYTES32,
                    transactionRoot: NULL_BYTES32,
                },
                accounts.slice(0, 5),
                epochId,
            );
            const tx = checkpoint.checkpoint(submission, [], epochId).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: paused.');
        });

        it('fails to call reject when paused', async () => {
            const blockNumber = await web3Wrapper.getBlockNumberAsync();
            const blockHash = await getBlockInfoAsync(blockNumber);
            const epochId = new BigNumber(2);
            const submissions = [
                await prepareSubmissionAsync(
                    {
                        blockNumber: new BigNumber(blockNumber),
                        blockHash,
                        stateRoot: hexUtils.leftPad('0x01'),
                        transactionRoot: hexUtils.leftPad('0x01'),
                    },
                    [accounts[0]],
                    epochId,
                ),
                await prepareSubmissionAsync(
                    {
                        blockNumber: new BigNumber(blockNumber),
                        blockHash,
                        stateRoot: hexUtils.leftPad('0x02'),
                        transactionRoot: hexUtils.leftPad('0x02'),
                    },
                    [accounts[1]],
                    epochId,
                ),
                await prepareSubmissionAsync(
                    {
                        blockNumber: new BigNumber(blockNumber),
                        blockHash,
                        stateRoot: hexUtils.leftPad('0x03'),
                        transactionRoot: hexUtils.leftPad('0x03'),
                    },
                    [accounts[2]],
                    epochId,
                ),
                await prepareSubmissionAsync(
                    {
                        blockNumber: new BigNumber(blockNumber),
                        blockHash,
                        stateRoot: hexUtils.leftPad('0x04'),
                        transactionRoot: hexUtils.leftPad('0x04'),
                    },
                    [accounts[3]],
                    epochId,
                ),
                await prepareSubmissionAsync(
                    {
                        blockNumber: new BigNumber(blockNumber),
                        blockHash,
                        stateRoot: hexUtils.leftPad('0x05'),
                        transactionRoot: hexUtils.leftPad('0x05'),
                    },
                    [accounts[4]],
                    epochId,
                ),
            ];
            const { signatures, indexes } = getSortedSignaturesAndIndexesForSubmissions(submissions, [
                { signer: accounts[0], signature: submissions[0].signatures[0] },
                { signer: accounts[1], signature: submissions[1].signatures[0] },
                { signer: accounts[2], signature: submissions[2].signatures[0] },
                { signer: accounts[3], signature: submissions[3].signatures[0] },
                { signer: accounts[4], signature: submissions[4].signatures[0] },
            ]);

            const tx = derivadex.rejectContract
                .reject(submissions, signatures, indexes, epochId)
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Checkpoint: paused.');
        });
    });
});
