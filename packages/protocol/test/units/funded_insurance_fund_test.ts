import { BigNumber, hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import {
    DDXApprovalEventArgs,
    DDXDelegateVotesChangedEventArgs,
    Derivadex,
    DummyTokenContract,
    ERC20ApprovalEventArgs,
    ERC20TransferEventArgs,
    FundedInsuranceFundFundedInsuranceFundUpdatedEventArgs,
    SafeERC20WrapperContract,
    TestCheckpointContract,
    TestRegistrationContract,
} from '@derivadex/contract-wrappers';
import {
    advanceBlocksAsync,
    advanceTimeAsync,
    generateCallData,
    generateTraderGroupKey,
    getSelectors,
    ItemKind,
} from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { FacetCutAction } from '@derivadex/types';
import { ContractArtifact, LogWithDecodedArgs, TransactionReceiptWithDecodedLogs } from 'ethereum-types';
import { ethers } from 'ethers';
import * as hardhat from 'hardhat';
import _ from 'lodash';

import { setupWithProviderAsync } from '../../deployment/setup';
import { Fixtures, GUSD, NULL_BYTES32, USDC, USDT, ZERO_ADDRESS } from '../fixtures';
import { TestFundedInsuranceFundContract } from '../generated-wrappers';
import { filterEventLogs } from '../utils';

const MAX_UINT256 = new BigNumber('0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff');

const artifactAbis = _.mapValues(artifacts, (artifact: ContractArtifact) => artifact.compilerOutput.abi);

const PUSH_LEAF_OPCODE = '0x4C';

enum FundedInsuranceFundUpdateKind {
    Deposit,
    Withdraw,
}

interface FundedInsuranceFundPosition {
    freeBalance: { tokens: string[]; amounts: BigNumber[] };
    frozenBalance: { tokens: string[]; amounts: BigNumber[] };
}

// TODO(jalextowle): The strategy used to utilize the test contracts is better in
// this test file that it is in the Trader and Operator tests. Utilize this pattern
// there and remove all instances of test contracts from the Deployer.
describe('FundedInsuranceFund Unit Tests', () => {
    const funders: string[] = [];
    let derivadex: Derivadex;
    let fundedInsuranceFund: TestFundedInsuranceFundContract;
    let registration: TestRegistrationContract;
    let checkpoint: TestCheckpointContract;
    let accounts: string[];
    let owner: string;
    let fixtures: Fixtures;
    let usdcContract: DummyTokenContract;
    let usdtContract: DummyTokenContract;
    let gusdContract: DummyTokenContract;

    const MAX_SUPPLY = new BigNumber('100000000e18');
    const OPERATOR_DECIMALS = 6;

    async function scaleAmountForTokenAsync(
        token: DummyTokenContract | SafeERC20WrapperContract,
        amount: BigNumber,
    ): Promise<BigNumber> {
        let scaledAmount = amount;
        const decimals = await token.decimals().callAsync();
        if (decimals > OPERATOR_DECIMALS) {
            scaledAmount = scaledAmount.dividedToIntegerBy(
                new BigNumber(10).exponentiatedBy(decimals - OPERATOR_DECIMALS),
            );
        } else if (decimals < OPERATOR_DECIMALS) {
            scaledAmount = scaledAmount.multipliedBy(new BigNumber(10).exponentiatedBy(OPERATOR_DECIMALS - decimals));
        }
        return scaledAmount;
    }

    /**
     * TODO(jalextowle): This is now used in several files. It probably makes
     * sense to make this a little bit more generic and add this to the
     * `DerivaDEX` contract wrapper.
     *
     * Passes a governance proposal that has already been made using the fixtures.
     * @param proposalNumber The index of the proposal to pass.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function passProposalAsync(proposalNumber: number): Promise<TransactionReceiptWithDecodedLogs> {
        await advanceBlocksAsync(derivadex.providerEngine, 2);
        await derivadex
            .castVote(proposalNumber, true)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalNumber).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, 259_200);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        return derivadex.execute(proposalNumber).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
    }

    /**
     * Attempts to add exchange collateral.
     * @param collateralAddress The address of the collateral.
     * @param minimumRateLimit The minimum rate limit that should be used.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function addExchangeCollateralAsync(
        collateralAddress: string,
        minimumRateLimit: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.collateralContract.getFunctionSignature('addExchangeCollateral')];
        const calldatas = [
            generateCallData(
                derivadex.collateralContract
                    .addExchangeCollateral(collateralAddress, minimumRateLimit)
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add exchange collateral.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * A convenience method that performs all of the necessary actions required
     * to add a funder to the funders array and ensure that they can fund the
     * FundedInsuranceFund.
     * @param funder The funder address to add.
     */
    async function addFunderAsync(funder: string): Promise<void> {
        funders.push(funder);
        await usdtContract
            .approve(derivadex.derivaDEXContract.address, MAX_UINT256)
            .awaitTransactionSuccessAsync({ from: funder });
    }

    /**
     * Funds the funded insurance fund with withdrawal fees.
     * @param collateralName The encoded name of the collateral to use when
     *        funding
     * @param amount The amount of collateral to use when funding.
     * @return The transaction receipt.
     */
    async function fundWithWithdrawalFeesAsync(
        collateralName: string,
        amount: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.fundedInsuranceFundContract.getFunctionSignature('fundWithWithdrawalFees')];
        const calldatas = [
            generateCallData(
                derivadex.fundedInsuranceFundContract
                    .fundWithWithdrawalFees(collateralName, amount)
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add exchange collateral.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Attempts to set the maximum DDX cap.
     * @param maxDDXCap The maximum amount of DDX that can be held in the SMT.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function setMaxDDXCapAsync(maxDDXCap: BigNumber): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.stakeContract.getFunctionSignature('setMaxDDXCap')];
        const calldatas = [
            generateCallData(derivadex.stakeContract.setMaxDDXCap(maxDDXCap).getABIEncodedTransactionData()),
        ];
        const description = 'Set the maximum DDX cap.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Attempts to set the rate limit parameters.
     * @param rateLimitPeriod The rate limit period.
     * @param rateLimitPercentage The rate limit percentage.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function setRateLimitParametersAsync(
        rateLimitPeriod: BigNumber,
        rateLimitPercentage: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.collateralContract.getFunctionSignature('setRateLimitParameters')];
        const calldatas = [
            generateCallData(
                derivadex.collateralContract
                    .setRateLimitParameters(rateLimitPeriod, rateLimitPercentage)
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Set the rate limit parameters.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    /**
     * Attempts to update exchange collateral.
     * @param collateralAddress The address of the collateral.
     * @param minimumRateLimit The minimum rate limit that should be used.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function updateExchangeCollateralAsync(
        collateralAddress: string,
        minimumRateLimit: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.collateralContract.getFunctionSignature('updateExchangeCollateral')];
        const calldatas = [
            generateCallData(
                derivadex.collateralContract
                    .updateExchangeCollateral(collateralAddress, minimumRateLimit)
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Update exchange collateral.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });

        // Set up the DerivaDEX contracts and redeploy token contracts.
        ({ derivadex, accounts, owner } = await setupWithProviderAsync(provider, {
            isFork: false,
            isGanache: true,
            isLocalSnapshot: false,
            isTest: true,
            useDeployedDDXToken: false,
        }));
        fixtures = new Fixtures(derivadex, accounts, owner);
        usdcContract = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            provider,
            { from: fixtures.derivaDEXWallet() },
            artifacts,
            'Circle USD Token',
            'USDC',
            new BigNumber(6),
        );
        usdtContract = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            provider,
            { from: fixtures.derivaDEXWallet() },
            artifacts,
            'Tether USD Token',
            'USDT',
            new BigNumber(6),
        );
        gusdContract = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            provider,
            { from: fixtures.derivaDEXWallet() },
            artifacts,
            'Gemini USD Token',
            'GUSD',
            new BigNumber(6),
        );

        for (const funder of accounts.slice(0, 3)) {
            await addFunderAsync(funder);
        }

        await derivadex
            .transferOwnershipToDerivaDEXProxyDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        await derivadex
            .transferDDX(fixtures.traderB(), 10000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderC(), 5000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderD(), 25000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        await derivadex.ddxContract
            .delegate(fixtures.traderE())
            .awaitTransactionSuccessAsync({ from: fixtures.traderD() });
        await advanceBlocksAsync(derivadex.providerEngine, 1);

        // Add Pause facet to the diamond
        const pauseSelectors = getSelectors(derivadex.pauseContract);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.pauseContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: pauseSelectors,
                    },
                ],
                derivadex.pauseContract.address,
                derivadex.initializePause().getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setPauseAddressToProxy();

        const governanceSelectors = getSelectors(derivadex.governanceContract, ['initialize']);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.governanceContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: governanceSelectors,
                    },
                ],
                derivadex.governanceContract.address,
                derivadex.initializeGovernance(10, 1, 17280, 1209600, 259200, 4, 1, 50).getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setGovernanceAddressToProxy();

        await derivadex.transferOwnershipToSelf().awaitTransactionSuccessAsync({ from: owner });

        // Add Collateral facet to the diamond
        const collateralSelectors = getSelectors(derivadex.collateralContract, ['initialize']);
        const collateralTargets = [derivadex.derivaDEXContract.address, derivadex.derivaDEXContract.address];
        const collateralValues = [0, 0];
        const collateralSignatures = [
            derivadex.diamondFacetContract.getFunctionSignature('diamondCut'),
            derivadex.collateralContract.getFunctionSignature('addExchangeCollateral'),
        ];
        const collateralCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.collateralContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: collateralSelectors,
                            },
                        ],
                        derivadex.collateralContract.address,
                        derivadex.collateralContract
                            .initialize(
                                new BigNumber(50),
                                new BigNumber(10),
                                new BigNumber(1_000),
                                Web3Wrapper.toBaseUnitAmount(1_000, 6),
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
            generateCallData(
                derivadex.collateralContract
                    .addExchangeCollateral(usdtContract.address, new BigNumber(100_000))
                    .getABIEncodedTransactionData(),
            ),
        ];
        const collateralDescription = 'Add Collateral contract as a facet.';

        await derivadex
            .propose(
                collateralTargets,
                collateralValues,
                collateralSignatures,
                collateralCalldatas,
                collateralDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        let proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setCollateralAddressToProxy();

        // Add Stake facet to the diamond
        const stakeSelectors = getSelectors(derivadex.stakeContract, ['initialize']);
        const stakeTargets = [derivadex.derivaDEXContract.address];
        const stakeValues = [0];
        const stakeSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const stakeCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.stakeContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: stakeSelectors,
                            },
                        ],
                        derivadex.stakeContract.address,
                        derivadex.stakeContract
                            .initialize(
                                Web3Wrapper.toBaseUnitAmount(10_000, 18),
                                Web3Wrapper.toBaseUnitAmount(40_000_000, 18),
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const stakeDescription = 'Add Stake contract as a facet.';

        await derivadex
            .propose(stakeTargets, stakeValues, stakeSignatures, stakeCalldatas, stakeDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setStakeAddressToProxy();

        // Added the Custodian facet to the DerivaDEX proxy.
        const custodianSelectors = getSelectors(derivadex.custodianContract, ['initialize']);
        const custodianTargets = [derivadex.derivaDEXContract.address];
        const custodianValues = [0];
        const custodianSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const custodianCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.custodianContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: custodianSelectors,
                            },
                        ],
                        derivadex.custodianContract.address,
                        derivadex.custodianContract
                            .initialize(new BigNumber(100), new BigNumber(200))
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const custodianDescription = 'Add Custodian contract as a facet.';
        await derivadex
            .propose(custodianTargets, custodianValues, custodianSignatures, custodianCalldatas, custodianDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);

        // Added the Registration facet to the DerivaDEX proxy.
        registration = await TestRegistrationContract.deployFrom0xArtifactAsync(
            artifacts.TestRegistration,
            derivadex.providerEngine,
            {
                from: fixtures.derivaDEXWallet(),
            },
            artifacts,
        );
        const registrationSelectors = getSelectors(registration, ['initialize']);
        const registrationTargets = [derivadex.derivaDEXContract.address];
        const registrationValues = [0];
        const registrationSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const registrationCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: registration.address,
                                action: FacetCutAction.Add,
                                functionSelectors: registrationSelectors,
                            },
                        ],
                        registration.address,
                        registration
                            .initialize({ mrEnclave: '0xdeadbeef', isvSvn: '0x0000' }, [])
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const registrationDescription = 'Add Registration contract as a facet.';
        await derivadex
            .propose(
                registrationTargets,
                registrationValues,
                registrationSignatures,
                registrationCalldatas,
                registrationDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        registration = new TestRegistrationContract(derivadex.derivaDEXContract.address, derivadex.providerEngine, {
            from: fixtures.derivaDEXWallet(),
        });

        // Added the Checkpoint facet to the DerivaDEX proxy.
        checkpoint = await TestCheckpointContract.deployFrom0xArtifactAsync(
            artifacts.TestCheckpoint,
            derivadex.providerEngine,
            {
                from: fixtures.derivaDEXWallet(),
            },
            artifacts,
        );
        const checkpointSelectors = getSelectors(checkpoint, ['initialize']);
        const checkpointTargets = [derivadex.derivaDEXContract.address];
        const checkpointValues = [0];
        const checkpointSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const checkpointCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: checkpoint.address,
                                action: FacetCutAction.Add,
                                functionSelectors: checkpointSelectors,
                            },
                        ],
                        checkpoint.address,
                        checkpoint.initialize(new BigNumber(51), new BigNumber(2)).getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const checkpointDescription = 'Add Checkpoint contract as a facet.';
        await derivadex
            .propose(
                checkpointTargets,
                checkpointValues,
                checkpointSignatures,
                checkpointCalldatas,
                checkpointDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        checkpoint = new TestCheckpointContract(derivadex.derivaDEXContract.address, derivadex.providerEngine, {
            from: fixtures.derivaDEXWallet(),
        });
        await checkpoint.setEpochId(new BigNumber(1)).awaitTransactionSuccessAsync();

        // Added the Banner facet to the DerivaDEX proxy.
        const bannerSelectors = getSelectors(derivadex.bannerContract, ['initialize']);
        const bannerTargets = [derivadex.derivaDEXContract.address];
        const bannerValues = [0];
        const bannerSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const bannerCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.bannerContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: bannerSelectors,
                            },
                        ],
                        derivadex.bannerContract.address,
                        derivadex.bannerContract
                            .initialize([accounts[accounts.length - 1]])
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const bannerDescription = 'Add Banner contract as a facet.';
        await derivadex
            .propose(bannerTargets, bannerValues, bannerSignatures, bannerCalldatas, bannerDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setBannerAddressToProxy();

        // Add the Insurance Fund facet to the diamond.
        const insuranceFundSelectors = getSelectors(derivadex.insuranceFundContract, ['initialize']);
        const insuranceFundTargets = [
            derivadex.derivaDEXContract.address,
            derivadex.derivaDEXContract.address,
            derivadex.derivaDEXContract.address,
        ];
        const insuranceFundValues = [0, 0, 0];
        const insuranceFundSignatures = [
            derivadex.diamondFacetContract.getFunctionSignature('diamondCut'),
            derivadex.insuranceFundContract.getFunctionSignature('addInsuranceFundCollateral'),
            derivadex.insuranceFundContract.getFunctionSignature('addInsuranceFundCollateral'),
        ];
        const insuranceFundCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.insuranceFundContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: insuranceFundSelectors,
                            },
                        ],
                        derivadex.insuranceFundContract.address,
                        derivadex
                            .initializeInsuranceFund(
                                50,
                                995,
                                1.189117199391172,
                                10,
                                2102400,
                                derivadex.diFundTokenFactoryContract.address,
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
            generateCallData(
                derivadex
                    .addInsuranceFundCollateral(
                        USDT.collateralName,
                        USDT.collateralSymbol,
                        ZERO_ADDRESS,
                        usdtContract.address,
                        USDT.collateralStakingType,
                    )
                    .getABIEncodedTransactionData(),
            ),
            generateCallData(
                derivadex
                    .addInsuranceFundCollateral(
                        GUSD.collateralName,
                        GUSD.collateralSymbol,
                        ZERO_ADDRESS,
                        gusdContract.address,
                        GUSD.collateralStakingType,
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const insuranceFundDescription = 'Add InsuranceFund contract as a facet.';
        await derivadex
            .propose(
                insuranceFundTargets,
                insuranceFundValues,
                insuranceFundSignatures,
                insuranceFundCalldatas,
                insuranceFundDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setInsuranceFundAddressToProxy();

        // Sets rate limit parameters.
        await setRateLimitParametersAsync(new BigNumber(50), new BigNumber(100));
    });

    it('adds FundedInsuranceFund facet', async () => {
        fundedInsuranceFund = await TestFundedInsuranceFundContract.deployFrom0xArtifactAsync(
            artifacts.TestFundedInsuranceFund,
            derivadex.providerEngine,
            { from: fixtures.derivaDEXWallet() },
            artifacts,
        );
        const selectors = getSelectors(fundedInsuranceFund);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: fundedInsuranceFund.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        ZERO_ADDRESS,
                        '0x',
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add FundedInsuranceFund contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        fundedInsuranceFund = new TestFundedInsuranceFundContract(
            derivadex.derivaDEXContract.address,
            derivadex.providerEngine,
            { from: fixtures.derivaDEXWallet() },
            artifactAbis,
        );
    });

    // TODO(jalextowle): Add test cases for DDX collateral.
    describe('#fund', () => {
        it('should fail if the sender is banned', async () => {
            // This account was banned during initialization of the Banner facet.
            const funder = accounts[accounts.length - 1];

            const tx = fundedInsuranceFund
                .fund(usdtContract.address, new BigNumber(1))
                .awaitTransactionSuccessAsync({ from: funder });
            return expect(tx).to.be.rejectedWith('FundedInsuranceFund: banned addresses cannot fund.');
        });

        it('should fail if the collateral has not been listed', async () => {
            const funder = funders[0];
            const tx = fundedInsuranceFund
                .fund(usdcContract.address, new BigNumber(1))
                .awaitTransactionSuccessAsync({ from: funder });
            return expect(tx).to.be.rejectedWith("FundedInsuranceFund: can't fund an unlisted token.");
        });

        it('should fail if the funder does not have an allowance set on the DerivaDEX proxy', async () => {
            const funder = accounts[4];
            await usdtContract.mint(funder, new BigNumber(1)).awaitTransactionSuccessAsync();
            const tx = fundedInsuranceFund
                .fund(usdtContract.address, new BigNumber(1))
                .awaitTransactionSuccessAsync({ from: funder });
            await expect(tx).to.be.rejectedWith('ERC20: insufficient allowance');
        });

        it('should fail if the funder does not have the required balance', async () => {
            const funder = funders[0];
            const balance = await usdtContract.balanceOf(funder).callAsync();
            const tx = fundedInsuranceFund
                .fund(usdtContract.address, balance.plus(1))
                .awaitTransactionSuccessAsync({ from: funder });
            await expect(tx).to.be.rejectedWith('ERC20: transfer amount exceeds balance');
        });

        it('should successfully fund the funded insurance fund', async () => {
            const funder = funders[0];
            const funderBalance = await usdtContract.balanceOf(funder).callAsync();
            const proxyBalance = await usdtContract.balanceOf(derivadex.derivaDEXContract.address).callAsync();

            const { logs } = await fundedInsuranceFund
                .fund(usdtContract.address, funderBalance)
                .awaitTransactionSuccessAsync({ from: funder });

            // Verify that the event logs are correct.
            expect(logs.length).to.be.eq(2);
            const fundedLog = logs[0] as LogWithDecodedArgs<FundedInsuranceFundFundedInsuranceFundUpdatedEventArgs>;
            expect(fundedLog.event).to.be.eq('FundedInsuranceFundUpdated');
            expect(fundedLog.args).to.be.deep.eq({
                amount: funderBalance,
                collateralAddress: usdtContract.address,
                contributor: funder,
                updateKind: FundedInsuranceFundUpdateKind.Deposit,
            });
            const transferLog = logs[1] as LogWithDecodedArgs<ERC20TransferEventArgs>;
            expect(transferLog.event).to.be.eq('Transfer');
            expect(transferLog.args.from).to.be.eq(funder);
            expect(transferLog.args.to).to.be.eq(derivadex.derivaDEXContract.address);
            expect(transferLog.args.value).to.be.bignumber.eq(funderBalance);

            // Verify that the funder and DerivaDEX proxy address were updated
            // appropriately.
            const postFundingFunderBalance = await usdtContract.balanceOf(funder).callAsync();
            const postFundingProxyBalance = await usdtContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            expect(postFundingFunderBalance).to.be.bignumber.eq(new BigNumber(0));
            expect(postFundingProxyBalance).to.be.bignumber.eq(proxyBalance.plus(funderBalance));
        });

        it('should fail to fund with DDX when the deposit exhausts the DDX max cap', async () => {
            const funder = funders[0];
            await setMaxDDXCapAsync(new BigNumber(1));
            await addExchangeCollateralAsync(derivadex.ddxContract.address, new BigNumber(100_000));
            await derivadex.ddxContract
                .approve(derivadex.derivaDEXContract.address, Web3Wrapper.toBaseUnitAmount(1, 12))
                .awaitTransactionSuccessAsync({ from: funder });

            const depositAmount = new BigNumber(10);
            const tx = fundedInsuranceFund
                .fund(derivadex.ddxContract.address, depositAmount)
                .awaitTransactionSuccessAsync({ from: funder });
            await expect(tx).to.be.rejectedWith(
                'LibCollateral: DDX deposit exceeds the maximum allowed DDX in the SMT.',
            );

            // Clean up the max DDX cap.
            await setMaxDDXCapAsync(MAX_SUPPLY.dividedToIntegerBy(2));
        });

        it('should successfully fund with DDX', async () => {
            // Get the balances before the transfer.
            const funder = funders[0];
            const ddxWithdrawalAllowanceBefore = await derivadex.collateralContract
                .getWithdrawalAllowance(derivadex.ddxContract.address)
                .callAsync();
            const traderBalanceBefore = await derivadex.ddxContract.balanceOf(funder).callAsync();
            const traderVotesBefore = await derivadex.ddxContract.getCurrentVotes(funder).callAsync();
            const diamondBalanceBefore = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();

            const depositAmount = Web3Wrapper.toBaseUnitAmount(1, 12);
            const { logs } = await fundedInsuranceFund
                .fund(derivadex.ddxContract.address, depositAmount)
                .awaitTransactionSuccessAsync({ from: funder });

            // Verify the logs.
            expect(logs.length).to.be.eq(5);
            const log = logs[0] as LogWithDecodedArgs<FundedInsuranceFundFundedInsuranceFundUpdatedEventArgs>;
            expect(log.event).to.be.eq('FundedInsuranceFundUpdated');
            expect(log.args).to.be.deep.eq({
                amount: new BigNumber(1),
                collateralAddress: derivadex.ddxContract.address,
                contributor: funder,
                updateKind: FundedInsuranceFundUpdateKind.Deposit,
            });
            const approvalLog = logs[1] as LogWithDecodedArgs<DDXApprovalEventArgs>;
            expect(approvalLog.event).to.be.eq('Approval');
            expect(approvalLog.args).to.be.deep.eq({
                owner: funder,
                spender: derivadex.derivaDEXContract.address,
                value: Web3Wrapper.toBaseUnitAmount(1, 12).minus(depositAmount),
            });
            const transferLog = logs[2] as LogWithDecodedArgs<ERC20TransferEventArgs>;
            expect(transferLog.event).to.be.eq('Transfer');
            expect(transferLog.args).to.be.deep.eq({
                from: funder,
                to: derivadex.derivaDEXContract.address,
                value: depositAmount,
            });
            const traderDelegateVotesChangedLog = logs[3] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
            expect(traderDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
            expect(traderDelegateVotesChangedLog.args).to.be.deep.eq({
                delegate: funder,
                previousBalance: traderVotesBefore,
                newBalance: traderVotesBefore.minus(depositAmount),
            });
            const diamondDelegateVotesChangedLog = logs[4] as LogWithDecodedArgs<DDXDelegateVotesChangedEventArgs>;
            expect(diamondDelegateVotesChangedLog.event).to.be.eq('DelegateVotesChanged');
            expect(diamondDelegateVotesChangedLog.args).to.be.deep.eq({
                delegate: derivadex.derivaDEXContract.address,
                previousBalance: diamondBalanceBefore,
                newBalance: diamondBalanceBefore.plus(depositAmount),
            });

            // Verify the DDX balances after the deposit.
            const diamondBalanceAfter = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const traderBalanceAfter = await derivadex.ddxContract.balanceOf(funder).callAsync();
            expect(diamondBalanceAfter).to.be.bignumber.eq(diamondBalanceBefore.plus(depositAmount));
            expect(traderBalanceAfter).to.be.bignumber.eq(traderBalanceBefore.minus(depositAmount));

            // Ensure that the DDX withdrawal allowance was increased.
            const ddxWithdrawalAllowanceAfter = await derivadex.collateralContract
                .getWithdrawalAllowance(derivadex.ddxContract.address)
                .callAsync();
            expect(ddxWithdrawalAllowanceAfter).to.be.bignumber.eq(ddxWithdrawalAllowanceBefore.plus(depositAmount));
        });
    });

    describe('#fundWithWithdrawalFees', () => {
        before(async () => {
            // Deposit USDC into the InsuranceFund and then immediately
            // unstake it to collect USDC in withdrawal fees.
            const amount = new BigNumber('100e6').times(new BigNumber('1e6'));
            await usdtContract
                .mint(fixtures.traderA(), amount)
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            await usdtContract
                .approve(derivadex.derivaDEXContract.address, MAX_UINT256)
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            await derivadex.insuranceFundContract
                .stakeToInsuranceFund(ethers.utils.formatBytes32String(USDT.collateralName), amount)
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            await derivadex.insuranceFundContract
                .withdrawFromInsuranceFund(ethers.utils.formatBytes32String(USDT.collateralName), amount)
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        });

        it('should fail when called directly', async () => {
            const tx = fundedInsuranceFund
                .fundWithWithdrawalFees(ethers.utils.formatBytes32String(USDC.collateralName), new BigNumber(10))
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            return expect(tx).to.be.rejectedWith('FundedInsuranceFund: must be called by Gov.');
        });

        it('should fail when called with an invalid collateral name', async () => {
            const tx = fundWithWithdrawalFeesAsync(ethers.utils.formatBytes32String('invalid'), new BigNumber(1000));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it("should fail when exchange collateral isn't listed", async () => {
            const tx = fundWithWithdrawalFeesAsync(
                ethers.utils.formatBytes32String(GUSD.collateralName),
                new BigNumber(1000),
            );
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it("should fail if there aren't sufficient withdrawal fees", async () => {
            const stakeCollateral = await derivadex.insuranceFundContract
                .getStakeCollateralByCollateralName(ethers.utils.formatBytes32String(USDT.collateralName))
                .callAsync();
            const tx = fundWithWithdrawalFeesAsync(
                ethers.utils.formatBytes32String(USDT.collateralName),
                stakeCollateral.withdrawalFeeCap.plus(1),
            );
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should successfully fund with withdrawal fees', async () => {
            const stakeCollateralBefore = await derivadex.insuranceFundContract
                .getStakeCollateralByCollateralName(ethers.utils.formatBytes32String(USDT.collateralName))
                .callAsync();
            const { logs } = await fundWithWithdrawalFeesAsync(
                ethers.utils.formatBytes32String(USDT.collateralName),
                stakeCollateralBefore.withdrawalFeeCap,
            );

            // Ensure that a FundedInsuranceFundUpdate event was emitted.
            const filteredLogs = filterEventLogs(logs, 'FundedInsuranceFundUpdated');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog =
                filteredLogs[0] as LogWithDecodedArgs<FundedInsuranceFundFundedInsuranceFundUpdatedEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                contributor: derivadex.derivaDEXContract.address,
                collateralAddress: usdtContract.address,
                amount: await scaleAmountForTokenAsync(usdtContract, stakeCollateralBefore.withdrawalFeeCap),
                updateKind: FundedInsuranceFundUpdateKind.Deposit,
            });

            // Ensure that the state was updated correctly.
            const stakeCollateralAfter = await derivadex.insuranceFundContract
                .getStakeCollateralByCollateralName(ethers.utils.formatBytes32String(USDT.collateralName))
                .callAsync();
            expect(stakeCollateralAfter.withdrawalFeeCap).to.be.bignumber.eq(0);
        });

        it('should fail to fund with withdrawal fees once the fee cap is exhausted', async () => {
            const tx = fundWithWithdrawalFeesAsync(
                ethers.utils.formatBytes32String(USDT.collateralName),
                new BigNumber(1),
            );
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });
    });

    // TODO(jalextowle): Add test cases for DDX collateral.
    describe('#unfund', () => {
        let committedLeaf: FundedInsuranceFundPosition;
        let funderAddress: string;
        let firstWithdrawalBlockNumber: number;
        const strategyId = NULL_BYTES32;

        before(async () => {
            funderAddress = fixtures.traderA();
            committedLeaf = {
                freeBalance: { tokens: [], amounts: [] },
                frozenBalance: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, new BigNumber(10_000))],
                },
            };

            // List USDC as collateral.
            await addExchangeCollateralAsync(usdcContract.address, new BigNumber(100_000));

            // Fund USDC.
            const fundAmount = new BigNumber(100_000);
            await usdcContract
                .approve(derivadex.derivaDEXContract.address, fundAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            await fundedInsuranceFund
                .fund(usdcContract.address, fundAmount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        });

        async function updateStateRootAsync(leaf: FundedInsuranceFundPosition): Promise<void> {
            const key = generateTraderGroupKey(ItemKind.InsuranceFundContribution, funderAddress);
            const value = await fundedInsuranceFund.hashFundedInsuranceFundPositionPublic(leaf).callAsync();
            const stateRoot = hexUtils.hash(hexUtils.concat(key, value));
            await fundedInsuranceFund.setStateRoot(stateRoot).awaitTransactionSuccessAsync();
        }

        async function assertFailedUnfundAsync(
            leaf: FundedInsuranceFundPosition,
            unfundAmount: BigNumber,
            revertReason: string,
        ): Promise<void> {
            const proof = PUSH_LEAF_OPCODE;
            const tx = fundedInsuranceFund
                .unfund(
                    leaf,
                    {
                        tokens: [usdcContract.address],
                        amounts: [unfundAmount],
                    },
                    proof,
                )
                .awaitTransactionSuccessAsync({ from: funderAddress });
            return expect(tx).to.be.rejectedWith(revertReason);
        }

        async function assertSuccessfulUnfundAsync(
            leaf: FundedInsuranceFundPosition,
            unfundAmount: BigNumber,
        ): Promise<void> {
            // Get the starting unprocessed withdrawals.
            const unprocessedWithdrawalsBefore = await derivadex.collateralContract
                .getUnprocessedWithdrawals(funderAddress, strategyId, usdcContract.address)
                .callAsync();

            // Get the starting token balances.
            const derivadexUSDCBalanceBefore = await usdcContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const traderUSDCBalanceBefore = await usdcContract.balanceOf(funderAddress).callAsync();

            // Execute the withdrawal.
            const proof = PUSH_LEAF_OPCODE;
            const { logs, blockNumber } = await fundedInsuranceFund
                .unfund(
                    leaf,
                    {
                        tokens: [usdcContract.address],
                        amounts: [unfundAmount],
                    },
                    proof,
                )
                .awaitTransactionSuccessAsync({ from: funderAddress });
            if (firstWithdrawalBlockNumber === undefined) {
                firstWithdrawalBlockNumber = blockNumber;
            }

            // Ensure that the unprocessed withdrawals were updated correctly.
            const unprocessedWithdrawalsAfter = await derivadex.collateralContract
                .getUnprocessedWithdrawals(funderAddress, strategyId, usdcContract.address)
                .callAsync();
            expect(unprocessedWithdrawalsAfter).to.be.bignumber.eq(
                unprocessedWithdrawalsBefore.plus(await scaleAmountForTokenAsync(usdcContract, unfundAmount)),
            );

            // Ensure that the ending token balances are correct.
            const derivadexUSDCBalanceAfter = await usdcContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const traderUSDCBalanceAfter = await usdcContract.balanceOf(funderAddress).callAsync();
            expect(derivadexUSDCBalanceAfter).to.be.bignumber.eq(derivadexUSDCBalanceBefore.minus(unfundAmount));
            expect(traderUSDCBalanceAfter).to.be.bignumber.eq(traderUSDCBalanceBefore.plus(unfundAmount));

            // Ensure that the logs and the state are correct after the withdrawal.
            expect(logs.length).to.be.eq(2);
            const transferLog = logs[0] as LogWithDecodedArgs<ERC20TransferEventArgs>;
            expect(transferLog.event).to.be.eq('Transfer');
            expect(transferLog.args).to.be.deep.eq({
                from: derivadex.derivaDEXContract.address,
                to: funderAddress,
                value: unfundAmount,
            });
            const unfundLog = logs[1] as LogWithDecodedArgs<FundedInsuranceFundFundedInsuranceFundUpdatedEventArgs>;
            expect(unfundLog.event).to.be.eq('FundedInsuranceFundUpdated');
            expect(unfundLog.args).to.be.deep.eq({
                amount: await scaleAmountForTokenAsync(usdcContract, unfundAmount),
                collateralAddress: usdcContract.address,
                contributor: funderAddress,
                updateKind: FundedInsuranceFundUpdateKind.Withdraw,
            });
        }

        it('should fail if the state root was not recovered', async () => {
            await assertFailedUnfundAsync(
                committedLeaf,
                new BigNumber(3_000),
                'FundedInsuranceFund: Did not recover state root.',
            );
        });

        it('should successfully partially unfund 3,000', async () => {
            await updateStateRootAsync(committedLeaf);
            await assertSuccessfulUnfundAsync(committedLeaf, new BigNumber(3_000));
        });

        it('should successfully partially unfund 5', async () => {
            await assertSuccessfulUnfundAsync(committedLeaf, new BigNumber(5));
        });

        it('should successfully partially unfund 6,995', async () => {
            await assertSuccessfulUnfundAsync(committedLeaf, new BigNumber(6_995));
        });

        it('should fail to unfund 1 after fully unfunding', async () => {
            await assertFailedUnfundAsync(committedLeaf, new BigNumber(1), 'LibCollateral: Invalid withdrawal amount.');
        });

        it('should fail to unfund 1 after fully unfunding if all of the unfunds are unprocessed', async () => {
            await checkpoint
                .setLastConfirmedBlockNumber(new BigNumber(firstWithdrawalBlockNumber - 1))
                .awaitTransactionSuccessAsync();
            await assertFailedUnfundAsync(committedLeaf, new BigNumber(1), 'LibCollateral: Invalid withdrawal amount.');
        });

        it('should fail to unfund 3,000 if the last two unfunds are unprocessed and the frozen collateral is 9,000', async () => {
            committedLeaf = {
                freeBalance: { tokens: [], amounts: [] },
                frozenBalance: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, new BigNumber(9_000))],
                },
            };
            await updateStateRootAsync(committedLeaf);
            await checkpoint
                .setLastConfirmedBlockNumber(new BigNumber(firstWithdrawalBlockNumber))
                .awaitTransactionSuccessAsync();
            await assertFailedUnfundAsync(
                committedLeaf,
                new BigNumber(3_000),
                'LibCollateral: Invalid withdrawal amount.',
            );
        });

        it('should successfully unfund 2,000', async () => {
            await assertSuccessfulUnfundAsync(committedLeaf, new BigNumber(2_000));
        });

        it('should fail to unfund when the withdrawal allowance is exhausted', async () => {
            committedLeaf = {
                freeBalance: { tokens: [], amounts: [] },
                frozenBalance: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, new BigNumber(100_000))],
                },
            };
            await updateStateRootAsync(committedLeaf);
            await assertFailedUnfundAsync(
                committedLeaf,
                new BigNumber(100_000),
                'LibCollateral: Maximum withdrawal exceeded.',
            );
        });

        it('should successfully unfund less than the rate limit', async () => {
            // Update the USDC rate limits.
            await setRateLimitParametersAsync(new BigNumber(50), new BigNumber(1));
            await updateExchangeCollateralAsync(usdcContract.address, new BigNumber(1_000));

            // Mine enough blocks to advance the period.
            await advanceBlocksAsync(derivadex.providerEngine, 50);

            // Successfully unfund less than the rate limit.
            committedLeaf = {
                freeBalance: { tokens: [], amounts: [] },
                frozenBalance: {
                    tokens: [usdcContract.address],
                    amounts: [await scaleAmountForTokenAsync(usdcContract, new BigNumber(15_000))],
                },
            };
            await updateStateRootAsync(committedLeaf);
            await assertSuccessfulUnfundAsync(committedLeaf, new BigNumber(1_000));
        });

        it('should fail to unfund more than the rate limit', async () => {
            await assertFailedUnfundAsync(
                committedLeaf,
                new BigNumber(3_000),
                'LibCollateral: Maximum withdrawal exceeded.',
            );

            // Update the global rate limits for future tests.
            await setRateLimitParametersAsync(new BigNumber(50), new BigNumber(100));
        });
    });

    describe('#isNotPaused', () => {
        before(async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.pauseContract.getFunctionSignature('setIsPaused')];
            const calldatas = [
                generateCallData(derivadex.pauseContract.setIsPaused(true).getABIEncodedTransactionData()),
            ];
            const description = 'Pause the DerivaDEX diamond.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            await passProposalAsync(proposalCount);
        });

        after(async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.pauseContract.getFunctionSignature('setIsPaused')];
            const calldatas = [
                generateCallData(derivadex.pauseContract.setIsPaused(false).getABIEncodedTransactionData()),
            ];
            const description = 'Unpause the DerivaDEX diamond.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            await passProposalAsync(proposalCount);
        });

        it('should fail to call `fund` when paused', async () => {
            const tx = fundedInsuranceFund
                .fund(usdcContract.address, new BigNumber(1))
                .awaitTransactionSuccessAsync({ from: funders[0] });
            return expect(tx).to.be.rejectedWith('FundedInsuranceFund: paused.');
        });

        it('should fail to call `fundWithWithdrawalFees` when paused', async () => {
            const tx = fundWithWithdrawalFeesAsync(usdcContract.address, new BigNumber(1));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('should fail to call `unfund` when paused', async () => {
            const emptyBalance = { tokens: [], amounts: [] };
            const tx = fundedInsuranceFund
                .unfund({ freeBalance: emptyBalance, frozenBalance: emptyBalance }, emptyBalance, '0x')
                .awaitTransactionSuccessAsync({ from: funders[0] });
            return expect(tx).to.be.rejectedWith('FundedInsuranceFund: paused.');
        });
    });
});
