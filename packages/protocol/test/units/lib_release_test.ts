import { hexUtils } from '@0x/utils';
import { BigNumber } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import { expect } from '@derivadex/test-utils';
import hardhat from 'hardhat';

import { NULL_BYTES32 } from '../fixtures';
import { TestLibReleaseContract } from '../generated-wrappers';

describe('LibRelease Unit Tests', () => {
    let sender: string;
    let tester: TestLibReleaseContract;
    let web3Wrapper: Web3Wrapper;

    before(async () => {
        const provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });
        [sender] = await web3Wrapper.getAvailableAddressesAsync();
        tester = await TestLibReleaseContract.deployFrom0xArtifactAsync(
            artifacts.TestLibRelease,
            provider,
            { from: sender },
            artifacts,
        );
    });

    describe('#commitReleaseScheduleUpdate', () => {
        it('fails to commit an initial entry with a non-zero starting epoch', async () => {
            const releaseHash = hexUtils.leftPad('0x1');
            const startingEpoch = new BigNumber(1);
            const tx = tester
                .commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch)
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith("LibRelease: can't commit an update before the current release.");
        });

        it('fails to commit an empty initial entry', async () => {
            const releaseHash = NULL_BYTES32;
            const startingEpoch = new BigNumber(0);
            const tx = tester
                .commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch)
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                "LibRelease: Can't delete a non-existent release or the current release.",
            );
        });

        it('commits the initial entry', async () => {
            const releaseHash = hexUtils.leftPad('0x1');
            const startingEpoch = new BigNumber(0);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure that the current release was updated.
            expect(wasUpdated).to.be.eq(true);

            // Ensure the state was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(releaseHash);
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(startingEpoch);
            const releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(0),
            });
        });

        it('fails to delete the current entry', async () => {
            const startingEpoch = new BigNumber(0);
            const tx = tester
                .commitReleaseScheduleUpdateExternal(NULL_BYTES32, startingEpoch)
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                "LibRelease: Can't delete a non-existent release or the current release.",
            );
        });

        it('updates the current entry', async () => {
            await tester.setEpochId(new BigNumber(5)).awaitTransactionSuccessAsync();

            const releaseHash = hexUtils.leftPad('0x2');
            const startingEpoch = new BigNumber(1);

            // Commit the release schedule update.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure that the current release was updated.
            expect(wasUpdated).to.be.eq(true);

            // Ensure the state was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(releaseHash);
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(startingEpoch);
            const releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(0),
            });
        });

        it('fails to delete a non-existent entry', async () => {
            // Update the current epoch ID.
            await tester.setEpochId(new BigNumber(5)).awaitTransactionSuccessAsync();

            // Try to delete an entry in epoch 3.
            let tx = tester
                .commitReleaseScheduleUpdateExternal(NULL_BYTES32, new BigNumber(3))
                .awaitTransactionSuccessAsync();
            await expect(tx).to.be.rejectedWith(
                "LibRelease: Can't delete a non-existent release or the current release.",
            );

            // Try to delete an entry in epoch 5.
            tx = tester
                .commitReleaseScheduleUpdateExternal(NULL_BYTES32, new BigNumber(5))
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith(
                "LibRelease: Can't delete a non-existent release or the current release.",
            );
        });

        it('commits a new current entry', async () => {
            const releaseHash = hexUtils.leftPad('0x2');
            const startingEpoch = new BigNumber(3);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry was updated.
            expect(wasUpdated).to.be.eq(true);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(releaseHash);
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(startingEpoch);
            const releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(0),
            });
        });

        it('commits a new current entry', async () => {
            const releaseHash = hexUtils.leftPad('0x3');
            const startingEpoch = new BigNumber(5);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry was updated.
            expect(wasUpdated).to.be.eq(true);

            // Ensure that the release schedule didn't change.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(releaseHash);
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(startingEpoch);
            const releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(0),
            });
        });

        it('fails to delete a non-existent entry', async () => {
            const releaseHash = NULL_BYTES32;
            const startingEpoch = new BigNumber(7);
            const tx = tester
                .commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch)
                .awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith("LibRelease: Can't delete a non-existent release.");
        });

        it("commits a new entry that isn't the current entry", async () => {
            const releaseHash = hexUtils.leftPad('0x4');
            const startingEpoch = new BigNumber(10);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: startingEpoch,
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: releaseHash,
                nextEpochId: new BigNumber(0),
            });
        });

        it('commits a new entry between the other two entries', async () => {
            const releaseHash = hexUtils.leftPad('0x5');
            const startingEpoch = new BigNumber(9);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: startingEpoch,
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: releaseHash,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: new BigNumber(0),
            });
        });

        it('updates the latest entry', async () => {
            const releaseHash = hexUtils.leftPad('0x6');
            const startingEpoch = new BigNumber(9);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: startingEpoch,
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: releaseHash,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: new BigNumber(0),
            });
        });

        it('deletes the latest entry', async () => {
            const releaseHash = NULL_BYTES32;
            const startingEpoch = new BigNumber(9);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: new BigNumber(0),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(0),
            });
        });

        it('commits a new entry at the end of the list', async () => {
            const releaseHash = hexUtils.leftPad('0x7');
            const startingEpoch = new BigNumber(100);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: startingEpoch,
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: releaseHash,
                nextEpochId: new BigNumber(0),
            });
        });

        it('commits a new entry', async () => {
            const releaseHash = hexUtils.leftPad('0x8');
            const startingEpoch = new BigNumber(27);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: startingEpoch,
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: releaseHash,
                nextEpochId: new BigNumber(100),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(100)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x7'),
                nextEpochId: new BigNumber(0),
            });
        });

        it('deletes a middle entry', async () => {
            const releaseHash = NULL_BYTES32;
            const startingEpoch = new BigNumber(27);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: new BigNumber(100),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(100)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x7'),
                nextEpochId: new BigNumber(0),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(0),
            });
        });

        it('deletes the last entry', async () => {
            const releaseHash = NULL_BYTES32;
            const startingEpoch = new BigNumber(100);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: new BigNumber(0),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(100)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(0),
            });
        });

        it('commit a new entry', async () => {
            const releaseHash = hexUtils.leftPad('0x9');
            const startingEpoch = new BigNumber(23);

            // Commit the entry.
            const wasUpdated = await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).callAsync();
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Ensure the current entry wasn't updated.
            expect(wasUpdated).to.be.eq(false);

            // Ensure that the release schedule was updated correctly.
            const currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: startingEpoch,
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(startingEpoch).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: releaseHash,
                nextEpochId: new BigNumber(0),
            });
        });
    });

    describe('#evaluateReleaseSchedule', () => {
        let epochId: BigNumber;

        it("doesn't advance the release schedule with epochId = 6", async () => {
            // Attempt to advance the release schedule.
            epochId = new BigNumber(6);
            let currentReleaseHash = await tester.evaluateReleaseScheduleExternal(epochId).callAsync();
            await tester.evaluateReleaseScheduleExternal(epochId).awaitTransactionSuccessAsync();

            // Ensure that the current release hash is correct.
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));

            // Ensure that the release schedule wasn't updated.
            currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x3'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(5));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(5)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: NULL_BYTES32,
                nextEpochId: new BigNumber(10),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: new BigNumber(23),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(23)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x9'),
                nextEpochId: new BigNumber(0),
            });
        });

        it('advances the release schedule correctly when epochId = 10', async () => {
            // Attempt to advance the release schedule.
            epochId = new BigNumber(10);
            let currentReleaseHash = await tester.evaluateReleaseScheduleExternal(epochId).callAsync();
            await tester.evaluateReleaseScheduleExternal(epochId).awaitTransactionSuccessAsync();

            // Ensure that the current release hash is correct.
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x4'));

            // Ensure that the release schedule was updated correctly.
            currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x4'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(10));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: new BigNumber(23),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(23)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x9'),
                nextEpochId: new BigNumber(0),
            });
        });

        it("doesn't advance the release schedule when epochId = 20", async () => {
            // Attempt to advance the release schedule.
            epochId = new BigNumber(10);
            let currentReleaseHash = await tester.evaluateReleaseScheduleExternal(epochId).callAsync();
            await tester.evaluateReleaseScheduleExternal(epochId).awaitTransactionSuccessAsync();

            // Ensure that the current release hash is correct.
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x4'));

            // Ensure that the release schedule wasn't updated.
            currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0x4'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(new BigNumber(10));
            let releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(10)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x4'),
                nextEpochId: new BigNumber(23),
            });
            releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(23)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0x9'),
                nextEpochId: new BigNumber(0),
            });
        });

        it('advances the release schedule when epochId = 30', async () => {
            // Commit a new entry to verify that the release schedule
            // can be advanced by more than one epoch.
            const releaseHash = hexUtils.leftPad('0xa');
            const startingEpoch = new BigNumber(26);
            await tester.commitReleaseScheduleUpdateExternal(releaseHash, startingEpoch).awaitTransactionSuccessAsync();

            // Attempt to advance the release schedule.
            epochId = new BigNumber(30);
            let currentReleaseHash = await tester.evaluateReleaseScheduleExternal(epochId).callAsync();
            await tester.evaluateReleaseScheduleExternal(epochId).awaitTransactionSuccessAsync();

            // Ensure that the current release hash is correct.
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0xa'));

            // Ensure that the release schedule wasn't updated.
            currentReleaseHash = await tester.getCurrentReleaseHashExternal().callAsync();
            expect(currentReleaseHash).to.be.eq(hexUtils.leftPad('0xa'));
            const currentReleaseStartingEpochId = await tester.getCurrentReleaseStartingEpochExternal().callAsync();
            expect(currentReleaseStartingEpochId).to.be.bignumber.eq(startingEpoch);
            const releaseScheduleEntry = await tester.getReleaseScheduleEntry(new BigNumber(26)).callAsync();
            expect(releaseScheduleEntry).to.be.deep.eq({
                releaseHash: hexUtils.leftPad('0xa'),
                nextEpochId: new BigNumber(0),
            });
        });
    });
});
