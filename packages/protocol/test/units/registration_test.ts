import { BigNumber, hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import {
    CustodianCustodianBondAddedEventArgs,
    CustodianCustodianInitializedEventArgs,
    CustodianCustodianPenalizedEventArgs,
    CustodianCustodiansJailedEventArgs,
    CustodianCustodianStatusesSetEventArgs,
    CustodianCustodiansUnjailedEventArgs,
    CustodianCustodianUnbondedEventArgs,
    CustodianCustodianUnbondPreparedEventArgs,
    CustodianMinimumBondSetEventArgs,
    CustodianUnbondDelaySetEventArgs,
    Derivadex,
    RegistrationFastPathFunctionSignaturesSetEventArgs,
    RegistrationRegistrationInitializedEventArgs,
    RegistrationReleaseScheduleUpdatedEventArgs,
    RegistrationSignerRegisteredEventArgs,
    TestCheckpointContract,
    TestRegistrationContract,
} from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, generateCallData, getSelectors } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { FacetCutAction } from '@derivadex/types';
import { LogWithDecodedArgs, TransactionReceiptWithDecodedLogs } from 'ethereum-types';
import { ContractArtifact } from 'ethereum-types';
import * as hardhat from 'hardhat';
import _ from 'lodash';

import { setupWithProviderAsync } from '../../deployment/setup';
import { Fixtures, NULL_BYTES2, NULL_BYTES32, ZERO_ADDRESS } from '../fixtures';
import { filterEventLogs, passProposalAsync, setIsPausedAsync, TestContext } from '../utils';

const OPERATOR_MINIMUM_BOND = new BigNumber(2);
const UNBOND_DELAY = new BigNumber(200);

const artifactAbis = _.mapValues(artifacts, (artifact: ContractArtifact) => artifact.compilerOutput.abi);

describe('Registration unit tests', () => {
    let web3Wrapper: Web3Wrapper;
    let derivadex: Derivadex;
    let checkpoint: TestCheckpointContract;
    let registration: TestRegistrationContract;
    let accounts: string[];
    let owner: string;
    let fixtures: Fixtures;
    let proposalCount: number;
    let ctx: TestContext;

    const mrEnclave = hexUtils.leftPad('0xdeadbeef');
    const isvSvn = '0x0000';
    const SIGNATURE = '0xdeadbabe';

    function encodeRAReport(
        quoteStatus: string,
        mrEnclave: string,
        isvSvn: string,
        custodian: string,
        signer: string,
    ): string {
        const quoteBody = Buffer.from(
            hexUtils
                .concat(
                    hexUtils.leftPad('0x0', 112),
                    mrEnclave,
                    hexUtils.leftPad('0x0', 162),
                    isvSvn,
                    hexUtils.leftPad('0x0', 60),
                    signer,
                    hexUtils.rightPad(custodian, 44),
                )
                .slice(2),
            'hex',
        ).toString('base64');
        const report = `{"id":"313676330620876491165606016975323788285","timestamp":"2019-04-16T18:26:24.798739","version":3,"isvEnclaveQuoteStatus":"${quoteStatus}","platformInfoBlob":"1502006504000E00000808020401010000000000000000000008000009000000020000000000000AF2A5ABAAA8988CDD680D705C023EA95EC943616ED98CB7789F6052C8AECBA5BABFC1E07A6953D3B7D85B3B53E3170ADD0930B9D110F0A765728F60237DD22325CF","isvEnclaveQuoteBody":"${quoteBody}"}`;
        return `0x${Array.from(report)
            .map((c) => c.charCodeAt(0).toString(16))
            .join('')}`;
    }

    function hashRegistrationMetadata(mrEnclave: string, isvSvn: string): string {
        return hexUtils.hash(hexUtils.concat(mrEnclave, hexUtils.rightPad(isvSvn, 32)));
    }

    async function verifyValidSignersAsync(minimumBond: BigNumber = OPERATOR_MINIMUM_BOND): Promise<void> {
        let expectedValidSignersCount = 0;
        const registeredCustodians = await registration.getRegisteredCustodians().callAsync();
        const registeredSigners = await derivadex.custodianContract
            .getSignersForCustodians(registeredCustodians)
            .callAsync();
        const registeredCustodianStates = await derivadex.custodianContract
            .getCustodianStates(registeredCustodians)
            .callAsync();
        for (let i = 0; i < registeredCustodians.length; i++) {
            let expectedHasValidSigner;
            if (
                registeredSigners[i] !== ZERO_ADDRESS &&
                registeredCustodianStates[i].approved &&
                registeredCustodianStates[i].balance.gte(minimumBond) &&
                !registeredCustodianStates[i].jailed &&
                registeredCustodianStates[i].unbondETA.eq(0)
            ) {
                expectedHasValidSigner = true;
                expectedValidSignersCount++;
            } else {
                expectedHasValidSigner = false;
            }

            // Verify that the result of `hasValidSigner` is correct.
            const hasValidSigner = await derivadex.custodianContract
                .hasValidSigner(registeredCustodians[i])
                .callAsync();
            expect(hasValidSigner).to.be.eq(expectedHasValidSigner);
        }
        const validSignersCount = await derivadex.custodianContract.getValidSignersCount().callAsync();
        expect(validSignersCount).to.be.bignumber.eq(expectedValidSignersCount);
    }

    async function setBanStatusesAsync(
        addresses: string[],
        statuses: boolean[],
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.bannerContract.getFunctionSignature('setBanStatuses')];
        const calldatas = [
            generateCallData(
                derivadex.bannerContract.setBanStatuses(addresses, statuses).getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Update the ban statuses of a batch of addresses.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function setCustodianStatusesAsync(
        custodians: string[],
        statuses: boolean[],
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.custodianContract.getFunctionSignature('setCustodianStatuses')];
        const calldatas = [
            generateCallData(
                derivadex.custodianContract.setCustodianStatuses(custodians, statuses).getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Set the custodian statuses for a list of addresses.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function setFastPathFunctionSignaturesAsync(
        functionSignatures: string[],
        statuses: boolean[],
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.registrationContract.getFunctionSignature('setFastPathFunctionSignatures')];
        const calldatas = [
            generateCallData(
                derivadex.registrationContract
                    .setFastPathFunctionSignatures(functionSignatures, statuses)
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Update the fast path function signature statuses.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function setMinimumBondAsync(minimumBond: BigNumber): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.custodianContract.getFunctionSignature('setMinimumBond')];
        const calldatas = [
            generateCallData(derivadex.custodianContract.setMinimumBond(minimumBond).getABIEncodedTransactionData()),
        ];
        const description = 'Update the minimum bond.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function setUnbondDelayAsync(unbondDelay: BigNumber): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.custodianContract.getFunctionSignature('setUnbondDelay')];
        const calldatas = [
            generateCallData(derivadex.custodianContract.setUnbondDelay(unbondDelay).getABIEncodedTransactionData()),
        ];
        const description = 'Update the unbond delay.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function jailAsync(custodians: string[]): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.custodianContract.getFunctionSignature('jail')];
        const calldatas = [
            generateCallData(derivadex.custodianContract.jail(custodians).getABIEncodedTransactionData()),
        ];
        const description = 'Jail a set of custodians.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function penalizeAsync(
        custodians: string[],
        penalties: BigNumber[],
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.custodianContract.getFunctionSignature('penalize')];
        const calldatas = [
            generateCallData(
                derivadex.custodianContract.penalize(custodians, penalties).getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Penalize a set of custodians.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function unjailAsync(custodians: string[]): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.custodianContract.getFunctionSignature('unjail')];
        const calldatas = [
            generateCallData(derivadex.custodianContract.unjail(custodians).getABIEncodedTransactionData()),
        ];
        const description = 'Unjail a set of custodians.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function updateReleaseScheduleAsync(
        releaseMetadata: { mrEnclave: string; isvSvn: string },
        startingEpochId: BigNumber,
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.registrationContract.getFunctionSignature('updateReleaseSchedule')];
        const calldatas = [
            generateCallData(
                derivadex.registrationContract
                    .updateReleaseSchedule(releaseMetadata, startingEpochId)
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Update the release schedule.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(ctx, proposalCount);
    }

    async function bondAsync(custodians: string[], amount: BigNumber = OPERATOR_MINIMUM_BOND): Promise<void> {
        for (const custodian of custodians) {
            await derivadex.ddxContract
                .transfer(custodian, amount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            await derivadex.ddxContract
                .approve(derivadex.derivaDEXContract.address, amount)
                .awaitTransactionSuccessAsync({ from: custodian });
            await derivadex.custodianContract.bond(amount).awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    async function fundAsync(custodians: string[], amount: BigNumber): Promise<void> {
        for (const custodian of custodians) {
            await derivadex.ddxContract
                .transfer(custodian, amount)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            await derivadex.ddxContract
                .approve(derivadex.derivaDEXContract.address, amount)
                .awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    async function prepareUnbondAsync(custodians: string[]): Promise<void> {
        for (const custodian of custodians) {
            await derivadex.custodianContract.prepareUnbond().awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    async function registerAsync(custodians: string[], mrEnclave_?: string, isvSvn_?: string): Promise<void> {
        for (const custodian of custodians) {
            const report = encodeRAReport('OK', mrEnclave_ || mrEnclave, isvSvn_ || isvSvn, custodian, custodian);
            await registration.register(report, SIGNATURE).awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });

        // Set up the testing infrastructure.
        ({ derivadex, accounts, owner } = await setupWithProviderAsync(provider, {
            isFork: false,
            isGanache: true,
            isLocalSnapshot: false,
            isTest: true,
            useDeployedDDXToken: false,
        }));
        fixtures = new Fixtures(derivadex, accounts, owner);
        ctx = { derivadex, fixtures };

        // Transfer ownership of DDX to the DerivaDEX proxy.
        await derivadex
            .transferOwnershipToDerivaDEXProxyDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        // Add the Pause facet to the diamond
        const pauseSelectors = getSelectors(derivadex.pauseContract);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.pauseContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: pauseSelectors,
                    },
                ],
                derivadex.pauseContract.address,
                derivadex.initializePause().getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setPauseAddressToProxy();

        // Add the Governance facet to the diamond.
        const governanceSelectors = getSelectors(derivadex.governanceContract, ['initialize']);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.governanceContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: governanceSelectors,
                    },
                ],
                derivadex.governanceContract.address,
                derivadex.initializeGovernance(10, 1, 17280, 1209600, 259200, 4, 1, 50).getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setGovernanceAddressToProxy();
        await derivadex.transferOwnershipToSelf().awaitTransactionSuccessAsync({ from: owner });

        // Add the Banner facet to the diamond.
        const initialBanList = [accounts[accounts.length - 2]];
        const bannerSelectors = getSelectors(derivadex.bannerContract, ['initialize']);
        const bannerTargets = [derivadex.derivaDEXContract.address];
        const bannerValues = [0];
        const bannerSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const bannerCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.bannerContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: bannerSelectors,
                            },
                        ],
                        derivadex.bannerContract.address,
                        derivadex.bannerContract.initialize(initialBanList).getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const bannerDescription = 'Add Banner contract as a facet.';
        await derivadex
            .propose(bannerTargets, bannerValues, bannerSignatures, bannerCalldatas, bannerDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        let proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount);
        derivadex.setBannerAddressToProxy();

        // Adds the Checkpoint facet to the diamond.
        checkpoint = await TestCheckpointContract.deployFrom0xArtifactAsync(
            artifacts.TestCheckpoint,
            derivadex.providerEngine,
            { from: fixtures.derivaDEXWallet() },
            artifacts,
        );
        const checkpointSelectors = getSelectors(checkpoint, ['initialize']);
        const checkpointTargets = [derivadex.derivaDEXContract.address];
        const checkpointValues = [0];
        const checkpointSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const checkpointCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: checkpoint.address,
                                action: FacetCutAction.Add,
                                functionSelectors: checkpointSelectors,
                            },
                        ],
                        checkpoint.address,
                        // Set the consensus threshold to 51, the quorum to two,
                        // and initial custodians to approve.
                        checkpoint.initialize(new BigNumber(51), new BigNumber(2)).getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const checkpointDescription = 'Add Checkpoint contract as a facet.';
        await derivadex
            .propose(
                checkpointTargets,
                checkpointValues,
                checkpointSignatures,
                checkpointCalldatas,
                checkpointDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount++);
        derivadex.setCheckpointAddressToProxy();
        checkpoint = new TestCheckpointContract(derivadex.derivaDEXContract.address, derivadex.providerEngine, {
            from: fixtures.derivaDEXWallet(),
        });
    });

    it('adds the Custodian facet to the diamond', async () => {
        // Add the Custodian facet to the diamond.
        const selectors = getSelectors(derivadex.custodianContract, ['initialize']);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.custodianContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.custodianContract.address,
                        derivadex.custodianContract
                            .initialize(OPERATOR_MINIMUM_BOND, UNBOND_DELAY)
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add Custodian contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        const { logs } = await passProposalAsync(ctx, proposalCount++);
        derivadex.setCustodianAddressToProxy();

        // Ensure that the correct CustodianInitialized event was emitted.
        expect(logs.length).to.be.eq(4);
        const log = logs[1] as LogWithDecodedArgs<CustodianCustodianInitializedEventArgs>;
        expect(log.event).to.be.eq('CustodianInitialized');
        expect(log.args).to.be.deep.eq({
            minimumBond: OPERATOR_MINIMUM_BOND,
            unbondDelay: UNBOND_DELAY,
        });
    });

    it('adds the Registration facet to the diamond', async () => {
        // Set up the initial custodians to register.
        const approvedCustodians = [
            fixtures.traderA(),
            fixtures.traderB(),
            fixtures.traderC(),
            fixtures.traderD(),
            fixtures.traderE(),
            fixtures.traderF(),
            fixtures.traderG(),
        ];

        // Deploy the test registration contract.
        registration = new TestRegistrationContract(
            derivadex.derivaDEXContract.address,
            derivadex.providerEngine,
            { from: fixtures.derivaDEXWallet() },
            artifactAbis,
        );

        // Add the Registration facet to the diamond.
        const selectors = getSelectors(registration, ['initialize']);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.registrationContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.registrationContract.address,
                        derivadex.registrationContract
                            .initialize({ mrEnclave, isvSvn }, approvedCustodians)
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add Registration contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        const { logs } = await passProposalAsync(ctx, proposalCount++);
        derivadex.setRegistrationAddressToProxy();

        // Ensure that the correct RegistrationInitialized event was emitted.
        expect(logs.length).to.be.eq(4);
        const log = logs[1] as LogWithDecodedArgs<RegistrationRegistrationInitializedEventArgs>;
        expect(log.event).to.be.eq('RegistrationInitialized');
        expect(log.args).to.be.deep.eq({
            mrEnclave,
            isvSvn,
            initialCustodians: approvedCustodians,
        });
    });

    describe('#setCustodianStatuses', () => {
        let snapshotId: string;

        async function assertSuccessfulCustodianStatusesSetAsync(
            custodians: string[],
            statuses: boolean[],
        ): Promise<void> {
            // Get any relevant state.
            const custodianStatesBefore = await derivadex.custodianContract.getCustodianStates(custodians).callAsync();

            // Set the custodian statuses.
            const { logs, blockNumber } = await setCustodianStatusesAsync(custodians, statuses);

            // Ensure the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'CustodianStatusesSet');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog = filteredLogs[0] as LogWithDecodedArgs<CustodianCustodianStatusesSetEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                custodians: custodians,
                statuses: statuses,
            });

            // Verify the valid signers count and states.
            await verifyValidSignersAsync();

            // Ensure the custodian states were updated correctly.
            const custodianStatesAfter = await derivadex.custodianContract.getCustodianStates(custodians).callAsync();
            for (let i = 0; i < custodians.length; i++) {
                const unbondETA =
                    !statuses[i] && custodianStatesBefore[i].balance.gt(0)
                        ? UNBOND_DELAY.plus(blockNumber)
                        : custodianStatesBefore[i].unbondETA;
                expect(custodianStatesAfter[i]).to.be.deep.eq({
                    approved: statuses[i],
                    balance: custodianStatesBefore[i].balance,
                    unbondETA,
                    jailed: custodianStatesBefore[i].jailed,
                });
            }
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
            ]);

            // Bond some custodians.
            await bondAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderG(),
            ]);

            // Prepare a custodian to unbond.
            await derivadex.custodianContract
                .prepareUnbond()
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });

            // Jail a custodian.
            await jailAsync([fixtures.traderC()]);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to be called directly', async () => {
            const tx = derivadex.custodianContract
                .setCustodianStatuses([fixtures.traderA()], [false])
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Registration: must be called by Gov.');
        });

        it('fails with an empty list of custodians', async () => {
            const tx = setCustodianStatusesAsync([], []);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails with malformed input data', async () => {
            const tx = setCustodianStatusesAsync([fixtures.traderA(), fixtures.traderB()], [false]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it("fails to set a custodian's status to the same value", async () => {
            const tx = setCustodianStatusesAsync([fixtures.traderA()], [true]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails to approve a custodian that is banned', async () => {
            const tx = setCustodianStatusesAsync([accounts[accounts.length - 2]], [true]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('sets the status of a custodian without a valid signer to false', async () => {
            await assertSuccessfulCustodianStatusesSetAsync([fixtures.traderB()], [false]);
        });

        it('sets the status of a custodian with a valid signer to false', async () => {
            await assertSuccessfulCustodianStatusesSetAsync([fixtures.traderA()], [false]);
        });

        it('sets the status of a custodian to true', async () => {
            await assertSuccessfulCustodianStatusesSetAsync([fixtures.traderH()], [true]);
        });

        it('sets the status of several custodians (some with valid signers)', async () => {
            await assertSuccessfulCustodianStatusesSetAsync(
                [
                    fixtures.traderA(),
                    fixtures.traderB(),
                    fixtures.traderC(),
                    fixtures.traderD(),
                    fixtures.traderE(),
                    fixtures.traderF(),
                    fixtures.traderG(),
                    fixtures.traderH(),
                ],
                [true, true, false, false, false, false, false, false],
            );
        });
    });

    describe('#setFastPathFunctionSignatures', () => {
        let snapshotId: string;

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to be called directly', async () => {
            const tx = registration
                .setFastPathFunctionSignatures(['jail(address[])'], [true])
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Registration: must be called by Gov.');
        });

        it('fails if the input is empty', async () => {
            const tx = setFastPathFunctionSignaturesAsync([], []);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails if there is an input parity mismatch', async () => {
            const tx = setFastPathFunctionSignaturesAsync(
                ['jail(address[])', 'setCustodianStatuses(address[], bool[])'],
                [true],
            );
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it("fails if the status isn't changed", async () => {
            const tx = setFastPathFunctionSignaturesAsync(['jail(address[])'], [false]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fast paths a function', async () => {
            const fastPathSignatures = ['jail(address[])'];
            const statuses = [true];

            // Fast path the functions.
            const { logs } = await setFastPathFunctionSignaturesAsync(fastPathSignatures, statuses);

            // Verify that the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'FastPathFunctionSignaturesSet');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog =
                filteredLogs[0] as LogWithDecodedArgs<RegistrationFastPathFunctionSignaturesSetEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                fastPathSignatures,
                statuses,
            });

            // Verify that the function is fast pathed.
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.custodianContract.getFunctionSignature('jail')];
            const calldatas = [
                generateCallData(derivadex.custodianContract.jail([fixtures.traderA()]).getABIEncodedTransactionData()),
            ];
            const description = 'Jail a set of custodians.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            return passProposalAsync(ctx, proposalCount, 1);
        });

        it("removes a function's fast path status", async () => {
            const fastPathSignatures = ['jail(address[])'];
            const statuses = [false];

            // Fast path the functions.
            const { logs } = await setFastPathFunctionSignaturesAsync(fastPathSignatures, statuses);

            // Verify that the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'FastPathFunctionSignaturesSet');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog =
                filteredLogs[0] as LogWithDecodedArgs<RegistrationFastPathFunctionSignaturesSetEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                fastPathSignatures,
                statuses,
            });

            // Verify that the function is no longer fast pathed.
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.custodianContract.getFunctionSignature('jail')];
            const calldatas = [
                generateCallData(derivadex.custodianContract.jail([fixtures.traderA()]).getABIEncodedTransactionData()),
            ];
            const description = 'Jail a set of custodians.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            const tx = passProposalAsync(ctx, proposalCount, 1);
            return expect(tx).to.be.rejectedWith("Governance: proposal hasn't finished queue time length.");
        });

        it('sets the fast path status of several functions', async () => {
            const fastPathSignatures = ['jail(address[])', 'setCustodianStatuses(address[],bool[])'];
            const statuses = [true, true];

            // Fast path the functions.
            const { logs } = await setFastPathFunctionSignaturesAsync(fastPathSignatures, statuses);

            // Verify that the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'FastPathFunctionSignaturesSet');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog =
                filteredLogs[0] as LogWithDecodedArgs<RegistrationFastPathFunctionSignaturesSetEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                fastPathSignatures,
                statuses,
            });

            // Verify that the jail function is fast pathed.
            let targets = [derivadex.derivaDEXContract.address];
            let values = [0];
            let signatures = [derivadex.custodianContract.getFunctionSignature('jail')];
            let calldatas = [
                generateCallData(derivadex.custodianContract.jail([fixtures.traderA()]).getABIEncodedTransactionData()),
            ];
            let description = 'Jail a set of custodians.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            let proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            await passProposalAsync(ctx, proposalCount, 1);

            // Verify that the setCustodianStatuses function is fast pathed.
            targets = [derivadex.derivaDEXContract.address];
            values = [0];
            signatures = [derivadex.custodianContract.getFunctionSignature('setCustodianStatuses')];
            calldatas = [
                generateCallData(
                    derivadex.custodianContract
                        .setCustodianStatuses([accounts[accounts.length - 1]], [true])
                        .getABIEncodedTransactionData(),
                ),
            ];
            description = 'Set the custodian statuses for a set of addresses.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            return passProposalAsync(ctx, proposalCount, 1);
        });
    });

    describe('#setMinimumBond', () => {
        let snapshotId: string;

        async function assertSuccessfulMinimumBondSetAsync(minimumBond: BigNumber): Promise<void> {
            // Update the minimum bond.
            const { logs } = await setMinimumBondAsync(minimumBond);

            // Ensure the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'MinimumBondSet');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog = filteredLogs[0] as LogWithDecodedArgs<CustodianMinimumBondSetEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                minimumBond,
            });

            // Verify the valid signers count and states.
            await verifyValidSignersAsync(minimumBond);

            // Ensure the minimum bond was updated correctly.
            const [actualMinimumBond] = await derivadex.custodianContract.getCustodianInfo().callAsync();
            expect(actualMinimumBond).to.be.bignumber.eq(minimumBond);
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
                fixtures.traderG(),
            ]);

            // Bond some custodians.
            await bondAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
                fixtures.traderG(),
            ]);

            // Bond some custodians again.
            await bondAsync([fixtures.traderA(), fixtures.traderB(), fixtures.traderC()]);

            // Prepare a custodian to unbond.
            await derivadex.custodianContract
                .prepareUnbond()
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });

            // Jail a custodian.
            await jailAsync([fixtures.traderC()]);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to call directly', async () => {
            const tx = derivadex.custodianContract
                .setMinimumBond(OPERATOR_MINIMUM_BOND.times(2))
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Registration: must be called by Gov.');
        });

        it('fails to set the minimum bond to the same value', async () => {
            const tx = setMinimumBondAsync(OPERATOR_MINIMUM_BOND);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('sets the minimum bond to twice the original minimum', async () => {
            await assertSuccessfulMinimumBondSetAsync(OPERATOR_MINIMUM_BOND.times(2));
        });

        it('sets the minimum bond to half the original minimum', async () => {
            await assertSuccessfulMinimumBondSetAsync(OPERATOR_MINIMUM_BOND.dividedToIntegerBy(2));
        });
    });

    describe('#setUnbondDelay', () => {
        let snapshotId: string;

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to be called directly', async () => {
            const tx = derivadex.custodianContract.setUnbondDelay(new BigNumber(1)).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('Registration: must be called by Gov.');
        });

        it('fails to set the same unbond delay', async () => {
            const tx = setUnbondDelayAsync(UNBOND_DELAY);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('updates the unbond delay', async () => {
            const unbondDelay = new BigNumber(1);
            const { logs } = await setUnbondDelayAsync(unbondDelay);

            // Ensure that the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'UnbondDelaySet');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog = filteredLogs[0] as LogWithDecodedArgs<CustodianUnbondDelaySetEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({ unbondDelay });

            // Ensure that the state was updated correctly.
            const [, actualUnbondDelay] = await derivadex.custodianContract.getCustodianInfo().callAsync();
            expect(actualUnbondDelay).to.be.bignumber.eq(unbondDelay);
        });
    });

    describe('#updateReleaseSchedule', () => {
        let snapshotId: string;
        let releaseSchedule: Array<{ releaseHash: string; startingEpochId: BigNumber }> = [];

        async function assertSuccessfulUpdateReleaseScheduleAsync(
            mrEnclave: string,
            isvSvn: string,
            startingEpochId: BigNumber,
            currentEpochId?: BigNumber,
        ): Promise<void> {
            if (currentEpochId !== undefined) {
                // Update the current epoch.
                await checkpoint.setEpochId(currentEpochId).awaitTransactionSuccessAsync();
            }

            // Update the expected version of the release schedule.
            let expectedReleaseHash: string;
            if (mrEnclave === NULL_BYTES32 && isvSvn === NULL_BYTES2) {
                expectedReleaseHash = NULL_BYTES32;
                releaseSchedule.splice(
                    releaseSchedule.findIndex((release) => release.startingEpochId.eq(startingEpochId)),
                    1,
                );
            } else {
                expectedReleaseHash = hashRegistrationMetadata(mrEnclave, isvSvn);
                const releaseIndex = releaseSchedule.findIndex((release) =>
                    release.startingEpochId.eq(startingEpochId),
                );
                if (releaseIndex === -1) {
                    let i = 0;
                    for (; i < releaseSchedule.length; i++) {
                        if (releaseSchedule[i].startingEpochId.gt(startingEpochId)) {
                            break;
                        }
                    }
                    releaseSchedule = releaseSchedule
                        .slice(0, i)
                        .concat({ releaseHash: expectedReleaseHash, startingEpochId })
                        .concat(releaseSchedule.slice(i, releaseSchedule.length - 1));
                } else {
                    releaseSchedule[releaseIndex].releaseHash = expectedReleaseHash;
                }
            }

            // Update the current release.
            const { logs } = await updateReleaseScheduleAsync({ mrEnclave, isvSvn }, startingEpochId);

            // Ensure that the logs are correct.
            const filteredLogs = filterEventLogs(logs, 'ReleaseScheduleUpdated');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog = filteredLogs[0] as LogWithDecodedArgs<RegistrationReleaseScheduleUpdatedEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                mrEnclave,
                isvSvn,
                startingEpochId,
            });

            // Get the expected starting release hash.
            const [, , , actualCurrentEpochId] = await checkpoint.getLatestCheckpoint().callAsync();
            let expectedCurrentReleaseHash = NULL_BYTES32;
            let expectedCurrentStartingEpoch = new BigNumber(0);
            for (let i = 0; i < releaseSchedule.length; i++) {
                if (releaseSchedule[i].startingEpochId.lte(actualCurrentEpochId)) {
                    expectedCurrentReleaseHash = releaseSchedule[i].releaseHash;
                    expectedCurrentStartingEpoch = releaseSchedule[i].startingEpochId;
                    break;
                }
            }

            // Verify the valid signers count and states.
            await verifyValidSignersAsync();

            // Ensure that the release state was updated correctly.
            const [actualReleaseHash, actualStartingEpochId] = await registration.getCurrentReleaseInfo().callAsync();
            expect(actualReleaseHash).to.be.eq(expectedCurrentReleaseHash);
            expect(actualStartingEpochId).to.be.bignumber.eq(expectedCurrentStartingEpoch);
            const actualReleaseSchedule = await registration.getReleaseSchedule().callAsync();
            expect(actualReleaseSchedule).to.be.deep.eq(releaseSchedule);
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers in an upcoming release.
            let mrEnclave = hexUtils.leftPad('0xdeadbabe');
            let isvSvn = '0x0001';
            await registerAsync(
                [
                    fixtures.traderA(),
                    fixtures.traderB(),
                    fixtures.traderC(),
                    fixtures.traderD(),
                    fixtures.traderE(),
                    fixtures.traderF(),
                ],
                mrEnclave,
                isvSvn,
            );

            // Register some signers in an upcoming release.
            mrEnclave = hexUtils.leftPad('0xbeefdead');
            isvSvn = '0x0002';
            await registerAsync(
                [fixtures.traderA(), fixtures.traderB(), fixtures.traderC(), fixtures.traderD(), fixtures.traderE()],
                mrEnclave,
                isvSvn,
            );

            // Bond some custodians.
            await bondAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
            ]);

            // Prepare to unbond some custodians.
            await prepareUnbondAsync([fixtures.traderE()]);

            // Jail some custodians.
            await jailAsync([fixtures.traderD()]);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails if called directly', async () => {
            const tx = registration
                .updateReleaseSchedule(
                    {
                        mrEnclave: hexUtils.leftPad('0xdecaf'),
                        isvSvn: '0x0000',
                    },
                    new BigNumber(100),
                )
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Registration: must be called by Gov.');
        });

        it("fails if the enclave measurement is empty and the ISV_SVN isn't", async () => {
            const tx = updateReleaseScheduleAsync({ mrEnclave: NULL_BYTES32, isvSvn: '0x0001' }, new BigNumber(100));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails to delete the current release', async () => {
            const tx = updateReleaseScheduleAsync({ mrEnclave: NULL_BYTES32, isvSvn: NULL_BYTES2 }, new BigNumber(0));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('adds a current release', async () => {
            await assertSuccessfulUpdateReleaseScheduleAsync(
                hexUtils.leftPad('0xdeadbabe'),
                '0x0001',
                new BigNumber(50),
                new BigNumber(100),
            );
        });

        it('fails to add a release before the current release', async () => {
            const mrEnclave = hexUtils.leftPad('0xbeefdead');
            const isvSvn = '0x0002';
            const startingEpochId = new BigNumber(49);
            const tx = updateReleaseScheduleAsync({ mrEnclave, isvSvn }, startingEpochId);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('updates the current release', async () => {
            await assertSuccessfulUpdateReleaseScheduleAsync(
                hexUtils.leftPad('0xbeefdead'),
                '0x0002',
                new BigNumber(50),
            );
        });

        it('adds a non-current release', async () => {
            await assertSuccessfulUpdateReleaseScheduleAsync(
                hexUtils.leftPad('0xbeefdead'),
                '0x0000',
                new BigNumber(200),
            );
        });

        it('adds another non-current release', async () => {
            await assertSuccessfulUpdateReleaseScheduleAsync(
                hexUtils.leftPad('0xfeedbeef'),
                '0x0001',
                new BigNumber(206),
            );
        });

        it('deletes a non-current release', async () => {
            await assertSuccessfulUpdateReleaseScheduleAsync(NULL_BYTES32, NULL_BYTES2, new BigNumber(200));
        });

        it('fails to delete a non-existent release', async () => {
            const tx = updateReleaseScheduleAsync({ mrEnclave: NULL_BYTES32, isvSvn: NULL_BYTES2 }, new BigNumber(150));
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('updates an existing release', async () => {
            await assertSuccessfulUpdateReleaseScheduleAsync(
                hexUtils.leftPad('0xbeeffeed'),
                '0x0002',
                new BigNumber(206),
            );
        });
    });

    describe('#register', () => {
        let snapshotId: string;

        const registeredCustodiansByReleaseHash: { [releaseHash: string]: string[] } = {};
        const signerStateByReleaseHash: {
            [releaseHash: string]: {
                [signer: string]: {
                    state: { custodian: string; report: string };
                    active: boolean;
                };
            };
        } = {};

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Bond some custodians.
            await bondAsync([fixtures.traderD()]);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        async function assertSuccessfulRegistrationAsync(
            custodian: string,
            signer: string,
            mrEnclave: string,
            isvSvn: string,
        ): Promise<void> {
            const quoteStatuses = ['OK', 'GROUP_OUT_OF_DATE'];
            const report = encodeRAReport(
                quoteStatuses[Math.floor(Math.random() * quoteStatuses.length)],
                mrEnclave,
                isvSvn,
                custodian,
                signer,
            );
            const releaseHash = hashRegistrationMetadata(mrEnclave, isvSvn);

            // Update the expected state.
            if (registeredCustodiansByReleaseHash[releaseHash] === undefined) {
                registeredCustodiansByReleaseHash[releaseHash] = [];
            }
            if (signerStateByReleaseHash[releaseHash] === undefined) {
                signerStateByReleaseHash[releaseHash] = {};
            }
            const oldSigner = _.findKey(
                signerStateByReleaseHash[releaseHash],
                (signer) => signer.state.custodian === custodian && signer.active,
            );
            if (oldSigner !== undefined) {
                signerStateByReleaseHash[releaseHash][oldSigner].active = false;
                signerStateByReleaseHash[releaseHash][signer] = { state: { custodian, report }, active: true };
            } else {
                registeredCustodiansByReleaseHash[releaseHash].push(custodian);
                signerStateByReleaseHash[releaseHash][signer] = { state: { custodian, report }, active: true };
            }

            // Register the signer.
            const { logs } = await registration
                .register(report, SIGNATURE)
                .awaitTransactionSuccessAsync({ from: custodian });

            // Ensure that the logs are correct.
            expect(logs.length).to.be.eq(1);
            const log = logs[0] as LogWithDecodedArgs<RegistrationSignerRegisteredEventArgs>;
            expect(log.event).to.be.eq('SignerRegistered');
            expect(log.args).to.be.deep.eq({
                releaseHash,
                custodian,
                signer,
            });

            // Ensure that the state was updated successfully.
            const [currentReleaseHash] = await registration.getCurrentReleaseInfo().callAsync();
            for (const releaseHash of Object.keys(registeredCustodiansByReleaseHash)) {
                if (releaseHash === currentReleaseHash) {
                    // Verify that the registered custodians are correct.
                    const actualRegisteredCustodians = await registration.getRegisteredCustodians().callAsync();
                    expect(actualRegisteredCustodians).to.be.deep.eq(registeredCustodiansByReleaseHash[releaseHash]);

                    // Verify that the custodians have the correct signers.
                    const expectedSignersByCustodians = actualRegisteredCustodians.map((custodian) =>
                        _.findKey(
                            signerStateByReleaseHash[releaseHash],
                            (signer) => signer.state.custodian === custodian && signer.active,
                        ),
                    );
                    const actualSignersByCustodians = await derivadex.custodianContract
                        .getSignersForCustodians(actualRegisteredCustodians)
                        .callAsync();
                    expect(actualSignersByCustodians).to.be.deep.eq(expectedSignersByCustodians);

                    // Verify that the signer state is correct.
                    const [signers, expectedSigners] = _.unzip(Object.entries(signerStateByReleaseHash[releaseHash]));
                    const expectedSignerStates = expectedSigners.map(
                        (s) => (s as { state: { custodian: string; report: string } }).state,
                    );
                    const actualSignerStates = await derivadex.custodianContract
                        .getSignerStates(signers as string[])
                        .callAsync();
                    expect(actualSignerStates).to.be.deep.eq(expectedSignerStates);
                }

                // Verify that the registered custodians are correct.
                const actualRegisteredCustodians = await registration
                    .getRegisteredCustodiansByReleaseHash(releaseHash)
                    .callAsync();
                expect(actualRegisteredCustodians).to.be.deep.eq(registeredCustodiansByReleaseHash[releaseHash]);

                // Verify that the custodians have the correct signers.
                const expectedSignersByCustodians = actualRegisteredCustodians.map((custodian) =>
                    _.findKey(
                        signerStateByReleaseHash[releaseHash],
                        (signer) => signer.state.custodian === custodian && signer.active,
                    ),
                );
                const actualSignersByCustodians = await derivadex.custodianContract
                    .getSignersForCustodiansAndReleaseHash(actualRegisteredCustodians, releaseHash)
                    .callAsync();
                expect(actualSignersByCustodians).to.be.deep.eq(expectedSignersByCustodians);

                // Verify that the signer state is correct.
                const [signers, expectedSigners] = _.unzip(Object.entries(signerStateByReleaseHash[releaseHash]));
                const expectedSignerStates = expectedSigners.map(
                    (s) => (s as { state: { custodian: string; report: string } }).state,
                );
                const actualSignerStates = await derivadex.custodianContract
                    .getSignerStatesByReleaseHash(signers as string[], releaseHash)
                    .callAsync();
                expect(actualSignerStates).to.be.deep.eq(expectedSignerStates);
            }
        }

        it("fails if the report's `isvEnclaveQuoteStatus` is invalid", async () => {
            const custodian = fixtures.traderA();
            const signer = ZERO_ADDRESS;
            const report = encodeRAReport('INVALID', mrEnclave, isvSvn, custodian, signer);
            const tx = registration.register(report, SIGNATURE).awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith('Registration: invalid isvEnclaveQuoteStatus in RA report.');
        });

        it("fails if there isn't a `isvEnclaveQuoteBody` field in the report", async () => {
            const custodian = fixtures.traderA();
            const report =
                '{"id":"313676330620876491165606016975323788285","timestamp":"2019-04-16T18:26:24.798739","version":3,"isvEnclaveQuoteStatus":"OK","platformInfoBlob":"1502006504000E00000808020401010000000000000000000008000009000000020000000000000AF2A5ABAAA8988CDD680D705C023EA95EC943616ED98CB7789F6052C8AECBA5BABFC1E07A6953D3B7D85B3B53E3170ADD0930B9D110F0A765728F60237DD22325CF"}';
            const encodedReport = `0x${Array.from(report)
                .map((c) => c.charCodeAt(0).toString(16))
                .join('')}`;
            const tx = registration
                .register(encodedReport, SIGNATURE)
                .awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith('Registration: isvEnclaveQuoteBody not found in report');
        });

        it('fails if the report cryptography check returns a non-zero value', async () => {
            // Set the cryptography check response to a non-zero value.
            await registration.setCryptographyCheckResponse(new BigNumber(1)).awaitTransactionSuccessAsync();

            // Fails to register an operator.
            const custodian = fixtures.traderA();
            const signer = fixtures.traderA();
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, signer);
            const tx = registration.register(report, SIGNATURE).awaitTransactionSuccessAsync({ from: custodian });
            await expect(tx).to.be.rejectedWith('Registration: verifying signature failed');

            // Reset the cryptography check response to zero.
            await registration.setCryptographyCheckResponse(new BigNumber(0)).awaitTransactionSuccessAsync();
        });

        it('fails to register the zero address as a signer', async () => {
            const custodian = fixtures.traderA();
            const signer = ZERO_ADDRESS;
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, signer);
            const tx = registration.register(report, SIGNATURE).awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith("Registration: can't register with the zero address.");
        });

        it('fails to register when custodian from report does not match message sender', async () => {
            const custodian = fixtures.traderA();
            const signer = fixtures.traderA();
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, signer);
            const tx = registration
                .register(report, SIGNATURE)
                .awaitTransactionSuccessAsync({ from: fixtures.traderB() });
            return expect(tx).to.be.rejectedWith('Registration: sender authentication failed');
        });

        it('fails to register a signer with an unapproved custodian', async () => {
            const custodian = fixtures.traderH();
            const signer = fixtures.traderH();
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, signer);
            const tx = registration.register(report, SIGNATURE).awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith('Registration: custodian must be approved to register a signer.');
        });

        it('fails to register a signer that has been banned after being approved', async () => {
            // Set the ban status for the custodian.
            const custodian = fixtures.traderG();
            await setBanStatusesAsync([custodian], [true]);

            // Attempt to register the signer.
            const signer = fixtures.traderG();
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, signer);
            const tx = registration.register(report, SIGNATURE).awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith('Registration: custodian must be approved to register a signer.');
        });

        it('registers a signer for an approved custodian in the current release', async () => {
            await assertSuccessfulRegistrationAsync(fixtures.traderA(), fixtures.traderA(), mrEnclave, isvSvn);
        });

        it('fails to register a duplicate signer', async () => {
            const custodian = fixtures.traderB();
            const signer = fixtures.traderA();
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, signer);
            const tx = registration.register(report, SIGNATURE).awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith("Registration: can't re-use a signing key.");
        });

        it('updates the signer in the current release hash of a custodian', async () => {
            await assertSuccessfulRegistrationAsync(fixtures.traderA(), fixtures.traderB(), mrEnclave, isvSvn);
        });

        it('fails to use a signer that is no longer used due to re-registration', async () => {
            const custodian = fixtures.traderB();
            const signer = fixtures.traderA();
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, signer);
            const tx = registration.register(report, SIGNATURE).awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith("Registration: can't re-use a signing key.");
        });

        it('updates the signer in the current release hash of a custodian to an old signer', async () => {
            await assertSuccessfulRegistrationAsync(fixtures.traderA(), fixtures.traderA(), mrEnclave, isvSvn);
        });

        it('registers a signer in a new release hash for the custodian', async () => {
            await assertSuccessfulRegistrationAsync(
                fixtures.traderA(),
                fixtures.traderB(),
                hexUtils.leftPad('0xdeadbabe'),
                isvSvn,
            );
        });

        it('registers a signer for a new custodian in the current release hash', async () => {
            await assertSuccessfulRegistrationAsync(fixtures.traderC(), fixtures.traderC(), mrEnclave, isvSvn);
        });

        it('registers a signer for a new custodian in the new release hash', async () => {
            const mrEnclave = hexUtils.leftPad('0xdeadbabe');
            await assertSuccessfulRegistrationAsync(fixtures.traderC(), fixtures.traderC(), mrEnclave, isvSvn);
        });

        it('updates the valid signers count when a bonded custodian registers a signer', async () => {
            await assertSuccessfulRegistrationAsync(fixtures.traderD(), fixtures.traderD(), mrEnclave, isvSvn);
        });
    });

    describe('#bond', () => {
        let snapshotId: string;

        async function assertSuccessfulBondAsync(custodian: string, amount: BigNumber): Promise<void> {
            // Get relevant state before the bonding takes place.
            const custodianDDXBalanceBefore = await derivadex.ddxContract.balanceOf(custodian).callAsync();
            const derivadexDDXBalanceBefore = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const [custodianStateBefore] = await derivadex.custodianContract
                .getCustodianStates([custodian])
                .callAsync();

            // Bond the custodian.
            const { logs } = await derivadex.custodianContract
                .bond(amount)
                .awaitTransactionSuccessAsync({ from: custodian });

            // Ensure that the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'CustodianBondAdded');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog = filteredLogs[0] as LogWithDecodedArgs<CustodianCustodianBondAddedEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                custodian,
                amount,
            });

            // Verify the valid signers count and states.
            await verifyValidSignersAsync();

            // Ensure that the state was updated correctly.
            const custodianDDXBalanceAfter = await derivadex.ddxContract.balanceOf(custodian).callAsync();
            expect(custodianDDXBalanceAfter).to.be.bignumber.eq(custodianDDXBalanceBefore.minus(amount));
            const derivadexDDXBalanceAfter = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            expect(derivadexDDXBalanceAfter).to.be.bignumber.eq(derivadexDDXBalanceBefore.plus(amount));
            const [custodianStateAfter] = await derivadex.custodianContract.getCustodianStates([custodian]).callAsync();
            expect(custodianStateAfter).to.be.deep.eq({
                approved: true,
                balance: custodianStateBefore.balance.plus(amount),
                unbondETA: new BigNumber(0),
                jailed: custodianStateBefore.jailed,
            });
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([fixtures.traderB(), fixtures.traderD()]);

            // Fund some custodians.
            await fundAsync(
                [fixtures.traderA(), fixtures.traderB(), fixtures.traderC(), fixtures.traderD()],
                OPERATOR_MINIMUM_BOND.times(2),
            );

            // Bond some custodians.
            await bondAsync([fixtures.traderC()]);

            // Prepare to bond some custodians.
            await prepareUnbondAsync([fixtures.traderC()]);

            // Jail some custodians.
            await jailAsync([fixtures.traderD()]);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails if the bond size is zero', async () => {
            const tx = derivadex.custodianContract
                .bond(new BigNumber(0))
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            return expect(tx).to.be.rejectedWith('Registration: non-zero bond amount.');
        });

        it('fails if the custodian is unapproved', async () => {
            const tx = derivadex.custodianContract
                .bond(OPERATOR_MINIMUM_BOND)
                .awaitTransactionSuccessAsync({ from: fixtures.traderH() });
            return expect(tx).to.be.rejectedWith('Registration: custodian must be approved to bond.');
        });

        it('fails if the custodian has already started unbonding', async () => {
            const tx = derivadex.custodianContract
                .bond(OPERATOR_MINIMUM_BOND)
                .awaitTransactionSuccessAsync({ from: fixtures.traderC() });
            return expect(tx).to.be.rejectedWith('Registration: custodian has already starting unbonding.');
        });

        it("bonds a custodian that doesn't have a registered signer", async () => {
            await assertSuccessfulBondAsync(fixtures.traderA(), OPERATOR_MINIMUM_BOND);
        });

        it('bonds a custodian with a registered signer below minimum bond', async () => {
            await assertSuccessfulBondAsync(fixtures.traderB(), OPERATOR_MINIMUM_BOND.dividedToIntegerBy(2));
        });

        it('bonds a custodian with a registered signer to the minimum bond', async () => {
            await assertSuccessfulBondAsync(fixtures.traderB(), OPERATOR_MINIMUM_BOND.dividedToIntegerBy(2));
        });

        it('bonds a custodian with a registered signer that already exceeded the minimum bond', async () => {
            await assertSuccessfulBondAsync(fixtures.traderB(), OPERATOR_MINIMUM_BOND);
        });

        it('bonds a jailed custodian', async () => {
            await assertSuccessfulBondAsync(fixtures.traderD(), OPERATOR_MINIMUM_BOND);
        });
    });

    describe('#prepareUnbond', () => {
        let snapshotId: string;

        async function assertSuccessfulPrepareUnbondAsync(custodian: string): Promise<void> {
            // Get any relevant state before preparing the unbond.
            const [custodianStateBefore] = await derivadex.custodianContract
                .getCustodianStates([custodian])
                .callAsync();

            // Prepare to unbond the custodian.
            const { logs, blockNumber } = await derivadex.custodianContract
                .prepareUnbond()
                .awaitTransactionSuccessAsync({ from: custodian });

            // Ensure that the correct logs were emitted.
            expect(logs.length).to.be.eq(1);
            const log = logs[0] as LogWithDecodedArgs<CustodianCustodianUnbondPreparedEventArgs>;
            expect(log.event).to.be.eq('CustodianUnbondPrepared');
            expect(log.args).to.be.deep.eq({
                custodian,
                unbondETA: new BigNumber(blockNumber).plus(UNBOND_DELAY),
            });

            // Verify the valid signers count and states.
            await verifyValidSignersAsync();

            // Ensure that the custodian state was updated correctly.
            const [custodianStateAfter] = await derivadex.custodianContract.getCustodianStates([custodian]).callAsync();
            expect(custodianStateAfter).to.be.deep.eq({
                ...custodianStateBefore,
                approved: true,
                unbondETA: new BigNumber(blockNumber).plus(UNBOND_DELAY),
            });
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([fixtures.traderA(), fixtures.traderC(), fixtures.traderD()]);

            // Bond some custodians.
            await bondAsync([fixtures.traderA(), fixtures.traderB(), fixtures.traderC()]);
            await bondAsync([fixtures.traderD()], OPERATOR_MINIMUM_BOND.dividedToIntegerBy(2));

            // Jail some custodians.
            await jailAsync([fixtures.traderC()]);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it("fails if the custodian doesn't have a bond posted", async () => {
            const custodian = fixtures.traderE();
            const tx = derivadex.custodianContract.prepareUnbond().awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith("Registration: custodian isn't bonded.");
        });

        it("prepares to unbond if the custodian didn't satisfy the minimum bond", async () => {
            await assertSuccessfulPrepareUnbondAsync(fixtures.traderD());
        });

        it("prepares to unbond if the custodian didn't have a signer", async () => {
            await assertSuccessfulPrepareUnbondAsync(fixtures.traderB());
        });

        it('prepares to unbond if the custodian is jailed', async () => {
            await assertSuccessfulPrepareUnbondAsync(fixtures.traderC());
        });

        it('prepares to unbond if the custodian had a valid signer', async () => {
            await assertSuccessfulPrepareUnbondAsync(fixtures.traderA());
        });

        it('fails if the custodian has already prepared to unbond', async () => {
            const custodian = fixtures.traderA();
            const tx = derivadex.custodianContract.prepareUnbond().awaitTransactionSuccessAsync({ from: custodian });
            return expect(tx).to.be.rejectedWith('Registration: custodian has already prepared to unbond.');
        });
    });

    describe('#unbond', () => {
        let snapshotId: string;

        async function assertSuccessfulUnbondAsync(custodian: string, unbondAmount: BigNumber): Promise<void> {
            // Get any relevant state.
            const validSignersCountBefore = await derivadex.custodianContract.getValidSignersCount().callAsync();
            const custodianDDXBalanceBefore = await derivadex.ddxContract.balanceOf(custodian).callAsync();
            const derivadexDDXBalanceBefore = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            const [custodianStateBefore] = await derivadex.custodianContract
                .getCustodianStates([custodian])
                .callAsync();

            // Unbond the custodian.
            const { logs } = await derivadex.custodianContract
                .unbond()
                .awaitTransactionSuccessAsync({ from: custodian });

            // Ensure the correct events were emitted.
            const filteredLogs = filterEventLogs(logs, 'CustodianUnbonded');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog = filteredLogs[0] as LogWithDecodedArgs<CustodianCustodianUnbondedEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                custodian,
                unbondAmount,
            });

            // Ensure the state was updated correctly.
            const validSignersCountAfter = await derivadex.custodianContract.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore);
            const custodianDDXBalanceAfter = await derivadex.ddxContract.balanceOf(custodian).callAsync();
            expect(custodianDDXBalanceAfter).to.be.bignumber.eq(custodianDDXBalanceBefore.plus(unbondAmount));
            const derivadexDDXBalanceAfter = await derivadex.ddxContract
                .balanceOf(derivadex.derivaDEXContract.address)
                .callAsync();
            expect(derivadexDDXBalanceAfter).to.be.bignumber.eq(derivadexDDXBalanceBefore.minus(unbondAmount));
            const [custodianStateAfter] = await derivadex.custodianContract.getCustodianStates([custodian]).callAsync();
            expect(custodianStateAfter).to.be.deep.eq({
                approved: custodianStateBefore.approved,
                balance: new BigNumber(0),
                jailed: false,
                unbondETA: new BigNumber(0),
            });
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([fixtures.traderA(), fixtures.traderB(), fixtures.traderC(), fixtures.traderD()]);

            // Bond some custodians.
            await bondAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
                fixtures.traderG(),
            ]);

            // Prepare to unbond some custodians.
            await prepareUnbondAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
            ]);

            // Jail some custodians.
            await jailAsync([fixtures.traderA(), fixtures.traderB()]);

            // Penalize some custodians.
            await penalizeAsync([fixtures.traderB()], [OPERATOR_MINIMUM_BOND]);

            // Unjail some custodians.
            await unjailAsync([fixtures.traderB()]);

            // Unapprove some custodians.
            await setCustodianStatusesAsync([fixtures.traderF()], [false]);

            // Advance enough blocks that the prepared custodians can unbond.
            await advanceBlocksAsync(derivadex.providerEngine, UNBOND_DELAY.toNumber());
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to unbond a jailed custodian', async () => {
            const tx = derivadex.custodianContract.unbond().awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            return expect(tx).to.be.rejectedWith("Registration: jailed custodians can't unbond.");
        });

        it("fails to unbond a custodian that hasn't prepared to unbond", async () => {
            const tx = derivadex.custodianContract.unbond().awaitTransactionSuccessAsync({ from: fixtures.traderG() });
            return expect(tx).to.be.rejectedWith("Registration: custodian isn't ready to unbond.");
        });

        it("fails to unbond a custodian whose unbonding waiting period hasn't elapsed", async () => {
            await prepareUnbondAsync([fixtures.traderG()]);
            const tx = derivadex.custodianContract.unbond().awaitTransactionSuccessAsync({ from: fixtures.traderG() });
            return expect(tx).to.be.rejectedWith("Registration: custodian isn't ready to unbond.");
        });

        it('unbonds a custodian in the block that equals `unbondETA`', async () => {
            const blockNumber = await web3Wrapper.getBlockNumberAsync();
            const [{ unbondETA }] = await derivadex.custodianContract
                .getCustodianStates([fixtures.traderG()])
                .callAsync();

            // Advance blocks until the current block number equals unbondETA - 1.
            await advanceBlocksAsync(derivadex.providerEngine, unbondETA.minus(blockNumber).toNumber());

            // Unbond the custodian.
            await assertSuccessfulUnbondAsync(fixtures.traderG(), OPERATOR_MINIMUM_BOND);
        });

        it('unbonds an unapproved custodian', async () => {
            await assertSuccessfulUnbondAsync(fixtures.traderF(), OPERATOR_MINIMUM_BOND);
        });

        it('unbonds a custodian that was penalized and has a bond of zero', async () => {
            await assertSuccessfulUnbondAsync(fixtures.traderB(), new BigNumber(0));
        });

        it("unbonds a custodian with a bond that doesn't have a signer", async () => {
            await assertSuccessfulUnbondAsync(fixtures.traderE(), OPERATOR_MINIMUM_BOND);
        });

        it('unbonds a custodian with a bond that has a signer', async () => {
            await assertSuccessfulUnbondAsync(fixtures.traderC(), OPERATOR_MINIMUM_BOND);
        });
    });

    describe('#jail', () => {
        let snapshotId: string;

        async function assertSuccessfulJailAsync(
            custodians: string[],
            validSignersCountDelta: number,
            shouldJail = true,
        ): Promise<void> {
            if (!shouldJail && validSignersCountDelta !== 0) {
                throw new Error("Shouldn't reduce valid signers if rejailing");
            }

            // Gets any relevant state before the jailing.
            const validSignersCountBefore = await derivadex.custodianContract.getValidSignersCount().callAsync();
            const custodianStatesBefore = await derivadex.custodianContract.getCustodianStates(custodians).callAsync();

            // Jails the custodians.
            const { logs } = await jailAsync(custodians);

            // Ensure the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'CustodiansJailed');
            if (shouldJail) {
                expect(filteredLogs.length).to.be.eq(1);
                const jailedLog = filteredLogs[0] as LogWithDecodedArgs<CustodianCustodiansJailedEventArgs>;
                expect(jailedLog.args).to.be.deep.eq({
                    custodians,
                });
            } else {
                expect(filteredLogs.length).to.be.eq(1);
                const jailedLog = filteredLogs[0] as LogWithDecodedArgs<CustodianCustodiansJailedEventArgs>;
                expect(jailedLog.args).to.be.deep.eq({
                    custodians: [],
                });
            }

            // Ensure the state was updated correctly.
            const validSignersCountAfter = await derivadex.custodianContract.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore.minus(validSignersCountDelta));
            const custodianStatesAfter = await derivadex.custodianContract.getCustodianStates(custodians).callAsync();
            if (shouldJail) {
                for (let i = 0; i < custodianStatesAfter.length; i++) {
                    expect(custodianStatesAfter[i].approved).to.be.eq(custodianStatesBefore[i].approved);
                    expect(custodianStatesAfter[i].balance).to.be.bignumber.eq(custodianStatesBefore[i].balance);
                    expect(custodianStatesAfter[i].unbondETA).to.be.bignumber.eq(custodianStatesBefore[i].unbondETA);
                    expect(custodianStatesAfter[i].jailed).to.be.eq(true);
                }
            } else {
                expect(custodianStatesAfter).to.be.deep.eq(custodianStatesBefore);
            }
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
            ]);

            // Bonds a subset of the custodians.
            await bondAsync([fixtures.traderA(), fixtures.traderB(), fixtures.traderC(), fixtures.traderD()]);

            // Prepares to unbond one of the custodians.
            await derivadex.custodianContract
                .prepareUnbond()
                .awaitTransactionSuccessAsync({ from: fixtures.traderD() });
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to be called directly', async () => {
            const tx = derivadex.custodianContract
                .jail([fixtures.traderA(), fixtures.traderB()])
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Registration: must be called by Gov.');
        });

        it('fails to jail an empty list of custodians', async () => {
            const tx = jailAsync([]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it("jails a custodian that doesn't have a signer", async () => {
            await assertSuccessfulJailAsync([fixtures.traderF()], 0);
        });

        it("jails a custodian that doesn't have a valid signer", async () => {
            await assertSuccessfulJailAsync([fixtures.traderE()], 0);
        });

        it('jails a custodian that has a valid signer', async () => {
            await assertSuccessfulJailAsync([fixtures.traderC()], 1);
        });

        it('jails a custodian that has prepared to unbond', async () => {
            await assertSuccessfulJailAsync([fixtures.traderD()], 0);
        });

        it('jails multiple custodians (some with valid signers)', async () => {
            await assertSuccessfulJailAsync([fixtures.traderA(), fixtures.traderB(), fixtures.traderG()], 2);
        });

        it('rejailing all of the jailed custodians has no effect', async () => {
            await assertSuccessfulJailAsync(
                [
                    fixtures.traderA(),
                    fixtures.traderB(),
                    fixtures.traderC(),
                    fixtures.traderD(),
                    fixtures.traderE(),
                    fixtures.traderF(),
                    fixtures.traderG(),
                    fixtures.traderH(),
                ],
                0,
                false,
            );
        });
    });

    describe('#penalize', () => {
        let snapshotId: string;

        async function assertSuccessfulPenalizeAsync(custodians: string[], penalties: BigNumber[]): Promise<void> {
            // Get any relevant state.
            const penaltiesBefore = await derivadex.custodianContract.getPenalties().callAsync();
            const custodianStatesBefore = await derivadex.custodianContract.getCustodianStates(custodians).callAsync();

            // Penalize the custodians.
            const { logs } = await penalizeAsync(custodians, penalties);

            // Ensure the correct events were emitted.
            const filteredLogs = filterEventLogs(logs, 'CustodianPenalized');
            expect(filteredLogs.length).to.be.eq(1);
            const filteredLog = filteredLogs[0] as LogWithDecodedArgs<CustodianCustodianPenalizedEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                custodians,
                penalties,
            });

            // Ensure the state was updated correctly.
            const penaltiesAfter = await derivadex.custodianContract.getPenalties().callAsync();
            expect(penaltiesAfter).to.be.bignumber.eq(
                penaltiesBefore.plus(penalties.reduce((acc, p) => acc.plus(p), new BigNumber(0))),
            );
            const custodianStatesAfter = await derivadex.custodianContract.getCustodianStates(custodians).callAsync();
            for (let i = 0; i < custodianStatesAfter.length; i++) {
                expect(custodianStatesAfter[i].approved).to.be.eq(custodianStatesBefore[i].approved);
                expect(custodianStatesAfter[i].balance).to.be.bignumber.eq(
                    custodianStatesBefore[i].balance.minus(penalties[i]),
                );
                expect(custodianStatesAfter[i].unbondETA).to.be.bignumber.eq(custodianStatesBefore[i].unbondETA);
                expect(custodianStatesAfter[i].jailed).to.be.eq(custodianStatesBefore[i].jailed);
                expect(custodianStatesAfter[i].jailed).to.be.eq(true);
            }
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
                fixtures.traderG(),
            ]);

            // Bond some custodians.
            await bondAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
            ]);

            // Bond some custodians with extra collateral.
            await bondAsync([fixtures.traderA(), fixtures.traderB(), fixtures.traderC()]);

            // Prepare some custodians to unbond.
            for (const custodian of [fixtures.traderA(), fixtures.traderB()]) {
                await derivadex.custodianContract.prepareUnbond().awaitTransactionSuccessAsync({ from: custodian });
            }

            // Jail some custodians
            await jailAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
            ]);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to be called directly', async () => {
            const tx = derivadex.custodianContract
                .penalize([fixtures.traderA()], [OPERATOR_MINIMUM_BOND])
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Registration: must be called by Gov.');
        });

        it('fails to be called with an empty list of custodians', async () => {
            const tx = penalizeAsync([], []);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails to be called if there is an input parity mismatch', async () => {
            const tx = penalizeAsync([fixtures.traderA(), fixtures.traderB()], [OPERATOR_MINIMUM_BOND]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        // FIXME(apalepu23): Should this simply skip the penalization of this
        // custodian? We have an informal rule of not failing during governance
        // interactions in situations like this. My personal opinion is that
        // failing should be allowed if there's no legitimate way for the
        // failure to be tripped (i.e. it's just a failure on the part of
        // governance to vet that the proposal can succeed). There is a similar
        // question surrounding some of our input parity validation.
        //
        // Depending on the answer to this question, I think it would be good
        // to start an informal "Smart Contract Development Guide" with style
        // elements like this to make it easier for us to onboard a new dev if
        // we are able to hire another Solidity developer.
        it("fails to penalize a custodian that isn't jailed", async () => {
            const tx = penalizeAsync([fixtures.traderG()], [OPERATOR_MINIMUM_BOND]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        // FIXME(apalepu23): This hits at the above question, except that in
        // this case, I actually think it's possible for us to trip this
        // condition legitimately. In particular, let's say that there is a
        // proposal to jail some custodians, but a custodian is able to unbond
        // before the proposal is submitted. In this case, penalizing them won't
        // be possible, and there's a chance this could trigger a failure in a
        // legitimate proposal.
        //
        // In practice, I think that governance should be
        // able to figure out whether or not this will happen (or will just
        // be able to wait to figure out what bonds each custodian has before
        // penalizing them). Regardless of the decision (I'm leaning towards
        // reverting in both this and the above case), we should write up a
        // document for governance participants that explains how and why the
        // various governance functions should be used (as well as any footguns
        // that we've identified). This would be great to add alongside the docs
        // you're creating. I've been thinking that something similar would be
        // useful for custodians (essentially an "operator's guide") that
        // explains the contracts from the operator's perspective so they know
        // what can happen if they mess up and how to avoid these consequences.
        it('fails to penalize a custodian with more bond than they have', async () => {
            const tx = penalizeAsync([fixtures.traderF()], [OPERATOR_MINIMUM_BOND]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('penalizes a list of custodians with heterogenous amounts', async () => {
            await assertSuccessfulPenalizeAsync(
                [fixtures.traderA(), fixtures.traderB(), fixtures.traderC(), fixtures.traderD(), fixtures.traderE()],
                [
                    OPERATOR_MINIMUM_BOND,
                    OPERATOR_MINIMUM_BOND,
                    OPERATOR_MINIMUM_BOND.times(2),
                    OPERATOR_MINIMUM_BOND,
                    OPERATOR_MINIMUM_BOND,
                ],
            );
        });

        it('penalizes several custodians a second time', async () => {
            await assertSuccessfulPenalizeAsync(
                [fixtures.traderA(), fixtures.traderB()],
                [OPERATOR_MINIMUM_BOND, OPERATOR_MINIMUM_BOND],
            );
        });
    });

    describe('#unjail', () => {
        let snapshotId: string;

        async function assertSuccessfulUnjailAsync(
            custodians: string[],
            validSignersCountDelta: number,
        ): Promise<void> {
            // Gets any relevant state before the jailing.
            const validSignersCountBefore = await derivadex.custodianContract.getValidSignersCount().callAsync();
            const custodianStatesBefore = await derivadex.custodianContract.getCustodianStates(custodians).callAsync();

            // Jails the custodians.
            const { logs } = await unjailAsync(custodians);

            // Ensure the correct logs were emitted.
            const filteredLogs = filterEventLogs(logs, 'CustodiansUnjailed');
            const filteredLog = filteredLogs[0] as LogWithDecodedArgs<CustodianCustodiansUnjailedEventArgs>;
            expect(filteredLog.args).to.be.deep.eq({
                custodians,
            });

            // Ensure the state was updated correctly.
            const validSignersCountAfter = await derivadex.custodianContract.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore.plus(validSignersCountDelta));
            const custodianStatesAfter = await derivadex.custodianContract.getCustodianStates(custodians).callAsync();
            for (let i = 0; i < custodianStatesAfter.length; i++) {
                expect(custodianStatesAfter[i].approved).to.be.eq(custodianStatesBefore[i].approved);
                expect(custodianStatesAfter[i].balance).to.be.bignumber.eq(custodianStatesBefore[i].balance);
                expect(custodianStatesAfter[i].unbondETA).to.be.bignumber.eq(custodianStatesBefore[i].unbondETA);
                expect(custodianStatesAfter[i].jailed).to.be.eq(false);
            }
        }

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderG(),
            ]);

            // Bond some custodians.
            await bondAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
                fixtures.traderG(),
            ]);

            // Prepare to unbond some custodians.
            await prepareUnbondAsync([fixtures.traderB()]);

            // Jail some custodians.
            await jailAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
            ]);

            // Penalize some custodians.
            await penalizeAsync(
                [fixtures.traderC(), fixtures.traderD()],
                [OPERATOR_MINIMUM_BOND, OPERATOR_MINIMUM_BOND],
            );
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('fails to be called directly', async () => {
            const tx = derivadex.custodianContract
                .unjail([fixtures.traderA()])
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Registration: must be called by Gov.');
        });

        it('fails when trying to unjail an empty list of custodians', async () => {
            const tx = unjailAsync([]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it("fails when trying to unjail a custodian that isn't jailed", async () => {
            const tx = unjailAsync([fixtures.traderG()]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('unjails a single custodian with a valid signer', async () => {
            await assertSuccessfulUnjailAsync([fixtures.traderA()], 1);
        });

        it('unjails a single custodian without a valid signer', async () => {
            await assertSuccessfulUnjailAsync([fixtures.traderC()], 0);
        });

        it('unjails multiple custodians (some with valid signers)', async () => {
            await assertSuccessfulUnjailAsync(
                [fixtures.traderB(), fixtures.traderD(), fixtures.traderE(), fixtures.traderF()],
                1,
            );
        });
    });

    describe('#isNotPaused', () => {
        let snapshotId: string;

        before(async () => {
            // Take a snapshot of the state before the test executes.
            snapshotId = await web3Wrapper.sendRawPayloadAsync<string>({
                method: 'evm_snapshot',
                params: [],
            });

            // Register some signers.
            await registerAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
                fixtures.traderG(),
            ]);

            // Bond some custodians.
            await bondAsync([
                fixtures.traderA(),
                fixtures.traderB(),
                fixtures.traderC(),
                fixtures.traderD(),
                fixtures.traderE(),
                fixtures.traderF(),
            ]);

            // Prepare to unbond some custodians.
            await prepareUnbondAsync([fixtures.traderF()]);

            // Set the paused status to true.
            await setIsPausedAsync(ctx, true);
        });

        after(async () => {
            // Revert to the last snapshot.
            await web3Wrapper.sendRawPayloadAsync<void>({
                method: 'evm_revert',
                params: [snapshotId],
            });
        });

        it('calls setCustodianStatuses when paused', async () => {
            await setCustodianStatusesAsync([fixtures.traderH()], [true]);
        });

        it('calls setFastPathFunctionSignatures when paused', async () => {
            await setFastPathFunctionSignaturesAsync(['jail(address[])'], [true]);
        });

        it('calls setMinimumBond when paused', async () => {
            await setMinimumBondAsync(OPERATOR_MINIMUM_BOND.times(2));
        });

        it('calls setUnbondDelay when paused', async () => {
            await setUnbondDelayAsync(UNBOND_DELAY.times(2));
        });

        it('calls jail when paused', async () => {
            await jailAsync([fixtures.traderD()]);
        });

        it('calls penalize when paused', async () => {
            await penalizeAsync([fixtures.traderD()], [OPERATOR_MINIMUM_BOND]);
        });

        it('calls unjail when paused', async () => {
            await unjailAsync([fixtures.traderD()]);
        });

        it('fails to call bond when paused', async () => {
            const tx = derivadex.custodianContract
                .bond(OPERATOR_MINIMUM_BOND)
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            return expect(tx).to.be.rejectedWith('Registration: paused.');
        });

        it('fails to call prepareUnbond when paused', async () => {
            const tx = derivadex.custodianContract
                .prepareUnbond()
                .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
            return expect(tx).to.be.rejectedWith('Registration: paused.');
        });

        it('fails to call unbond when paused', async () => {
            const tx = derivadex.custodianContract.unbond().awaitTransactionSuccessAsync({ from: fixtures.traderF() });
            return expect(tx).to.be.rejectedWith('Registration: paused.');
        });

        it('calls updateReleaseSchedule when paused', async () => {
            await updateReleaseScheduleAsync(
                { mrEnclave: hexUtils.leftPad('0xdeadbeefdead'), isvSvn: '0x0000' },
                new BigNumber(10_000),
            );
        });

        it('fails to call register when paused', async () => {
            const tx = registration
                .register(
                    encodeRAReport(
                        'OK',
                        hexUtils.leftPad('0xdeadbeefdead'),
                        '0x0000',
                        fixtures.traderA(),
                        fixtures.traderH(),
                    ),
                    SIGNATURE,
                )
                .awaitTransactionSuccessAsync({
                    from: fixtures.traderA(),
                });
            return expect(tx).to.be.rejectedWith('Registration: paused.');
        });
    });
});
