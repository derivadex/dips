import { BigNumber } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { artifacts } from '@derivadex/contract-artifacts';
import { CompetitionDummyTokenContract } from '@derivadex/contract-wrappers';
import { expect } from '@derivadex/test-utils';
import hardhat from 'hardhat';

describe('CompetitionDummyToken unit tests', () => {
    let deployer: string;
    let derivaDAO: string;
    let normal0: string;
    let normal1: string;
    let accounts: string[];
    let token: CompetitionDummyTokenContract;

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });
        [deployer, derivaDAO, normal0, normal1, ...accounts] = await web3Wrapper.getAvailableAddressesAsync();

        token = await CompetitionDummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.CompetitionDummyToken,
            provider,
            { from: deployer },
            artifacts,
            'Competition USDC',
            'USDC',
            6,
            derivaDAO,
        );
    });

    describe('#setTransferStatuses', () => {
        it('should fail if called by non-deployer', async () => {
            const tx = token.setTransferStatuses([accounts[0]], [true]).awaitTransactionSuccessAsync({ from: normal0 });
            return expect(tx).to.be.rejectedWith('CompetitionDummyToken: sender is not deployer.');
        });

        it('should fail if address list is empty', async () => {
            const tx = token.setTransferStatuses([], [true]).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('CompetitionDummyToken: malformed input.');
        });

        it('should fail if address list has a different length than the statuses list', async () => {
            const tx = token.setTransferStatuses([accounts[0], accounts[1]], [true]).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('CompetitionDummyToken: malformed input.');
        });

        it('should successfully set statuses to true', async () => {
            await token.setTransferStatuses([accounts[0], accounts[1]], [true, true]).awaitTransactionSuccessAsync();
            expect(await token.transferStatuses(accounts[0]).callAsync()).to.be.eq(true);
            expect(await token.transferStatuses(accounts[1]).callAsync()).to.be.eq(true);
        });

        it('should successfully set mixed statuses', async () => {
            await token.setTransferStatuses([accounts[0], accounts[2]], [false, true]).awaitTransactionSuccessAsync();
            expect(await token.transferStatuses(accounts[0]).callAsync()).to.be.eq(false);
            expect(await token.transferStatuses(accounts[2]).callAsync()).to.be.eq(true);
        });
    });

    describe('#mint', () => {
        let RECIPIENT: string;
        const MINT_AMOUNT = Web3Wrapper.toBaseUnitAmount(1000, 6);

        before(async () => {
            RECIPIENT = accounts[accounts.length - 1];
        });

        async function assertMintFailureAsync(sender: string, amount: BigNumber): Promise<void> {
            const tx = token.mint(RECIPIENT, amount).awaitTransactionSuccessAsync({ from: sender });
            return expect(tx).to.be.rejectedWith('CompetitionDummyToken: invalid mint.');
        }

        async function assertMintSuccessAsync(sender: string): Promise<void> {
            const recipientBalanceBefore = await token.balanceOf(RECIPIENT).callAsync();
            await token.mint(RECIPIENT, MINT_AMOUNT).awaitTransactionSuccessAsync({ from: sender });
            const recipientBalanceAfter = await token.balanceOf(RECIPIENT).callAsync();
            expect(recipientBalanceAfter).to.be.bignumber.eq(recipientBalanceBefore.plus(MINT_AMOUNT));
        }

        it('should fail to mint from a normal account', async () => {
            await assertMintFailureAsync(normal0, MINT_AMOUNT);
        });

        it('should fail to mint MAX_UINT256 of tokens', async () => {
            await assertMintFailureAsync(normal0, new BigNumber(2).exponentiatedBy(256).minus(1));
        });

        it('should successfully mint with the deployer account', async () => {
            await assertMintSuccessAsync(deployer);
        });

        it('should successfully mint with an admin account', async () => {
            await assertMintSuccessAsync(accounts[1]);
        });
    });

    describe('#transfer', () => {
        const TRANSFER_AMOUNT = Web3Wrapper.toBaseUnitAmount(1, 6);

        async function assertTransferFailureAsync(sender: string, recipient: string): Promise<void> {
            await token.transfer(sender, TRANSFER_AMOUNT.times(2)).awaitTransactionSuccessAsync({ from: deployer });

            let tx = token.transfer(recipient, TRANSFER_AMOUNT).awaitTransactionSuccessAsync({ from: sender });
            await expect(tx).to.be.rejectedWith('CompetitionDummyToken: invalid transfer.');

            await token.approve(deployer, TRANSFER_AMOUNT).awaitTransactionSuccessAsync({ from: sender });
            tx = token.transferFrom(sender, recipient, TRANSFER_AMOUNT).awaitTransactionSuccessAsync();
            return expect(tx).to.be.rejectedWith('CompetitionDummyToken: invalid transfer.');
        }

        async function assertTransferSuccessAsync(sender: string, recipient: string): Promise<void> {
            await token.transfer(sender, TRANSFER_AMOUNT.times(2)).awaitTransactionSuccessAsync({ from: deployer });

            // Verify a successful transfer
            let senderBalanceBefore = await token.balanceOf(sender).callAsync();
            let recipientBalanceBefore = await token.balanceOf(recipient).callAsync();
            await token.transfer(recipient, TRANSFER_AMOUNT).awaitTransactionSuccessAsync({ from: sender });
            let senderBalanceAfter = await token.balanceOf(sender).callAsync();
            let recipientBalanceAfter = await token.balanceOf(recipient).callAsync();
            expect(senderBalanceAfter).to.be.bignumber.eq(senderBalanceBefore.minus(TRANSFER_AMOUNT));
            expect(recipientBalanceAfter).to.be.bignumber.eq(recipientBalanceBefore.plus(TRANSFER_AMOUNT));

            // Verify a successful transferFrom
            await token.approve(deployer, TRANSFER_AMOUNT).awaitTransactionSuccessAsync({ from: sender });
            senderBalanceBefore = await token.balanceOf(sender).callAsync();
            recipientBalanceBefore = await token.balanceOf(recipient).callAsync();
            await token
                .transferFrom(sender, recipient, TRANSFER_AMOUNT)
                .awaitTransactionSuccessAsync({ from: deployer });
            senderBalanceAfter = await token.balanceOf(sender).callAsync();
            recipientBalanceAfter = await token.balanceOf(recipient).callAsync();
            expect(senderBalanceAfter).to.be.bignumber.eq(senderBalanceBefore.minus(TRANSFER_AMOUNT));
            expect(recipientBalanceAfter).to.be.bignumber.eq(recipientBalanceBefore.plus(TRANSFER_AMOUNT));
        }

        it('should successfully transfer tokens from the deployer to another account', async () => {
            return assertTransferSuccessAsync(deployer, normal0);
        });

        it('should fail to transfer tokens from a normal account to the deployer', async () => {
            return assertTransferFailureAsync(normal0, deployer);
        });

        it('should fail to transfer tokens from a normal account to another normal account', async () => {
            return assertTransferFailureAsync(normal0, normal1);
        });

        it('should successfully transfer tokens from a normal account to the DerivaDAO', async () => {
            return assertTransferSuccessAsync(normal0, derivaDAO);
        });

        it('should successfully transfer tokens from the DerivaDAO to a normal account', async () => {
            return assertTransferSuccessAsync(derivaDAO, normal0);
        });

        it('should successfully transfer from allowed account', async () => {
            return assertTransferSuccessAsync(accounts[1], normal0);
        });
    });
});
