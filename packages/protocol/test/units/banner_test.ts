import { BigNumber, hexUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { BannerSetBannedStatusesEventArgs, Derivadex } from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, advanceTimeAsync, generateCallData, getSelectors } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { FacetCutAction } from '@derivadex/types';
import { LogWithDecodedArgs, TransactionReceiptWithDecodedLogs } from 'ethereum-types';
import * as hardhat from 'hardhat';

import { setupWithProviderAsync } from '../../deployment/setup';
import { Fixtures, ZERO_ADDRESS } from '../fixtures';

const OPERATOR_MINIMUM_BOND = new BigNumber(2);

describe('Banner Unit Tests', () => {
    let derivadex: Derivadex;
    let accounts: string[];
    let owner: string;
    let fixtures: Fixtures;

    const mrEnclave = hexUtils.leftPad('0xdeadbeef');
    const isvSvn = '0x0000';
    const UNBOND_DELAY = new BigNumber(200);
    const SIGNATURE = '0xdeadbabe';

    function encodeRAReport(
        quoteStatus: string,
        mrEnclave: string,
        isvSvn: string,
        custodian: string,
        signer: string,
    ): string {
        const quoteBody = Buffer.from(
            hexUtils
                .concat(
                    hexUtils.leftPad('0x0', 112),
                    mrEnclave,
                    hexUtils.leftPad('0x0', 162),
                    isvSvn,
                    hexUtils.leftPad('0x0', 60),
                    signer,
                    hexUtils.rightPad(custodian, 44),
                )
                .slice(2),
            'hex',
        ).toString('base64');
        const report = `{"id":"313676330620876491165606016975323788285","timestamp":"2019-04-16T18:26:24.798739","version":3,"isvEnclaveQuoteStatus":"${quoteStatus}","platformInfoBlob":"1502006504000E00000808020401010000000000000000000008000009000000020000000000000AF2A5ABAAA8988CDD680D705C023EA95EC943616ED98CB7789F6052C8AECBA5BABFC1E07A6953D3B7D85B3B53E3170ADD0930B9D110F0A765728F60237DD22325CF","isvEnclaveQuoteBody":"${quoteBody}"}`;
        return `0x${Array.from(report)
            .map((c) => c.charCodeAt(0).toString(16))
            .join('')}`;
    }

    /**
     * TODO(jalextowle): This is now used in this file and './TestOperator.ts'.
     * It probably makes sense to make this a little bit more generic and add this
     * to the `DerivaDEX` contract wrapper.
     *
     * Passes a governance proposal that has already been made using the fixtures.
     * @param proposalNumber The index of the proposal to pass.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function passProposalAsync(proposalNumber: number): Promise<TransactionReceiptWithDecodedLogs> {
        await advanceBlocksAsync(derivadex.providerEngine, 2);
        await derivadex
            .castVote(proposalNumber, true)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex.queue(proposalNumber).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
        await advanceTimeAsync(derivadex.providerEngine, 259200);
        await advanceBlocksAsync(derivadex.providerEngine, 1);
        return derivadex.execute(proposalNumber).awaitTransactionSuccessAsync({ from: fixtures.traderB() });
    }

    /**
     * Attempts to set the ban statuses for a list of addresses in the exchange.
     * @param addresses The list of addresses to update.
     * @param statuses The list of statuses to update.
     * @returns The transaction receipt of the call that executed the proposal.
     */
    async function setBanStatusesAsync(
        addresses: string[],
        statuses: boolean[],
    ): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.bannerContract.getFunctionSignature('setBanStatuses')];
        const calldatas = [
            generateCallData(
                derivadex.bannerContract.setBanStatuses(addresses, statuses).getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Update the ban statuses of a batch of addresses.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    async function jailAsync(custodians: string[]): Promise<TransactionReceiptWithDecodedLogs> {
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.custodianContract.getFunctionSignature('jail')];
        const calldatas = [
            generateCallData(derivadex.custodianContract.jail(custodians).getABIEncodedTransactionData()),
        ];
        const description = 'Jail a set of custodians.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        return passProposalAsync(proposalCount);
    }

    async function bondAsync(custodians: string[]): Promise<void> {
        for (const custodian of custodians) {
            await derivadex.ddxContract
                .transfer(custodian, OPERATOR_MINIMUM_BOND)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            await derivadex.ddxContract
                .approve(derivadex.derivaDEXContract.address, OPERATOR_MINIMUM_BOND)
                .awaitTransactionSuccessAsync({ from: custodian });
            await derivadex.custodianContract
                .bond(OPERATOR_MINIMUM_BOND)
                .awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    async function prepareUnbondAsync(custodians: string[]): Promise<void> {
        for (const custodian of custodians) {
            await derivadex.custodianContract.prepareUnbond().awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    async function registerAsync(custodians: string[]): Promise<void> {
        for (const custodian of custodians) {
            const report = encodeRAReport('OK', mrEnclave, isvSvn, custodian, custodian);
            await derivadex.registrationContract
                .register(report, SIGNATURE)
                .awaitTransactionSuccessAsync({ from: custodian });
        }
    }

    before(async () => {
        // Reset the hardhat network provider to a fresh state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [],
        });

        ({ derivadex, accounts, owner } = await setupWithProviderAsync(provider, {
            isFork: false,
            isGanache: true,
            isLocalSnapshot: false,
            isTest: true,
            useDeployedDDXToken: false,
        }));

        fixtures = new Fixtures(derivadex, accounts, owner);

        await derivadex
            .transferOwnershipToDerivaDEXProxyDDX(derivadex.derivaDEXContract.address)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        await derivadex
            .transferDDX(fixtures.traderB(), 10000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderC(), 5000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        await derivadex
            .transferDDX(fixtures.traderD(), 25000)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });

        await derivadex.ddxContract
            .delegate(fixtures.traderE())
            .awaitTransactionSuccessAsync({ from: fixtures.traderD() });
        await advanceBlocksAsync(derivadex.providerEngine, 1);

        // Add Pause facet to the diamond
        const pauseSelectors = getSelectors(derivadex.pauseContract);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.pauseContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: pauseSelectors,
                    },
                ],
                derivadex.pauseContract.address,
                derivadex.initializePause().getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setPauseAddressToProxy();

        const governanceSelectors = getSelectors(derivadex.governanceContract, ['initialize']);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.governanceContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: governanceSelectors,
                    },
                ],
                derivadex.governanceContract.address,
                derivadex.initializeGovernance(10, 1, 17280, 1209600, 259200, 4, 1, 50).getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setGovernanceAddressToProxy();

        await derivadex.transferOwnershipToSelf().awaitTransactionSuccessAsync({ from: owner });

        const custodianSelectors = getSelectors(derivadex.custodianContract, ['initialize']);
        const custodianTargets = [derivadex.derivaDEXContract.address];
        const custodianValues = [0];
        const custodianSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const custodianCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.custodianContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: custodianSelectors,
                            },
                        ],
                        derivadex.custodianContract.address,
                        derivadex.custodianContract
                            .initialize(OPERATOR_MINIMUM_BOND, UNBOND_DELAY)
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const custodianDescription = 'Add Custodian contract as a facet.';
        await derivadex
            .propose(custodianTargets, custodianValues, custodianSignatures, custodianCalldatas, custodianDescription)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        let proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setCustodianAddressToProxy();

        const initialCustodians = accounts.slice(0, 12);
        const registrationSelectors = getSelectors(derivadex.registrationContract, ['initialize']);
        const registrationTargets = [derivadex.derivaDEXContract.address];
        const registrationValues = [0];
        const registrationSignatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const registrationCalldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.registrationContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: registrationSelectors,
                            },
                        ],
                        derivadex.registrationContract.address,
                        // Set the consensus threshold to 51, the quorum to two,
                        // and initial custodians to approve.
                        derivadex.registrationContract
                            .initialize({ mrEnclave, isvSvn }, initialCustodians)
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const registrationDescription = 'Add Registration contract as a facet.';
        await derivadex
            .propose(
                registrationTargets,
                registrationValues,
                registrationSignatures,
                registrationCalldatas,
                registrationDescription,
            )
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setRegistrationAddressToProxy();

        // Register some signers.
        await registerAsync(accounts.slice(5, 11));

        // Bond some custodians.
        await bondAsync(accounts.slice(5, 9).concat(accounts.slice(10, 12)));

        // Prepare to unbond some custodians.
        await prepareUnbondAsync([accounts[5]]);

        // Jail some custodians.
        await jailAsync([accounts[6]]);
    });

    it('adds Banner facet', async () => {
        const initialBanList = accounts.slice(15, 17);
        const selectors = getSelectors(derivadex.bannerContract, ['initialize']);
        const targets = [derivadex.derivaDEXContract.address];
        const values = [0];
        const signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        const calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.bannerContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.bannerContract.address,
                        derivadex.bannerContract.initialize(initialBanList).getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        const description = 'Add Banner contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(proposalCount);
        derivadex.setBannerAddressToProxy();

        for (const address of initialBanList) {
            const status = await derivadex.bannerContract.getBanStatus(address).callAsync();
            expect(status).to.be.eq(true);
        }
    });

    describe('#setBanStatuses', () => {
        it('fails if the function is called directly', async () => {
            const tx = derivadex.bannerContract
                .setBanStatuses([accounts[0]], [true])
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            return expect(tx).to.be.rejectedWith('Banner: must be called by Gov.');
        });

        it('fails if the addresses list is empty', async () => {
            const tx = setBanStatusesAsync([], []);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails if the addresses and statuses lists have different lengths', async () => {
            const tx = setBanStatusesAsync(accounts.slice(0, 2), [true]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails if one of the addresses is the zero address', async () => {
            const tx = setBanStatusesAsync([accounts[0], ZERO_ADDRESS], [true, true]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('fails if setting the same status', async () => {
            const tx = setBanStatusesAsync([accounts[0]], [false]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted.');
        });

        it('bans multiple addresses', async () => {
            const addresses = accounts.slice(0, 2);
            const statuses = [true, true];
            const { logs } = await setBanStatusesAsync(addresses, statuses);
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<BannerSetBannedStatusesEventArgs>;
            expect(log.event).to.be.eq('SetBannedStatuses');
            expect(log.args).to.be.deep.eq({
                addresses,
                statuses,
            });
            for (let i = 0; i < addresses.length; i++) {
                const status = await derivadex.bannerContract.getBanStatus(addresses[i]).callAsync();
                expect(status).to.be.eq(statuses[i]);
            }
        });

        it('bans another address and unban the previous addresses', async () => {
            const addresses = [accounts[0], accounts[1], accounts[accounts.length - 1]];
            const statuses = [false, false, true];
            const { logs } = await setBanStatusesAsync(addresses, statuses);
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<BannerSetBannedStatusesEventArgs>;
            expect(log.event).to.be.eq('SetBannedStatuses');
            expect(log.args).to.be.deep.eq({
                addresses,
                statuses,
            });
            for (let i = 0; i < addresses.length; i++) {
                const status = await derivadex.bannerContract.getBanStatus(addresses[i]).callAsync();
                expect(status).to.be.eq(statuses[i]);
            }
        });

        it('bans approved addresses with and without valid signers', async () => {
            const addresses = accounts.slice(5, 12);
            const statuses = addresses.map(() => true);

            // Get any relevant state before setting the ban statuses.
            const validSignersCountBefore = await derivadex.custodianContract.getValidSignersCount().callAsync();
            const custodianStatesBefore = await derivadex.custodianContract.getCustodianStates(addresses).callAsync();

            // Set the ban statuses.
            const { logs, blockNumber } = await setBanStatusesAsync(addresses, statuses);

            // Ensure the logs were emitted correctly.
            expect(logs.length).to.be.eq(3);
            const log = logs[0] as LogWithDecodedArgs<BannerSetBannedStatusesEventArgs>;
            expect(log.event).to.be.eq('SetBannedStatuses');
            expect(log.args).to.be.deep.eq({
                addresses,
                statuses,
            });

            // Ensure the state was updated correctly.
            const validSignersCountAfter = await derivadex.custodianContract.getValidSignersCount().callAsync();
            expect(validSignersCountAfter).to.be.bignumber.eq(validSignersCountBefore.minus(3));
            const custodianStatesAfter = await derivadex.custodianContract.getCustodianStates(addresses).callAsync();
            for (let i = 0; i < addresses.length; i++) {
                const status = await derivadex.bannerContract.getBanStatus(addresses[i]).callAsync();
                expect(status).to.be.eq(statuses[i]);
                expect(custodianStatesAfter[i].approved).to.be.eq(false);
                expect(custodianStatesAfter[i].balance).to.be.bignumber.eq(custodianStatesBefore[i].balance);
                expect(custodianStatesAfter[i].jailed).to.be.eq(custodianStatesBefore[i].jailed);
                expect(custodianStatesAfter[i].unbondETA).to.be.bignumber.eq(
                    custodianStatesBefore[i].approved && custodianStatesBefore[i].balance.gt(0)
                        ? new BigNumber(blockNumber).plus(UNBOND_DELAY)
                        : custodianStatesBefore[i].unbondETA,
                );
            }
        });
    });

    describe('#isNotPaused', () => {
        before(async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.pauseContract.getFunctionSignature('setIsPaused')];
            const calldatas = [
                generateCallData(derivadex.pauseContract.setIsPaused(true).getABIEncodedTransactionData()),
            ];
            const description = 'Pause the DerivaDEX diamond.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            await passProposalAsync(proposalCount);
        });

        after(async () => {
            const targets = [derivadex.derivaDEXContract.address];
            const values = [0];
            const signatures = [derivadex.pauseContract.getFunctionSignature('setIsPaused')];
            const calldatas = [
                generateCallData(derivadex.pauseContract.setIsPaused(false).getABIEncodedTransactionData()),
            ];
            const description = 'Unpause the DerivaDEX diamond.';
            await derivadex
                .propose(targets, values, signatures, calldatas, description)
                .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
            const proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
            await passProposalAsync(proposalCount);
        });

        it('should fail to call "setBanStatuses" when paused', async () => {
            const tx = setBanStatusesAsync([accounts[0]], [true]);
            return expect(tx).to.be.rejectedWith('Governance: transaction execution reverted');
        });
    });
});
