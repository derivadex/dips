import { Derivadex } from '@derivadex/contract-wrappers';
import { advanceBlocksAsync, advanceTimeAsync, generateCallData } from '@derivadex/dev-utils';
import { DecodedLogArgs, LogEntry, LogWithDecodedArgs, TransactionReceiptWithDecodedLogs } from 'ethereum-types';

import { Fixtures } from './fixtures';

export function filterEventLogs(
    logs: Array<LogWithDecodedArgs<DecodedLogArgs> | LogEntry>,
    event: string,
): Array<LogWithDecodedArgs<DecodedLogArgs> | LogEntry> {
    return logs.filter((log) => (log as LogWithDecodedArgs<DecodedLogArgs>).event === event);
}

export interface TestContext {
    derivadex: Derivadex;
    fixtures: Fixtures;
}

/**
 * Passes a governance proposal that has already been made using the fixtures.
 * @param ctx The test context.
 * @param proposalNumber The index of the proposal to pass.
 * @param timeDelay Time delay proposal is in queue.
 * @returns The transaction receipt of the call that executed the proposal.
 */
export async function passProposalAsync(
    ctx: TestContext,
    proposalNumber: number,
    timeDelay = 259200,
): Promise<TransactionReceiptWithDecodedLogs> {
    await advanceBlocksAsync(ctx.derivadex.providerEngine, 2);
    await ctx.derivadex
        .castVote(proposalNumber, true)
        .awaitTransactionSuccessAsync({ from: ctx.fixtures.derivaDEXWallet() });
    await ctx.derivadex.queue(proposalNumber).awaitTransactionSuccessAsync({ from: ctx.fixtures.traderB() });
    await advanceTimeAsync(ctx.derivadex.providerEngine, timeDelay);
    await advanceBlocksAsync(ctx.derivadex.providerEngine, 1);
    return ctx.derivadex.execute(proposalNumber).awaitTransactionSuccessAsync({ from: ctx.fixtures.traderB() });
}

export async function setIsPausedAsync(
    ctx: TestContext,
    isPaused: boolean,
): Promise<TransactionReceiptWithDecodedLogs> {
    const targets = [ctx.derivadex.derivaDEXContract.address];
    const values = [0];
    const signatures = [ctx.derivadex.pauseContract.getFunctionSignature('setIsPaused')];
    const calldatas = [
        generateCallData(ctx.derivadex.pauseContract.setIsPaused(isPaused).getABIEncodedTransactionData()),
    ];
    const description = 'Set the pause status of the DerivaDEX diamond.';
    await ctx.derivadex
        .propose(targets, values, signatures, calldatas, description)
        .awaitTransactionSuccessAsync({ from: ctx.fixtures.derivaDEXWallet() });
    const proposalCount = (await ctx.derivadex.getProposalCountAsync()).toNumber();
    return passProposalAsync(ctx, proposalCount);
}
