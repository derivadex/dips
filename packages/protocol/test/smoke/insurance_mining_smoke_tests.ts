import { providerUtils } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { Derivadex } from '@derivadex/contract-wrappers';
import { expect } from '@derivadex/test-utils';
import * as hardhat from 'hardhat';

import { FORK_URL } from '../constants';

describe('#Insurance Mining Smoke Tests', function () {
    let derivadex: Derivadex;

    before(async () => {
        // Reset to a fresh forked state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [
                {
                    forking: {
                        jsonRpcUrl: FORK_URL,
                    },
                },
            ],
        });
        const chainId = await providerUtils.getChainIdAsync(provider);
        const { addresses: derivadexAddresses, chainId: chainId_ } = await getContractDeploymentAsync('derivadex');
        derivadex = new Derivadex(derivadexAddresses, provider, chainId);
        expect(chainId).to.be.eq(chainId_);
    });

    describe('DDX Token', () => {
        const ddxTokenName = 'DerivaDAO';
        const ddxTokenSymbol = 'DDX';
        const ddxTokenDecimals = 18;
        const ddxTokenVersion = '1';

        it('should have the correct name', async () => {
            expect(await derivadex.ddxContract.name().callAsync()).to.be.eq(ddxTokenName);
        });

        it('should have the correct symbol', async () => {
            expect(await derivadex.ddxContract.symbol().callAsync()).to.be.eq(ddxTokenSymbol);
        });

        it('should have the correct decimals', async () => {
            expect(await derivadex.ddxContract.decimals().callAsync()).to.be.eq(ddxTokenDecimals);
        });

        it('should have the correct version', async () => {
            expect(await derivadex.ddxContract.version().callAsync()).to.be.eq(ddxTokenVersion);
        });

        describe('GnosisSafeProxy', () => {
            it('should not be the token issuer', async () => {
                expect(await derivadex.ddxContract.issuer().callAsync()).to.be.eq(derivadex.derivaDEXContract.address);
            });
        });

        it('should have transferred ownership', async () => {
            expect(await derivadex.ddxContract.ownershipTransferred().callAsync()).to.be.eq(true);
        });
    });
});
