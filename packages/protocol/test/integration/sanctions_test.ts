import { BigNumber } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { Derivadex } from '@derivadex/contract-wrappers';
import { generateCallData, generateStrategyId, getSelectors } from '@derivadex/dev-utils';
import { expect } from '@derivadex/test-utils';
import { FacetCutAction } from '@derivadex/types';
import * as hardhat from 'hardhat';

import { setupWithProviderAsync } from '../../deployment/setup';
import { FORK_URL } from '../constants';
import { Fixtures } from '../fixtures';
import { passProposalAsync, TestContext } from '../utils';

// https://etherscan.io/tx/0xfc6b06392e8e1431e2c9d987b0fda7bc5c8a4e2e4b99ec986174d6935f822f6b#eventlog
const SANCTIONED_ADDRESS = '0x3ad9db589d201a710ed237c829c7860ba86510fc';

describe('#Sanctions', function () {
    let derivadex: Derivadex;
    let accounts: string[];
    let owner: string;
    let fixtures: Fixtures;
    let ctx: TestContext;

    before(async () => {
        // Reset to a fresh forked state.
        const provider = hardhat.network.provider;
        const web3Wrapper = new Web3Wrapper(provider);
        await web3Wrapper.sendRawPayloadAsync<void>({
            method: 'hardhat_reset',
            params: [
                {
                    forking: {
                        jsonRpcUrl: FORK_URL,
                    },
                },
            ],
        });

        // Set up contractWrappers and facets as standalone entities. They
        // are not yet part of the diamond.
        ({ derivadex, accounts, owner } = await setupWithProviderAsync(provider, {
            isFork: false, // this hits some logic specific to the old insurance_mining_test which is
            // not needed for this particular test setup
            isGanache: true,
            isLocalSnapshot: false,
            isTest: false,
            useDeployedDDXToken: false,
        }));
        fixtures = new Fixtures(derivadex, accounts, owner);
        ctx = { derivadex, fixtures };

        // Add Pause facet to the diamond
        const pauseSelectors = getSelectors(derivadex.pauseContract);
        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.pauseContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: pauseSelectors,
                    },
                ],
                derivadex.pauseContract.address,
                derivadex.initializePause().getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setPauseAddressToProxy();

        // Add Governance facet to the diamond
        const governanceSelectors = getSelectors(derivadex.governanceContract, ['initialize']);

        await derivadex
            .diamondCut(
                [
                    {
                        facetAddress: derivadex.governanceContract.address,
                        action: FacetCutAction.Add,
                        functionSelectors: governanceSelectors,
                    },
                ],
                derivadex.governanceContract.address,
                derivadex
                    .initializeGovernance(
                        10,
                        1,
                        17280,
                        1209600,
                        0, // 259200 (3 days worth of seconds normally)
                        4,
                        1,
                        50,
                    )
                    .getABIEncodedTransactionData(),
            )
            .awaitTransactionSuccessAsync({ from: owner });
        derivadex.setGovernanceAddressToProxy();

        // Transfer ownership of DerivaDEX contract to itself, i.e.
        // governance now controls everything
        await derivadex.transferOwnershipToSelf().awaitTransactionSuccessAsync({ from: owner });

        // Add Banner facet to Diamond
        let selectors = getSelectors(derivadex.bannerContract, ['initialize']);
        let targets = [derivadex.derivaDEXContract.address];
        let values = [0];
        let signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        let calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.bannerContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.bannerContract.address,
                        derivadex.bannerContract.initialize([]).getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        let description = 'Add Banner contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        let proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount);
        derivadex.setBannerAddressToProxy();

        // Add Collateral facet to Diamond
        selectors = getSelectors(derivadex.collateralContract, ['initialize']);
        targets = [derivadex.derivaDEXContract.address];
        values = [0];
        signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.collateralContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.collateralContract.address,
                        derivadex.collateralContract
                            .initialize(
                                new BigNumber(50),
                                new BigNumber(10),
                                new BigNumber(1000),
                                Web3Wrapper.toBaseUnitAmount(new BigNumber(1_000), 6),
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        description = 'Add Collateral contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount);
        derivadex.setCollateralAddressToProxy();

        // Add USDC collateral to Collateral contract
        targets = [derivadex.derivaDEXContract.address];
        values = [0];
        signatures = [derivadex.collateralContract.getFunctionSignature('addExchangeCollateral')];
        calldatas = [
            generateCallData(
                derivadex.collateralContract
                    .addExchangeCollateral(
                        derivadex.usdcContract!.address,
                        Web3Wrapper.toBaseUnitAmount(new BigNumber(1_000_000), 6),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        description = 'Add USDC collateral.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount);

        // Add Stake facet to Diamond
        selectors = getSelectors(derivadex.stakeContract, ['initialize']);
        targets = [derivadex.derivaDEXContract.address];
        values = [0];
        signatures = [derivadex.diamondFacetContract.getFunctionSignature('diamondCut')];
        calldatas = [
            generateCallData(
                derivadex
                    .diamondCut(
                        [
                            {
                                facetAddress: derivadex.stakeContract.address,
                                action: FacetCutAction.Add,
                                functionSelectors: selectors,
                            },
                        ],
                        derivadex.stakeContract.address,
                        derivadex.stakeContract
                            .initialize(
                                Web3Wrapper.toBaseUnitAmount(new BigNumber(10_000), 18),
                                Web3Wrapper.toBaseUnitAmount(new BigNumber(40_000_000), 18),
                            )
                            .getABIEncodedTransactionData(),
                    )
                    .getABIEncodedTransactionData(),
            ),
        ];
        description = 'Add Stake contract as a facet.';
        await derivadex
            .propose(targets, values, signatures, calldatas, description)
            .awaitTransactionSuccessAsync({ from: fixtures.derivaDEXWallet() });
        proposalCount = (await derivadex.getProposalCountAsync()).toNumber();
        await passProposalAsync(ctx, proposalCount);
        derivadex.setStakeAddressToProxy();

        // transfer USDC to non-sanctioned address
        await derivadex
            .usdcContract!.transfer(fixtures.traderA(), Web3Wrapper.toBaseUnitAmount(100_000, 6))
            .awaitTransactionSuccessAsync({
                from: fixtures.owner,
            });

        // transfer USDC to sanctioned address
        await derivadex
            .usdcContract!.transfer(SANCTIONED_ADDRESS, Web3Wrapper.toBaseUnitAmount(100_000, 6))
            .awaitTransactionSuccessAsync({
                from: fixtures.owner,
            });

        // transfer DDX to non-sanctioned address
        await derivadex.ddxContract
            .transfer(fixtures.traderA(), Web3Wrapper.toBaseUnitAmount(1_000, 18))
            .awaitTransactionSuccessAsync({
                from: fixtures.owner,
            });

        // transfer DDX to sanctioned address
        await derivadex.ddxContract
            .transfer(SANCTIONED_ADDRESS, Web3Wrapper.toBaseUnitAmount(1_000, 18))
            .awaitTransactionSuccessAsync({
                from: fixtures.owner,
            });

        // Fund the sanctioned address with ether
        const tx = await web3Wrapper.sendTransactionAsync({
            from: fixtures.owner!,
            to: SANCTIONED_ADDRESS,
            value: Web3Wrapper.toBaseUnitAmount(1, 18),
        });
        await web3Wrapper.awaitTransactionSuccessAsync(tx);
    });

    it('successfully deposits USDC with non-sanctioned address', async () => {
        const depositAmount = Web3Wrapper.toBaseUnitAmount(100_000, 6);
        await derivadex
            .usdcContract!.approve(derivadex.derivaDEXContract.address, depositAmount)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        await derivadex.collateralContract
            .deposit(derivadex.usdcContract!.address, generateStrategyId('main'), depositAmount)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
    });

    it('unsuccessfully deposits USDC with sanctioned address', async () => {
        await new Web3Wrapper(hardhat.network.provider).sendRawPayloadAsync({
            method: 'hardhat_impersonateAccount',
            params: [SANCTIONED_ADDRESS],
        });

        const depositAmount = Web3Wrapper.toBaseUnitAmount(100_000, 6);
        await derivadex
            .usdcContract!.approve(derivadex.derivaDEXContract.address, depositAmount)
            .awaitTransactionSuccessAsync({ from: SANCTIONED_ADDRESS });
        await expect(
            derivadex.collateralContract
                .deposit(derivadex.usdcContract!.address, generateStrategyId('main'), depositAmount)
                .awaitTransactionSuccessAsync({ from: SANCTIONED_ADDRESS }),
        ).to.be.rejectedWith('Collateral: cannot deposit if on sanctions list.');
    });

    it('successfully deposits DDX with non-sanctioned address', async () => {
        const depositAmount = Web3Wrapper.toBaseUnitAmount(1_000, 18);
        await derivadex.ddxContract
            .approve(derivadex.derivaDEXContract.address, depositAmount)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
        await derivadex.stakeContract
            .depositDDX(depositAmount)
            .awaitTransactionSuccessAsync({ from: fixtures.traderA() });
    });

    it('unsuccessfully deposits DDX with sanctioned address', async () => {
        const depositAmount = Web3Wrapper.toBaseUnitAmount(1_000, 18);
        await derivadex.ddxContract
            .approve(derivadex.derivaDEXContract.address, depositAmount)
            .awaitTransactionSuccessAsync({ from: SANCTIONED_ADDRESS });
        await expect(
            derivadex.stakeContract
                .depositDDX(depositAmount)
                .awaitTransactionSuccessAsync({ from: SANCTIONED_ADDRESS }),
        ).to.be.rejectedWith('Stake: cannot deposit DDX if on sanctions list.');
    });
});
