import { transformContractsAsync } from './utils';

const nameReplacements: { old: string; new: string }[] = [
    { old: 'Babaganoush', new: 'Banner' },
    { old: 'Chorizo', new: 'Checkpoint' },
    { old: 'Couscous', new: 'Collateral' },
    { old: 'CauliflowerGyro', new: 'CollateralGuarded' },
    { old: 'Capicola', new: 'Custodian' },
    { old: 'chickpea', new: 'ddx' },
    { old: 'CHICKPEA', new: 'DDX' },
    { old: 'Hummus', new: 'Deriva' },
    { old: 'Falafel', new: 'FundedInsuranceFund' },
    { old: 'Gorgonzola', new: 'Specs' },
    { old: 'Garbanzo', new: 'Governance' },
    { old: 'Lemon', new: 'IFund' },
    { old: 'Olive', new: 'Operator' },
    { old: 'Polenta', new: 'Pause' },
    { old: 'Ricotta', new: 'Reject' },
    { old: 'Shakshouka', new: 'Stake' },
    { old: 'Tahini', new: 'Trader' },
];

(async () => {
    await transformContractsAsync(nameReplacements);
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
