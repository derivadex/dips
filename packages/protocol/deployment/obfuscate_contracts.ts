import { transformContractsAsync } from './utils';

const nameReplacements: { old: string; new: string }[] = [
    { old: 'Banner', new: 'Babaganoush' },
    { old: 'Checkpoint', new: 'Chorizo' },
    { old: 'Collateral', new: 'Couscous' },
    { old: 'CollateralGuarded', new: 'CauliflowerGyro' },
    { old: 'Custodian', new: 'Capicola' },
    { old: 'ddx', new: 'chickpea' },
    { old: 'DDX', new: 'CHICKPEA' },
    { old: 'Deriva', new: 'Hummus' },
    { old: 'FundedInsuranceFund', new: 'Falafel' },
    { old: 'Specs', new: 'Gorgonzola' },
    { old: 'Governance', new: 'Garbanzo' },
    { old: 'IFund', new: 'Lemon' },
    { old: 'Operator', new: 'Olive' },
    { old: 'Pause', new: 'Polenta' },
    { old: 'Reject', new: 'Ricotta' },
    { old: 'Stake', new: 'Shakshouka' },
    { old: 'Trader', new: 'Tahini' },
];

(async () => {
    await transformContractsAsync(nameReplacements);
})()
    .then(() => {
        process.exit(0);
    })
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
