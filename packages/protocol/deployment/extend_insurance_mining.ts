import { SupportedProvider } from '@0x/subproviders';
import { BigNumber } from '@0x/utils';
import { Derivadex } from '@derivadex/contract-wrappers';
import { generateCallData } from '@derivadex/dev-utils';
import { ContractAddresses } from '@derivadex/types';

/**
 * Extends the insurance mining program for a specified number of blocks.
 * @param ddxAddresses DerivaDEX system contract addresses.
 * @param provider Web3 provider.
 * @param from ETH address sending transactions.
 */
export async function extendInsuranceMiningAsync(
    ddxAddresses: ContractAddresses,
    provider: SupportedProvider,
    from: string,
    extension: BigNumber,
): Promise<void> {
    const derivadex = new Derivadex(ddxAddresses, provider);
    derivadex.setFacetAddressesToProxy();

    const extendInsuranceMiningTargets = [derivadex.derivaDEXContract.address];
    const extendInsuranceMiningValues = [0];
    const extendInsuranceMiningSignatures = [
        derivadex.insuranceFundContract.getFunctionSignature('extendInsuranceMining'),
    ];
    const extendInsuranceMiningCalldatas = [
        generateCallData(
            derivadex.insuranceFundContract.extendInsuranceMining(extension).getABIEncodedTransactionData(),
        ),
    ];
    const extendInsuranceMiningDescription = 'Extend the insurance mining program.';

    await derivadex
        .propose(
            extendInsuranceMiningTargets,
            extendInsuranceMiningValues,
            extendInsuranceMiningSignatures,
            extendInsuranceMiningCalldatas,
            extendInsuranceMiningDescription,
        )
        .awaitTransactionSuccessAsync({ from });
}
