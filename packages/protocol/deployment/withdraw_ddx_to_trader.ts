import { SupportedProvider } from '@0x/subproviders';
import { Derivadex } from '@derivadex/contract-wrappers';
import { ContractAddresses } from '@derivadex/types';

/**
 * Withdraw DDX to trader from their trader wallet.
 * @param ddxAddresses DerivaDEX system contract addresses.
 * @param provider Web3 provider.
 * @param from ETH address sending transactions.
 * @param amount Amount of DDX to withdraw.
 * @param shouldSendTransaction Whether transaction is sent or logs the calldata.
 */
export async function withdrawDDXToTraderAsync(
    ddxAddresses: ContractAddresses,
    provider: SupportedProvider,
    from: string,
    amount: number,
    shouldSendTransaction: boolean,
): Promise<void> {
    const derivadex = new Derivadex(ddxAddresses, provider);
    derivadex.setFacetAddressesToProxy();

    if (shouldSendTransaction) {
        await derivadex.withdrawDDXToTrader(amount).awaitTransactionSuccessAsync({ from });
    } else {
        console.log(
            `Withdraw DDX to Trader Calldata: ${derivadex.withdrawDDXToTrader(amount).getABIEncodedTransactionData()}`,
        );
    }
}
