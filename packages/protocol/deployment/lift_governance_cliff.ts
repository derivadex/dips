import { SupportedProvider } from '@0x/subproviders';
import { Derivadex } from '@derivadex/contract-wrappers';
import { generateCallData } from '@derivadex/dev-utils';
import { ContractAddresses } from '@derivadex/types';

/**
 * Lifts governance cliff, allowing for token transferability.
 * @param ddxAddresses DerivaDEX system contract addresses.
 * @param provider Web3 provider.
 * @param from ETH address sending transactions.
 */
export async function liftGovernanceCliffAsync(
    ddxAddresses: ContractAddresses,
    provider: SupportedProvider,
    from: string,
): Promise<void> {
    const derivadex = new Derivadex(ddxAddresses, provider);
    derivadex.setFacetAddressesToProxy();

    const liftGovernanceCliffTargets = [derivadex.derivaDEXContract.address];
    const liftGovernanceCliffValues = [0];
    const liftGovernanceCliffSignatures = [derivadex.traderContract.getFunctionSignature('setRewardCliff')];
    const liftGovernanceCliffCalldatas = [
        generateCallData(derivadex.traderContract.setRewardCliff(true).getABIEncodedTransactionData()),
    ];
    const liftGovernanceCliffDescription = 'Lift the governance cliff allowing for token transferability.';

    await derivadex
        .propose(
            liftGovernanceCliffTargets,
            liftGovernanceCliffValues,
            liftGovernanceCliffSignatures,
            liftGovernanceCliffCalldatas,
            liftGovernanceCliffDescription,
        )
        .awaitTransactionSuccessAsync({ from });
}
