import { readFileAsync, writeFileAsync } from '@derivadex/dev-utils';
import { ContractAddresses } from '@derivadex/types';
import hardhat from 'hardhat';

const regexDynamicPrefixStringAssignment = '[^"\\(]*?"[^"]*?';
const regexDynamicSuffixStringAssignment = '([^"]*?")';

const regexDynamicPrefixBuiltinCall = '[^"]*?"[^"]*?';
const regexDynamicSuffixBuiltinCall = '([^"]*?")';

const stringAssigmentSearchTerms = ['name', 'symbol'];
const builtinCallsSearchTerms = ['revert\\(', 'require\\(', 'safe32\\(', 'safe96\\(', 'safe224\\('];

/**
 * Transforms identifying information from the DerivaDEX contracts that will be
 * deployed. This identifying information includes names, symbols, and any form
 * of revert message.
 * @param nameReplacements The list of replacements that should be used.
 */
export async function transformContractsAsync(nameReplacements: { old: string; new: string }[]): Promise<void> {
    // Construct the list of replacement regular expressions.
    //
    // NOTE(jalextowle): This set of replacements does not attempt to be completely
    // perfect, but it does strive to be very good. Some information leakages that
    // I can immediately think of are:
    // 1. Someone savvy can simply read the bytecode and figure out what is going
    //    on. This can be aided by tooling. Our proxy pattern may throw people off
    //    the scent, but it will be fairly obvious that we are utilizing a
    //    delegatecall proxy pattern if anyone looks carefully at our Etherscan logs.
    // 2. Someone that knows the semantic structure of DerivaDAO revert messages will
    //    still be able to identify our set of contracts. That said, the names are
    //    obfuscated (with a tasty hummus theme!) within all of these messages, which
    //    will make it much more difficult to identify the contracts.
    // 3. Another important identifying characteristic is that the ABI of the
    //    DerivaDAO facets can be understood by analyzing calls to the contracts and
    //    determining a probalistic approximation of the ABI by analyzing successful
    //    and unsuccessful function calls.
    const replacements: { old: RegExp; new: string }[] = [];
    for (const replacement of nameReplacements) {
        for (const term of stringAssigmentSearchTerms) {
            replacements.push({
                old: new RegExp(
                    `(${term}${regexDynamicPrefixStringAssignment})${replacement.old}(${regexDynamicSuffixStringAssignment})`,
                    'g',
                ),
                new: `$1${replacement.new}$2`,
            });
        }
        for (const term of builtinCallsSearchTerms) {
            replacements.push({
                old: new RegExp(
                    `(${term}${regexDynamicPrefixBuiltinCall})${replacement.old}(${regexDynamicSuffixBuiltinCall})`,
                    'g',
                ),
                new: `$1${replacement.new}$2`,
            });
        }
    }

    // Get the list of paths to transform. We only need to transform contracts
    // in `src/` as test contracts will not be deployed.
    const pathsToTransform = (await hardhat.artifacts.getAllFullyQualifiedNames())
        .filter((fullyQualifiedName) => /contracts\/src/.test(fullyQualifiedName))
        .map((fullyQualifiedName) => fullyQualifiedName.split(':')[0]);

    // Perform the code transformations. We perform it several times to attempt
    // to find all matches (some can be missed since our regexes will stop as
    // soon as they find the first term of a match). This can be increased to
    // offer better protection if necessary (adding an iteration will allow the
    // tool to replace one extra instance of the same word in a match).
    for (let i = 0; i < 5; i++) {
        for (const path of pathsToTransform) {
            const oldContents = (await readFileAsync(path)).toString();
            let newContents = oldContents;
            for (const replacement of replacements) {
                newContents = newContents.replace(replacement.old, replacement.new);
            }
            if (oldContents !== newContents) {
                await writeFileAsync(path, newContents);
            }
        }
    }
}

/**
 * Converts any undefined fields of a partial ContractAddresses object to empty
 * strings.
 * @param addresses The parial ContractAddresses object to base the result
 *        ContractAddresses off of.
 */
export function fromPartialContractAddresses(addresses: Partial<ContractAddresses>): ContractAddresses {
    return {
        derivaDEXAddress: '',
        ddxAddress: '',
        ddxWalletCloneableAddress: '',
        diFundTokenFactoryAddress: '',
        bannerAddress: '',
        checkpointAddress: '',
        collateralAddress: '',
        custodianAddress: '',
        fundedInsuranceFundAddress: '',
        governanceAddress: '',
        insuranceFundAddress: '',
        pauseAddress: '',
        registrationAddress: '',
        rejectAddress: '',
        specsAddress: '',
        stakeAddress: '',
        traderAddress: '',
        ausdcAddress: '',
        cusdcAddress: '',
        usdcAddress: '',
        ausdtAddress: '',
        cusdtAddress: '',
        usdtAddress: '',
        gusdAddress: '',
        husdAddress: '',
        gnosisSafeAddress: '',
        gnosisSafeProxyFactoryAddress: '',
        gnosisSafeProxyAddress: '',
        createCallAddress: '',
        ...addresses,
    };
}
