import { BaseContract } from '@0x/base-contract';
import { providerUtils } from '@0x/utils';
import { Derivadex } from '@derivadex/contract-wrappers';
import { ContractAddresses, ObjectMap } from '@derivadex/types';
import { SupportedProvider } from 'ethereum-types';

import { Deployer } from './deployer';
import { fromPartialContractAddresses } from './utils';

export interface DDXDeployment {
    accounts: string[];
    owner: string;
    derivadex: Derivadex;
    contractWrappers: ObjectMap<BaseContract>;
    contractAddresses: ContractAddresses;
}

export interface SetupConfig {
    isFork: boolean;
    isGanache: boolean;
    isTest: boolean;
    isLocalSnapshot: boolean;
    useDeployedDDXToken: boolean;
}

/**
 * Setup deployment using a provider.
 * @param provider A supported provider.
 * @returns Deployment of the DerivaDEX smart contracts.
 */
export async function setupWithProviderAsync(provider: SupportedProvider, config: SetupConfig): Promise<DDXDeployment> {
    const chainId = await providerUtils.getChainIdAsync(provider);
    const deployer = new Deployer({
        chainId,
        deployment: 'test',
        provider,
        isSingleNodeDeployment: true,
        ...config,
    });
    const contractAddresses = fromPartialContractAddresses(await deployer.deployAsync());
    const derivadex = new Derivadex(
        {
            derivaDEXAddress: deployer.contracts.derivaDEX.address,
            bannerAddress: deployer.contracts.banner.address,
            checkpointAddress: deployer.contracts.checkpoint.address,
            collateralAddress: deployer.contracts.collateral.address,
            custodianAddress: deployer.contracts.custodian.address,
            governanceAddress: deployer.contracts.governance.address,
            insuranceFundAddress: deployer.contracts.insuranceFund.address,
            fundedInsuranceFundAddress: deployer.contracts.fundedInsuranceFund.address,
            registrationAddress: deployer.contracts.registration.address,
            rejectAddress: deployer.contracts.reject.address,
            traderAddress: deployer.contracts.trader.address,
            pauseAddress: deployer.contracts.pause.address,
            specsAddress: deployer.contracts.specs.address,
            stakeAddress: deployer.contracts.stake.address,
            ddxWalletCloneableAddress: deployer.contracts.ddxWalletCloneable.address,
            diFundTokenFactoryAddress: deployer.contracts.diFundTokenFactory.address,
            ddxAddress: deployer.contracts.ddx.address,
            usdtAddress: deployer.contracts.usdt ? deployer.contracts.usdt.address : '',
            ausdtAddress: deployer.contracts.ausdt ? deployer.contracts.ausdt.address : '',
            cusdtAddress: deployer.contracts.cusdt ? deployer.contracts.cusdt.address : '',
            usdcAddress: deployer.contracts.usdc ? deployer.contracts.usdc.address : '',
            ausdcAddress: deployer.contracts.ausdc ? deployer.contracts.ausdc.address : '',
            cusdcAddress: deployer.contracts.cusdc ? deployer.contracts.cusdc.address : '',
            husdAddress: deployer.contracts.husd ? deployer.contracts.husd.address : '',
            gusdAddress: deployer.contracts.gusd ? deployer.contracts.gusd.address : '',
            gnosisSafeAddress: deployer.contracts.gnosisSafeAddress ? deployer.contracts.gnosisSafeAddress.address : '',
            gnosisSafeProxyFactoryAddress: deployer.contracts.gnosisSafeProxyFactoryAddress
                ? deployer.contracts.gnosisSafeProxyFactoryAddress.address
                : '',
            gnosisSafeProxyAddress: deployer.contracts.gnosisSafeProxyAddress
                ? deployer.contracts.gnosisSafeProxyAddress.address
                : '',
            createCallAddress: deployer.contracts.createCallAddress ? deployer.contracts.createCallAddress.address : '',
        },
        provider,
        chainId,
    );
    return {
        accounts: deployer.accounts,
        owner: deployer.owner,
        derivadex,
        contractWrappers: deployer.contracts,
        contractAddresses,
    };
}
