import { BaseContract } from '@0x/base-contract';
import { MnemonicWalletSubprovider } from '@0x/subproviders';
import { SupportedProvider, Web3Wrapper } from '@0x/web3-wrapper';
import { getContractDeploymentAsync } from '@derivadex/contract-addresses';
import { artifacts } from '@derivadex/contract-artifacts';
import {
    AUSDTContract,
    BannerContract,
    CheckpointContract,
    CollateralContract,
    CUSDTContract,
    CustodianContract,
    DDXContract,
    DDXWalletCloneableContract,
    DerivaDEXContract,
    DIFundTokenFactoryContract,
    DummyTokenContract,
    FundedInsuranceFundContract,
    GasConsumerContract,
    GovernanceContract,
    InsuranceFundContract,
    PauseContract,
    RegistrationContract,
    RejectContract,
    SafeERC20WrapperContract,
    SpecsContract,
    StakeContract,
    TestCheckpointContract,
    TestCheckpointMultiNodeDeploymentContract,
    TestCheckpointSingleNodeDeploymentContract,
    TestRegistrationContract,
    TestRegistrationWithResetContract,
    TraderContract,
} from '@derivadex/contract-wrappers';
import { ContractAddresses, ObjectMap } from '@derivadex/types';

import { TestCollateralContract, TestStakeContract } from '../test/generated-wrappers';

export interface DeployerConfig {
    chainId: number;
    deployment: string;
    provider: SupportedProvider;
    useDeployedDDXToken: boolean;
    isFork: boolean;
    isGanache: boolean;
    isLocalSnapshot: boolean;
    isSingleNodeDeployment: boolean;
    isTest: boolean;
}

const unlockedAddressUSDT = '0xbebc44782c7db0a1a60cb6fe97d0b483032ff1c7';
const unlockedAddressCUSDT = '0x7d6149aD9A573A6E2Ca6eBf7D4897c1B766841B4';
const unlockedAddressAUSDT = '0x83f798e925bcd4017eb265844fddabb448f1707d';

// TODO: This really needs to be cleaned up.
export class Deployer {
    public accounts: string[];
    public chainId: number;
    public contracts: ObjectMap<BaseContract>;
    public deployment: string;
    public isFork: boolean;
    public isGanache: boolean;
    public isLocalSnapshot: boolean;
    public isSingleNodeDeployment: boolean;
    public isTest: boolean;
    public useDeployedDDXToken: boolean;
    public owner: string;
    public provider: SupportedProvider;

    public static getLibraryPath(libraryName: string): string {
        return `contracts/margin/impl/${libraryName}.sol:${libraryName}`;
    }

    constructor(config: DeployerConfig) {
        this.chainId = config.chainId;
        this.deployment = config.deployment;
        this.provider = config.provider;
        this.accounts = [];
        this.contracts = {};
        this.owner = '';
        this.isFork = config.isFork;
        this.isGanache = config.isGanache;
        this.isSingleNodeDeployment = config.isSingleNodeDeployment;
        this.isTest = config.isTest;
        this.useDeployedDDXToken = config.useDeployedDDXToken;
        this.isLocalSnapshot = config.isLocalSnapshot;
    }

    public async initAccountsAsync(): Promise<void> {
        const web3Wrapper = new Web3Wrapper(this.provider);
        // NOTE: If the chain ID is 100 (meaning that the chain is our dev configuration
        // of Geth), then we hardcode the 30 funded accounts since they are simply
        // pre-funded rather than unlocked accounts. Additionally, we add a mnemonic
        // subprovider to the provider stack to ensure that the correct accounts will
        // be able to submit transactions to Geth.
        if (this.chainId === 100) {
            const mnemonicSubprovider = new MnemonicWalletSubprovider({
                mnemonic: 'concert load couple harbor equip island argue ramp clarify fence smart topic',
                chainId: this.chainId,
            });
            this.accounts = await mnemonicSubprovider.getAccountsAsync(30);
        } else {
            this.accounts = await web3Wrapper.getAvailableAddressesAsync();
        }
        this.owner = this.accounts[0];
    }

    public async deployDDXAsync(gasConsumer?: GasConsumerContract): Promise<DDXContract> {
        let ddx;
        if (this.useDeployedDDXToken) {
            const { addresses, chainId } = await getContractDeploymentAsync(this.deployment);
            if (chainId !== this.chainId) {
                throw new Error('deployer: Chain ID mismatch');
            }
            ddx = this.contracts.ddx = new DDXContract(addresses.ddxAddress, this.provider);
        } else {
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            ddx = this.contracts.ddx = await DDXContract.deployFrom0xArtifactAsync(
                artifacts.DDX,
                this.provider,
                { from: this.owner },
                artifacts,
            );
        }
        return ddx;
    }

    public async deployDerivaDEXAsync(gasConsumer?: GasConsumerContract): Promise<DerivaDEXContract> {
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        const derivaDEX = (this.contracts.derivaDEX = await DerivaDEXContract.deployFrom0xArtifactAsync(
            artifacts.DerivaDEX,
            this.provider,
            { from: this.owner },
            artifacts,
            this.contracts.ddx.address,
        ));
        return derivaDEX;
    }

    public async distributeUSDTAsync(): Promise<void> {
        const usdtAddress = '0xdAC17F958D2ee523a2206206994597C13D831ec7';
        const cusdtAddress = '0xf650c3d88d12db855b8bf7d11be6c55a4e07dcc9';
        const ausdtAddress = '0x71fc860f7d3a592a4a98740e39db31d25db65ae8';
        const tokenCount = 500000;
        const cusdtTokenCount = 500;
        const ausdtTokenCount = 5000;

        const usdt = (this.contracts.usdt = new SafeERC20WrapperContract(usdtAddress, this.provider));
        const cusdt = (this.contracts.cusdt = new CUSDTContract(cusdtAddress, this.provider, { gas: 500000 }));
        const ausdt = (this.contracts.ausdt = new AUSDTContract(ausdtAddress, this.provider, { gas: 500000 }));
        const amount = Web3Wrapper.toBaseUnitAmount(tokenCount, 6);
        const cusdtAmount = Web3Wrapper.toBaseUnitAmount(cusdtTokenCount, 8);
        const ausdtAmount = Web3Wrapper.toBaseUnitAmount(ausdtTokenCount, 6);
        for (const account of this.accounts.slice(0, 5)) {
            if (account !== this.owner) {
                await usdt
                    .transfer(account, amount)
                    .awaitTransactionSuccessAsync({ gasPrice: 0, from: unlockedAddressUSDT });
                await cusdt
                    .transfer(account, cusdtAmount)
                    .awaitTransactionSuccessAsync({ gasPrice: 0, from: unlockedAddressCUSDT });
                await ausdt
                    .transfer(account, ausdtAmount)
                    .awaitTransactionSuccessAsync({ gasPrice: 0, from: unlockedAddressAUSDT });
            }
        }
    }

    public async distributeUSDTNoForkAsync(gasConsumer?: GasConsumerContract): Promise<void> {
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        const usdt = (this.contracts.usdt = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            this.provider,
            { from: this.owner },
            artifacts,
            'USDT',
            'USDT',
            6,
        ));
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        const cusdt = (this.contracts.cusdt = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            this.provider,
            { from: this.owner },
            artifacts,
            'CUSDT',
            'CUSDT',
            8,
        ));
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        const ausdt = (this.contracts.ausdt = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            this.provider,
            { from: this.owner },
            artifacts,
            'AUSDT',
            'AUSDT',
            6,
        ));
        const safeUSDT = new SafeERC20WrapperContract(usdt.address, this.provider);
        const safeCUSDT = new SafeERC20WrapperContract(cusdt.address, this.provider);
        const safeAUSDT = new SafeERC20WrapperContract(ausdt.address, this.provider);
        const tokenCount = 500000;
        const cusdtTokenCount = 500;
        const ausdtTokenCount = 5000;
        const amount = Web3Wrapper.toBaseUnitAmount(tokenCount, 6);
        const cusdtAmount = Web3Wrapper.toBaseUnitAmount(cusdtTokenCount, 8);
        const ausdtAmount = Web3Wrapper.toBaseUnitAmount(ausdtTokenCount, 6);
        for (const account of this.accounts.slice(0, 5)) {
            if (account !== this.owner) {
                if (gasConsumer !== undefined) {
                    await this.consumeGasAsync(gasConsumer);
                }
                await safeUSDT.transfer(account, amount).awaitTransactionSuccessAsync({ from: this.owner });
                await safeCUSDT.transfer(account, cusdtAmount).awaitTransactionSuccessAsync({ from: this.owner });
                await safeAUSDT.transfer(account, ausdtAmount).awaitTransactionSuccessAsync({ from: this.owner });
            }
        }
    }

    public async distributeUSDCAsync(): Promise<void> {
        const usdcAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48';
        const cusdcAddress = '0x39aa39c021dfbae8fac545936693ac917d5e7563';
        const ausdcAddress = '0x9bA00D6856a4eDF4665BcA2C2309936572473B7E';

        this.contracts.usdc = new SafeERC20WrapperContract(usdcAddress, this.provider);
        this.contracts.cusdc = new CUSDTContract(cusdcAddress, this.provider);
        this.contracts.ausdc = new AUSDTContract(ausdcAddress, this.provider);
    }

    public async distributeUSDCNoForkAsync(gasConsumer?: GasConsumerContract): Promise<void> {
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.usdc = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            this.provider,
            { from: this.owner },
            artifacts,
            'USDC',
            'USDC',
            6,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.cusdc = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            this.provider,
            { from: this.owner },
            artifacts,
            'CUSDC',
            'CUSDC',
            8,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.ausdc = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            this.provider,
            { from: this.owner },
            artifacts,
            'AUSDC',
            'AUSDC',
            6,
        );
    }

    public async distributeHUSDAsync(): Promise<void> {
        const husdAddress = '0xdf574c24545e5ffecb9a659c229253d4111d87e1';
        const unlockedAddressHUSD = '0xe93381fb4c4f14bda253907b18fad305d799241a';
        const tokenCount = 1000;

        const husd = (this.contracts.husd = new SafeERC20WrapperContract(husdAddress, this.provider));
        const amount = Web3Wrapper.toBaseUnitAmount(tokenCount, 8);
        for (const account of this.accounts.slice(0, 5)) {
            if (account !== this.owner) {
                // Distributing 100 tokens to each account (we have 18 decimals)
                await husd.transfer(account, amount).awaitTransactionSuccessAsync({ from: unlockedAddressHUSD });
            }
        }
    }

    public async distributeHUSDNoForkAsync(gasConsumer?: GasConsumerContract): Promise<void> {
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        const husd = (this.contracts.husd = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            this.provider,
            { from: this.owner },
            artifacts,
            'HDUM',
            'HDUM',
            8,
        ));
        const safeHUSD = new SafeERC20WrapperContract(husd.address, this.provider);
        const tokenCount = 1000;
        const amount = Web3Wrapper.toBaseUnitAmount(tokenCount, 8);
        for (const account of this.accounts.slice(0, 5)) {
            if (account !== this.owner) {
                if (gasConsumer !== undefined) {
                    await this.consumeGasAsync(gasConsumer);
                }
                // Distributing 100 tokens to each account (we have 18 decimals)
                await safeHUSD.transfer(account, amount).awaitTransactionSuccessAsync({ from: this.owner });
            }
        }
    }

    public async distributeGUSDAsync(): Promise<void> {
        const gusdAddress = '0x056fd409e1d7a124bd7017459dfea2f387b6d5cd';
        const unlockedAddressGUSD = '0xe93381fb4c4f14bda253907b18fad305d799241a';
        const tokenCount = 1000;

        const gusd = (this.contracts.gusd = new SafeERC20WrapperContract(gusdAddress, this.provider));
        const amount = Web3Wrapper.toBaseUnitAmount(tokenCount, 8);
        for (const account of this.accounts.slice(0, 5)) {
            if (account !== this.owner) {
                // Distributing 100 tokens to each account (we have 18 decimals)
                await gusd.transfer(account, amount).awaitTransactionSuccessAsync({ from: unlockedAddressGUSD });
            }
        }
    }

    public async distributeGUSDNoForkAsync(gasConsumer?: GasConsumerContract): Promise<void> {
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        const gusd = (this.contracts.gusd = await DummyTokenContract.deployFrom0xArtifactAsync(
            artifacts.DummyToken,
            this.provider,
            { from: this.owner },
            artifacts,
            'GDUM',
            'GDUM',
            2,
        ));
        const safeGUSD = new SafeERC20WrapperContract(gusd.address, this.provider);
        const tokenCount = 1000;
        const amount = Web3Wrapper.toBaseUnitAmount(tokenCount, 8);
        for (const account of this.accounts.slice(0, 5)) {
            if (account !== this.owner) {
                if (gasConsumer !== undefined) {
                    await this.consumeGasAsync(gasConsumer);
                }
                // Distributing 100 tokens to each account (we have 18 decimals)
                await safeGUSD.transfer(account, amount).awaitTransactionSuccessAsync({ from: this.owner });
            }
        }
    }

    public async deployGovernanceAsync(gasConsumer?: GasConsumerContract): Promise<GovernanceContract> {
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        return (this.contracts.governance = await GovernanceContract.deployFrom0xArtifactAsync(
            artifacts.Governance,
            this.provider,
            { from: this.owner },
            artifacts,
        ));
    }

    public async deployProtocolAsync(gasConsumer?: GasConsumerContract): Promise<void> {
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.ddxWalletCloneable = await DDXWalletCloneableContract.deployFrom0xArtifactAsync(
            artifacts.DDXWalletCloneable,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.diFundTokenFactory = await DIFundTokenFactoryContract.deployFrom0xArtifactAsync(
            artifacts.DIFundTokenFactory,
            this.provider,
            { from: this.owner },
            artifacts,
            this.contracts.derivaDEX.address,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.banner = await BannerContract.deployFrom0xArtifactAsync(
            artifacts.Banner,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.custodian = await CustodianContract.deployFrom0xArtifactAsync(
            artifacts.Custodian,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.fundedInsuranceFund = await FundedInsuranceFundContract.deployFrom0xArtifactAsync(
            artifacts.FundedInsuranceFund,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.insuranceFund = await InsuranceFundContract.deployFrom0xArtifactAsync(
            artifacts.InsuranceFund,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.pause = await PauseContract.deployFrom0xArtifactAsync(
            artifacts.Pause,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.reject = await RejectContract.deployFrom0xArtifactAsync(
            artifacts.Reject,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.specs = await SpecsContract.deployFrom0xArtifactAsync(
            artifacts.Specs,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (gasConsumer !== undefined) {
            await this.consumeGasAsync(gasConsumer);
        }
        this.contracts.trader = await TraderContract.deployFrom0xArtifactAsync(
            artifacts.Trader,
            this.provider,
            { from: this.owner },
            artifacts,
        );
        if (this.isLocalSnapshot) {
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            this.contracts.collateral = await TestCollateralContract.deployFrom0xArtifactAsync(
                artifacts.TestCollateral,
                this.provider,
                { from: this.owner },
                artifacts,
            );
            this.contracts.stake = await TestStakeContract.deployFrom0xArtifactAsync(
                artifacts.TestStake,
                this.provider,
                { from: this.owner },
                artifacts,
            );
            if (this.isSingleNodeDeployment) {
                if (gasConsumer !== undefined) {
                    await this.consumeGasAsync(gasConsumer);
                }
                this.contracts.checkpoint = await TestCheckpointSingleNodeDeploymentContract.deployFrom0xArtifactAsync(
                    artifacts.TestCheckpointSingleNodeDeployment,
                    this.provider,
                    { from: this.owner },
                    artifacts,
                );
            } else {
                if (gasConsumer !== undefined) {
                    await this.consumeGasAsync(gasConsumer);
                }
                this.contracts.checkpoint = await TestCheckpointMultiNodeDeploymentContract.deployFrom0xArtifactAsync(
                    artifacts.TestCheckpointMultiNodeDeployment,
                    this.provider,
                    { from: this.owner },
                    artifacts,
                );
            }
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            this.contracts.registration = await TestRegistrationWithResetContract.deployFrom0xArtifactAsync(
                artifacts.TestRegistrationWithReset,
                this.provider,
                { from: this.owner },
                artifacts,
            );
        } else if (this.isTest) {
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            this.contracts.collateral = await TestCollateralContract.deployFrom0xArtifactAsync(
                artifacts.TestCollateral,
                this.provider,
                { from: this.owner },
                artifacts,
            );
            this.contracts.stake = await TestStakeContract.deployFrom0xArtifactAsync(
                artifacts.TestStake,
                this.provider,
                { from: this.owner },
                artifacts,
            );
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            this.contracts.checkpoint = await TestCheckpointContract.deployFrom0xArtifactAsync(
                artifacts.TestCheckpoint,
                this.provider,
                { from: this.owner },
                artifacts,
            );
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            this.contracts.registration = await TestRegistrationContract.deployFrom0xArtifactAsync(
                artifacts.TestRegistration,
                this.provider,
                { from: this.owner },
                artifacts,
            );
        } else {
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            this.contracts.collateral = await CollateralContract.deployFrom0xArtifactAsync(
                artifacts.Collateral,
                this.provider,
                { from: this.owner },
                artifacts,
            );
            this.contracts.stake = await StakeContract.deployFrom0xArtifactAsync(
                artifacts.Stake,
                this.provider,
                { from: this.owner },
                artifacts,
            );
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            this.contracts.checkpoint = await CheckpointContract.deployFrom0xArtifactAsync(
                artifacts.Checkpoint,
                this.provider,
                { from: this.owner },
                artifacts,
            );
            if (gasConsumer !== undefined) {
                await this.consumeGasAsync(gasConsumer);
            }
            this.contracts.registration = await RegistrationContract.deployFrom0xArtifactAsync(
                artifacts.Registration,
                this.provider,
                { from: this.owner },
                artifacts,
            );
        }
    }

    public async deployAsync(): Promise<Partial<ContractAddresses>> {
        await this.initAccountsAsync();

        // If this is a Geth deployment, deploy the GasConsumer contract
        // to ensure that the gas limit doesn't get too low during deployments.
        let gasConsumer;
        if (this.chainId === 100) {
            gasConsumer = await GasConsumerContract.deployFrom0xArtifactAsync(
                artifacts.GasConsumer,
                this.provider,
                { from: this.owner },
                artifacts,
            );
        }

        await this.deployDDXAsync(gasConsumer);
        await this.deployDerivaDEXAsync(gasConsumer);
        await this.deployGovernanceAsync(gasConsumer);

        // Distribute USDC and USDT depending on the network configuration that
        // is being used.
        if (this.isGanache) {
            if (this.isFork) {
                // Unlock the accounts that we need to impersonate
                await new Web3Wrapper(this.provider).sendRawPayloadAsync({
                    method: 'hardhat_impersonateAccount',
                    params: [unlockedAddressUSDT],
                });
                await new Web3Wrapper(this.provider).sendRawPayloadAsync({
                    method: 'hardhat_impersonateAccount',
                    params: [unlockedAddressAUSDT],
                });
                await new Web3Wrapper(this.provider).sendRawPayloadAsync({
                    method: 'hardhat_impersonateAccount',
                    params: [unlockedAddressCUSDT],
                });

                await this.distributeUSDTAsync();
                await this.distributeUSDCAsync();

                // Re-lock the accounts that we need to impersonate
                await new Web3Wrapper(this.provider).sendRawPayloadAsync({
                    method: 'hardhat_stopImpersonatingAccount',
                    params: [unlockedAddressUSDT],
                });
                await new Web3Wrapper(this.provider).sendRawPayloadAsync({
                    method: 'hardhat_stopImpersonatingAccount',
                    params: [unlockedAddressAUSDT],
                });
                await new Web3Wrapper(this.provider).sendRawPayloadAsync({
                    method: 'hardhat_stopImpersonatingAccount',
                    params: [unlockedAddressCUSDT],
                });
            } else {
                await this.distributeUSDTNoForkAsync(gasConsumer);
                await this.distributeUSDCNoForkAsync(gasConsumer);
            }
        }

        await this.deployProtocolAsync(gasConsumer);

        // NOTE(jalextowle): Several of these addresses can be undefined to
        // avoid overwriting previous entries with the spread operator.
        return {
            derivaDEXAddress: this.contracts.derivaDEX.address,
            bannerAddress: this.contracts.banner.address,
            checkpointAddress: this.contracts.checkpoint.address,
            collateralAddress: this.contracts.collateral.address,
            custodianAddress: this.contracts.custodian.address,
            governanceAddress: this.contracts.governance.address,
            insuranceFundAddress: this.contracts.insuranceFund.address,
            fundedInsuranceFundAddress: this.contracts.fundedInsuranceFund.address,
            registrationAddress: this.contracts.registration.address,
            rejectAddress: this.contracts.reject.address,
            specsAddress: this.contracts.specs.address,
            stakeAddress: this.contracts.stake.address,
            traderAddress: this.contracts.trader.address,
            pauseAddress: this.contracts.pause.address,
            ddxWalletCloneableAddress: this.contracts.ddxWalletCloneable.address,
            diFundTokenFactoryAddress: this.contracts.diFundTokenFactory.address,
            ddxAddress: this.contracts.ddx.address,
            usdtAddress: this.contracts.usdt ? this.contracts.usdt.address : undefined,
            cusdtAddress: this.contracts.cusdt ? this.contracts.cusdt.address : undefined,
            ausdtAddress: this.contracts.ausdt ? this.contracts.ausdt.address : undefined,
            usdcAddress: this.contracts.usdc ? this.contracts.usdc.address : undefined,
            cusdcAddress: this.contracts.cusdc ? this.contracts.cusdc.address : undefined,
            ausdcAddress: this.contracts.ausdc ? this.contracts.ausdc.address : undefined,
            gasConsumerAddress: gasConsumer !== undefined ? gasConsumer.address : undefined,
        };
    }

    async consumeGasAsync(gasConsumer: GasConsumerContract): Promise<void> {
        try {
            await gasConsumer.consume().awaitTransactionSuccessAsync({
                from: this.owner,
                gas: 10_000_000,
            });
            throw new Error('Expected `consume` call to fail.');
        } finally {
            return;
        }
    }
}
