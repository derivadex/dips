import { BigNumber } from '@0x/utils';
import { DecodedLogArgs, LogEntry, LogWithDecodedArgs } from 'ethereum-types';

export type EthereumLogs = Array<LogWithDecodedArgs<DecodedLogArgs> | LogEntry>;

export type Nullable<T> = { [K in keyof T]: T[K] | null };

export interface ObjectMap<T> {
    [key: string]: T;
}

export interface LegacyContractAddresses {
    derivaDEXAddress: string;
    quoteTokenAddress: string;
    derivaDEXTokenAddress: string;
}

export interface Account {
    margin: BigNumber;
    maxLeverage: BigNumber;
    status: number;
}

export type PositionForFundingRate = PositionToADL;

export interface PositionToADL {
    trader: string;
    account: string;
}

export type PositionToADLForContract = PositionToADL;

export interface TraderInfo {
    active: boolean;
    specialAffiliate: boolean;
    referralAddress: string;
    ddxBalance: BigNumber;
}

export enum Network {
    Ganache = 50,
    Mainnet = 1,
    Rinkeby = 40,
    Kovan = 42,
    Goerli = 5,
}

export enum Chain {
    Ganache = 1337,
    Kovan = 42,
    Mainnet = 1,
    Goerli = 5,
}

export interface DatabaseConfig {
    connectionIdentifier: string;
    user: string;
    host: string;
    database: string;
    password: string;
    port: number;
    max?: number;
    connectionTimeoutMillis?: number;
}

export interface BlockchainConfig {
    addresses: ContractAddresses;
    chainId: string;
    deployment: string;
}

export interface ContractAddresses {
    bannerAddress: string;
    derivaDEXAddress: string;
    governanceAddress: string;
    ddxAddress: string;
    checkpointAddress: string;
    collateralAddress: string;
    custodianAddress: string;
    insuranceFundAddress: string;
    fundedInsuranceFundAddress: string;
    registrationAddress: string;
    rejectAddress: string;
    specsAddress: string;
    stakeAddress: string;
    traderAddress: string;
    pauseAddress: string;
    ddxWalletCloneableAddress: string;
    diFundTokenFactoryAddress: string;
    cusdtAddress: string;
    ausdtAddress: string;
    usdtAddress: string;
    cusdcAddress: string;
    ausdcAddress: string;
    usdcAddress: string;
    husdAddress: string;
    gusdAddress: string;
    gnosisSafeAddress: string;
    gnosisSafeProxyFactoryAddress: string;
    gnosisSafeProxyAddress: string;
    createCallAddress: string;
    gasConsumerAddress?: string;
}

export interface EthereumAccount {
    address: string;
    privateKey: string;
}

export interface ContractDeployment {
    addresses: ContractAddresses;
    chainId: number;
    custodians: EthereumAccount[];
    fundedAccounts?: string[];
    faucet?: EthereumAccount;
    timestamp?: Date;
}

export interface ContractDeployments {
    [deployment: string]: ContractDeployment;
}

export interface ExchangeContractAddresses {
    derivaDEXAddress: string;
    quoteTokenAddress: string;
    derivaDEXTokenAddress: string;
}

export interface TransferRecipient {
    recipient: string;
    amount: number;
}

export interface TransferArbitraryTokensRecipient {
    tokenAddress: string;
    recipient: string;
    amount: number;
}

export interface CompTokenDistribution {
    amount: number;
    ethAmount: number;
    recipients: Array<string>;
}

export interface CastVote {
    proposalId: number;
    support: boolean;
}

export interface WithdrawDDXToTrader {
    amount: number;
    shouldSendTransaction: boolean;
}

export interface MigrationEnv {
    rpcUrl: string;
    from: string;
    deployment: string;
    confirmations: number;
    pk?: string;
    owners?: Array<string>;
    accountsToFund?: Array<string>;
    compTokenDistribution?: CompTokenDistribution;
    threshold?: number;
    to?: string;
    data?: string;
    fallbackHandler?: string;
    paymentToken?: string;
    payment?: number;
    paymentReceiver?: string;
    useDeployedDDXToken?: boolean;
    shouldPause?: boolean;
    castVote?: CastVote;
    withdrawDDXToTrader?: WithdrawDDXToTrader;
    transferRecipients?: Array<TransferRecipient>;
    transferArbitraryTokensRecipients?: Array<TransferArbitraryTokensRecipient>;
    trezor?: boolean;
    custodians?: Array<string>;
    registrationData?:
        | {
              report: string;
              signature: string;
          }
        | string;
    registrationParams?: {
        shouldApproveDDX?: boolean;
        shouldBond?: boolean;
    };
    proposedRelease?: {
        mrEnclave: string;
        isvSvn: string;
        startingEpochId: number;
    };
    proposedSpecsUpserts?: Array<{
        key: string;
        expr: string;
    }>;
}

export interface Facet {
    facetAddress: string;
    action: FacetCutAction;
    functionSelectors: string[];
}

export enum FacetCutAction {
    Add = 0,
    Replace = 1,
    Remove = 2,
}

export interface GovernanceParameters {
    proposalMaxOperations: BigNumber;
    votingDelay: BigNumber;
    votingPeriod: BigNumber;
    gracePeriod: BigNumber;
    timelockDelay: BigNumber;
    quorumVotes: BigNumber;
    proposalThreshold: BigNumber;
    skipRemainingVotingThreshold: BigNumber;
}

export interface Proposal {
    canceled: boolean;
    executed: boolean;
    proposer: string;
    delay: BigNumber;
    forVotes: BigNumber;
    againstVotes: BigNumber;
    id: BigNumber;
    eta: BigNumber;
    startBlock: BigNumber;
    endBlock: BigNumber;
}

export enum ProposalState {
    Pending,
    Active,
    Canceled,
    Defeated,
    Succeeded,
    Queued,
    Expired,
    Executed,
}

export interface InsuranceMineInfo {
    interval: BigNumber;
    withdrawalFactor: BigNumber;
    advanceIntervalReward: BigNumber;
    minedAmount: BigNumber;
    mineRatePerBlock: BigNumber;
    ddxMarketStateIndex: BigNumber;
    miningFinalBlockNumber: BigNumber;
    ddxMarketStateBlock: BigNumber;
    collateralNames: string[];
}

export interface DDXClaimantState {
    index: BigNumber;
    claimedDDX: BigNumber;
}

export interface StakeCollateral {
    underlyingToken: string;
    collateralToken: string;
    diFundToken: string;
    cap: BigNumber;
    withdrawalFeeCap: BigNumber;
    flavor: StakeFlavor;
}

export interface CurrentTotalStakes {
    normalizedStakerStakeSum: BigNumber;
    normalizedGlobalCapSum: BigNumber;
}

export interface CurrentStakeByCollateralNameAndStaker {
    stakerStake: BigNumber;
    globalCap: BigNumber;
    normalizedStakerStake: BigNumber;
    normalizedGlobalCap: BigNumber;
}

export interface VoteReceipt {
    hasVoted: boolean;
    support: boolean;
    votes: BigNumber;
}

export interface DDXSupplyInfo {
    issuedSupply: BigNumber;
    totalSupply: BigNumber;
}

export enum StakeFlavor {
    Vanilla = 0,
    Compound = 1,
    Aave = 2,
}

export interface TraderAttributes {
    ddxBalance: BigNumber;
    ddxWalletContract: string;
}

export enum SpecsKind {
    Market,
    MarketGateway,
}

export interface CurrencyPair {
    base: string;
    quote: string;
}
