// NOTE(jalextowle): The use of require-style imports below is very important. If
// a mixture of import types are used, the typings will not be resolved correctly
// in dependant packages like @derivadex/protocol. Source:
// https://github.com/serenity-js/serenity-js/issues/30#issuecomment-486845695
/* eslint-disable @typescript-eslint/no-var-requires */
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const ChaiBigNumber = require('chai-bignumber');
/* eslint-enable @typescript-eslint/no-var-requires */

chai.config.includeStack = true;

// Order matters.
chai.use(ChaiBigNumber());
chai.use(chaiAsPromised);
export const expect = chai.expect;
