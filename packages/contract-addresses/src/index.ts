import { readFileAsync } from '@derivadex/dev-utils';
import { ContractDeployment, ContractDeployments } from '@derivadex/types';
import { join } from 'path';

export { BASE_DERIVATION_PATH } from './configs';

export async function getContractDeploymentsAsync(): Promise<ContractDeployments> {
    let derivaDEXFilePath;
    if (process.env.ADDRESSES_VOLUME) {
        derivaDEXFilePath = process.env.ADDRESSES_VOLUME + '/addresses.json';
    } else {
        // This reference is made from the transpiled lib/ folder, thus we go back an extra directory
        derivaDEXFilePath = join(__dirname, '../../addresses.json');
    }
    const bufferedAddresses = await readFileAsync(derivaDEXFilePath);
    return JSON.parse(bufferedAddresses.toString());
}

/**
 * Gets the specified deployment or throws if it doesn't exist.
 * @param deployment The desired deployment.
 * @returns The deployment that was specified.
 */
export async function getContractDeploymentAsync(deployment: string, url?: string): Promise<ContractDeployment> {
    let remoteAddresses = null;
    if (url) {
        const fetch = typeof window === 'undefined' ? require('node-fetch') : window.fetch;
        const remoteAddressesResponse = await fetch(url);
        remoteAddresses = await remoteAddressesResponse.json();
    }
    const deployedAddresses = remoteAddresses ? remoteAddresses : await getContractDeploymentsAsync();
    if (deployedAddresses[deployment] === undefined) {
        throw new Error(
            `Unknown deployment (${deployment}). No known DerivaDEX contracts have been deployed with this name.`,
        );
    }
    return deployedAddresses[deployment];
}
