/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
    solidity: {
        compilers: [
            {
                version: '0.8.15',
                settings: {
                    evmVersion: 'istanbul',
                    optimizer: {
                        enabled: true,
                        runs: 10000,
                        details: {
                            yul: false,
                            deduplicate: true,
                            cse: true,
                            constantOptimizer: true,
                        },
                    },
                    outputSelection: {
                        '*': {
                            '*': [
                                'abi',
                                'devdoc',
                                'evm.bytecode.linkReferences',
                                'evm.bytecode.object',
                                'evm.bytecode.sourceMap',
                                'evm.deployedBytecode.object',
                                'evm.deployedBytecode.sourceMap',
                                'evm.gasEstimates',
                            ],
                        },
                    },
                },
            },
        ],
    },
    paths: {
        sources: './contracts',
        cache: './cache',
        artifacts: './artifacts',
    },
};
