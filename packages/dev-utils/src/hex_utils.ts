import { hexUtils } from '@0x/utils';
import { SpecsKind } from '@derivadex/types';
import * as ethers from 'ethers';

export enum ItemKind {
    Empty,
    Trader,
    Strategy,
    Position,
    BookOrder,
    Price,
    InsuranceFund,
    Stats,
    Release,
    Specs,
    InsuranceFundContribution,
    Signer,
}

export function generateCallData(txData: string): string {
    return `0x${txData.slice(10)}`;
}

export function remove0xPrefix(hexString: string): string {
    return hexString.slice(2);
}

export function generateSpecsEncoding(specsKind: SpecsKind, name: string): string {
    return hexUtils.rightPad(
        hexUtils.concat(
            hexUtils.toHex(specsKind as number),
            hexUtils.toHex(name.length),
            `0x${Buffer.from(name).toString('hex')}`,
        ),
        30,
    );
}

export function generateSpecsKey(specsKind: SpecsKind, name: string): string {
    return hexUtils.rightPad(
        hexUtils.concat(hexUtils.toHex(ItemKind.Specs as number), '0x00', generateSpecsEncoding(specsKind, name)),
        32,
    );
}

export function generateStrategyId(strategyId: string): string {
    return hexUtils.rightPad(
        hexUtils.concat(hexUtils.toHex(strategyId.length), `0x${Buffer.from(strategyId).toString('hex')}`),
        32,
    );
}

export function generateStrategyKey(traderAddress: string, strategyId: string): string {
    return hexUtils.rightPad(
        hexUtils.concat(
            hexUtils.slice(generateTraderGroupKey(ItemKind.Strategy, traderAddress), 0, 22),
            hexUtils.slice(hexUtils.hash(strategyId), 0, 4),
        ),
        32,
    );
}

export function generateTraderGroupKey(itemKind: ItemKind, traderAddress: string): string {
    return hexUtils.rightPad(hexUtils.concat(hexUtils.toHex(itemKind as number), '0x00', traderAddress), 32);
}

export function generateSpecsInitializationData(keys: string[], specs: string[]): string {
    const iface = new ethers.utils.Interface(['function initialize(bytes30[],string[])']);
    return iface.encodeFunctionData('initialize', [keys, specs]);
}

export function stringToEthereumBytes(str: string): string {
    const hex = `0x${Buffer.from(str).toString('hex')}`;
    return hexUtils.concat(hexUtils.leftPad(hexUtils.toHex(hex.length / 2 - 1), 1), hexUtils.rightPad(hex, 31));
}
