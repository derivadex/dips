import * as fs from 'fs';
import { promisify } from 'util';

export const appendFileAsync = promisify(fs.appendFile);
export const existsAsync = promisify(fs.exists);
export const lstatAsync = promisify(fs.lstat);
export const mkdirAsync = promisify(fs.mkdir);
export const readFileAsync = promisify(fs.readFile);
export const readdirAsync = promisify(fs.readdir);
export const writeFileAsync = promisify(fs.writeFile);
