import { CurrencyPair } from '@derivadex/types';

export async function sleepAsync(timeout: number): Promise<void> {
    return new Promise<void>((resolve) => setTimeout(resolve, timeout));
}

export function toSymbol(currencyPair: CurrencyPair): string {
    return `${currencyPair.base.toUpperCase()}/${currencyPair.quote.toUpperCase()}`;
}

export function toCurrencyPair(symbol: string): CurrencyPair {
    const parts = symbol.split('/');
    return { base: parts[0].toLowerCase(), quote: parts[1].toLowerCase() };
}
