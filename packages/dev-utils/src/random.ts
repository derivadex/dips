import { BigNumber } from './configured_bignumber';

const MAX_DIGITS = 75;

/**
 * Generates a pseudo-random 256-bit number.
 * @return  A pseudo-random 256-bit number.
 */
export function generateRandomSaltWithLeverage(leverage: number): BigNumber {
    // BigNumber.random returns a pseudo-random number between 0 & 1 with a passed in number of decimal places.
    // Source: https://mikemcl.github.io/bignumber.js/#random
    const randomNumber = BigNumber.random(MAX_DIGITS);
    const factor = new BigNumber(10).pow(MAX_DIGITS - 1);
    return new BigNumber(
        randomNumber.times(factor).integerValue().toString() + leverage.toString().padStart(3, '0'),
    ).integerValue();
}
