import { BaseContract } from '@0x/base-contract';
import { ContractArtifact, MethodAbi, OutputField } from 'ethereum-types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import _ from 'lodash';

interface ContractWrapper extends BaseContract {
    getSelector: (method: string) => string;
}

export function getSelectors(contract: ContractWrapper, excludeList: string[] = []): string[] {
    const methods = contract.abi.filter((defintion) => defintion.type === 'function') as MethodAbi[];
    const filteredMethods = _.filter(methods, (method) => {
        return !_.includes(excludeList, method.name);
    });
    return filteredMethods.reduce((accumulator, method) => {
        return accumulator.concat([contract.getSelector(method.name)]);
    }, [] as string[]);
}

type EVMVersion = 'homestead' | 'tangerineWhistle' | 'spuriousDragon' | 'byzantium' | 'constantinople';

interface HardhatOutputSelection {
    [fileName: string]: {
        [contractName: string]: string[];
    };
}

interface ZeroExOutputSelection {
    [fileName: string]: {
        [contractName: string]: OutputField[];
    };
}

function hardhatToZeroExOutputSelection(hardhatOutputSelection: HardhatOutputSelection): ZeroExOutputSelection {
    const outputSelection: ZeroExOutputSelection = {};
    for (const sourceFile of Object.keys(hardhatOutputSelection)) {
        for (const contractName of Object.keys(hardhatOutputSelection[sourceFile])) {
            outputSelection[sourceFile] = {
                ...outputSelection[sourceFile],
                [contractName]: hardhatOutputSelection[sourceFile][contractName].map(
                    (value: string) => value as OutputField,
                ),
            };
        }
    }
    return outputSelection;
}

export interface GeneratedArtifacts {
    zeroExArtifact: ContractArtifact;
    // HACK(jalextowle): It doesn't make sense to create better typings for this
    // right now because we should just use `@nomiclabs/hardhat-etherscan` if possible.
    // If that isn't possible, then re-evaluate improving this tooling.
    standardInputArtifact: any;
}

/**
 * Generates most of a 0x contract artifact using a combination of Hardhat build
 * info and Hardhat artifacts. It also generates a Solidity Standard Input
 * artifact using Hardhat's build info.
 * @param hardhat The hardhat runtime environment to get the build info and contract
 *        artifact from.
 * @param fullyQualifiedContractName A fully qualified name that points to a contract
 *        in the provided hardhat runtime environment.
 * @returns A 0x contract artifact that contains all of the necessary fields to
 *          generate 0x contract wrappers.
 */
export async function generateArtifactsFromHardhatArtifactsAsync(
    hardhat: HardhatRuntimeEnvironment,
    fullyQualifiedContractName: string,
): Promise<GeneratedArtifacts> {
    const { sourceName, contractName } = parseName(fullyQualifiedContractName);
    if (sourceName === undefined) {
        throw new Error(`Can't interpret ${fullyQualifiedContractName} as a fully qualified name`);
    }
    const artifact = await hardhat.artifacts.readArtifact(fullyQualifiedContractName);
    if (!artifact) {
        throw new Error(`Couldn't find artifact for fully qualified name: ${fullyQualifiedContractName}`);
    }
    const buildInfo = await hardhat.artifacts.getBuildInfo(fullyQualifiedContractName);
    if (!buildInfo) {
        throw new Error(`Couldn't find buildInfo for fully qualified name: ${fullyQualifiedContractName}`);
    }
    const richArtifact = buildInfo.output.contracts[sourceName][contractName];
    return {
        zeroExArtifact: {
            schemaVersion: '2.0.0',
            contractName: artifact.contractName,
            compiler: {
                name: 'solc',
                version: buildInfo.solcLongVersion,
                settings: {
                    evmVersion: buildInfo.input.settings.evmVersion as EVMVersion,
                    optimizer: {
                        enabled: buildInfo.input.settings.optimizer.enabled || false,
                        runs: buildInfo.input.settings.optimizer.runs || 200,
                        ...buildInfo.input.settings.optimizer,
                    },
                    outputSelection: hardhatToZeroExOutputSelection(buildInfo.input.settings.outputSelection),
                    libraries: buildInfo.input.settings.libraries,
                    metadata: { useLiteralContent: true },
                },
            },
            // NOTE(jalextowle): We must cast to any at some points because the artifact
            // has more data than the typing is aware of.
            compilerOutput: {
                abi: richArtifact.abi,
                evm: {
                    bytecode: {
                        linkReferences: artifact.linkReferences,
                        object: artifact.bytecode,
                        sourceMap: richArtifact.evm.bytecode.sourceMap,
                    },
                    deployedBytecode: {
                        linkReferences: artifact.deployedLinkReferences,
                        object: artifact.deployedBytecode,
                        sourceMap: richArtifact.evm.deployedBytecode.sourceMap,
                    },
                },
                devdoc: (richArtifact as any).devdoc,
            },
            // HACK(jalextowle): `sol-compiler` and `npx hardhat compile` generate artifacts
            // that have irreconcilable differences in the artifacts that they produce. Despite
            // this, Hardhat's artifacts contain _most_ of the information needed to produce
            // 0x artifacts. All of the fields that cannot be easily replicated are fields that
            // will not affect the behavior of generated contract wrappers:
            // - sourceCodes
            // - sources
            // - sourceTreeHashHex
            // - chains
            sourceTreeHashHex: '',
            sources: {},
            sourceCodes: {},
            chains: {},
        },
        standardInputArtifact: {
            ...buildInfo.input,
        },
    };
}

/// HACK(jalextowle): The code below is copied from Hardhat as they do not export
/// it.

/**
 * Parses a name, which can be a bare contract name, or a fully qualified name.
 */
export function parseName(name: string): { sourceName?: string; contractName: string } {
    const parts = name.split(':');

    if (parts.length === 1) {
        return { contractName: parts[0] };
    }

    const contractName = parts[parts.length - 1];
    const sourceName = parts.slice(0, parts.length - 1).join(':');

    return { sourceName, contractName };
}
