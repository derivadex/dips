export function hexToBinary(hex: string): string {
    let result = '';
    for (let i = 2; i < hex.length; i++) {
        result += parseInt(hex[i], 16).toString(2).padStart(4, '0');
    }
    return result;
}

export function binaryToHex(binary: string): string {
    let result = '0x';
    for (let i = 0; i < binary.length; i += 4) {
        result += parseInt(binary.slice(i, i + 4), 2).toString(16);
    }
    return result;
}

export function getBit(value: string, idx: number): boolean {
    const bytePosition = Math.floor(idx / 8);
    const bitPosition = idx % 8;
    const binary = hexToBinary(value);
    return binary[bytePosition * 8 + (7 - bitPosition)] == '1';
}

export function copyBits(value: string, idx: number): string {
    const bytePosition = Math.floor(idx / 8);
    const bitPosition = idx % 8;
    const binary = hexToBinary(value);

    // Copy the most significant bytes.
    const result = [...binary.slice(bytePosition * 8)];

    // Copy the remaining bits.
    for (let i = 8 - bitPosition; i < 8; i++) {
        result[i] = '0';
    }

    return binaryToHex(result.join('').padStart(256, '0'));
}
