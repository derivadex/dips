import { SpecsKind } from '@derivadex/types';

import { generateSpecsEncoding } from './hex_utils';

export const ERC20_DECIMALS = 18;
export const USDC_DECIMALS = 6;
export const MINUTE_MULTIPLIER = 60;
export const TEN_MINUTE_MULTIPLIER = 10;
export const ONE_SECOND_MS = 1000;
export const ONE_MINUTE_MS = ONE_SECOND_MS * MINUTE_MULTIPLIER;
export const TEN_MINUTES_MS = ONE_MINUTE_MS * TEN_MINUTE_MULTIPLIER;
export const HEX_BASE = 16;
export const DEFAULT_SPEC_KEYS = [
    generateSpecsEncoding(SpecsKind.Market, 'ETHP'),
    generateSpecsEncoding(SpecsKind.Market, 'BTCP'),
    generateSpecsEncoding(SpecsKind.Market, 'DOGEP'),
    generateSpecsEncoding(SpecsKind.Market, 'DDXP'),
    generateSpecsEncoding(SpecsKind.MarketGateway, 'api.gemini.com'),
    generateSpecsEncoding(SpecsKind.MarketGateway, 'api.binance.com'),
    generateSpecsEncoding(SpecsKind.MarketGateway, 'api.coinbase.com'),
];
export const DEFAULT_SPECS = [
    `(Market :name "ETHP"
        :tick-size 0.1
        :max-order-notional 1000000
        :max-taker-price-deviation 0.02
        :min-order-size 0.0001
    )`,
    `(Market :name "BTCP"
        :tick-size 1
        :max-order-notional 1000000
        :max-taker-price-deviation 0.02
        :min-order-size 0.00001
    )`,
    `(Market :name "DOGEP"
        :tick-size 0.000001
        :max-order-notional 100000
        :max-taker-price-deviation 0.1
        :min-order-size 1
    )`,
    `(Market :name "DDXP"
        :tick-size 0.1
        :max-order-notional 0
        :max-taker-price-deviation 0.02
        :min-order-size 0.0001
    )`,
    `(MarketGateway :name "Gemini"
        :hostname "api.gemini.com"
        :symbols '("ETHP" "BTCP" "DOGEP")
        :get-time (Get
            :query "/v1/heartbeat"
            :reader (jq "result")
        )
        :get-spot-price (Get
            :query (format "/v2/ticker/{}")
            :reader (jq "[symbol,close]")
        )
        :tr-symbol (sed "s/(?P<base>[A-Z]+)P/\${base}USD/;")
    )`,
    `(MarketGateway :name "Binance"
        :hostname "api.binance.com"
        :symbols '("ETHP" "BTCP" "DOGEP")
        :get-time (Get
            :query "/api/v3/time"
            :reader (jq "serverTime")
        )
        :get-spot-price (Get
            :query (format "/api/v3/ticker/price?symbol={}")
            :reader (jq "[symbol,price]")
        )
        :tr-symbol (sed "s/(?P<base>[A-Z]+)P/\${base}USDC/;")
    )`,
    `(MarketGateway :name "coinbase"
        :hostname "api.coinbase.com"
        :symbols '("ETHP" "BTCP" "DDXP" "DOGEP")
        :get-time (Get
            :query "/v2/time"
            :reader (jq "data.epoch")
        )
        :get-spot-price (Get
            :query (format "/v2/prices/{}/spot")
            :reader (chain (jq "[data.base,data.amount]") (sed "s/(?P<base>[A-Z]+)/\${base}-USD/;"))
        )
        :tr-symbol (sed "s/(?P<base>[A-Z]+)P/\${base}-USD/;")
    )`,
];
