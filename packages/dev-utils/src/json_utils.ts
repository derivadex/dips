export function prettyPrintJSON(o?: Record<string, any>): void {
    if (o === undefined) {
        return;
    }
    console.log(JSON.stringify(o, null, 4).concat('\n'));
}

/***
 * Postgres row_to_json returns db number values unquoted. This leads to undesirable results in JSON.parse and ts/js number types.
 * For example, pg notify may return a payload of:
 * > payload: {"strategy_id":"main","max_leverage":10,"free_collateral":1000.00000000000001, [...] }
 * Where the unquoted 'free_collateral' value will be (correctly) interpreted by ts/js as number, but will lose precision.
 * In this example, JSON.parse(payload).free_collateral === 1000 (!)
 * This function scrubs the json from row_to_json by quoting unquoted decimal values.
 * Once quoted, the output can be parsed as usual with JSON.parse, which will treat the quoted values as a strings
 * with no loss of precision.
 */
export function quoteJsonDecimals(json: string): any {
    /*
    ("[^"]+"\s*:\s*)        The key value, wrapped in parenthesis for back reference $1. Keys start with a quote, have some chars that are not quotes,
                            end with a quote. For simplicity, we include spaces after the quote up to the colon, and any space after the 
                            colon as part of the key. For example, we'll grab >>"key"  :  << as the $1 back reference
    ([0-9]+\.[0-9]+)        A group of one or more (+) digits followed by a decimal followed by one or more digits
                            Note that [0-9]*\.[0-9]* would match both 1.1 and . vs the + which matches only 1.1
    \s*([,}])               Allow (but don't capture) space after the match above, up until a comma or closing }
    /g                      Global. We want any matches, not just the first one
    $1"$2"$3               The replacement value. 
    - $1 is the key (plus colon + whitespace)
    - $2 is the unquoted value which we now quote with "$2"
    - $3 is either a comma or closing }
    */
    return json.replace(/("[^"]+"\s*:\s*)([0-9]+\.[0-9]+)\s*([,}])/g, '$1"$2"$3');
}
