import { SupportedProvider, Web3Wrapper } from '@0x/web3-wrapper';
import { ContractReceipt, ContractTransaction } from 'ethers';
import _ from 'lodash';

// These are unique identifiers contained in the response of the
// web3_clientVersion call.
const nodeVersionIds = {
    hardhat: 'HardhatNetwork',
    ganache: 'EthereumJS TestRPC',
};

export async function advanceBlocksAsync(providerEngine: SupportedProvider, numberOfBlocks: number): Promise<void> {
    const web3Wrapper = new Web3Wrapper(providerEngine);
    for (let i = 0; i < numberOfBlocks; i++) {
        await web3Wrapper.mineBlockAsync();
    }
}

// HACK(jalextowle): This doesn't work for all providers. This should really be
// included within `@0x/web3-wrapper` so that it can be more generic across different
// providers.
export async function advanceTimeAsync(providerEngine: SupportedProvider, timeDelta: number): Promise<void> {
    const web3Wrapper = new Web3Wrapper(providerEngine);
    await web3Wrapper.sendRawPayloadAsync<number>({ method: 'evm_increaseTime', params: [timeDelta] });
}

export async function getBlockNumberAsync(providerEngine: SupportedProvider): Promise<number> {
    const web3Wrapper = new Web3Wrapper(providerEngine);
    return web3Wrapper.getBlockNumberAsync();
}

export async function setExpiryAsync(providerEngine: SupportedProvider, expiry: number): Promise<number> {
    const web3Wrapper = new Web3Wrapper(providerEngine);
    const timestamp = await web3Wrapper.getBlockTimestampAsync(await web3Wrapper.getBlockNumberAsync());
    return timestamp + expiry;
}

export async function ethersWaitTx(
    tx: ContractTransaction,
    confirmations: number,
    provider: SupportedProvider,
): Promise<ContractReceipt> {
    const web3Wrapper = new Web3Wrapper(provider);
    const version = await web3Wrapper.getNodeVersionAsync();
    if (
        _.some([nodeVersionIds.ganache, nodeVersionIds.hardhat], (nodeVersionId) => _.includes(version, nodeVersionId))
    ) {
        await advanceBlocksAsync(provider, confirmations);
        return await tx.wait();
    }
    return await tx.wait(confirmations);
}
