import { expect } from '@derivadex/test-utils';
import * as hardhat from 'hardhat';

import { generateArtifactsFromHardhatArtifactsAsync } from '../../src';
import ExpectedLibCloneArtifact from './data/LibClone.json';

describe('Artifact Utils', () => {
    describe('#generateZeroExArtifactFromHardhatArtifactsAsync', () => {
        it('converts a valid hardhat artifact to the correct zeroEx artifact', async () => {
            const { zeroExArtifact } = await generateArtifactsFromHardhatArtifactsAsync(
                hardhat,
                'contracts/LibClone.sol:LibClone',
            );
            // NOTE(jalextowle): Chai treats undefined and undeclared variables
            // differently (because they are different):
            // https://github.com/chaijs/chai/issues/735. In this case, the
            // distinction is not important to us, so we manually test libraries
            // to undefined.
            (ExpectedLibCloneArtifact.compiler.settings as any) = {
                libraries: undefined,
                ...ExpectedLibCloneArtifact.compiler.settings,
            };
            expect(ExpectedLibCloneArtifact).to.be.deep.eq(zeroExArtifact);
        });
    });
});
