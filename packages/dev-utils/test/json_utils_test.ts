import { expect } from '@derivadex/test-utils';

import { quoteJsonDecimals } from '../src';

describe('Test parsing pg notify row_to_json format', () => {
    it('parses one row of clean row_to_json', () => {
        const json = `{ "strategy_id":"main",
                "trader":"\\\\x303699848c84529987e14ba32c8a66def67e9ece",
                "max_leverage":10,
                "free_collateral":1000.00000000000001,
                "frozen_collateral":1000000001,
                "frozen":false,
                "created_at": "2021-04-06T22:12:42.269892+01:00",
                "updated_at":"2021-04-06T22:12:42.269892+01:00",
                "ten": 10,
                "last_one": 123456789.987654321
            }`;

        // Demonstrate where things go wrong ...
        const bad = JSON.parse(json);
        expect(bad.free_collateral).eq(1000);

        // Demonstrate working fix ...
        const obj = JSON.parse(quoteJsonDecimals(json));
        expect(obj.strategy_id).eq('main');
        // Integers are number:
        expect(obj.max_leverage).eq(10);
        expect(obj.ten).eq(10);
        expect(obj.frozen_collateral).eq(1000000001);
        // Decimals are quoted
        expect(obj.free_collateral).eq('1000.00000000000001');
        expect(obj.last_one).eq('123456789.987654321');
        // booleans are not quoted
        expect(obj.frozen).eq(false);
        expect(obj.created_at).eq('2021-04-06T22:12:42.269892+01:00');
    });

    it('parses tx_log messages', () => {
        const json =
            '{"c": [{"price": "2665.1746", "amount": "0.5", "reason": "Trade", "symbol": "ETHP", "makerOrderHash": "0x9eaf061b2b876bdaabc3a354dca886b5f34b413e17c0b7ae82d72c18f98a24cd", "takerOrderHash": "0x0e98b7e26628bce39087deb2ff46cf3f07fada3b59f289b6b412cbc85c8ba42b", "collateralToken": "0xb69e673309512a9d726f87304c6984054f87a93b", "makerRealizedVal": {"fee": "0", "trader": "0x7e98fdf18dae0b7b1925d6735d4b77f4889879e1", "strategy": "main", "realizedPnl": "0", "ddxFeeElection": false}, "takerRealizedVal": {"fee": "26.651746", "trader": "0x5bcb10204f39f2d3755038c1b5a305203900099a", "strategy": "main", "realizedPnl": "-1026.315695", "ddxFeeElection": false}, "makerNewCollateral": "200000", "takerNewCollateral": "98923.157909781270345536", "makerOrderRemainingAmount": "9.5"}], "t": "CompleteFill"}';
        const obj = JSON.parse(quoteJsonDecimals(json));
        // All values were quoted in the original and stay quoted when parsed
        expect(obj.c[0].price).eq('2665.1746');
        expect(obj.c[0].takerNewCollateral).eq('98923.157909781270345536');
        expect(obj.c[0].makerRealizedVal.fee).eq('0');
        expect(obj.c[0].makerNewCollateral).eq('200000');
    });

    it('parses one row of badly formatted row_to_json', () => {
        /* 
        Being careful to test scenarios:
        1) "number": 123123,
        2) "number": 123123  ,
        3) "number": 123123 } 
        */
        const json = `{ "strategy_id"  :  "main","trader" : "0x00000",
                "max_leverage":10,
                "free_collateral":1000.00000000000000000000001,
                "frozen_collateral":1000000001,
                "frozen":false,
                "isTrue"    :          true          ,
                "bad_data": "one,two,'three,four':",
                "created_at": "2021-04-06T22:12:42.269892+01:00",
                "updated_at":"2021-04-06T22:12:42.269892+01:00",
                "ten": 10       ,
                "twenty"   : 20.0001,
                "thirty":30.1 }
            `;

        const obj = JSON.parse(quoteJsonDecimals(json));
        expect(obj.strategy_id).eq('main');
        // non-decimal numbers not quoted:
        expect(obj.max_leverage).eq(10);
        expect(obj.frozen_collateral).eq(1000000001);
        expect(obj.ten).eq(10);
        // Booleans not quoted
        expect(obj.frozen).eq(false);
        expect(obj.isTrue).eq(true);
        expect(obj.bad_data).eq("one,two,'three,four':");
        expect(obj.created_at).eq('2021-04-06T22:12:42.269892+01:00');
        // Decimals are quoted:
        expect(obj.twenty).eq('20.0001');
        expect(obj.free_collateral).eq('1000.00000000000000000000001');
        expect(obj.thirty).eq('30.1');
    });
});
